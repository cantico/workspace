## Espaces collaboratifs ##

Module ajoutant une fonction de gestion d'Espaces Collaboratifs sur votre "plate-forme" Ovidentia
Workspaces - Espaces Collaboratifs
Les espaces collaboratifs peuvent etre déployés en quelques clics de souris.
La gestion des espaces collaboratifs peut être délégué et intégre les options suivantes :

* Publication en ligne des actualités (rubrique d'articles)
* Agenda de groupe avec possibilité de superposition de l'agenda personnel
* Forum de discussions
* Partage de fichiers avec «versioning»
* Annuaire des utilisateurs et des contacts
* Page d'accueil dynamique présentant les dernières contributions
* Ergonomie simple et intuitive : glisser / déposer, éditeur de texte «riche»
* Notication des contributions par e-mail
* Dépoiement d'un espace en 3 clics
* 3 niveaux d'accès :
	* Simple visiteur
	* Contributeur
	* Administrateur
* Possibilité d'inviter à tout moment de nouveaux participants à l'espace collaboratif
* Moteur de recherche
* Contenus entièrement intégrés à la "plate-forme" OVIDENTIA
* Nombre d'espaces collaboratifs illimité