/**
 * Cookie plugin
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

/**
 * Create a cookie with the given name and value and other optional parameters.
 *
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Set the value of a cookie.
 * @example $.cookie('the_cookie', 'the_value', { expires: 7, path: '/', domain: 'jquery.com', secure: true });
 * @desc Create a cookie with all available options.
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Create a session cookie.
 * @example $.cookie('the_cookie', null);
 * @desc Delete a cookie by passing null as value. Keep in mind that you have to use the same path and domain
 *       used when the cookie was set.
 *
 * @param String name The name of the cookie.
 * @param String value The value of the cookie.
 * @param Object options An object literal containing key/value pairs to provide optional cookie attributes.
 * @option Number|Date expires Either an integer specifying the expiration date from now on in days or a Date object.
 *                             If a negative value is specified (e.g. a date in the past), the cookie will be deleted.
 *                             If set to null or omitted, the cookie will be a session cookie and will not be retained
 *                             when the the browser exits.
 * @option String path The value of the path atribute of the cookie (default: path of page that created the cookie).
 * @option String domain The value of the domain attribute of the cookie (default: domain of page that created the cookie).
 * @option Boolean secure If true, the secure attribute of the cookie will be set and the cookie transmission will
 *                        require a secure protocol (like HTTPS).
 * @type undefined
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */

/**
 * Get the value of a cookie with the given name.
 *
 * @example $.cookie('the_cookie');
 * @desc Get the value of a cookie.
 *
 * @param String name The name of the cookie.
 * @return The value of the cookie.
 * @type String
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */
jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};



function workspace_labelFormat(label)
{
    if (jQuery.browser.msie && parseInt(jQuery.browser.version.substr(0,1), 10) <= 6) {
        return label;
    }
    function addZeroWidthSpaces(match)
    {
        return "\u200B" + match;
    }
    return label.replace(/./g, addZeroWidthSpaces);
}




jQuery(document).ready(function() {

    var pageId = jQuery('body').attr('id');

    jQuery('#workspaceselectorblock').hover(
        function(){//in
            jQuery('#workspaceselectorlbl').addClass("selectorHover");
        },
        function(){//out
            jQuery('#workspaceselectorlbl').removeClass("selectorHover");
        }
    );

    jQuery('html').click(function(){
        //alert(jQuery('#workspaceselectorlbl').hasClass("selectorHover"));
        if(jQuery('#workspaceselectorlbl').hasClass("selectorHover") == false){
            jQuery('#workspaceselector').css('display','none');
        }else{
            if( jQuery('#workspaceselector').css('display') == 'none' ){
                jQuery('#workspaceselector').css('display','block');
            }else{
                jQuery('#workspaceselector').css('display','none');
            }
        }
    });


//// File manager
/////////////////
    jQuery('.workspace-filemanager-view .widget-layout-flow-item .widget-icon .widget-icon-label').each(function()
        {
            var label = jQuery(this);
            text = label.text();
            label.text(workspace_labelFormat(text));
        }
    );

    // Make draggable elements.
    jQuery('.workspace-filemanager-view .widget-layout-flow-item .workspace-draggable').draggable(
        {
            helper: 'clone',
            revert: false,
            scroll: true,
            opacity: 0.5,
            drag: function(e, ui) {
                jQuery(ui.helper).addClass('workspace-draggable-dragged');
            },
            stop: function(e, ui) {
                jQuery(ui.element).removeClass('workspace-draggable-dragged');
            }
        }
    );

    // Make droppable elements (for filemanager).
    jQuery('.workspace-droppable, #treeview div.line').droppable(
        {
            tolerance: 'pointer',
            accept: '.workspace-draggable',
            activeClass: 'workspace-droppable-accepting',
            hoverClass: 'workspace-droppable-hovered',
            drop: function(ev, ui) {
                var sourceId = ui.draggable.attr('id');
                var sourcePathname = window.babAddonWidgets.getMetadata(sourceId).pathname;

                var destId = this.id;
                var destPathname = window.babAddonWidgets.getMetadata(destId).pathname;
                var sourceFileType = window.babAddonWidgets.getMetadata(sourceId).filetype;
                var moveUrl = (sourceFileType === 'file') ? window.babAddonWidgets.getMetadata('workspace_file_listview').movefileurl
                                                              : window.babAddonWidgets.getMetadata('workspace_file_listview').movefolderurl;
                moveUrl = moveUrl.replace('__1__', sourcePathname);
                moveUrl = moveUrl.replace('__2__', destPathname);

                window.location.href = moveUrl;
            }
        }
    );


    jQuery('.readNext, #article_list .widget-listelement, #thread_list .widget-listelement, #post_list .widget-listelement')
    .click(function () {
        var listItem = jQuery(this);
        if(listItem.hasClass("readNext")){
            if(listItem.text() == "Fermer >"){
                listItem.html("Lire la suite >");
            }else{
                listItem.html("Fermer >");
            }
            listItem = listItem.parent().parent();
        }
        listItem.toggleClass('workspace-folded');
        var contentSummaries = jQuery(listItem.find('.workspace-content-summary'));
        if (contentSummaries.length > 0) {
            var contentSummary = jQuery(contentSummaries.get(0));
            var contentSummaryBody = jQuery(contentSummary.find('.body'));

            if (contentSummaryBody && contentSummaryBody.offset() && contentSummaryBody.offset().top + contentSummaryBody.height() > contentSummary.offset().top + contentSummary.height() + 1) {
                listItem.addClass('overflow');
            } else {
                listItem.removeClass('overflow');
            }
        }else{
            var contentSummaries = jQuery(listItem.find('.workspace-home-summary'));
            if (contentSummaries.length > 0) {
                var contentSummary = jQuery(contentSummaries.get(0));
                var contentSummaryBody = jQuery(contentSummary.find('.workspace-article-body'));

                if (contentSummaryBody && contentSummaryBody.offset() && contentSummaryBody.offset().top + contentSummaryBody.height() > contentSummary.offset().top + contentSummary.height() + 1) {
                    listItem.addClass('overflow');
                } else {
                    listItem.removeClass('overflow');
                }
            }
        }
    });

    jQuery('.readNext, #article_list .widget-listelement, #thread_list .widget-listelement')
    .each(function () {
        var listItem = jQuery(this);
        if(listItem.hasClass("readNext")){
            listItem = listItem.parent().parent();
        }
        listItem.addClass('workspace-folded');
        var contentSummaries = jQuery(listItem.find('.workspace-content-summary'));
        if (contentSummaries.length > 0) {
            var contentSummary = jQuery(contentSummaries.get(0));
            var contentSummaryBody = jQuery(contentSummary.find('.body'));

            if (contentSummaryBody && contentSummaryBody.offset() && contentSummaryBody.offset().top + contentSummaryBody.height() > contentSummary.offset().top + contentSummary.height() + 1) {
                listItem.addClass('overflow');
            } else {
                listItem.removeClass('overflow');
            }
        }else{
            var contentSummaries = jQuery(listItem.find('.workspace-home-summary'));
            if (contentSummaries.length > 0) {
                var contentSummary = jQuery(contentSummaries.get(0));
                var contentSummaryBody = jQuery(contentSummary.find('.workspace-article-body'));

                if (contentSummaryBody && contentSummaryBody.offset() && contentSummaryBody.offset().top + contentSummaryBody.height() > contentSummary.offset().top + contentSummary.height() + 1) {
                    listItem.addClass('overflow');
                } else {
                    listItem.removeClass('overflow');
                }
            }
        }
    });



//// General interface tweaks
/////////////////////////////
    jQuery('.widget-input-mandatory:first').focus();

    //-----------------------------------------------------------------------------

    jQuery('.workspace-resizable-panel')
    .resizable({
        handles: 'e',
        autoHide: false,
        start: function (event, ui) {
            window.noclick = true;
            jQuery(this)
                .removeClass('folded');
            jQuery.cookie('panel_' + pageId + '_folded', null);
        },
        stop: function (event, ui) {
            jQuery.cookie('panel_' + pageId + '_width', ui.size.width);
            if (ui.size.width <= 40) {
                jQuery(this)
                    .width(8)
                    .addClass('folded');
                jQuery.cookie('panel_' + pageId + '_folded', 1);
            }
        }
    });


    var panelWidth = parseInt(jQuery.cookie('panel_' + pageId + '_width')) || 250;
    jQuery('.workspace-resizable-panel')
    .width(panelWidth);

    jQuery('.workspace-resizable-panel').each(
        function () {
            var resizable = this;
            if (jQuery.cookie('panel_' + pageId + '_folded') == 1) {
                jQuery(resizable)
                    .css('width', '8px')
                    .addClass('folded');
            }
            jQuery(this).find('.ui-resizable-e').click(

                function () {
                    if (window.noclick == true) {
                        window.noclick = false;
                    } else {
                        if (jQuery(resizable).width() == 8) {
                            var panelWidth = parseInt(jQuery.cookie('panel_' + pageId + '_width')) || 250;
                            jQuery(resizable)
                                .animate({ width: panelWidth + 'px' }, 'fast')
                                .removeClass('folded');
                            jQuery.cookie('panel_' + pageId + '_folded', null);
                        } else {
                            jQuery(resizable)
                                .animate({ width: '8px' }, 'fast')
                                .addClass('folded');
                            jQuery.cookie('panel_' + pageId + '_folded', 1);
                        }
                    }
                }
            );
        }
    );


    /* Administration configuration : form to create/modify a workspace */
    workspace_configadmin_skinselector('onload');
    jQuery('#workspace_configadmin_skinselector').change(function() {
        workspace_configadmin_skinselector('onchange');
    });

//	jQuery(window).resize(function () {
//		window.babAddonWorkspace.windowResize();
//	});
});

jQuery(window).on('load', function () {
    window.babAddonWorkspace.windowResize();
    //window.babAddonWorkspace.workspace_equalizeDirectoryEntries();
});


jQuery(document).ready(function () {
    window.babAddonWorkspace.workspace_equalizeDirectoryEntries();
});


/* Administration configuration : form to create/modify a workspace
 * mode = onload, onchange */
function workspace_configadmin_skinselector(mode) {
    /* Current skin name */
    var nameskin = jQuery('#workspace_configadmin_skinselector').val();
    /* Show only good label */
    jQuery('#workspace_configadmin_stylesselector').find("option").each(function(i) {
        var optgroupLabel = jQuery(this).parent().attr("label");
        if (optgroupLabel != nameskin) {
            /* Hide the optgroup */
            jQuery(this).parent().hide();
        } else {
            /* Show the optgroup */
            jQuery(this).parent().show();
            /* Select the option */
            if (mode == 'onchange') {
                jQuery(this).attr('selected', 'selected');
            }
        }
    });
}




window.babAddonWorkspace = {};


window.babAddonWorkspace.workspace_equalizeDirectoryEntries = function()
{
    var largestHeight = 0;
    jQuery('.workspace-directory-entry').each(
        function() {
            var height = jQuery(this).height();
//            console.debug(this);
//            console.debug(height);
            if (height > largestHeight) {
                largestHeight = height;
            }
        }
    );
    jQuery('.workspace-directory-entry').height(largestHeight);
};


window.babAddonWorkspace.windowResize = function()
{
    var mainPanel = jQuery('#crm_main_panel');
    var windowInnerHeight = jQuery(window).height();

    jQuery('.widget-fullcalendar').each(function () {
        var top = jQuery(this).offset().top;
        jQuery(this).fullCalendar('option', 'height', (windowInnerHeight - top));
    });
};
