<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/workspaces.php';
require_once dirname(__FILE__) . '/controller.class.php';
require_once dirname(__FILE__) . '/page.class.php';
require_once dirname(__FILE__) . '/forum.php';
require_once dirname(__FILE__) . '/forum.ui.php';



/**
 * This controller manages actions that can be performed on forums.
 */
class workspace_CtrlForum extends workspace_Controller
{
	/**
	 * Displays the list of threads.
	 *
	 * @return workspace_Action
	 */
	public function listThreads($sort = null, $workspace = null, $active = true, $inactive = false)
	{
	    $W = bab_Widgets();

		workspace_BreadCrumbs::setCurrentPosition($this->proxy()->listThreads(), workspace_translate("Forum"));

		if (isset($workspace)) {
			workspace_setCurrentWorkspace($workspace);
		}

		$delegationId = workspace_getCurrentWorkspace();

		$page = workspace_Page();
//		$page->addItemMenu('forum', workspace_translate('Forum'), '')
		$page->addItemMenu('open_threads', workspace_translate('Open threads'), workspace_Controller()->Forum()->listThreads($sort, $workspace, true, false)->url());
		$page->addItemMenu('closed_threads', workspace_translate('Closed threads'), workspace_Controller()->Forum()->listThreads($sort, $workspace, false, true)->url());

		if ($inactive) {
		    $page->setCurrentItemMenu('closed_threads');
		} else {
		    $page->setCurrentItemMenu('open_threads');
		}

		if (!workspace_canViewForumThreads()) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You must be logged in to view the forum threads.'));
			$GLOBALS['babBody']->addError(workspace_translate('Access denied'));
		} else {

		    if ($active) {
    			$toolbar = workspace_Toolbar('main-toolbar');

            	if (workspace_canCreateThread()) {
            		$newIcon = $W->Icon(workspace_translate('New thread'), Func_Icons::ACTIONS_ARTICLE_NEW);
            		$newButton = $W->Link($newIcon, workspace_Controller()->Forum()->editThread());

            		$toolbar->addItem($newButton);
            	}
            	$page->addItem($toolbar);

            	$page->addItem($W->Title(workspace_translate('Thread list'), 1));
		    } else {
		        $page->addItem($W->Title(workspace_translate('Closed thread list'), 1));
		    }



			$list = workspace_threadList($inactive, $active);
			$page->addItem($list);
		}

		return $page;
	}



	/**
	 * Displays the list of posts in the specified thread.
	 *
	 * @param int	$thread
	 * @param int	$post		The (optionally) selected post id that will be highlighted.
	 * @param int   $workspace	The workspace id (optional)
	 * @return workspace_Action
	 */
	public function listPosts($thread, $post = null, $workspace = null)
	{
	    $W = bab_Widgets();

		require_once dirname(__FILE__) . '/forum.php';
		require_once dirname(__FILE__) . '/forum.ui.php';

		if (isset($workspace)) {
		    workspace_setCurrentWorkspace($workspace);
		}

		workspace_BreadCrumbs::setCurrentPosition($this->proxy()->listPosts($thread), workspace_translate('Thread'));

		$delegationId = workspace_getCurrentWorkspace();


		$page = workspace_Page()
//			->setTitle(workspace_translate('Post list'))
			->addItemMenu('forum', workspace_translate('Forum'), '')
			->setCurrentItemMenu('forum');

		if (!workspace_canViewThread($thread)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You must be logged in to view this forum thread.'));
			$GLOBALS['babBody']->addError(workspace_translate('Access denied'));
			$page->displayHtml();
			return;
		}



    	$toolbar = workspace_Toolbar('main-toolbar');

     	$newIcon = $W->Icon(workspace_translate('Back to thread list'), Func_Icons::ACTIONS_GO_PREVIOUS);
     	$newButton = $W->Link($newIcon, workspace_Controller()->Forum()->listThreads());

     	$toolbar->addItem($newButton);
		$page->addItem($toolbar);

//		$page->addItem($W->Title(workspace_translate('Thread list'), 1));

		$list = workspace_postList($thread, $post);
		$page->addItem($list);


		return $page;
	}




	/**
	 * Displays a form to edit an existing or a new forum thread.
	 *
	 * @return Widget_Action
	 */
	public function editThread($thread = null)
	{
		$page = workspace_Page();
		$editor = workspace_ThreadEditor();

		if (!is_null($thread)) {
			$thread = workspace_getThread($thread);

			$post = new workspace_Post($thread['post']);
			$editor->setValue(array('thread', 'title'), $post->subject);
			$editor->setValue(array('thread', 'body'), $post->message);
			$editor->setHiddenValue('thread[id]', $thread['id']);
		}
		if (is_array($thread) && isset($thread['id'])) {
			$page->setTitle(workspace_translate('Edit thread'));
		} else {
			$page->setTitle(workspace_translate('Create a new thread'));
		}

		$page->addItemMenu('forum', workspace_translate('Forum'), '')
			 ->setCurrentItemMenu('forum')
			 ->addItem($editor);
		$page->displayHtml();
	}




	/**
	 * Displays a form to reply another specified forum post.
	 *
	 * @param int	$post		The post to which we reply
	 * @return Widget_Action
	 */
	public function reply($post)
	{
		$page = workspace_Page();
		$page->addItemMenu('forum', workspace_translate('Forum'), '')
			->setCurrentItemMenu('forum')
			->setTitle(workspace_translate('Reply to post'));

		$postFrame = workspace_postDisplay($post);

		$editor = workspace_PostEditor($post);

		$editor->setHiddenValue('post[parent]', $post);




		$page->addItem($postFrame)
			 ->addItem($editor);

		return $page;
	}



	/**
	 * Displays a form to edit an existing or a new forum post.
	 *
	 * @return Widget_Action
	 */
	public function editPost($post = null)
	{
		$page = workspace_Page();
		$editor = workspace_PostEditor();

		if (!is_null($post)) {
			$post = new workspace_Post($post);
			$editor->setValue(array('post', 'title'), $post->subject);
			$editor->setValue(array('post', 'body'), $post->message);
			$editor->setHiddenValue('post[id]', $post->id);
			$editor->setHiddenValue('post[parent]', $post->parent);
		}

		if (is_null($post)) {
			$page->setTitle(workspace_translate('Create a new post'));
		} else {
			$page->setTitle(workspace_translate('Edit post'));
		}

		$page->addItemMenu('forum', workspace_translate('Forum'), '')
			 ->setCurrentItemMenu('forum')
			 ->addItem($editor);

		return $page;
	}



	/**
	 * Displays a preview of the post content.
	 *
	 * @param array $post
	 * @return unknown_type
	 */
	public function previewPost($post)
	{
		$preview = workspace_PostPreview($post);

		$page = workspace_Page()
			->setTitle(workspace_translate('Post preview'))
			->addItemMenu('forum', workspace_translate('Forum'), '')
			->setCurrentItemMenu('forum')
			->addItem($preview);

		return $page;
	}


	/**
	 * Saves the thread and returns to the thread list.
	 *
	 * @param array $thread
	 * @return Widget_Action
	 */
	public function saveThread($thread = null)
	{
		if (isset($thread['id'])) {
			workspace_saveThread($thread['title'], $thread['body'], $thread['id']);
		} else {
			workspace_saveThread($thread['title'], $thread['body']);
		}

		workspace_redirect($this->proxy()->listThreads());
	}



	/**
	 * Saves the post and returns to the post list.
	 *
	 * @param array $post
	 * @return Widget_Action
	 */
	public function savePost($post = null)
	{
		$post['subject'] = $post['title'];
		$post['message'] = $post['body'];
		$post['id_parent'] = $post['parent'];
		$post['confirmed'] = 'Y';

		$post = new workspace_Post($post);

		$post->save();

		workspace_redirect(workspace_BreadCrumbs::last());
	}



	/**
	 * Deletes the specified thread.
	 *
	 * @param int $thread
	 * @return Widget_Action
	 */
	public function deleteThread($thread)
	{
		workspace_deleteThread($thread);
		workspace_redirect(workspace_BreadCrumbs::last());
	}



	/**
	 * Re-opens the specified thread.
	 *
	 * @param int $thread
	 * @return Widget_Action
	 */
	public function openThread($thread)
	{
		workspace_openThread($thread);
		workspace_redirect(workspace_BreadCrumbs::last());
	}



	/**
	 * Closes the specified thread.
	 *
	 * @param int $thread
	 * @return Widget_Action
	 */
	public function closeThread($thread)
	{
		workspace_closeThread($thread);
		workspace_redirect(workspace_BreadCrumbs::last());
	}



	/**
	 * Deletes the specified post.
	 *
	 * @param int $post
	 * @return Widget_Action
	 */
	public function deletePost($post)
	{
		$post = workspace_getPost($post);

		workspace_deletePost($post['id']);
		workspace_redirect(workspace_BreadCrumbs::last());
	}



	/**
	 * Does nothing and return to the previous page.
	 *
	 * @return Widget_Action
	 */
	public function cancel()
	{
		workspace_redirect(workspace_BreadCrumbs::last());
	}


	/**
	 * Displays help on this page.
	 *
	 * @return Widget_Action
	 */
	public function help()
	{

	}
}
