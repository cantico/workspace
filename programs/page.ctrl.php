<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__) . '/page.class.php';
/* @var $App Func_App_Workspace */
$App = bab_functionality::get('App/Workspace');
$App->includeRecordController();

/**
 * This controller manages actions that can be performed on workspaces.
 */
class workspace_CtrlPage extends workspace_Controller
{
    /**
     * Initialize portlets on home page if containers are empty.
     *
     * @param workspace_Workspace $workspace
     */
    private function populateHomeContainers(workspace_Workspace $workspace)
    {
        $containerId = 'workspace_home_' . $workspace->id . '_left';
        if (!Func_PortletBackend::isEmptyContainer($containerId)) {
            return;
        }
        $functionalities = $workspace->getFunctionalities();
        if (count($functionalities) <= 0) {
            return;
        }
        foreach ($functionalities as $functionalityName) {
            $workspaceAddon = bab_functionality::get('WorkspaceAddon/' . $functionalityName);
            if (!$workspaceAddon) {
                continue;
            }
            $portletId = $workspaceAddon->getHomePortletDefinitionId();
            if (!$portletId) {
                continue;
            }
            $portletConfiguration = $workspaceAddon->getHomePortletConfiguration($workspace->id);
            Func_PortletBackend::addPortlet(
                $functionalityName,
                $portletId,
                $containerId,
                0,
                $portletConfiguration
            );
        }
    }

    /**
     *
     * @param string $activity
     * @param workspace_Workspace $workspace
     */
    private function populateActivityContainers($activity, workspace_Workspace $workspace)
    {
        $containerId = 'workspace_' . $activity . '_' . $workspace->id . '_bottom';
        if (!Func_PortletBackend::isEmptyContainer($containerId) || !$workspace->hasFunctionality($activity)) {
            return;
        }
        $workspaceAddon = bab_functionality::get('WorkspaceAddon/' . $activity);
        if (!$workspaceAddon) {
            return;
        }
        $portletId = $workspaceAddon->getPortletDefinitionId();
        if (!$portletId) {
            return;
        }
        $portletConfiguration = $workspaceAddon->getPortletConfiguration($workspace->id);
        Func_PortletBackend::addPortlet(
            $activity,
            $portletId,
            $containerId,
            0,
            $portletConfiguration
        );
    }

    /**
     *
     * @param string $activity
     * @param workspace_Workspace $workspace
     */
    private function populateContainers($activity, workspace_Workspace $workspace)
    {
        if ($activity == 'home') {
            $this->populateHomeContainers($workspace);
        } else {
            $this->populateActivityContainers($activity, $workspace);
        }
    }


    public function display($activity = 'home', $workspace = null)
    {
        $W = bab_Widgets();
        @bab_functionality::includefile('PortletBackend');

        $App = workspace_App();
        $set = $App->WorkspaceSet();
        $page = $App->Ui()->Page();

        /* @var $workspace workspace_Workspace */
        $workspace = $set->request($set->id->is($workspace));
        $delegationId = $workspace->delegation;

        if ($delegationId == 0) {
            $page->addError(workspace_translate('You do not have any accessible workspace'));
        } elseif (!workspace_workspaceExist($delegationId)) {
            $page->addError(workspace_translate('This workspace does not exist anymore'));
        } else {

            $this->populateContainers($activity, $workspace);

            $homeOvml = 'addons/workspace/workspacePage.html';

            /* Homepage is a OVML file : homeworkspace.html */
            $content = bab_printOvmlTemplate(
                $homeOvml,
                array(
                    'activity' => $activity,
                    'workspaceId' => $workspace->id
                )
            );

            $page->addItem($W->Html($content));
        }

        return $page;
    }
}
