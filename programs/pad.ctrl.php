<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/workspaces.php';
require_once dirname(__FILE__) . '/controller.class.php';
require_once dirname(__FILE__) . '/page.class.php';
require_once dirname(__FILE__) . '/pad.php';
require_once dirname(__FILE__) . '/pad.ui.php';



/**
 * This controller manages actions that can be performed on pads.
 */
class workspace_CtrlPad extends workspace_Controller
{
	/**
	 * Displays the list of pads.
	 *
	 * @return workspace_Action
	 */
	public function displayList($sort = null, $workspace = null)
	{
        $W = bab_Widgets();

		workspace_BreadCrumbs::setCurrentPosition($this->proxy()->displayList(), workspace_translate("Pads"));


		if (isset($workspace)) {
			workspace_setCurrentWorkspace($workspace);
		}

		$delegationId = workspace_getCurrentWorkspace();


		$page = workspace_Page();
		$page->addItemMenu('pads', workspace_translate('Pads'), '');
		$page->setCurrentItemMenu('pads');

		if (false) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You must be logged in to view the forum threads.'));
			$GLOBALS['babBody']->addError(workspace_translate('Access denied'));
		} else {
		    $toolbar = workspace_Toolbar('main-toolbar');

		    if (workspace_canCreatePad()) {
		        $newIcon = $W->Icon(workspace_translate('New pad'), Func_Icons::ACTIONS_LIST_ADD);
		        $newButton = $W->Link($newIcon, workspace_Controller()->Pad()->editPad());

		        $toolbar->addItem($newButton);
		    }
		    $page->addItem($toolbar);

		    $page->addItem($W->Title(workspace_translate('Pad list'), 1));

		    try {
		        $list = workspace_padList($delegationId);
		    } catch (Exception $exception) {

		        $body = bab_getBody();
		        $body->addError($exception->getMessage());
		        return $page;
		    }

 			$page->addItem($list);
		}

		return $page;
	}



	/**
	 * Edits the pad content.
	 *
	 * @param string   $pad       The pad id.
	 * @return workspace_Action
	 */
	public function edit($pad = null)
	{
        $W = bab_Widgets();

		workspace_BreadCrumbs::setCurrentPosition($this->proxy()->edit($pad), workspace_translate('Pad'));

		$delegationId = workspace_getCurrentWorkspace();

		$page = workspace_Page();
		$page->addItemMenu('pads', workspace_translate('Pads'), $this->proxy()->displayList()->url());
		$page->addItemMenu('pad', workspace_translate('Pad'), '');
		$page->setCurrentItemMenu('pad');

		if (!workspace_canViewPad($pad)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You must be logged in to view this pad.'));
			$GLOBALS['babBody']->addError(workspace_translate('Access denied'));
			return $page;
		}

		/* @var $etherpad Func_Etherpad */
		$etherpad = bab_functionality::get('etherpad');

		$padSet = $etherpad->PadSet();
		$pad = $padSet->get($pad);

        $padName = $pad->name;
        $page->addItem($W->Title($padName, 1));

        $iframe = $pad->getIframe();

		$page->addItem($W->Html($iframe, 'workspace-pad-editor'));

		return $page;
	}



	/**
	 * Displays the pad content.
	 *
	 * @param string   $pad       The pad id.
	 * @return workspace_Action
	 */
	public function display($pad = null)
	{
	    workspace_BreadCrumbs::setCurrentPosition($this->proxy()->display($pad), workspace_translate('Pad'));

	    $delegationId = workspace_getCurrentWorkspace();

	    $page = workspace_Page();
	    $page->addItemMenu('pads', workspace_translate('Pads'), $this->proxy()->displayList()->url());
	    $page->addItemMenu('pad', workspace_translate('Pad'), '');
	    $page->setCurrentItemMenu('pad');

	    if (!workspace_canViewPad($pad)) {
	        bab_userIsloggedin() || bab_requireCredential(workspace_translate('You must be logged in to view this pad.'));
	        $GLOBALS['babBody']->addError(workspace_translate('Access denied'));
	        $page->displayHtml();
	        return;
	    }

	    /* @var $etherpad Func_Etherpad */
	    $etherpad = bab_functionality::get('etherpad');

		$padSet = $etherpad->PadSet();
		/* @var $pad LibEtherpad_Pad */
		$pad = $padSet->get($pad);

	    $html = $pad->getHtml();

	    $W = bab_Widgets();
        $padName = $pad->name;
        $page->addItem($W->Title($padName, 1));
	    $page->addItem($W->Html($html));

	    return $page;
	}



	/**
	 * Displays the pad content.
	 *
	 * @param string   $pad       The pad id.
	 * @return workspace_Action
	 */
	public function displayArchive($pad = null, $rev = null)
	{
	    workspace_BreadCrumbs::setCurrentPosition($this->proxy()->display($pad), workspace_translate('Pad'));

	    $delegationId = workspace_getCurrentWorkspace();

	    $page = workspace_Page();
	    $page->addItemMenu('pads', workspace_translate('Pads'), $this->proxy()->displayList()->url());
	    $page->addItemMenu('pad', workspace_translate('Pad'), '');
	    $page->setCurrentItemMenu('pad');

	    if (!workspace_canViewPad($pad)) {
	        bab_userIsloggedin() || bab_requireCredential(workspace_translate('You must be logged in to view this pad.'));
	        $GLOBALS['babBody']->addError(workspace_translate('Access denied'));
	        $page->displayHtml();
	        return;
	    }

	    /* @var $etherpad Func_Etherpad */
	    $etherpad = bab_functionality::get('etherpad');

		$padSet = $etherpad->PadSet();
		$pad = $padSet->get($pad);

	    $html = $pad->getArchivedContent($rev);

	    $W = bab_Widgets();
        $padName = $pad->name;
        $page->addItem($W->Title($padName, 1));
	    $page->addItem($W->Html($html));

	    return $page;
	}



	/**
	 * Displays an editor to rename the specified pad or create a new one.
	 *
	 * @param string   $pad    The pad id.
	 * @return Widget_Action
	 */
	public function editPad($pad = null)
	{
	    $W = bab_Widgets();
	    $page = workspace_Page();
	    $page->addItemMenu('pad', workspace_translate('Pad'), '');
	    $page->setCurrentItemMenu('pad');

	    $padEditor = workspace_PadEditor($pad);

	    if (!isset($pad)) {
	        $page->addItem($W->Title(workspace_translate('Create a new pad'), 1));
	    } else {
	        $page->addItem($W->Title(workspace_translate('Rename pad'), 1));

	    }

	    $page->addItem($padEditor);

	    return $page;
	}



	/**
	 * Saves the pad.
	 *
	 * @param array   $pad    The pad data.
	 * @return Widget_Action
	 */
	public function savePad($pad = null)
	{
	    $delegationId = workspace_getCurrentWorkspace();

	    /* @var $etherpad Func_Etherpad */
	    $etherpad = bab_functionality::get('etherpad');


	    $padName = $pad['title'];

	    $pad = $etherpad->createPad($padName, workspace_reference($delegationId));
	    $pad->setText($padName);

	    workspace_redirect(workspace_BreadCrumbs::last());
	}



	public function archivePad($pad)
	{
	    workspace_getCurrentWorkspace();

	    /* @var $etherpad Func_Etherpad */
	    $etherpad = bab_functionality::get('etherpad');

		$pad = $etherpad->getPad($pad);
	    $pad->archiveContent();

	    workspace_redirect(workspace_BreadCrumbs::last());
	}





	/**
	 * Deletes the specified pad.
	 *
	 * @param string   $pad    The pad id.
	 * @return Widget_Action
	 */
	public function delete($pad)
	{
	    /* @var $etherpad Func_Etherpad */
	    $etherpad = bab_functionality::get('etherpad');

	    //$etherpad->init();

		$padSet = $etherpad->PadSet();
		$pad = $padSet->get($pad);
	    $pad->delete();

	    bab_getBody()->addError(workspace_translate('The pad has been deleted.'));

		workspace_redirect(workspace_BreadCrumbs::last());
	}





	/**
	 * Saves the specified pad as an article.
	 *
	 * @param string   $pad    The pad id.
	 * @return Widget_Action
	 */
	public function saveAsArticle($pad)
	{
	    /* @var $etherpad Func_Etherpad */
	    $etherpad = bab_functionality::get('etherpad');

	    $pad = $etherpad->getPad($pad);

	    require_once dirname(__FILE__) . '/articles.php';
	    $topicId = workspace_getTopicId();

	    workspace_saveArticle($pad->name, bab_getStringAccordingToDataBase($pad->getHtml(), bab_charset::UTF_8), null, $topicId);

	    bab_getBody()->addMessage(workspace_translate('The pad has been saved to a new article.'));

	    workspace_redirect(workspace_BreadCrumbs::last());
	}





	/**
	 * Does nothing and return to the previous page.
	 *
	 * @return Widget_Action
	 */
	public function cancel()
	{
		workspace_redirect(workspace_BreadCrumbs::last());
	}
}
