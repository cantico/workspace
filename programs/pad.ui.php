<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/workspacewidgets.php';





/**
 * @return Widget_Form
 */
function workspace_PadEditor($pad = null)
{
    $W = bab_Widgets();

    $form = $W->Form();
    $frame = new workspace_BaseForm('pad_editor');
    $frame->setName('pad')
        ->addClass('workspace-dialog');

    $titleFormItem = workspace_FormField(
        workspace_translate('Title:'),
        $W->LineEdit()
            ->setName('title')
            ->setSize(80)->setName('title')
            ->setMandatory(true, workspace_translate("The pad title must not be empty."))
    );

    if (isset($pad)) {

    }

    $frame->addItem($titleFormItem);


    $frame->addButton(
        $W->SubmitButton('save')
        ->validate(true)
        ->setLabel(workspace_translate("Save"))
        ->setAction(workspace_Controller()->Pad()->savePad())
    );
    $frame->addButton(
        $W->SubmitButton('cancel')
        ->setLabel(workspace_translate("Cancel"))
        ->setAction(workspace_Controller()->Pad()->cancel())
    );

    $form->setLayout($W->VBoxLayout());
    $form->addItem($frame);

    workspace_setSelfPageHiddenFields($form);

    return $form;
}







/**
 * @return Widget_Frame
 */
function workspace_padList($delegationId)
{
    require_once dirname(__FILE__) . '/pad.php';

    $W = bab_Widgets();


    $listView = new workspace_BaseList('pad_list');
    $listView->addClass('workspace-list');

    /* @var $etherpad Func_Etherpad */
    $etherpad = bab_functionality::get('etherpad');

    if (!$etherpad) {
        throw new Exception('The Etherpad functionality is not available.');
    }

    $padSet = $etherpad->PadSet();

    $pads = $padSet->select($padSet->reference->is(workspace_reference($delegationId)));

    $nbPads = 0;
    foreach ($pads as $pad) {

        $padId = $pad->id;
        $nbPads++;



        $buttonBox = workspace_buttonBox();

        $revisionsBox =  $W->FlowItems()->setHorizontalSpacing(4, 'px');

        if (workspace_canEditPad($padId)) {
            $buttonBox->addItem(
                $W->Link(
                    workspace_translate('Edit'),
                    workspace_Controller()->Pad()->edit($padId)
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                ->setSizePolicy(Widget_SizePolicy::MINIMUM)
            );
            $buttonBox->addItem(
                $W->Link(
                    workspace_translate('Save as article'),
                    workspace_Controller()->Pad()->saveAsArticle($padId)
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_SAVE)
                ->setSizePolicy(Widget_SizePolicy::MINIMUM)
            );
        } elseif (workspace_canViewPad($padId)) {
            $buttonBox->addItem(
                $W->Link(
                    workspace_translate('View'),
                    workspace_Controller()->Pad()->display($padId)
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_VIEW_LIST_DETAILS)
                ->setSizePolicy(Widget_SizePolicy::MINIMUM)
            );
        }

        if (workspace_canDeletePad($padId)) {
            $buttonBox->addItem(
                $W->Link(
                    workspace_translate('Delete'),
                    workspace_Controller()->Pad()->delete($padId)
                )->setConfirmationMessage(workspace_translate('Are you sure you want to delete this pad?'))
                ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE)
                ->setSizePolicy(Widget_SizePolicy::MINIMUM)
            );
        }

        /*
        $revisions = $pad->getArchivedRevisions();
        foreach ($revisions as $revision) {
            $revisionsBox->addItem(
                $W->Link(
                    workspace_translate($revision),
                    workspace_Controller()->Pad()->displayArchive($padId, $revision)
                )->addClass('widget-actionbutton')
                ->setSizePolicy(Widget_SizePolicy::MINIMUM)
            );
        }
        */


        $padName = $pad->name;

        try {
            $text = $pad->getText();
            $revisionsCount = $pad->getRevisionsCount();
            $lastEdited = bab_shortDate($pad->getLastEdited());
        } catch (Exception $e) {
            $text = '';
            $revisionsCount = '-';
            $lastEdited = '-';
        }

        $listElement = $W->VBoxItems(
            $W->VBoxItems(
                $W->Title($W->Link($padName, workspace_Controller()->Pad()->display($padId)), 3),
                $W->Label(sprintf(workspace_translate('Last edited on %s'), $lastEdited)),
//                $W->Label(bab_abbr($text, BAB_ABBR_FULL_WORDS, 1000)),
                $W->Html($pad->getHtml()),
 //               $W->Label($revisionsCount),

                $revisionsBox
            )->setVerticalSpacing(1, 'em'),
            $buttonBox
        )->setSizePolicy('widget-list-element');

        $listView->addItem($listElement);

    }

    if ($nbPads < 1) {
        $row = $W->VBoxItems(
            $W->Title(workspace_translate('No pads were created yet'), 4),
            $W->Label(workspace_translate('You can add a new one by clicking \'New pad\' in the toolbar above.'))
        );
        $listView->addItem($row->addClass('workspace-empty-list-notice'));
    }

    return $listView;
}
