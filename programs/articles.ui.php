<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/workspacewidgets.php';
require_once dirname(__FILE__) . '/articles.php';
require_once dirname(__FILE__) . '/files.ctrl.php';
require_once dirname(__FILE__) . '/files.php';






/**
 * @return Widget_Form
 */
function workspace_ArticleEditor()
{
	$W = bab_Widgets();

	$form = $W->Form();
	$frame = new workspace_BaseForm('article_editor');
	$frame->setName('article')
			->addClass('workspace-dialog');

	switch (workspace_getWorkspacesTextEditor()) {
		case 'plain':
			$bodyEditor = $W->TextEdit('bab_article_body');
			break;
		case 'wysiwyg':
			$bodyEditor = $W->BabHtmlEdit('bab_article_body');
			break;
		case 'rich':
		default:
			$bodyEditor = $W->SimpleHtmlEdit('bab_article_body');
			break;
	}

	$submitableTopics = workspace_getSubmitableTopics();

	if (count($submitableTopics) > 1) {
		$topicSelect = $W->Select()->setName('topic');
		foreach ($submitableTopics as $topicId => $topicName) {
			$topicSelect->addOption($topicId, $topicName);
		}

		$topicFormItem = workspace_FormField(
			workspace_translate('Topic:'),
			$topicSelect
		);

	} else {
	    foreach ($submitableTopics as $topicId => $topicName) {
	        $topicFormItem = $W->Hidden()->setName('topic')->setValue($topicId);
	    }
	}

	$titleFormItem = workspace_FormField(
		workspace_translate('Title:'),
		$W->LineEdit()
			->setName('title')
			->setSize(80)
			->setMandatory(true, workspace_translate('The article title must not be empty'))
	);
	$bodyFormItem = workspace_FormField(
		workspace_translate('Body:'),
		$bodyEditor
			->setName('body')
			->setLines(20)
			->setColumns(100)
			->setMandatory(true, workspace_translate('The article body must not be empty'))
	);

	if ($topicFormItem) {
		$frame->addItem($topicFormItem);
	}
	$frame->addItem($titleFormItem);
	$frame->addItem($bodyFormItem);


	$frame->addButton(
	    $W->SubmitButton('save')
			->validate(true)
			->setLabel(workspace_translate("Save"))
			->setAction(workspace_Controller()->Articles()->save())
	)
	->addButton(
	    $W->SubmitButton('cancel')
			->setLabel(workspace_translate("Cancel"))
			->setAction(workspace_Controller()->Articles()->cancel())
	)
	->addButton(
	    $W->SubmitButton('preview')
			->setLabel(workspace_translate("Preview"))
			->setAction(workspace_Controller()->Articles()->preview())
	);

	$form->setLayout($W->VBoxLayout())->addItem($frame);

	workspace_setSelfPageHiddenFields($form);

	return $form;
}




/**
 * @return Widget_Form
 */
function workspace_ArticlePreview(array $article)
{
	$W = bab_Widgets();

	$form = $W->Form();
	$frame = new workspace_BaseForm('article_preview');
	$frame->setName('article')
			->addClass('workspace-dialog')
			->addClass('workspace-article');

	$title = $W->Html('<h1>' . $article['title'] . '</h1>
						<div class="workspace-article-date icon-left-16 icon-16x16 icon-left">
						<span class="workspace-article-date">' . workspace_formatDateTime(time()) . '</span>
   						</div>'
					);

	$r = new workspace_replace();
	if (is_array($article['body'])) {
		$body = $article['body']['raw'];
		$article['body'] = $body;
	} else {
		$body = $article['body'];
	}


	$r->ref($body);

	$body = $W->Html('<div class="workspace-article-body">' . $body . '</div>');

	$frame->addItem($title);
	$frame->addItem($body);


	$frame->addButton($W->SubmitButton('return')
							->setLabel(workspace_translate("Return to edit mode"))
							->setAction(workspace_Controller()->Articles()->edit())
							)
		  ->addButton($W->SubmitButton('save')
		  					->validate(true)
							->setLabel(workspace_translate("Save"))
							->setAction(workspace_Controller()->Articles()->save())
							)
		;

	$form->setLayout($W->VBoxLayout())->addItem($frame);

	workspace_setSelfPageHiddenFields($form);
    $article['id_topic'] = $article['topic'];
	$form->setHiddenValues('article', $article);

	return $form;
}


/**
* @return Widget_Frame
*/
function workspace_ArticleShow(array $article)
{
	$W = bab_Widgets();

	$toolbar = workspace_Toolbar('main-toolbar');

	$returnIcon = $W->Icon(workspace_translate('Back to article list'), Func_Icons::ACTIONS_GO_PREVIOUS);
	$returnButton = $W->Link($returnIcon, workspace_Controller()->Articles()->displayList());
	$toolbar->addItem($returnButton);

	if (workspace_canCreateArticle()) {
		$newIcon = $W->Icon(workspace_translate('New article'), Func_Icons::ACTIONS_ARTICLE_NEW);
		$newButton = $W->Link($newIcon, workspace_Controller()->Articles()->edit());
		$toolbar->addItem($newButton);
	}

	$listView = new workspace_BaseList('article');
	$listView->addClass('workspace-list');

	$row = workspace_articleFrame($article);

	$listView->addItem($row);

	$splitview = $W->Frame(
		'workspace_article_splitview',
		$W->HBoxItems(
			$listView->setSizePolicy(Widget_SizePolicy::MAXIMUM)
		)->addClass('expand')
	);

	$vbox = $W->VBoxItems($toolbar, $splitview);

	return $vbox;
}





/**
 * Returns a frame with the article as displayed in the article list.
 *
 * @param array $article
 *
 * @return Widget_Frame
 */
function workspace_articleFrame(array $article, $allTopicIds = null, $selectedTopic = null)
{
	$W = bab_Widgets();

	/* @var $T Func_Thumbnailer */
	$T = bab_functionality::get('Thumbnailer');

	/* @var $replaceFactory workspace_replace */
	$replaceFactory = bab_getInstance('workspace_replace');

	$article['topic_name'] = bab_getTopicTitle($article['id_topic']);
	$replaceFactory->ref($article['head']);

	$userId = $article['id_author'];
	$authorIcon = workspace_userPreview($userId, workspace_formatDateTime(bab_mktime($article['date'])));

//	$dirEntry = bab_getDirEntry($article['id_author'], BAB_DIR_ENTRY_ID_USER);

// 	$authorIcon = $W->Icon(bab_getUserName($article['id_author'], true) . "\n" . workspace_formatDateTime(bab_mktime($article['date'])));

// 	$authorIcon->setStockImage(Func_Icons::APPS_USERS);
// 	$authorIcon->addClass('bab-user-' . $article['id_author']);

// 	if (isset($dirEntry['jpegphoto']['photo'])) {
// 		$photo = $dirEntry['jpegphoto']['photo'];

// 		if ($T && $photo instanceof bab_dirEntryPhoto) {
// 			$T->setSourceBinary($photo->getData(), $photo->lastUpdate());
// 			$T->setBorder(1, '#cccccc', 1);
// 			$imageUrl = $T->getThumbnail(30, 30);
// 			$authorIcon->setImageUrl($imageUrl);
// 		}
// 	}


	$nbComment = bab_getArticleNbComment($article['id']);
	if ($nbComment > 0) {
		$strComment = workspace_translate('Comments');
		$strComment .= ' ('. $nbComment . ')';
	} else {
		$strComment = workspace_translate('Add a comment');
	}

	$buttonBox = workspace_buttonBox();

	// If more than one article topics are available in the workspace,
	// we display the article topic name below the author in the left box.
	if (isset($selectedTopic) || count($allTopicIds) > 1) {
		$buttonBox->addItem(
			$W->Link(
				sprintf(workspace_translate('Topic: %s'), $article['topic_name']),
				workspace_Controller()->Articles()->displayList($article['id_topic'])
			)->addClass('workspace-topic-label')
		);
	}

	if (workspace_canCommentTopic($article['id_topic'])) {
		$buttonBox->addItem(
			$W->Link(
				$strComment,
				"javascript:bab_popup('?tg=comments&idx=List&topics=" . $article['id_topic'] . "&article=" . $article['id'] . "', 6)"
			)->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_NOTE_NEW)
		    ->setSizePolicy(Widget_SizePolicy::MINIMUM)
		);
	}
	if (workspace_canEditArticle($article['id'])) {
		$buttonBox->addItem(
			$W->Link(
				workspace_translate('Edit'),
			    workspace_Controller()->Articles()->edit($article['id'])
			)->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
		    ->setSizePolicy(Widget_SizePolicy::MINIMUM)
		);
	}
	if (workspace_canDeleteArticle($article['id'])) {
		$buttonBox->addItem(
			$W->Link(
				workspace_translate('Delete'),
				workspace_Controller()->Articles()->delete($article['id'])
			)
			->setConfirmationMessage(workspace_translate('Are you sure you want to delete this article?'))
			->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE)
		    ->setSizePolicy(Widget_SizePolicy::MINIMUM)
		);
	}

	$row = $W->HBoxItems(
		$authorIcon->setSizePolicy(Func_Icons::ICON_LEFT_48)
		    ->addClass('widget-15em'),
		$W->VBoxItems(
			$W->Link(
				$W->Label($article['title'])->addClass('title', 'widget-strong'),
				workspace_Controller()->Articles()->show($article['id'])
			),
			$W->Html($article['head'])->addClass('body'),
		    $buttonBox
		)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
		->addClass('workspace-content-summary')
	)->setHorizontalSpacing(1, 'em')
    ->setVerticalAlign('top');

	return $row;
}




/**
 *
 * @param int $selectedTopic If specified filter articles on this topic.
 *
 * @return Widget_Frame
 */
function workspace_ArticleList($selectedTopic = null)
{
	global $babDB;

	$allTopicIds = workspace_getAllWorkspaceTopics();
	if (isset($selectedTopic)) {
		if (isset($allTopicIds[$selectedTopic])) {
			$allTopicIds = array($selectedTopic => $allTopicIds[$selectedTopic]);
		} else {
			$allTopicIds = array();
		}
	}

	$W = bab_Widgets();

	/* @var $T Func_Thumbnailer */
	$T = bab_functionality::get('Thumbnailer');

	$toolbar = workspace_Toolbar('main-toolbar');

	if (isset($selectedTopic)) {
		$returnIcon = $W->Icon(workspace_translate('All topics'), Func_Icons::ACTIONS_GO_PREVIOUS);
		$returnButton = $W->Link($returnIcon, workspace_Controller()->Articles()->displayList());

		$toolbar->addItem($returnButton);
	}
	if (workspace_canCreateArticle()) {
		$newIcon = $W->Icon(workspace_translate('New article'), Func_Icons::ACTIONS_ARTICLE_NEW);
		$newButton = $W->Link($newIcon, workspace_Controller()->Articles()->edit());

		$toolbar->addItem($newButton);
	}

	$listView = new workspace_BaseList('article_list');
	$listView->addClass('workspace-list');

	$articles = workspace_getArticles(null, array_keys($allTopicIds), workspace_getCurrentWorkspace());
	$topicId = workspace_getTopicId();

	$nbArticles = 0;

	while ($articles && ($article = $babDB->db_fetch_assoc($articles))) {

		$row = workspace_articleFrame($article, $allTopicIds, $selectedTopic);

		$listView->addItem($row);
		$nbArticles++;
	}

	if ($nbArticles < 1) {
		$row = $W->VBoxItems(
			$W->Title(workspace_translate('No article were published yet'), 4),
			$W->Label(workspace_translate('You can add a new one by clicking \'New article\' in the toolbar above.'))
		);
		$row->addClass('workspace-empty-list-notice');
		$listView->addItem($row);
	}

	$splitview = $W->Frame(
		'workspace_article_splitview',
		$W->HBoxItems(
			$listView->setSizePolicy(Widget_SizePolicy::MAXIMUM)
		)->addClass('expand')
	);

	$vbox = $W->VBoxItems($toolbar, $splitview);

	return $vbox;
}



