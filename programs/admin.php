<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/workspaces.php';
require_once dirname(__FILE__) . '/directories.php';



/**
 * Checks if the specified name is already the name of a root folder.
 *
 * @param int    $workspaceId
 * @param string $name
 *
 * @return bool
 */
function checkRootFolderNameExists($workspaceId, $name)
{
    include_once $GLOBALS['babInstallPath'] . 'utilit/fileincl.php';

    $sName = replaceInvalidFolderNameChar($name);

    $rootFolderId = workspace_getWorkspaceRootFolderId();

    $oFmFolderSet = new BAB_FmFolderSet();

    $oId = $oFmFolderSet->aField['iId'];
    $oName = $oFmFolderSet->aField['sName'];
    $oIdDgOwner = $oFmFolderSet->aField['iIdDgOwner'];

    $oCriteria = $oId->notIn($rootFolderId);
    $oCriteria = $oCriteria->_and($oName->in($sName));
    $oCriteria = $oCriteria->_and($oIdDgOwner->in($workspaceId));
    $oFmFolder = $oFmFolderSet->get($oCriteria);
    if (!is_null($oFmFolder)) {
        return true;
    }
    return false;
}


/**
 * Updates the name of a root folder.
 *
 * @param int    $workspaceId
 * @param string $name
 *
 * @return bool
 */
function workspace_updateRootFolderName($workspaceId, $name)
{
    include_once $GLOBALS['babInstallPath'] . 'utilit/fileincl.php';

    $sName = replaceInvalidFolderNameChar($name);

    if (!isStringSupportedByFileSystem($sName)) {
        return false;
    }

    $rootFolderId = workspace_getWorkspaceRootFolderId($workspaceId);


    $sRootFmPath = BAB_FileManagerEnv::getCollectivePath($workspaceId);
    $sRelativePath = '';

    $oFmFolder = BAB_FmFolderHelper::getFmFolderById($rootFolderId);

    if (!$oFmFolder) {
        return false;
    }

    if ($oFmFolder->getName() != $sName) {
        BAB_FmFolderSet::rename($sRootFmPath, '', $oFmFolder->getName(), $sName);
        BAB_FmFolderCliboardSet::rename('', $oFmFolder->getName(), $sName, 'Y');
        BAB_FolderFileSet::renameFolder($oFmFolder->getName() . '/', $sName, 'Y');

        $oFmFolder->setName($sName);
        $oFmFolder->save();
    }

    return true;
}


/**
 * Updates the name of the specified workspace.
 *
 * @param int    $workspaceId
 * @param string $workspaceName
 * @param int    $userId
 * @param int    $groupId
 * @param string $skin				name of the skin associated to the workspace
 * @param string $skin_styles		name of the stylesheet of the skin associated to the workspace
 * @param int    $num_article
 * @param int    $num_forum
 * @param int    $num_agenda
 * @param int    $num_file
 * @param bool   $other_them
 * @param bool   $file_size
 * @param bool   $file_size
 * @param bool   $comments
 * @param bool   $forum_notif
 * @param bool   $update_directory
 * @param bool   $versioning
 * @param bool   $disabled
 * @param bool   $notifyCalendar
 * @param int    $category
 * @param bool   $allowUserCreation
 */
function workspace_updateWorkspace($workspaceId, $workspaceName, $notify, $skin, $skin_styles, $num_article, $num_forum, $num_agenda, $num_file, $other_them, $file_size, $comments, $forum_notif, $update_directory, $versioning, $disabled, $notifyCalendar, $category, $allowUserCreation)
{
    global $babDB;

    $res = $babDB->db_query('SELECT * FROM '.BAB_DG_GROUPS_TBL.'
                            WHERE id!='.$babDB->quote($workspaceId).' AND name='.$babDB->quote($workspaceName));
    if( $babDB->db_num_rows($res) > 0) {
        return false;
    }

    $req = 'UPDATE '.BAB_DG_GROUPS_TBL.' SET
                name='.$babDB->quote($workspaceName).',
				iIdCategory='.$babDB->quote($category).'
            WHERE
                id='.$babDB->quote($workspaceId);
    $res = $babDB->db_query($req);

    if ($res) {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/workspace/workspaces/' . $workspaceId);
        $registry->setKeyValue('name', $workspaceName);
        $registry->setKeyValue('skin', $skin);
        $registry->setKeyValue('skin_styles', $skin_styles);
        $registry->setKeyValue('num_article', $num_article);
        $registry->setKeyValue('num_forum', $num_forum);
        $registry->setKeyValue('num_agenda', $num_agenda);
        $registry->setKeyValue('num_file', $num_file);
        $registry->setKeyValue('other_them', $other_them);
        $registry->setKeyValue('update_directory', $update_directory);



        if( $file_size > floor($GLOBALS['babMaxTotalSize']/1048576) ){
            $file_size = floor($GLOBALS['babMaxTotalSize']/1048576);
        }
        if( $file_size > floor($GLOBALS['babMaxGroupSize']/1048576) ){
            $file_size = floor($GLOBALS['babMaxGroupSize']/1048576);
        }
        $registry->setKeyValue('file_size', $file_size);
        $registry->setKeyValue('disabled', (bool) $disabled);

        workspace_updateRootFolderName($workspaceId, $workspaceName);
        workspace_setWorkspaceFileVersioning($versioning, $workspaceId);
        workspace_setWorkspaceFileNotifications($notify, $workspaceId);
        workspace_setWorkspaceForumNotifications($forum_notif, $workspaceId);
        workspace_setWorkspaceCalendarNotifications($notifyCalendar, $workspaceId);
        workspace_setWorkspaceComments($comments, $workspaceId);
        workspace_setWorkspaceAllowUserCreation($allowUserCreation, $workspaceId);

        workspace_setPortletContainerConfiguration($workspaceId);
    }
}



/**
 * If the Portlets functionality is available, configure access rights to workspace
 * portlet containers.
 *
 * @param int $workspaceId
 */
function workspace_setPortletContainerConfiguration($workspaceId)
{
    /* @var $portlet Func_Portlet */
    $portlets = @bab_functionality::get('Portlets');
    if (!$portlets) {
        return;
    }

    $administratorsGroupId = workspace_getWorkspaceAdministratorsGroupId($workspaceId);

    $containerConfiguration = $portlets->getContainerConfiguration('workspace_' . $workspaceId . '_1');
    $containerConfiguration->addPortletManagerGroup($administratorsGroupId);
    $containerConfiguration = $portlets->getContainerConfiguration('workspace_' . $workspaceId . '_2');
    $containerConfiguration->addPortletManagerGroup($administratorsGroupId);

    /* @var $portletsBackend Func_PortletBackend */
    $portletsBackend = @bab_functionality::get('PortletBackend');
    if (!$portletsBackend) {
        return;
    }
    if (Func_PortletBackend::isEmptyContainer('workspace_' . $workspaceId . '_1') && Func_PortletBackend::isEmptyContainer('workspace_' . $workspaceId . '_2')) {
        //Add workspace portlets in page
        $infos = workspace_getWorkspaceInfo($workspaceId);

        if (isset($infos['name'])) {
            $calendarName = sprintf(workspace_translate('%s agendas'), $infos['name']);
            $forumName = sprintf(workspace_translate('%s forum'), $infos['name']);
            $articlesName = sprintf(workspace_translate('%s articles'), $infos['name']);
            $filesName = sprintf(workspace_translate('%s files'), $infos['name']);

            $calendarPref = array(
                '_blockTitleType' => 'custom',
                '_blockTitle' => $calendarName,
                'workspaceIds' => array($workspaceId)
            );
            $forumPref = array(
                '_blockTitleType' => 'custom',
                '_blockTitle' => $forumName,
                'workspaceIds' => array($workspaceId),
                'threadNumber' => 4,
                'strlen' => 100
            );
            $articlesPref = array(
                '_blockTitleType' => 'custom',
                '_blockTitle' => $articlesName,
                'workspaceIds' => array($workspaceId),
                'articleNumber' => 4,
                'strlen' => 100
            );
            $filesPref = array(
                '_blockTitleType' => 'custom',
                '_blockTitle' => $filesName,
                'workspaceIds' => array($workspaceId),
                'fileNumber' => 4
            );
        } else {
            $calendarPref = array(
                '_blockTitleType' => 'portlet',
                '_blockTitle' => '_',
                'workspaceIds' => array($workspaceId)
            );
            $forumPref = array(
                '_blockTitleType' => 'portlet',
                '_blockTitle' => '_',
                'workspaceIds' => array($workspaceId),
                'threadNumber' => 4,
                'strlen' => 100
            );
            $articlesPref = array(
                '_blockTitleType' => 'portlet',
                '_blockTitle' => '_',
                'workspaceIds' => array($workspaceId),
                'articleNumber' => 4,
                'strlen' => 100
            );
            $filesPref = array(
                '_blockTitleType' => 'portlet',
                '_blockTitle' => '_',
                'workspaceIds' => array($workspaceId),
                'fileNumber' => 4
            );
        }
        $portletsBackend->addPortlet('Workspace', 'calendars', 'workspace_' . $workspaceId . '_1', 0, $calendarPref);
        $portletsBackend->addPortlet('Workspace', 'forum', 'workspace_' . $workspaceId . '_2', 0, $forumPref);
        $portletsBackend->addPortlet('Workspace', 'articles', 'workspace_' . $workspaceId . '_2', 1, $articlesPref);
        $portletsBackend->addPortlet('Workspace', 'files', 'workspace_' . $workspaceId . '_2', 2, $filesPref);
    }
}


/**
 * Removes the specified workspace WITHOUT removing associated components and content.
 * The workspace will not be accessible to its members through the interface
 * but forums, articles, etc. will still exist.
 *
 * @param int		$workspaceId
 * @param	bool	$deleteObjects		true : objects in delegation are deleted, false : objects are moved into main site (DG0)
 * @return bool
 */
function workspace_removeWorkspace($workspaceId, $deleteObjects)
{
    include_once $GLOBALS['babInstallPath'].'utilit/delincl.php';

    if (!function_exists('bab_deleteDelegation'))
    {
        throw new Exception('This function requires ovidentia >= 7.7.100');
    }

    $registry = bab_getRegistryInstance();
    workspace_setCurrentWorkspace(0);
    if (!$registry->isDirectory('/workspace/workspaces/' . $workspaceId)) {
        return false;
    }

// 	$registry->changeDirectory('/workspace/workspaces/' . $workspaceId);
// 	$registry->deleteDirectory();

    $registry->changeDirectory('/workspace/removedWorkspaces/' . $workspaceId);
    $registry->deleteDirectory();
    if ($registry->isDirectory('/workspace/removedWorkspaces/' . $workspaceId)) {
        return false;
    }
    $registry->moveDirectory('/workspace/workspaces/' . $workspaceId, '/workspace/removedWorkspaces/' . $workspaceId);
    $registry->changeDirectory('/workspace/removedWorkspaces/' . $workspaceId);
    $registry->setKeyValue('removed', date('Y-m-d H:i:s'));


    bab_deleteDelegation($workspaceId, $deleteObjects);

    return true;
}


/**
 * Deletes all information linked to a workspace.
 * Forums, articles, etc. will be definitely lost.
 *
 * - Group
 * - Delegation
 * - Topic category
 * - Directory
 * - Forum
 * - Root folder
 * - Calendar
 *
 * @param $workspaceId
 * @return bool
 */
function workspace_clearWorkspace($workspaceId)
{
    global $babDB;
    include_once dirname(__FILE__).'/workspaces.php';
    include_once $GLOBALS['babInstallPath'].'utilit/delincl.php';
    include_once $GLOBALS['babInstallPath'].'utilit/calincl.php';

    if ($workspaceId == 0) {
        return;
    }

    $topicId = workspace_getWorkspaceTopicId($workspaceId);

    if (isset($topicId)) {
        $sql = 'SELECT id FROM ' . BAB_TOPICS_CATEGORIES_TBL . ' WHERE id_dgowner=' . $babDB->quote($workspaceId);
        $categories = $babDB->db_query($sql);
        while ($category = $babDB->db_fetch_assoc($categories)) {
            bab_deleteTopicCategory($category['id']);
        }
    }

    $forumId = workspace_getWorkspaceForumId($workspaceId);

    if (isset($forumId)) {
        $sql = 'SELECT id FROM ' . BAB_FORUMS_TBL . ' WHERE id_dgowner=' . $babDB->quote($workspaceId);
        $forums = $babDB->db_query($sql);
        while ($forum = $babDB->db_fetch_assoc($forums)) {
            bab_deleteForum($forum['id']);
        }
    }

    $folderId = workspace_getWorkspaceRootFolderId($workspaceId);

    if (isset($folderId)) {
        $sql = 'SELECT id FROM ' . BAB_FM_FOLDERS_TBL . ' WHERE id_dgowner=' . $babDB->quote($workspaceId);
        $folders = $babDB->db_query($sql);
        while ($folder = $babDB->db_fetch_assoc($folders)) {
            bab_deleteFolder($folder['id']);
        }
    }

    $directoryId = workspace_getWorkspaceDirectoryId($workspaceId);

    if (isset($directoryId)) {
        $sql = 'SELECT id FROM ' . BAB_DB_DIRECTORIES_TBL . ' WHERE id_dgowner=' . $babDB->quote($workspaceId);
        $directories = $babDB->db_query($sql);
        while ($directory = $babDB->db_fetch_assoc($directories)) {
            bab_deleteDbDirectory($directory['id']);
        }
    }

    $calendarId = workspace_getWorkspaceCalendarId($workspaceId);

    if (isset($calendarId)) {
        $sql = 'SELECT crt.id, ct.id AS idcal
                FROM ' . BAB_CAL_PUBLIC_TBL . ' crt
                LEFT JOIN ' . BAB_CALENDAR_TBL . ' ct ON ct.owner=crt.id AND ct.type=' . BAB_CAL_PUB_TYPE . '
                WHERE crt.id_dgowner=' . $babDB->quote($workspaceId);
        $publicCalendars = $babDB->db_query($sql);
        while ($publicCalendar = $babDB->db_fetch_assoc($publicCalendars)) {
            bab_deleteCalendar($publicCalendar['idcal']);
            $babDB->db_query('DELETE FROM ' . BAB_CAL_PUBLIC_TBL . ' WHERE id=' . $babDB->quote($publicCalendar['id']));
        }

        $sql = 'SELECT crt.id, ct.id AS idcal
                FROM ' . BAB_CAL_RESOURCES_TBL . ' crt
                LEFT JOIN ' . BAB_CALENDAR_TBL . ' ct ON ct.owner=crt.id AND ct.type=' . BAB_CAL_RES_TYPE . '
                WHERE crt.id_dgowner=' . $babDB->quote($workspaceId);
        $resourceCalendars = $babDB->db_query($sql);
        while ($resourceCalendar = $babDB->db_fetch_assoc($resourceCalendars)) {
            bab_deleteCalendar($resourceCalendar['idcal']);
            $babDB->db_query('DELETE FROM ' . BAB_CAL_RESOURCES_TBL . ' WHERE id=' . $babDB->quote($resourceCalendar['id']));
        }
    }

    $babDB->db_query('DELETE from ' . BAB_DG_ADMIN_TBL . ' WHERE id_dg=' . $babDB->quote($workspaceId));
    $babDB->db_query('DELETE from ' . BAB_DG_GROUPS_TBL . ' WHERE id=' . $babDB->quote($workspaceId));
    $babDB->db_query('DELETE from ' . BAB_DG_ACL_GROUPS_TBL . ' WHERE id=' . $babDB->quote($workspaceId));

    return true;
}


/**
 * Creates all the components of a workspace, with the specified name and in the
 * specified delegation.
 *
 * @param string	$workspaceName
 * @param int		$delegationId
 * @param int		$userId
 * @param int		$readersGroupId
 * @param int		$writersGroupId
 * @param int		$administratorsGroupId
 * @param string	$skin				name of the skin associated to the workspace
 * @param string	$skin_styles		name of the stylesheet of the skin associated to the workspace
 * @param $num_article
 * @param $num_forum
 * @param $num_agenda
 * @param $num_file
 * @param $other_them
 * @param $file_size
 * @param $comment
 * @param $forum_notif
 * @param $update_directory
 * @param $versioning
 * @param $notifyCalendar
 *
 */
function workspace_createWorkspaceComponents($workspaceName, $delegationId, $userId, $readersGroupId, $writersGroupId, $administratorsGroupId,  $skin, $skin_styles, $num_article, $num_forum, $num_agenda, $num_file, $other_them, $file_size, $comment, $forum_notif, $update_directory, $versioning, $notifyCalendar)
{
    global $babBody;

    require_once $GLOBALS['babInstallPath'] . 'utilit/delegincl.php';

    // Store info in the registry.
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/workspace/workspaces/' . $delegationId);
    $registry->setKeyValue('name', $workspaceName);


    // We temporarily force the adm group.
    $currentAdmGroup = bab_getCurrentAdmGroup();

    $delegation = bab_getInstance('bab_currentDelegation');
    /*@var $delegation bab_currentDelegation */
    $delegation->set($delegationId);

    $description = '';

    // Create an article topic category.
    $categoryId = workspace_createArticleCategory($workspaceName, $description, $delegationId, $readersGroupId, $writersGroupId, $administratorsGroupId);

    $registry->changeDirectory('/workspace/workspaces/' . $delegationId);
    $registry->setKeyValue('categoryId', $categoryId);


    // Create an article topic.
    $topicId = workspace_createArticleTopic($workspaceName, $description, $categoryId, $readersGroupId, $writersGroupId, $administratorsGroupId, $comment);

    $registry->changeDirectory('/workspace/workspaces/' . $delegationId);
    $registry->setKeyValue('topicId', $topicId);


    // Create a directory.
    $directoryId = workspace_createDirectory($workspaceName, $delegationId, $readersGroupId, $writersGroupId, $administratorsGroupId);

    $registry->changeDirectory('/workspace/workspaces/' . $delegationId);
    $registry->setKeyValue('directoryId', $directoryId);


    // Create a forum.
    $forumId = workspace_createForum($workspaceName, $description, $delegationId, $readersGroupId, $writersGroupId, $administratorsGroupId, $forum_notif);

    $registry->changeDirectory('/workspace/workspaces/' . $delegationId);
    $registry->setKeyValue('forumId', $forumId);


    // Create a calendar.
    $calendarId = workspace_createCalendar($workspaceName, $description, $delegationId, $readersGroupId, $writersGroupId, $administratorsGroupId, $notifyCalendar);

    $registry->changeDirectory('/workspace/workspaces/' . $delegationId);
    $registry->setKeyValue('calendarId', $calendarId);


    // Create a root folder in ovidentia file system.
    $rootFolder = workspace_createRootFolder($workspaceName, $delegationId, $readersGroupId, $writersGroupId, $administratorsGroupId, true, 0, false, $versioning);

    $rootFolderId = $rootFolder->getId();

    $registry->changeDirectory('/workspace/workspaces/' . $delegationId);
    $registry->setKeyValue('rootFolderId', $rootFolderId);

    workspace_setWorkspaceFileVersioning($versioning);

    // We restore the adm group.
    $delegation->set($currentAdmGroup);

    $registry->changeDirectory('/workspace/workspaces/' . $delegationId);
    $registry->setKeyValue('created', date('Y-m-d H:i:s'));

    // Define skin associated to the workspace
    if ($skin !== null && $skin !== '') {
        $registry->setKeyValue('skin', $skin);
    } else {
        $registry->setKeyValue('skin', 'workspace');
    }
    if ($skin_styles !== null && $skin_styles !== '') {
        $registry->setKeyValue('skin_styles', $skin_styles);
    } else {
        $registry->setKeyValue('skin_styles', 'blue.css');
    }

    if ($num_article !== null && $num_article !== '') {
        $registry->setKeyValue('num_article', $num_article);
    } else {
        $registry->setKeyValue('num_article', '3');
    }
    if ($num_forum !== null && $num_forum !== '') {
        $registry->setKeyValue('num_forum', $num_forum);
    } else {
        $registry->setKeyValue('num_forum', '3');
    }
    if ($num_agenda !== null && $num_agenda !== '') {
        $registry->setKeyValue('num_agenda', $num_agenda);
    } else {
        $registry->setKeyValue('num_agenda', '3');
    }
    if ($num_file !== null && $num_file !== '') {
        $registry->setKeyValue('num_file', $num_file);
    } else {
        $registry->setKeyValue('num_file', '3');
    }
    if ($other_them !== null && $other_them !== '') {
        $registry->setKeyValue('other_them', $other_them);
    } else {
        $registry->setKeyValue('other_them', false);
    }
    if ($file_size !== null && $file_size !== '') {
        if ($file_size > floor($GLOBALS['babMaxTotalSize'] / 1048576)) {
            $file_size = floor($GLOBALS['babMaxTotalSize'] / 1048576);
        }
        if ($file_size > floor($GLOBALS['babMaxGroupSize'] / 1048576)) {
            $file_size = floor($GLOBALS['babMaxGroupSize'] / 1048576);
        }
        $registry->setKeyValue('file_size', $file_size);
    } else {
        $registry->setKeyValue('file_size', 0);
    }
    if ($update_directory !== null && $update_directory !== '') {
        $registry->setKeyValue('update_directory', $update_directory);
    } else {
        $registry->setKeyValue('update_directory', false);
    }

}


/**
 * Creates a workspace and the associated elements.
 *
 * - Group
 * - Delegation
 * - Topic category
 * - Article topic
 * - Directory
 * - Forum
 * - Root folder
 * - Calendar
 *
 * @param string	$workspaceName
 * @param int		$userId			The future workspace admin (current user if not specified).
 * @param int		$groupId		The id of user group associated to the workspace (the group will be created if not specified).
 * @param string	$skin			Name of the skin associated to the workspace
 * @param string	$skin_styles	Name of the stylesheets associated to the skin of the workspace
 * @param int		$num_article
 * @param int		$num_forum
 * @param int		$num_agenda
 * @param int		$num_file
 * @param int       $other_them
 * @param int       $file_size
 * @param int       $comment
 * @param int       $forum_notif
 * @param int       $update_directory
 * @param int       $versioning
 * @param int       $notifyCalendar
 * @param bool      $allowUserCreation
 *
 * @return bool		true on success.
 */
function workspace_createWorkspace($workspaceName, $userId = null, $groupId = null, $skin = null, $skin_styles = null, $num_article = null, $num_forum = null, $num_agenda = null, $num_file = null, $other_them = null, $file_size = null, $comment = null, $forum_notif = null, $update_directory = null, $versioning = null, $notifyCalendar = null, $category = 0, $allowUserCreation = null)
{
    global $babBody;

    if (empty($userId)) {
        $userId = $GLOBALS['BAB_SESS_USERID'];
    }

    if (empty($groupId)) {

        // If the group is not specified, we create one with the same name as the workspace.

        if (workspace_groupNameExist($workspaceName)) {
            throw new Exception('Group name already exists');
        }

        $parentGroup = workspace_getWorkspacesParentGroup();
        $groupId = bab_createGroup($workspaceName, '', 0, $parentGroup);

    } else {

        if (!bab_isGroup($groupId)) {
            throw new Exception('Invalid group id');
        }
    }

    //  The readers group is groupId
    //	$readersGroupId = bab_createGroup('readers', '', 0, $groupId); //

    // We try to create the 'writers' and 'administrators' sub-group if they do not already exist.
    $writersGroup = bab_groupIsChildOf($groupId, 'writers');
    if ($writersGroup === false) {
        $writersGroupId = bab_createGroup('writers', '', 0, $groupId);
    } else {
        $writersGroupId = $writersGroup['id'];
    }

    $administratorsGroup = bab_groupIsChildOf($groupId, 'administrators');
    if ($administratorsGroup === false) {
        $administratorsGroupId = bab_createGroup('administrators', '', 0, $groupId);
    } else {
        $administratorsGroupId = $administratorsGroup['id'];
    }

    // We ensure that the workspace administrator is member of the administered group.
    bab_addUserToGroup($userId, $groupId);
    bab_addUserToGroup($userId, $administratorsGroupId);

    $delegationCategory = $category;

    // Create the delegation.
    $delegationId = workspace_createDelegation(
        $workspaceName, $groupId, '', '', true,
        array('articles', 'forums', 'calendars', 'directories', 'filemanager'),
        $delegationCategory
    );

    // Sets the specified user as delegation administrator.
    workspace_addDelegationAdmin($delegationId, $userId);


    try {
        // We try to create all the workspace components.
        workspace_createWorkspaceComponents($workspaceName, $delegationId, $userId, $groupId, $writersGroupId, $administratorsGroupId, $skin, $skin_styles, $num_article, $num_forum, $num_agenda, $num_file, $other_them, $file_size, $comment, $forum_notif, $update_directory, $versioning, $notifyCalendar);

        // Once the directory is created we create the administrator entry in it.
        $directoryId = workspace_getWorkspaceDirectoryId();

        $entry = workspace_getDirectoryEntryByUserId($userId);

        if ($entry['photo'] instanceof bab_dirEntryPhoto) {
            $photoData = $entry['photo']->getData();
        } else {
            $photoData = '';
        }


        workspace_addDbContact($directoryId, $entry, $photoData, $userId);

    } catch (Exception $exception) {
        // If anything went wrong, we remove completely the workspace.
        $babBody->addError($exception->getMessage());
        workspace_clearWorkspace($delegationId);
        return false;
    }

    workspace_setWorkspaceAllowUserCreation($allowUserCreation, $delegationId);

    workspace_setPortletContainerConfiguration($delegationId);

    return $delegationId;
}



/**
 * Creates a new delegation
 *
 * @param string	$name			The delegation name.
 * @param int		$group			The group id associated to (administered by) the delegation.
 * @param string	$description	The delegation description.
 * @param string	$color			The color (hex value eg. 'CC6633').
 * @param bool		$attach			Can attach a user to the delegation group.
 * @param array		$delegitems		The list of items included in the delegation ('users', 'groups', 'sections', 'articles', 'faqs', 'forums', 'calendars', 'mails', 'directories', 'approbations', 'filemanager', 'orgchart', 'taskmanager')
 * @param int		$categoryId		The delegation category.
 *
 * @return int		The new delegation id or false on error.
 */
function workspace_createDelegation($name, $group, $description = '', $color = 'FFFFFF', $attach = false, $delegitems = array(), $categoryId = 0)
{
    global $babDB;

    $res = $babDB->db_query('SELECT * FROM ' . BAB_DG_GROUPS_TBL . ' WHERE name=' . $babDB->quote($name));
    if ($babDB->db_num_rows($res) > 0) {
        throw new Exception('The delegation name already exists.');
    }

    $attach = ($attach ? 'Y' : 'N');

    $req1 = '(name, description, color, battach';
    $req2 = '(' .$babDB->quote($name). ', ' . $babDB->quote($description). ', ' . $babDB->quote($color) . ', ' . $babDB->quote($attach);
    for ($i = 0; $i < count($delegitems); $i++) {
        $req1 .= ', '. $babDB->db_escape_string($delegitems[$i]);
        $req2 .= ", 'Y'";
    }

    $req1 .= ',iIdCategory ';
    $req2 .= ', ' . $babDB->quote($categoryId);

    $req1 .= ',id_group )';
    $req2 .= ', ' . $group . ' )';
    $babDB->db_query('INSERT INTO ' . BAB_DG_GROUPS_TBL . ' ' . $req1 . ' VALUES ' . $req2);
    $delegationId = $babDB->db_insert_id();

    return $delegationId;
}


/**
 * Creates a topic category owned by $delegationId, and sets all access rights to users of group $groupId.
 *
 * @param string	$name
 * @param string	$description
 * @param int		$delegationId
 * @param int		$readersGroupId
 * @param int		$writersGroupId
 * @param int		$administratorsGroupId
 *
 * @return int	The new category id.
 */
function workspace_createArticleCategory($name, $description, $delegationId, $readersGroupId, $writersGroupId, $administratorsGroupId)
{
    require_once $GLOBALS['babInstallPath'] . 'utilit/artapi.php';
    require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';

    $categoryId = bab_addTopicsCategory($name, $description, 'N', '', '', 0, $delegationId);

    if ($categoryId === false) {
        throw new Exception('The article category could not be created.');
    }

    $readersRightsTables = array(
        BAB_DEF_TOPCATCOM_GROUPS_TBL,
        BAB_DEF_TOPCATVIEW_GROUPS_TBL
    );
    $writersRightsTables = array(
        BAB_DEF_TOPCATCOM_GROUPS_TBL,
        BAB_DEF_TOPCATMOD_GROUPS_TBL,
        BAB_DEF_TOPCATSUB_GROUPS_TBL,
        BAB_DEF_TOPCATVIEW_GROUPS_TBL
    );
    $administratorsRightsTables = array(
        BAB_DEF_TOPCATCOM_GROUPS_TBL,
        BAB_DEF_TOPCATMAN_GROUPS_TBL,
        BAB_DEF_TOPCATMOD_GROUPS_TBL,
        BAB_DEF_TOPCATSUB_GROUPS_TBL,
        BAB_DEF_TOPCATVIEW_GROUPS_TBL
    );

    foreach ($readersRightsTables as $rightsTable) {
        aclAdd($rightsTable, $readersGroupId, $categoryId);
    }
    foreach ($writersRightsTables as $rightsTable) {
        aclAdd($rightsTable, $writersGroupId, $categoryId);
    }
    foreach ($administratorsRightsTables as $rightsTable) {
        aclAdd($rightsTable, $administratorsGroupId, $categoryId);
    }

    return $categoryId;
}



/**
 * Creates a topic under the category $categoryId, and sets all access rights to users of group $groupId.
 *
 * @param string	$name
 * @param string	$description
 * @param int		$categoryId
 * @param int		$readersGroupId
 * @param int		$writersGroupId
 * @param int		$administratorsGroupId
 *
 * @return int	The new topic id.
 */
function workspace_createArticleTopic($name, $description, $categoryId, $readersGroupId, $writersGroupId, $administratorsGroupId, $comment)
{
    require_once $GLOBALS['babInstallPath'] . 'utilit/artapi.php';
    require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';

    $topicId = bab_addTopic($name, $description, $categoryId, $error);
    if ($topicId == 0) {
        throw new Exception('The article topic could not be created.');
    }

    $readersRightsTables = array(
        BAB_TOPICSVIEW_GROUPS_TBL
    );
    $writersRightsTables = array(
        BAB_TOPICSMOD_GROUPS_TBL,
        BAB_TOPICSSUB_GROUPS_TBL,
        BAB_TOPICSVIEW_GROUPS_TBL
    );
    $administratorsRightsTables = array(
        BAB_TOPICSMAN_GROUPS_TBL,
        BAB_TOPICSMOD_GROUPS_TBL,
        BAB_TOPICSSUB_GROUPS_TBL,
        BAB_TOPICSVIEW_GROUPS_TBL
    );


    aclRemove(BAB_TOPICSCOM_GROUPS_TBL, $readersGroupId, $topicId);
    aclRemove(BAB_TOPICSCOM_GROUPS_TBL, $writersGroupId, $topicId);
    aclRemove(BAB_TOPICSCOM_GROUPS_TBL, $administratorsGroupId, $topicId);
    if( $comment ){
        aclAdd(BAB_TOPICSCOM_GROUPS_TBL, $readersGroupId + BAB_ACL_GROUP_TREE, $topicId);
    }

    foreach ($readersRightsTables as $rightsTable) {
        aclAdd($rightsTable, $readersGroupId, $topicId);
    }
    foreach ($writersRightsTables as $rightsTable) {
        aclAdd($rightsTable, $writersGroupId, $topicId);
    }
    foreach ($administratorsRightsTables as $rightsTable) {
        aclAdd($rightsTable, $administratorsGroupId, $topicId);
    }

    return $topicId;
}



/**
 * Adds the specified user to the list of the delegation administrators.
 *
 * @param int	$delegationId			The id of the delegation to add a new administrator.
 * @param int	$userId					The new administrator user id.
 *
 * @return bool		true on success.
 */
function workspace_addDelegationAdmin($delegationId, $userId)
{
    global $babDB;

    $res = $babDB->db_query('SELECT COUNT(*)
                                FROM ' . BAB_DG_ADMIN_TBL . '
                                WHERE id_dg=' . $babDB->quote($delegationId) . '
                                    AND id_user=' . $babDB->quote($userId));
    list($n) = $babDB->db_fetch_array($res);
    if ($n > 0) {
        //The user is already in the list.
        return false;
    }
    $babDB->db_query('INSERT INTO
                        ' . BAB_DG_ADMIN_TBL . ' (id_dg, id_user)
                        VALUES (' . $babDB->quote($delegationId) . ',' . $babDB->quote($userId) . ')');
    return true;
}




/**
 * Removes the specified user from the list of the specified delegation administrators.
 *
 * @param int	$delegationId
 * @param int	$userId
 *
 * @return bool		true on success.
 */
function workspace_removeDelegationAdmin($delegationId, $userId)
{
    global $babDB;

    $res = $babDB->db_query('SELECT COUNT(*)
                                FROM ' . BAB_DG_ADMIN_TBL . '
                                WHERE id_dg=' . $babDB->quote($delegationId) . '
                                    AND id_user=' . $babDB->quote($userId));
    list($n) = $babDB->db_fetch_array($res);
    if ($n <= 0) {
        //The user is not in the list.
        return false;
    }
    $babDB->db_query('DELETE FROM ' . BAB_DG_ADMIN_TBL . '
                            WHERE id_dg=' . $babDB->quote($delegationId) . '
                                AND id_user=' . $babDB->quote($userId));
    return true;
}



/**
 * Creates a new (database) directory in the current delegation.
 *
 * @param string	$name					The directory name.
 * @param int		$delegationId
 * @param int		$readersGroupId
 * @param int		$writersGroupId
 * @param int		$administratorsGroupId
 * @param string	$description			The directory description.
 * @param bool		$displayiu				Display date and author of update in directory entry.
 * @param array		$defaultFieldValues		Array indexed by field name and containing field default values.
 * @param array		$rw						Array containing ids of modifiable fields.
 * @param array		$rq						Array containing ids of required fields.
 * @param array		$ml						Array containing ids of multiline fields.
 * @param array		$dz						Array containing ids of disabled fields.
 * @return int		The new directory id of false on error.
 */
function workspace_createDirectory($name, $delegationId, $readersGroupId, $writersGroupId, $administratorsGroupId, $description = '', $displayiu = false, $defaultFieldValues = array(), $rw = array(), $rq = array(), $ml = array(), $dz = array())
{
    global $babDB, $babBody;

    if (empty($name)) {
        throw new Exception('The directory name is empty.');
    }

    $res = $babDB->db_query('SELECT name FROM ' . BAB_DB_DIRECTORIES_TBL . ' WHERE name=' . $babDB->quote($name));
    if ($res && $babDB->db_num_rows($res) > 0) {
        throw new Exception('A directory with this name already exists.');
    }

    $req = 'INSERT INTO ' . BAB_DB_DIRECTORIES_TBL . ' (name, description, show_update_info, id_dgowner)
                VALUES (' .$babDB->quote($name). ', ' . $babDB->quote($description). ', ' .$babDB->quote($displayiu ? 'Y' : 'N'). ', ' .$babDB->quote($delegationId). ')';
    $babDB->db_query($req);
    $directoryId = $babDB->db_insert_id();

    $res = $babDB->db_query('SELECT * FROM ' . BAB_DBDIR_FIELDS_TBL);
    $k = 0;
    while ($arr = $babDB->db_fetch_array($res)) {
        if( count($rw) > 0 && in_array($arr['id'], $rw))
            $modifiable = 'Y';
        else
            $modifiable = 'N';
        if( count($rq) > 0 && in_array($arr['id'], $rq))
            $required = 'Y';
        else
            $required = 'N';
        if( count($ml) > 0 && in_array($arr['id'], $ml))
            $multiline = 'Y';
        else
            $multiline = 'N';
        if( count($dz) > 0 && in_array($arr['id'], $dz))
            $disabled = 'Y';
        else
            $disabled = 'N';

        switch ($arr['name']) {
            case 'givenname':
                $ordering = 1; break;
            case 'sn':
                $ordering = 2; break;
            default:
                $ordering = 0; break;
        }

        $req = 'INSERT INTO ' . BAB_DBDIR_FIELDSEXTRA_TBL . ' (id_directory, id_field, default_value, modifiable, required, multilignes, disabled, ordering, list_ordering) VALUES (' .$babDB->quote($directoryId). ', ' . $babDB->quote($arr['id']). ', 0, ' . $babDB->quote($modifiable). ', ' . $babDB->quote($required). ', ' . $babDB->quote($multiline). ', ' . $babDB->quote($disabled) . ', ' . $babDB->quote($ordering) . ', ' . ($k++) .')';
        $babDB->db_query($req);
        $fxid = $babDB->db_insert_id();
        if (!empty($fields)) {
            $fieldval = trim($fields[$arr['name']]);
            if( !empty($fieldval)) {
                $babDB->db_query('INSERT INTO ' . BAB_DBDIR_FIELDSVALUES_TBL . ' (id_fieldextra, field_value) VALUES (' .$babDB->quote($fxid). ', ' . $babDB->quote($fieldval) . ')');
                $fvid = $babDB->db_insert_id();
                $babDB->db_query('UPDATE ' . BAB_DBDIR_FIELDSEXTRA_TBL . ' SET default_value=' . $babDB->quote($fvid) . ' WHERE id =' . $babDB->quote($fxid));
            }
        }
    }

    $readersRightsTables = array(
        BAB_DBDIRVIEW_GROUPS_TBL,
        BAB_DBDIREXPORT_GROUPS_TBL
    );
    $writersRightsTables = array(
        BAB_DBDIRVIEW_GROUPS_TBL,
        BAB_DBDIREXPORT_GROUPS_TBL
    );
    $administratorsRightsTables = array(
        BAB_DBDIRVIEW_GROUPS_TBL,
        BAB_DBDIRADD_GROUPS_TBL,
        BAB_DBDIRUPDATE_GROUPS_TBL,
        BAB_DBDIRDEL_GROUPS_TBL,
        BAB_DBDIREXPORT_GROUPS_TBL,
        BAB_DBDIRIMPORT_GROUPS_TBL,
        BAB_DBDIRBIND_GROUPS_TBL,
        BAB_DBDIRUNBIND_GROUPS_TBL,
        BAB_DBDIREMPTY_GROUPS_TBL
    );

    foreach ($readersRightsTables as $rightsTable) {
        aclAdd($rightsTable, $readersGroupId, $directoryId);
    }
    foreach ($writersRightsTables as $rightsTable) {
        aclAdd($rightsTable, $writersGroupId, $directoryId);
    }
    foreach ($administratorsRightsTables as $rightsTable) {
        aclAdd($rightsTable, $administratorsGroupId, $directoryId);
    }

    return $directoryId;
}




/**
 * Creates a root level folder.
 *
 * @param string	$name			The folder name
 * @param int		$delegationId
 * @param int		$readersGroupId
 * @param int		$writersGroupId
 * @param int		$administratorsGroupId
 * @param bool		$active			Whether the folder is visible in the file manager (default true).
 * @param int		$said			Approbation id (default 0: none)
 * @param bool		$notification
 * @param bool		$version		Whether versioning is activated on this folder content.
 * @param bool		$visible
 * @param bool		$bautoapp
 * @param bool		$baddtags
 *
 * @return BAB_FmFolder		The new folder.
 */
function workspace_createRootFolder($name, $delegationId, $readersGroupId, $writersGroupId, $administratorsGroupId, $active = true, $said = 0, $notification = false, $versioning = false, $visible = true, $bautoapp = false, $baddtags = false)
{
    global $babDB;

    if (empty($name)) {
        throw new Exception('The folder name is empty.');
    }

    include_once $GLOBALS['babInstallPath'] . 'utilit/fileincl.php';
    include_once $GLOBALS['babInstallPath'] . 'utilit/delegincl.php';

    bab_setCurrentUserDelegation($delegationId);
    $oFileManagerEnv = getEnvObject();
    if (!$oFileManagerEnv->pathValid()) {
        throw new Exception('The folder path is not valid.');
    }

    $oFmFolderSet	= new BAB_FmFolderSet();
    $oName			= $oFmFolderSet->aField['sName'];
    $oIdDgOwner		= $oFmFolderSet->aField['iIdDgOwner'];

    $sName = replaceInvalidFolderNameChar($name);

    if (!isStringSupportedByFileSystem($sName)) {
        throw new Exception('The folder name "' . $sName .  '" contains characters not supported by the file system.');
    }

    $oCriteria = $oName->in($sName);
    $oCriteria = $oCriteria->_and($oIdDgOwner->in($delegationId));
    $oFmFolder = $oFmFolderSet->get($oCriteria);
    if (!is_null($oFmFolder)) {
        throw new Exception('The folder already exists.');
    }

    $sFullPathName = BAB_FileManagerEnv::getCollectivePath($delegationId) . $sName;

    if (!BAB_FmFolderHelper::createDirectory($sFullPathName)) {
        throw new Exception('Could not create folder');
    }

    $oFmFolder = new BAB_FmFolder();
    $oFmFolder->setApprobationSchemeId($said);
    $oFmFolder->setDelegationOwnerId($delegationId);
    $oFmFolder->setName($sName);
    $oFmFolder->setRelativePath('');
    $oFmFolder->setFileNotify($notification);
    $oFmFolder->setActive($active);
    $oFmFolder->setAddTags($baddtags);
    $oFmFolder->setVersioning($versioning ? 'Y' : 'N');
    $oFmFolder->setHide($visible);
    $oFmFolder->setAutoApprobation($bautoapp);

    $oFmFolder->save();

    $folderId = $oFmFolder->getId();

    $readersRightsTables = array(
        BAB_FMDOWNLOAD_GROUPS_TBL
    );
    $writersRightsTables = array(
        BAB_FMUPLOAD_GROUPS_TBL,
        BAB_FMDOWNLOAD_GROUPS_TBL,
        BAB_FMUPDATE_GROUPS_TBL
    );
    $administratorsRightsTables = array(
        BAB_FMUPLOAD_GROUPS_TBL,
        BAB_FMDOWNLOAD_GROUPS_TBL,
        BAB_FMUPDATE_GROUPS_TBL,
        BAB_FMMANAGERS_GROUPS_TBL
    );

    foreach ($readersRightsTables as $rightsTable) {
        aclAdd($rightsTable, $readersGroupId, $folderId);
    }
    foreach ($writersRightsTables as $rightsTable) {
        aclAdd($rightsTable, $writersGroupId, $folderId);
    }
    foreach ($administratorsRightsTables as $rightsTable) {
        aclAdd($rightsTable, $administratorsGroupId, $folderId);
    }

    return $oFmFolder;
}





/**
 * Creates a forum and sets all access rights to users of group $groupId.
 *
 * @param string	$name
 * @param string	$description
 * @param int		$delegationId
 * @param int		$readersGroupId
 * @param int		$writersGroupId
 * @param int		$administratorsGroupId
 *
 * @return int
 */
function workspace_createForum($name, $description, $delegationId, $readersGroupId, $writersGroupId, $administratorsGroupId, $forum_notif)
{
    global $babDB;

    $sql = 'SELECT * FROM '.BAB_FORUMS_TBL.' WHERE name='.$babDB->quote($name);
    $res = $babDB->db_query($sql);

    if ($babDB->db_num_rows($res) > 0) {
        throw new Exception('Forum name already exists');
    }

    $res = $babDB->db_query('SELECT MAX(ordering) FROM '.BAB_FORUMS_TBL);
    if ($res) {
        $arr = $babDB->db_fetch_array($res);
        $max = $arr[0] + 1;
    } else {
        $max = 0;
    }
    $moderation = 'N';
    $notification = 'N';
    $nbmsgdisplay = 20;
    $nbrecipients = 30;
    $active = 'Y';
    $bdisplayemailaddress = 'N';
    $bdisplayauhtordetails = 'N';
    $bflatview = 'Y';
    $bupdatemoderator = 'Y';
    $bupdateauthor = 'N';

    $sql = 'INSERT INTO '.BAB_FORUMS_TBL.' (name, description, display, moderation, notification, active, ordering, id_dgowner, bdisplayemailaddress, bdisplayauhtordetails, bflatview, bupdatemoderator, bupdateauthor)';

    $sql .= ' VALUES (
        ' . $babDB->quote($name). ',
        ' . $babDB->quote($description). ',
        ' . $babDB->quote($nbmsgdisplay). ',
        ' . $babDB->quote($moderation). ',
        ' . $babDB->quote($notification). ',
        ' . $babDB->quote($active). ',
        ' . $babDB->quote($max). ',
        ' . $babDB->quote($delegationId). ',
        ' . $babDB->quote($bdisplayemailaddress). ',
        ' . $babDB->quote($bdisplayauhtordetails). ',
        ' . $babDB->quote($bflatview). ',
        ' . $babDB->quote($bupdatemoderator). ',
        ' . $babDB->quote($bupdateauthor). '
    )';

    $babDB->db_query($sql);
    $forumId = $babDB->db_insert_id();

    $readersRightsTables = array(
        BAB_FORUMSVIEW_GROUPS_TBL
    );
    $writersRightsTables = array(
        BAB_FORUMSPOST_GROUPS_TBL,
        BAB_FORUMSREPLY_GROUPS_TBL,
        BAB_FORUMSVIEW_GROUPS_TBL,
        BAB_FORUMSFILES_GROUPS_TBL
    );

    $administratorsRightsTables = array(
        BAB_FORUMSMAN_GROUPS_TBL,
        BAB_FORUMSPOST_GROUPS_TBL,
        BAB_FORUMSREPLY_GROUPS_TBL,
        BAB_FORUMSVIEW_GROUPS_TBL,
        BAB_FORUMSFILES_GROUPS_TBL
    );

    if($forum_notif){
        aclAdd(BAB_FORUMSNOTIFY_GROUPS_TBL, $readersGroupId + BAB_ACL_GROUP_TREE, $forumId);
    }

    foreach ($readersRightsTables as $rightsTable) {
        aclAdd($rightsTable, $readersGroupId, $forumId);
    }
    foreach ($writersRightsTables as $rightsTable) {
        aclAdd($rightsTable, $writersGroupId, $forumId);
    }
    foreach ($administratorsRightsTables as $rightsTable) {
        aclAdd($rightsTable, $administratorsGroupId, $forumId);
    }

    return $forumId;
}



/**
 * Creates a public calendar and sets all access rights to users of groups.
 *
 * @param string	$name
 * @param string	$description
 * @param int		$delegationId
 * @param int		$readersGroupId
 * @param int		$writersGroupId
 * @param int		$administratorsGroupId
 *
 * @return int
 */
function workspace_createCalendar($name, $description, $delegationId, $readersGroupId, $writersGroupId, $administratorsGroupId, $notifyCalendar)
{
    global $babDB;

    $sql = 'SELECT * FROM '.BAB_CAL_PUBLIC_TBL.' WHERE name='.$babDB->quote($name);
    $res = $babDB->db_query($sql);

    if ($babDB->db_num_rows($res) > 0) {
        throw new Exception('Calendar name already exists');
    }

    $sql = 'INSERT INTO '.BAB_CAL_PUBLIC_TBL.' (name, description, id_dgowner, idsa) VALUES (' .$babDB->quote($name). ', '.$babDB->quote($description).', '.$babDB->quote($delegationId).', '.$babDB->quote(0).')';
    $babDB->db_query($sql);

    $ownerId = $babDB->db_insert_id();
    $sql = 'INSERT INTO '.BAB_CALENDAR_TBL.' (owner, type) VALUES (' .$babDB->quote($ownerId).', '.BAB_CAL_PUB_TYPE.')';
    $babDB->db_query($sql);
    $calendarId = $babDB->db_insert_id();

    $readersRightsTables = array(
        BAB_CAL_PUB_VIEW_GROUPS_TBL,
        BAB_CAL_PUB_NOT_GROUPS_TBL
    );
    $writersRightsTables = array(
        BAB_CAL_PUB_MAN_GROUPS_TBL,
        BAB_CAL_PUB_VIEW_GROUPS_TBL,
        BAB_CAL_PUB_NOT_GROUPS_TBL
    );
    $administratorsRightsTables = array(
        BAB_CAL_PUB_MAN_GROUPS_TBL,
        BAB_CAL_PUB_VIEW_GROUPS_TBL,
        BAB_CAL_PUB_NOT_GROUPS_TBL
    );

    foreach ($readersRightsTables as $rightsTable) {
        aclAdd($rightsTable, $readersGroupId, $calendarId);
    }
    foreach ($writersRightsTables as $rightsTable) {
        aclAdd($rightsTable, $writersGroupId, $calendarId);
    }
    foreach ($administratorsRightsTables as $rightsTable) {
        aclAdd($rightsTable, $administratorsGroupId, $calendarId);
    }

    workspace_setWorkspaceCalendarNotifications($notifyCalendar, $delegationId);

    return $calendarId;
}



/**
 * Sets the administrator profile for the specified user.
 *
 * @param int		$userId			The user to whom the profile will be associated
 * @param int		$workspaceId	The current workspace if not specified
 */
function workspace_setUserAdministrator($userId, $workspaceId = null)
{
    if (!isset($workspaceId)) {
        $workspaceId = workspace_getCurrentWorkspace();
    }
    $administratorsGroupId = workspace_getWorkspaceAdministratorsGroupId($workspaceId);
    $writersGroupId = workspace_getWorkspaceWritersGroupId($workspaceId);
//	$readersGroupId = workspace_getWorkspaceReadersGroupId($workspaceId);
    bab_addUserToGroup($userId, $administratorsGroupId);
    bab_removeUserFromGroup($userId, $writersGroupId);
//	bab_removeUserFromGroup($userId, $readersGroupId);

    // Sets the specified user as delegation administrator.
    workspace_addDelegationAdmin($workspaceId, $userId);
}


/**
 * Sets the writer profile for the specified user.
 *
 * @param int		$userId			The user to whom the profile will be associated
 * @param int		$workspaceId	The current workspace if not specified
 */
function workspace_setUserWriter($userId, $workspaceId = null)
{
    if (!isset($workspaceId)) {
        $workspaceId = workspace_getCurrentWorkspace();
    }
    $administratorsGroupId = workspace_getWorkspaceAdministratorsGroupId($workspaceId);
    $writersGroupId = workspace_getWorkspaceWritersGroupId($workspaceId);
//	$readersGroupId = workspace_getWorkspaceReadersGroupId($workspaceId);

    bab_removeUserFromGroup($userId, $administratorsGroupId);
    bab_addUserToGroup($userId, $writersGroupId);
//	bab_removeUserFromGroup($userId, $readersGroupId);

    // Removes the specified user from delegation administrators.
    workspace_removeDelegationAdmin($workspaceId, $userId);
}

/**
 * Sets the reader profile for the specified user.
 *
 * @param int		$userId			The user to whom the profile will be associated
 * @param int		$workspaceId	The current workspace if not specified
 */
function workspace_setUserReader($userId, $workspaceId = null)
{
    if (!isset($workspaceId)) {
        $workspaceId = workspace_getCurrentWorkspace();
    }
    $administratorsGroupId = workspace_getWorkspaceAdministratorsGroupId($workspaceId);
    $writersGroupId = workspace_getWorkspaceWritersGroupId($workspaceId);
//	$readersGroupId = workspace_getWorkspaceReadersGroupId($workspaceId);
    bab_removeUserFromGroup($userId, $administratorsGroupId);
    bab_removeUserFromGroup($userId, $writersGroupId);
//	bab_addUserToGroup($userId, $readersGroupId);

    // Removes the specified user from delegation administrators.
    workspace_removeDelegationAdmin($workspaceId, $userId);
}


/**
 * Sets the profile (administrator, writer or reader) for the specified user.
 *
 * @param int		$userId			The user to whom the profile will be associated
 * @param string	$profile		'administrator', 'writer' or 'reader'
 * @param int		$workspaceId	The current workspace if not specified
 */
function workspace_setUserProfile($userId, $profile, $workspaceId = null)
{
    switch ($profile) {
        case 'administrator':
            workspace_setUserAdministrator($userId, $workspaceId);
            break;
        case 'writer':
            workspace_setUserWriter($userId, $workspaceId);
            break;
        case 'reader':
            workspace_setUserReader($userId, $workspaceId);
            break;
    }
}
