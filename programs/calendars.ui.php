<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/workspacewidgets.php';



/**
 *
 * @param string	$recipients		A string of comma-separated email addresses.
 *
 * @param bool $iCreation		The form is displayed for creating a new event (true) or editing one (false).
 * @return Widget_Form
 */
function workspace_eventEditor($isCreation)
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/calapi.php';
	$W = bab_Widgets();

	$form = $W->Form();
	$frame = new workspace_BaseForm('event_editor');
	$frame->setName('event')
		->addClass('workspace-dialog');

	$titleFormItem = workspace_FormField(
	    workspace_translate('Title:'),
		$W->LineEdit()
			->setName('title')
			->setSize(80)
			->setMandatory(true, workspace_translate('The event title must not be empty'))
	);
	$categorieSelect = $W->Select()
		->setName('category')
		->addOption('','')
		->setId('category_selector');
	$categorieFormItem = workspace_FormField(
	    workspace_translate('Category:'),
		$categorieSelect
	);
	$locationFormItem = workspace_FormField(
	    workspace_translate('Location:'),
	    $W->LineEdit()
	        ->setName('location')
	        ->setSize(90)
	);
	$startdateFormItem = workspace_FormField(
	    workspace_translate('Begin date:'),
		$W->DatePicker()
			->setName('startdate')
			->setMandatory(true, workspace_translate('The event date must not be empty'))
	);
	$starttimeFormItem = workspace_FormField(
	    workspace_translate('Start time:'),
		$W->TimeEdit()
			->setName('starttime')
			->setMandatory(true, workspace_translate('The event start time must not be empty'))
	);
	$endtimeFormItem = workspace_FormField(
	    workspace_translate('End time:'),
		$W->TimeEdit()
			->setName('endtime')
			->setMandatory(true, workspace_translate('The event end time must not be empty'))
	);
	$enddateFormItem = workspace_FormField(
	    workspace_translate('End date:'),
		$W->DatePicker()
			->setName('enddate')
	);

	switch (workspace_getWorkspacesTextEditor()) {
	    case 'plain':
	        $bodyEditor = $W->TextEdit('bab_post_body');
	        break;
	    case 'wysiwyg':
	        $bodyEditor = $W->BabHtmlEdit('bab_post_body');
	        break;
	    case 'rich':
	    default:
	        $bodyEditor = $W->SimpleHtmlEdit('bab_post_body');
	        break;
	}

	$descriptionFormItem = workspace_FormField(
	    workspace_translate('Description:'),
		$bodyEditor
			->setName('description')
			->setLines(5)
			->setColumns(100)
	);

	$frame->addItem(
		$topBox = $W->HBoxItems(
			$titleFormItem,
			$categorieFormItem
		)->setHorizontalSpacing(1, 'em')
	);

	$categories = bab_calGetCategories();
	foreach($categories as $categorie){
		$categorieSelect->addOption($categorie['id'],$categorie['name']);
	}

	$personal = true;
	if ($_SESSION['workspace_personal_calendar_visible'] == false) {
		$personal = false;
	}
	$shared = true;
	if ($_SESSION['workspace_shared_calendar_visible']  == false) {
		$shared = false;
	}
	if ($personal && !$shared) {
		$caladendarSelectorValue = 'personal';
	} elseif (!$personal && $shared) {
		$caladendarSelectorValue = 'public';
	} else {
		$caladendarSelectorValue = 'public';
	}
	if ($isCreation) {
		$arrayType = array('' => '');
		if (workspace_getPersonalCalendarId() != null) {
			$arrayType['personal'] = workspace_translate('Personal');
		}
		if (workspace_getUserProfile($GLOBALS['BAB_SESS_USERID']) != 'reader') {
			$arrayType['public'] = workspace_translate('Public');
		}
		$calendarSelector = workspace_FormField(
		    workspace_translate('Calendar:'),
			$W->Select()
				->setMandatory(true,workspace_translate('The event calendar type must not be empty'))
				->setOptions($arrayType)
				->setName('calendar')
				->setId('calendar_selector')
				->setValue($caladendarSelectorValue)
		);
		$topBox->addItem($calendarSelector, 0);
	}
	$frame->addItem(
		$W->HBoxItems(
			$startdateFormItem,
			$starttimeFormItem,
			$endtimeFormItem,
			$enddateFormItem
		)->setHorizontalSpacing(1, 'em')
	);
	$frame->addItem($locationFormItem);
	$frame->addItem($descriptionFormItem);


    $notifyFormItem = workspace_FormField(
        workspace_translate('Notify workspace members'),
        $W->CheckBox()
            ->setCheckedValue('1')
            ->setUncheckedValue('0')
            ->setName('notify')
            ->setValue(workspace_getWorkspaceCalendarNotifications())
    );

    $frame->addItem($notifyFormItem);


	$frame->addButton(
	    $W->SubmitButton('send')
			->validate(true)
			->setLabel(workspace_translate("Save event"))
			->setAction(workspace_Controller()->Calendars()->saveEvent())
	)
    ->addButton(
        $W->SubmitButton('cancel')
    		->setLabel(workspace_translate("Cancel"))
	    	->setAction(workspace_Controller()->Calendars()->cancel())
	);

	if (!$isCreation) {
        $frame->addButton(
		    $W->SubmitButton('delete')
			    ->setLabel(workspace_translate("Delete"))
			    ->setAction(workspace_Controller()->Calendars()->deleteEvent())
		);
	}

	$form->setLayout($W->VBoxLayout())->addItem($frame);

	workspace_setSelfPageHiddenFields($form);

	return $form;
}
//function workspace_eventEditor($isCreation)
//{
//	$W = workspace_WidgetFactory();
//
//	$form = $W->Form();
//	$frame = new workspace_BaseForm('event_editor');
//	$frame->setName('event')
//			->addClass('workspace-dialog');
//
//
//
//	$titleFormItem = workspace_FormField(workspace_translate('Title:'),
//											$W->LineEdit()
//												->setName('title')
//												->setSize(100)
//												->setMandatory(true, workspace_translate("The event title must not be empty")));
//	$locationFormItem = workspace_FormField(workspace_translate('Location:'),
//											$W->LineEdit()
//												->setName('location')
//												->setSize(100));
//	$dateFormItem = workspace_FormField(workspace_translate('Date:'),
//											$W->DatePicker()
//												->setName('date')
//												->setMandatory(true, workspace_translate("The event date must not be empty")));
//	$starttimeFormItem = workspace_FormField(workspace_translate('Start time:'),
//											$W->TimeEdit()
//												->setName('starttime')
//												->setMandatory(true, workspace_translate("The event start time must not be empty")));
//	$endtimeFormItem = workspace_FormField(workspace_translate('End time:'),
//											$W->TimeEdit()
//												->setName('endtime')
//												->setMandatory(true, workspace_translate("The event start time must not be empty")));
//	$descriptionFormItem = workspace_FormField(workspace_translate('Description:'),
//											$W->TextEdit()
//												->setName('description')
//												->setLines(5)
//												->setColumns(100)
//											);
//
//	if ($isCreation) {
//		$calendarSelector = workspace_FormField(workspace_translate('Calendar:'),
//												$W->Select()
//													->setOptions(array(
//																	'personal' => workspace_translate('Personal'),
//																	'public' => workspace_translate('Public')))
//													->setName('calendar')
//													->setId('calendar_selector')
//												);
//		$frame->addItem($calendarSelector);
//	}
//	$frame->addItem($titleFormItem);
//	$frame->addItem($locationFormItem);
//	$frame->addItem(
//		$W->HBoxItems(
//			$dateFormItem,
//			$starttimeFormItem,
//			$endtimeFormItem
//		)->setHorizontalSpacing(1, 'em')
//	);
//	$frame->addItem($descriptionFormItem);
//
//
//	$frame->addButton($W->SubmitButton('send')
//							->validate(true)
//							->setLabel(workspace_translate("Save event"))
//							->setAction(workspace_Controller()->Calendars()->saveEvent())
//							)
//		  ->addButton($W->SubmitButton('cancel')
//							->setLabel(workspace_translate("Cancel"))
//							->setAction(workspace_Controller()->Calendars()->cancel())
//							)
//	;
//
//	if (!$isCreation) {
//		  $frame->addButton($W->SubmitButton('delete')
//							->setLabel(workspace_translate("Delete"))
//							->setAction(workspace_Controller()->Calendars()->deleteEvent())
//							)
//		;
//	}
//
//	$form->setLayout($W->VBoxLayout())->addItem($frame);
//
//	workspace_setSelfPageHiddenFields($form);
//
//	return $form;
//}




///**
// * @return Widget_SimpleTreeview
// */
//function workspace_CalendarTreeView()
//{
//	require_once dirname(__FILE__) . '/calendars.ctrl.php';
//
//	$W = workspace_WidgetFactory();
//
//	$treeview = $W->SimpleTreeview('workspace_calendar_treeview')
//					->addClass('workspace-side-panel')
//					->hideToolbar()
//					;
//
//	$allCalendars =& $treeview->createElement('all',
//												'calendar',
//												workspace_translate("Calendars"),
//												workspace_translate("Calendars"),
//												/*workspace_Controller()->Calendars()->displayEntries('all')->url()*/  '');
//	$allCalendars->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/folder.png');
//	$treeview->appendElement($allCalendars, null);
//
//	$sharedCalendar =& $treeview->createElement('shared',
//												'shared',
//												workspace_translate("Shared calendar"),
//												workspace_translate("Shared calendar"),
//												/*workspace_Controller()->Directories()->displayEntries('users')->url()*/ '');
//	$sharedCalendar->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/collective_folder.png');
//	$treeview->appendElement($sharedCalendar, 'all');
//
//	$personalCalendar =& $treeview->createElement('personal',
//												'personal',
//												workspace_translate("Personal calendar"),
//												workspace_translate("Personal calendar"),
//												/*workspace_Controller()->Directories()->displayEntries('users')->url()*/ '');
//	$personalCalendar->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/personal_folder.png');
//	$treeview->appendElement($personalCalendar, 'all');
//
//	return $treeview;
//}


/**
 * @return Widget_SimpleTreeview
 */
function workspace_CalendarTreeView()
{
	require_once dirname(__FILE__) . '/calendars.ctrl.php';
	require_once $GLOBALS['babInstallPath'] . 'utilit/icons.php';

	$W = bab_Widgets();

	$calendarList = $W->Frame('')
						->setLayout($W->VBoxLayout()->setVerticalSpacing(0.5, 'em'))
						->addClass('icon-left-24 icon-24x24 icon-left')
						->addClass('workspace-calendars')
						->addClass('workspace-side-panel');

	$sharedCalendar = $W->Link(
								$W->Icon(workspace_translate('Shared'), Func_Icons::APPS_GROUPS),
								 workspace_Controller()->Calendars()->toggleVisible('shared')
						  	)
						  		->addClass('workspace-calendar-shared');

	$personalCalendar = $W->Link(
								$W->Icon(workspace_translate('Personal'), Func_Icons::APPS_USERS),
								 workspace_Controller()->Calendars()->toggleVisible('personal')
								)->addClass('workspace-calendar-personal');


	if( workspace_getPersonalCalendarId() == null ){
		$personalCalendar = $W->label("");
	}

	if ($_SESSION['workspace_personal_calendar_visible'] == false) {
		$personalCalendar
			->setTitle(workspace_translate('Click to show personal events'))
			->addClass('workspace-calendar-hidden');
	} else {
		$personalCalendar
			->setTitle(workspace_translate('Click to hide personal events'));
	}
	if ($_SESSION['workspace_shared_calendar_visible'] == false) {
		$sharedCalendar
			->setTitle(workspace_translate('Click to show shared events'))
			->addClass('workspace-calendar-hidden');
	} else {
		$sharedCalendar
			->setTitle(workspace_translate('Click to hide shared events'));
	}

	$calendarList
		->addItem($W->VBoxItems(
					$W->Title(workspace_translate('Selected calendars'), 5),
					$sharedCalendar,
					$personalCalendar,
					$W->Label(workspace_translate('Click on a calendar label to toggle its visibility'))->addClass('workspace-form-description')
					)
					->setVerticalSpacing(0.5, 'em')
					->addClass('workspace-calendar-list')
			);
	return $calendarList;
}




function workspace_calendarToolbar()
{
	$W = bab_Widgets();

	$toolbar = workspace_Toolbar('main-toolbar');

	$monthViewIcon = $W->Icon(workspace_translate('Month view'), Func_Icons::ACTIONS_VIEW_CALENDAR_MONTH);
	$monthViewButton = $W->Link($monthViewIcon, workspace_Controller()->Calendars()->display(workspace_CtrlCalendars::TYPE_MONTH));

	$dayViewIcon = $W->Icon(workspace_translate('Day view'), Func_Icons::ACTIONS_VIEW_CALENDAR_DAY);
	$dayViewButton = $W->Link($dayViewIcon, workspace_Controller()->Calendars()->display(workspace_CtrlCalendars::TYPE_DAY));

	$weekViewIcon = $W->Icon(workspace_translate('Week view'), Func_Icons::ACTIONS_VIEW_CALENDAR_WEEK);
	$weekViewButton = $W->Link($weekViewIcon, workspace_Controller()->Calendars()->display(workspace_CtrlCalendars::TYPE_WEEK));

	$workWeekViewIcon = $W->Icon(workspace_translate('Work week view'), Func_Icons::ACTIONS_VIEW_CALENDAR_WORKWEEK);
	$workWeekViewButton = $W->Link($workWeekViewIcon, workspace_Controller()->Calendars()->display(workspace_CtrlCalendars::TYPE_WORKWEEK));

	$timelineViewIcon = $W->Icon(workspace_translate('Timeline view'), Func_Icons::ACTIONS_VIEW_CALENDAR_TIMELINE);
	$timelineViewButton = $W->Link($timelineViewIcon, workspace_Controller()->Calendars()->display(workspace_CtrlCalendars::TYPE_TIMELINE));

	$newEventIcon = $W->Icon(workspace_translate('New event'), Func_Icons::ACTIONS_EVENT_NEW);
	$newEventButton = $W->Link($newEventIcon, workspace_Controller()->Calendars()->addEvent());

	$toolbar->addItem($newEventButton);
	$toolbar->addItem($monthViewButton);
	$toolbar->addItem($weekViewButton);
	$toolbar->addItem($workWeekViewButton);
	$toolbar->addItem($dayViewButton);
	$toolbar->addItem($timelineViewButton);

	return $toolbar;
}





function workspace_calendarView($type = 'week', $showDate = null)
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

	$W = bab_Widgets();


	$calendar = $W->FullCalendar('calendar');

	$calendar->setFirstDayOfWeek(1)
		->setSizePolicy(Widget_SizePolicy::MAXIMUM);

	$calendar->onViewDisplayed(workspace_Controller()->Calendars()->setDate());

	if (isset($showDate)) {
		$showDate = BAB_DateTime::fromIsoDateTime($showDate . ' 00:00:00');
		$calendar->setDate($showDate);
	}


	switch ($type) {
		case workspace_CtrlCalendars::TYPE_DAY:
			$calendar->setView(Widget_FullCalendar::VIEW_DAY);
			break;
		case workspace_CtrlCalendars::TYPE_WEEK:
			$calendar->setView(Widget_FullCalendar::VIEW_WEEK);
			break;
		case workspace_CtrlCalendars::TYPE_WORKWEEK:
			$calendar->setView(Widget_FullCalendar::VIEW_WORKWEEK);
			break;
		case workspace_CtrlCalendars::TYPE_MONTH:
		default:
			$calendar->setView(Widget_FullCalendar::VIEW_MONTH);
			break;
	}


	$calendar->fetchPeriods( workspace_Controller()->Calendars()->events());

	$calendar->onDoubleClick(workspace_Controller()->Calendars()->addEvent());

	$calendar->onPeriodMoved(workspace_Controller()->Calendars()->moveEvent());

	$calendar->onPeriodDoubleClick(workspace_Controller()->Calendars()->editEvent());


	$treeview = workspace_CalendarTreeView();

	$splitview = $W->HBoxItems(
		$treeview->setSizePolicy('widget-col-15em'),
		$W->VBoxItems(
			$calendar->setCanvasOptions(Widget_Item::Options()->height('95', '%')->width('100', '%'))
		)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
	)->addClass('expand');

	$explorer = $W->Frame('calendar_explorer')
		->setLayout($W->VBoxLayout())
		->addItem(workspace_calendarToolbar())
		->addItem($splitview);
	return $explorer;
}




function workspace_date($date, $justDate = 0)
{
	if ($date === '' || $date === null) {
		return '';
	}
	if (strlen($date) === 6 && is_numeric($date)) {
		preg_match_all('/\d{2}/', $date, $dateArray);
		return '20' . $dateArray[0][2] . '-' . $dateArray[0][1] .'-' . $dateArray[0][0];
	}
	$dateTime = explode(' ', $date);
	$dateTab = explode('-', $dateTime[0]);
	if (isset($dateTime[1]) && $justDate != 1 ){
		$time = explode(':', $dateTime[1]);
		if ($justDate == 2) {
			return $time[0] . 'h' . $time[1];
		}
		return $dateTab[2] .'-' . $dateTab[1] . '-' . $dateTab[0] . ' ' . $time[0] . 'h' . $time[1];
	} else {
		return $dateTab[2] . '-' . $dateTab[1] . '-' . $dateTab[0];
	}
}



function workspace_timelineView()
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
	require_once $GLOBALS['babInstallPath'] . '/utilit/calapi.php';

	$W = bab_Widgets();


	$timeline = $W->Timeline('timeline');

	$timeline->setAutoWidth(true);

	$timeline->addBand(new Widget_TimelineBand('100%', Widget_TimelineBand::INTERVAL_DAY, 70, Widget_TimelineBand::TYPE_OVERVIEW));
	$timeline->addBand(new Widget_TimelineBand('100%', Widget_TimelineBand::INTERVAL_DAY, 70, Widget_TimelineBand::TYPE_ORIGINAL));
	$timeline->addBand(new Widget_TimelineBand('15%', Widget_TimelineBand::INTERVAL_MONTH, 150, Widget_TimelineBand::TYPE_OVERVIEW));

	$now = BAB_DateTime::now();
	$dateStart = BAB_DateTime::now();
	$dateEnd = BAB_DateTime::now();
	$nowIso = $now->getIsoDate();
	$dateStart->add(-60);
	$dateEnd->add(60);
	$events = workspace_getCalendarEvents($dateStart, $dateEnd);

	foreach ($events as $event) {

		$startDatetime = $event->getProperty('DTSTART');
		$start = substr($startDatetime,0,4) . '-' . substr($startDatetime,4,2) . '-' . substr($startDatetime,6,2) . 'T' .  substr($startDatetime,9,2) . ':' . substr($startDatetime,11,2) . ':'  . substr($startDatetime,13,2);
		$endDatetime = $event->getProperty('DTEND');
		$end = substr($endDatetime,0,4) . '-' . substr($endDatetime,4,2) . '-' . substr($endDatetime,6,2) . 'T' .  substr($endDatetime,9,2) . ':' . substr($endDatetime,11,2) . ':'  . substr($endDatetime,13,2);
		$start = BAB_DateTime::fromIsoDateTime($start);
		$end = BAB_DateTime::fromIsoDateTime($end);
		$period = $timeline->createPeriod($start, $end);
		$period->setTitle($event->getProperty('SUMMARY'));

		if ($event->getProperty('UID') == workspace_getPersonalCalendarId()) {
			$period->setColor('#ddbbee');
		}
		if ($event->getProperty('UID') == workspace_getSharedCalendarId()) {
			$period->setColor('#bbddee');
		}
		if( $event->getProperty('CATEGORIES') != 0 ){
			$category = bab_calGetCategories($event->getProperty('CATEGORIES'));
			$period->setColor($category['color']);
		}
		$color = $event->getProperty('X-CTO-COLOR');
		if (!empty($color)) {
			$period->setColor('#' . $color);
		}


		$title = $W->Title($event->getProperty('SUMMARY'), 2);

		$collection = $event->getCollection();
		$calendar = $collection->getCalendar();


		if ($calendar) {
			$action = workspace_Controller()->Calendars()->editEvent($event->getCollection()->getCalendar()->getUrlIdentifier() . ":" . $event->getProperty('UID'));
			$title = $W->Link($title, $action);
		}

		if ($start->getIsoDate() == $end->getIsoDate()) {
			$period->setBubbleContent(
				$W->VBoxItems(
					$title,
					$W->Html(workspace_translate('On') . ' <b>' . workspace_date($start->getIsoDateTime(),1) . '</b> ' . workspace_translate('from') . ' <b>' . workspace_date($start->getIsoDateTime(),2) . '</b> ' . workspace_translate('to') . ' <b>' . workspace_date($end->getIsoDateTime(),2) . '</b>'),
					$W->Html(workspace_translate('Location:') . ' <b>' . $event->getProperty('LOCATION') . '</b>'),
					$W->Html($event->getProperty('DESCRIPTION'))
				)->setVerticalSpacing(0.5, 'em')
			);
		} else {
			$period->setBubbleContent(
				$W->VBoxItems(
					$title,
					$W->Html(workspace_translate('Begin date:') . ' <b>' . workspace_date($start->getIsoDateTime()) . '</b>'),
					$W->Html(workspace_translate('End date:') . ' <b>' . workspace_date($end->getIsoDateTime()) . '</b>'),
					$W->Html(workspace_translate('Location:') . ' <b>' . $event->getProperty('LOCATION') . '</b>'),
					$W->Html($event->getProperty('DESCRIPTION'))
				)->setVerticalSpacing(0.5, 'em')
			);
		}

		$timeline->addPeriod($period);

	}

	$treeview = workspace_CalendarTreeView();

	$splitview = $W->HBoxItems(
		$treeview->setSizePolicy(Widget_SizePolicy::MINIMUM),
		$W->VBoxItems(
			$timeline->setCanvasOptions(Widget_Item::Options()->height('95', '%')->width('100', '%'))
		)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
	)->addClass('expand');

	$explorer = $W->Frame('calendar_explorer')
		->setLayout(
			$W->VBoxItems(
				workspace_calendarToolbar(),
				$splitview
			)
		);
	return $explorer;
}
