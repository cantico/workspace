<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/controller.class.php';
require_once dirname(__FILE__) . '/page.class.php';
require_once dirname(__FILE__) . '/articles.php';



/**
 * This controller manages actions that can be performed on articles.
 */
class workspace_CtrlArticles extends workspace_Controller
{
	/**
	 * Displays the list of articles.
	 *
	 * @return workspace_Action
	 */
	public function displayList($topic = null, $workspace = null)
	{
		require_once dirname(__FILE__) . '/articles.ui.php';

		workspace_BreadCrumbs::setCurrentPosition($this->proxy()->displayList(), workspace_translate('Articles'));


		if (isset($workspace)) {
			workspace_setCurrentWorkspace($workspace);
		}

		$list = workspace_ArticleList($topic);

		if (isset($topic)) {
			$pageTitle = sprintf(workspace_translate('Articles in topic %s'), bab_getTopicTitle($topic));
		} else {
			$pageTitle = workspace_translate('Article list');
		}

		$page = workspace_Page()
					->setTitle($pageTitle)
					->addItemMenu('articles', workspace_translate('Articles'), '')
					->setCurrentItemMenu('articles')
					->addItem($list);

		return $page;
	}




	/**
	 * Displays a form to edit an existing or a new article.
	 *
	 * @return Widget_Action
	 */
	public function edit($article = null)
	{
		require_once dirname(__FILE__) . '/articles.ui.php';

		$page = workspace_Page();
		$editor = workspace_ArticleEditor();

		if (!is_null($article)) {
			if (!is_array($article)) {
				$article = workspace_getArticle($article);

				$article['body'] = $article['head'];
			}
			$editor->setValue(array('article', 'topic'), $article['id_topic']);
			$editor->setValue(array('article', 'title'), $article['title']);
			if (is_array($article['body'])) {
				$editor->setValue(array('article', 'body'), $article['body']['raw']);
			} else {
				$editor->setValue(array('article', 'body'), $article['body']);
			}

		}
		if (is_array($article) && isset($article['id'])) {
			$editor->setHiddenValue('article[id]', $article['id']);
			$page->setTitle(workspace_translate("Edit article"));
		} else {
			$page->setTitle(workspace_translate("Create a new article"));
		}

		$page->addItemMenu('articles', workspace_translate("Articles"), '')
			 ->setCurrentItemMenu('articles')
			 ->addItem($editor);
		$page->displayHtml();
	}


	/**
	 * Displays a preview of the article content.
	 *
	 * @param array $article	The article content.
	 * @return Widget_Action
	 */
	public function preview($article = null)
	{
//		require_once $GLOBALS['babInstallPath'] . 'utilit/editorincl.php';
//		$bodyeditor = new bab_contentEditor('bodyeditor');
//		$article['body'] = $bodyeditor->getContent();

		require_once dirname(__FILE__) . '/articles.ui.php';

		$preview = workspace_ArticlePreview($article);

		$page = workspace_Page()
					->setTitle(workspace_translate("Article preview"))
					->addItemMenu('articles', workspace_translate("Articles"), '')
					->setCurrentItemMenu('articles')
					->addItem($preview);
		$page->displayHtml();
	}



	/**
	 * Displays the article content.
	 *
	 * @param int $article		The article id
	 * @param int $workspace	The workspace id (optional)
	 * @return Widget_Action
	 */
	public function show($article = null, $workspace = null)
	{
		if (!workspace_canViewArticle($article)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You must be logged in to view this article.'));
			$GLOBALS['babBody']->addError(workspace_translate('Access denied'));
		}

		if (isset($workspace)) {
		    workspace_setCurrentWorkspace($workspace);
		}

		require_once dirname(__FILE__) . '/articles.ui.php';

		$article = workspace_getArticle($article);
// 		$r = new workspace_replace();
// 		$r->ref($article['head']);
		$article['body'] = array('html' => $article['head']);

//		$preview = workspace_ArticleShow($article, true);

//		$preview = workspace_articleFrame($article);


		$listView = workspace_ArticleShow($article);


		$page = workspace_Page()
					->addItemMenu('articles', workspace_translate('Articles'), '')
					->setCurrentItemMenu('articles')
					->addItem($listView);
		$page->displayHtml();
	}

	/**
	 * Saves the article and returns to the article list.
	 *
	 * @param array $article
	 * @return Widget_Action
	 */
	public function save($article = null)
	{

		switch (workspace_getWorkspacesTextEditor()) {
			case 'wysiwyg':
				require_once dirname(__FILE__) . '/workspacewidgets.php';
				$W = bab_Widgets();
				$bodyEditor = $W->BabHtmlEdit('bab_article_body');
				$body = $bodyEditor->getContent(array('article', 'body'));
				break;
			default:
				$body = $article['body'];
				break;
		}


		try {

			if (!isset($article['topic'])) {
				$article['topic'] = null;
			}
			if (isset($article['id'])) {
				// This is an article update
				$articleId = workspace_saveArticle($article['title'], $body, $article['id'], $article['topic']);
				$modification = 1;
			} else {
				// This is an article creation
				$articleId = workspace_saveArticle($article['title'], $body, null, $article['topic']);
				$modification = 0;
			}


			if (isset($articleId)) {
				$this->proposeNotification($articleId, $modification);
				return;
			} else {
				$GLOBALS['babBody']->addError(workspace_translate('Cannot save article'));
			}

		} catch (workspace_AccessException $e) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You must be logged in to save this article.'));
			$GLOBALS['babBody']->addError(workspace_translate('Access denied'));
		}

		workspace_redirect(workspace_BreadCrumbs::last());
	}



	/**
	 * @param int	$article		The article id
	 * @return Widget_Action
	 */
	public function proposeNotification($article, $modification = 0)
	{
		require_once dirname(__FILE__) . '/workspacewidgets.php';
		$W = bab_Widgets();

		$form = $W->Form();
		$frame = new workspace_BaseForm('workspace_dialog');
		$frame->setName('configuration');
		$frame->addClass('workspace-dialog icon-left-48 icon-48x48 icon-left');

		$mainTitle = $modification ? workspace_translate('You just edited an article') : workspace_translate('You just published an article');

		$message =	$W->FlowItems(
			$W->Icon('', Func_Icons::STATUS_DIALOG_QUESTION),
			$W->VBoxItems(
				$W->Title($mainTitle, 2),
				$W->Title(workspace_translate('Would you like to notify other workspace members by email?'), 3)
				)
		)->setVerticalAlign('top');

		$frame->addItem($message);


		$frame->addButton($W->SubmitButton('notify')
								->setAction(workspace_Controller()->Articles()->notify())
								->setLabel(workspace_translate('Notify other workspace members')))
			  ->addButton($W->SubmitButton('cancel')
			  					->setAction(workspace_Controller()->Articles()->Cancel())
			  					->setLabel(workspace_translate('Do not send notifications')))
			;

		$form->setLayout($W->VBoxLayout())->addItem($frame);

		$form->setHiddenValue('article', $article);
		$form->setHiddenValue('tg', bab_rp('tg'));

		$page = workspace_Page()
			->addItemMenu('articles', workspace_translate('Articles'), $this->proxy()->proposeNotification($article)->url())
			->setCurrentItemMenu('articles')
			->addItem($form);

		$page->displayHtml();
	}


	/**
	 * @param int	$article		The article id
	 * @return Widget_Action
	 */
	public function notify($article = null)
	{
		require_once dirname(__FILE__) . '/notifications.php';

		$article = workspace_getArticle($article);

		$author = bab_getUserName($article['id_author'], true);

		if ($article['date_modification'] > $article['date_publication']) {
			$title = workspace_translate('An article has been edited');
			$changeType = 'updatedArticle';
		} else {
			$title = workspace_translate('An article has been published');
			$changeType = 'newArticle';
		}

		$workspaceId = workspace_getCurrentWorkspace();

		$workspaceGroupId = workspace_getWorkspaceGroupId($workspaceId);
		$groups = bab_getGroups($workspaceGroupId, true);
		$groupIds = array($workspaceGroupId => $workspaceGroupId);
		foreach ($groups['id'] as $group) {
			$groupIds[$group] = $group;
		}

		$members = bab_getGroupsMembers($groupIds);

		$workspaceUsersToNotify = array();
		foreach ($members as $member) {
			$workspaceUsersToNotify[$member['id']] = $member;
		}

		$message = '';

		workspace_notifyArticleChange($author, $article['id'], $workspaceUsersToNotify, $title, $message, $changeType);

		workspace_redirect(workspace_BreadCrumbs::last());
	}


	/**
	 * Deletes the specified article.
	 *
	 * @param int $article
	 * @return Widget_Action
	 */
	public function delete($article)
	{
		try {

			workspace_deleteArticle($article);

		} catch (workspace_AccessException $e) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You must be logged in to delete this article.'));
			$GLOBALS['babBody']->addError(workspace_translate('Access denied'));
		}

		workspace_redirect(workspace_BreadCrumbs::last());
	}


	/**
	 * Does nothing and return to the previous page.
	 *
	 * @return Widget_Action
	 */
	public function cancel()
	{
		workspace_redirect(workspace_BreadCrumbs::last());
	}


	/**
	 * Displays help on this page.
	 *
	 * @return Widget_Action
	 */
	public function help()
	{

	}
}
