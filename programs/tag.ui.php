<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


$W = bab_Widgets();

$W->includePhpClass('Widget_Form');





/**
 * Display the tags associated to a workspace_Record.
 */
class workspace_TagsDisplay extends Widget_Form
{
    /**
     * @param Func_App_Workspace		$app
     * @param array|Iterator	$tags		Array or Iterator of workspace_Tag
     * @param string			$id
     */
    public function __construct($app, $tags, ORM_Record $associatedObject, Widget_Action $tagAction = null, $id = null)
    {
        parent::__construct($id);
        $W = bab_Widgets();

        $this->addClass(Func_Icons::ICON_LEFT_SYMBOLIC);

        foreach ($tags as $tag) {
            /* @var $tag workspace_Tag */
            if ($tag instanceof workspace_Link) {
                $tag = $tag->targetId;
            }

            $tagLabel = $W->Label($tag->label);
            if (isset($tagAction)) {
                $tagLabel = $W->Link($tagLabel, $tagAction->setParameter('tag', $tag->label));
            }

            $tagDeleteLink = $W->Link(
                '',
                $app->Controller()->Tag()->unlink($tag->id, $associatedObject->getRef())
            )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE);

            $tagLayout = $W->FlowItems(
                $tagLabel,
                $tagDeleteLink
            )->setVerticalAlign('middle')
            ->addClass('app-tag');

            if (!$tag->checked) {
                $tagLayout->addClass('app-not-checked');
            }
            $this->addItem(
                $tagLayout
            );
        }
    }
}





/**
 * Display the tags associated to a ORM_Record.
 *
 * @extends Widget_FlowLayout
 */
function workspace_displayTags(ORM_Record $record, Widget_Action $tagAction = null, $id = null)
{
	$W = bab_Widgets();

	$App = $record->App();

	$tagSet = $App->TagSet();
	$tags = $tagSet->selectFor($record);

	$layout = $W->FlowLayout($id);
	$layout->setSpacing(2, 'px');

	foreach ($tags as $tag) {

		/* @var $tag workspace_Tag */
		if ($tag instanceof workspace_Link) {
			$tag = $tag->targetId;
		}

		if (isset($tagAction)) {
			$tagAction->setParameter('tag', $tag->label);
			$tagLabel = $W->Link($tag->label, $tagAction);
		} else {
			$tagLabel = $W->Label($tag->label);
		}
		$tagLabel->setTitle($tagLabel);

		$tagDeleteLink = $W->Link(
			$W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE),
			$App->Controller()->Tag()->unlink($tag->id, $record->getRef())
		)->setAjaxAction($App->Controller()->Tag()->unlink($tag->id, $record->getRef()), $tagLabel);

		$tagLayout = $W->FlowItems(
			$tagLabel,
			$tagDeleteLink
		)
		->setVerticalAlign('middle')
		->addClass('app-tag')->addClass('icon-left-16 icon-16x16 icon-left-16');

		if (!$tag->checked) {
			$tagLayout->addClass('app-not-checked');
		}
		$layout->addItem(
			$tagLayout
		);
	}

	return $layout;
}