<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

$W = bab_Widgets();
$W->includePhpClass('Widget_SuggestLineEdit');






/**
 * Constructs a workspace_SuggestTag.
 *
 * @param string		$id			The item unique id.
 * @return workspace_SuggestTag
 */
function workspace_SuggestTag($id = null)
{
    return new workspace_SuggestTag($id);
}


/**
 * A workspace_SuggestTag
 */
class workspace_SuggestTag extends Widget_SuggestLineEdit implements Widget_Displayable_Interface
{

	private $category = null;


	/**
	 * @param Func_App_Workspace $app		The category to select from.
	 * @param string $id			The item unique id.
	 * @return Widget_LineEdit
	 */
	public function __construct($app, $id = null)
	{
		$this->app = $app;
		parent::__construct($id);
		$this->setMinChars(0);
	}

	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'workspace-suggesttag';
		return $classes;
	}





	/**
	 * Send suggestions
	 */
	public function suggest()
	{
		$App = $this->app;

		if (false !== $keyword = $this->getSearchKeyword()) {

			$tagSet = $App->TagSet();

			$entries = $tagSet->select(
				$tagSet->label->contains($keyword)
			);
			$entries->orderAsc($tagSet->label);

			$i = 0;
			foreach ($entries as $tag) {
				/* @var $label workspace_Tag */

				$i++;
				if ($i > Widget_SuggestLineEdit::MAX) {
					break;
				}


				parent::addSuggestion(
					$tag->id,
					$tag->label,
					''
				);
			}

			parent::sendSuggestions();
		}
	}




	public function display(Widget_Canvas $canvas)
	{
		$this->suggest();
		return parent::display($canvas);
	}

}