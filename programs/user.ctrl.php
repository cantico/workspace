<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/controller.class.php';
require_once dirname(__FILE__) . '/workspace.ctrl.php';
require_once dirname(__FILE__) . '/page.class.php';
require_once dirname(__FILE__) . '/workspaces.php';
require_once dirname(__FILE__) . '/user.ui.php';



/**
 * This controller manages actions that can be performed.
 */
class workspace_CtrlUser extends workspace_Controller
{

    /**
     * Displays the list of workspaces available to the user.
     *
     * @return Widget_Action
     */
    public function listWorkspaces()
    {
        workspace_BreadCrumbs::setCurrentPosition($this->proxy()->listWorkspaces(), workspace_translate("My collaborative workspaces"));

        $list = workspace_WorkspaceUserList();

        $page = workspace_Page()
            ->setTitle(workspace_translate('My collaborative workspaces'))
            ->addItemMenu('user', workspace_translate('Collaborative workspaces'), '')
            ->setCurrentItemMenu('user')
            ->addItem($list);

        return $page;
    }


    /**
     * Selects the user current workspace and redirects to this workspace home page.
     * Tries to keep the user in the same "area" of the new workspace (for example stay
     * in the forums).
     *
     * @param int	$workspace
     * @return Widget_Action
     */
    public function selectWorkspace($workspace = null)
    {
        workspace_setCurrentWorkspace($workspace);

        require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';
        $url = workspace_BreadCrumbs::last();
        if (!$url) {
            $url = $this->proxy()->displayHome($workspace)->url();
            workspace_redirect($url);
        }

        $baburl = new bab_url($url);
        if ($baburl->idx) {
            list($ctrl) = explode('.', $baburl->idx);

            // Check in which area of the workspace the user was and redirect to the same area og the new workspace.
            switch ($ctrl) {
                case 'forum':
                    $url = workspace_Controller()->Forum()->listThreads(null, $workspace)->url();
                    break;
                case 'calendars':
                    $url = workspace_Controller()->Calendars()->display('week', null, $workspace)->url();
                    break;
                case 'files':
                    $url = workspace_Controller()->Files()->browse(null, $workspace)->url();
                    break;
                case 'directories':
                    $url = workspace_Controller()->Directories()->displayEntries(null, null, $workspace)->url();
                    break;
                case 'articles':
                    $url = workspace_Controller()->Articles()->displayList(null, $workspace)->url();
                    break;
                case 'pad':
                    $url = workspace_Controller()->Pad()->displayList(null, $workspace)->url();
                    break;
                case 'admin':
                    $url = workspace_Controller()->Admin()->edit($workspace)->url();
                    break;
                default:
                    $url = $this->proxy()->displayHome($workspace)->url();
                    break;
            }
            workspace_redirect($url);
        }

//         if (strripos($url, 'path=DG') !== false) {
//             $url = bab_url::mod($url, 'path', 'DG'.$workspace);
//         }

        $url = $this->proxy()->displayHome($workspace)->url();
        workspace_redirect($url);
    }


    /**
     * Displays the current workspace home page.
     *
     * @return Widget_Action
     */
    public function displayHome($workspace = null)
    {
        workspace_BreadCrumbs::setCurrentPosition($this->proxy()->displayHome($workspace), workspace_translate("Home"));
        
        $App = workspace_App();
        $set = $App->WorkspaceSet();
        $workspace = $set->request($set->id->is($workspace));
        
        $page = workspace_Page();

        require_once dirname(__FILE__) . '/articles.php';
        require_once dirname(__FILE__) . '/calendars.php';
        $delegationId = $workspace->delegation;
        $workspaceInfo = workspace_getWorkspaceInfo($delegationId);

        if ($delegationId == 0) {
            $page->addError(workspace_translate('You do not have any accessible workspace'));
        } elseif (!workspace_workspaceExist($delegationId)) {
            $page->addError(workspace_translate('This workspace does not exist anymore'));
        } else {
            
            // $homeOvml = 'addons/workspace/homeworkspace.html';
            $homeOvml = 'addons/workspace/home2cols.html';

            /* Homepage is a OVML file : homeworkspace.html */
            $content = bab_printOvmlTemplate(
                $homeOvml,
                array(
                    'workspaceId' => $workspace->id
                )
            );

            $html = workspace_PageContent($content);

            $page->addItem($html);
        }

        return $page;
    }


    /**
     * Does nothing and return to the previous page.
     *
     * @param array	$message
     * @return Widget_Action
     */
    public function cancel()
    {
        workspace_redirect(workspace_BreadCrumbs::last());
        die();
    }


    /**
     * Displays help on this page.
     *
     * @return Widget_Action
     */
    public function help()
    {

    }
}
