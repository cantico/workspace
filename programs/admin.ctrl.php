<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/record.ctrl.php';
require_once dirname(__FILE__) . '/page.class.php';

/* @var $App Func_App_Workspace */
$App = bab_functionality::get('App/Workspace');
$App->includeRecordController();

/**
 * This controller manages actions that can be performed on articles.
 */
class workspace_CtrlAdmin extends workspace_Controller
{

    /**
     * {@inheritDoc}
     * @see workspace_CtrlRecord::getRecordClassName()
     */
    protected function getRecordClassName()
    {
        $recordClassname = 'Workspace';
        return $recordClassname;
    }


    /**
     * @param int   $id
     * @param int   $width
     * @param int   $height
     * @param string $itemId
     * @return Widget_VBoxLayout
     */
    public function logo($id, $width = 300, $height = 150, $itemId = null)
    {
        $W = bab_Widgets();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $photoItem = $record->logo($width, $height, false);
        $photoItem->addClass('img-responsive');

        $box = $W->VBoxItems(
            $photoItem
        );

        if ($record->isUpdatable()) {
            $imagesFormItem = $W->ImagePicker();
            $imagesFormItem->oneFileMode();
            $imagesFormItem->hideFiles();

            $imagesFormItem->setAssociatedDropTarget($photoItem);

            $photoUploadPath = $record->getLogoFolderPath();
            if (!is_dir($photoUploadPath->toString())) {
                $photoUploadPath->createDir();
            }
            $imagesFormItem->setFolder($photoUploadPath);
            $imagesFormItem->setAjaxAction($this->proxy()->cancel());

            $box->addItem(
                $imagesFormItem
            );
            $box->addClass('workspace-image-upload');
        }

        if (isset($itemId)) {
            $box->setId($itemId);
        }

        $box->setReloadAction($this->proxy()->logo($record->id, $width, $height, $box->getId()));

        return $box;
    }



    /**
     * @param widget_TableModelView $tableView
     * @return app_Toolbar
     */
    protected function toolbar(widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $proxy = $this->proxy();

        $filter = $tableView->getFilterValues();

        $toolbar = $W->FlowItems();
        $toolbar->addClass('widget-toolbar', Func_Icons::ICON_LEFT_16);

        $viewTypes = $this->getAvailableModelViewTypes();

        if (count($viewTypes) > 1) {
            $viewsBox = $W->Items();
            $viewsBox->setSizePolicy('pull-right');
            $toolbar->addItem($viewsBox);

            $filteredViewType = $this->getFilteredViewType($tableView->getId());

            foreach ($viewTypes as $viewTypeId => $viewType) {
                $viewsBox->addItem(
                    $W->Link(
                        '',
                        $proxy->setFilteredViewType($tableView->getId(), $viewTypeId)
                    )->addClass('widget-actionbutton', 'icon', $viewType['icon'] . ($filteredViewType === $viewTypeId ? ' active' : ''))
                    ->setTitle($viewType['label'])
                    ->setAjaxAction()
                );
            }
        }

        $toolbar->addItem($W->Label(bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp())->setSizePolicy('pull-right'));

        $filtersBox = $W->Items();
        $filtersBox->setSizePolicy('pull-right');
        $filtersBox->addClass('form-inline');
        $toolbar->addItem($filtersBox);


        $filtersBox->addItem(
            $W->Link(
                $App->translate('Filter'),
                $proxy->toggleFilterVisibility($tableView->getId())
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_FILTER)
            ->setTitle($App->translate('Display filter panel'))
            ->setAjaxAction()
        );

        $filtersBox->addItem($W->Label(bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp()));

        $filtersBox->addItem(
            $W->LineEdit()
                ->setPlaceHolder($App->translate('Search...'))
                ->addClass('form-control')
        );


        $toolbar->addItem(
            $W->Link(
                $App->translate('New workspace'),
                $proxy->add()
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
            ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );

//         $toolbar->setHorizontalSpacing(1, 'em');

        return $toolbar;
    }


    public function add()
    {
        $App = $this->App();

        $recordSet = $this->getEditRecordSet();

        $record = $recordSet->newRecord();
        $record->name = '';
        $record->save();

        return $this->edit($record->id);
    }



	/**
	 * Displays the list of registered workspaces.
	 *
	 * @return Widget_BabPage
	 */
	public function listWorkspaces()
	{
		require_once dirname(__FILE__) . '/admin.ui.php';

		workspace_BreadCrumbs::setCurrentPosition($this->proxy()->listWorkspaces(), workspace_translate('Collaborative workspaces'));

		$list = workspace_WorkspaceAdminList();

		$page = workspace_Page()
			->setTitle(workspace_translate('Collaborative workspaces'))
			->addItemMenu('list', workspace_translate('Administration'), '')
			->addItemMenu('configuration', workspace_translate('Global configuration'), $this->proxy()->editGlobalConfiguration()->url())
			->setCurrentItemMenu('list')
			->addItem($list);

		return $page;
	}


	/**
	 * Displays a form to create a new workspace.
	 *
	 * @return Widget_BabPage
	 */
	public function _add()
	{
		if (!bab_isUserAdministrator()) {
			throw new Exception('Access denied');
		}

		require_once dirname(__FILE__) . '/admin.ui.php';

		$editor = workspace_WorkspaceConfigurationEditor(true);

		$page = workspace_Page()
			->setTitle(workspace_translate("Create a new collaborative workspace"))
			->addItemMenu('admin', workspace_translate("Administration"), '')
			->setCurrentItemMenu('admin')
			->addItem($editor);

		return $page;
	}



	/**
	 * Displays a form to edit an existing workspace.
	 *
	 * @param int	$workspace.
	 * @return Widget_BabPage
	 */
	public function editGlobalConfiguration($workspace = null, $errorMessages = array())
	{
		require_once dirname(__FILE__) . '/admin.ui.php';
		require_once dirname(__FILE__) . '/workspaces.php';

		$editor = workspace_WorkspaceGlobalConfigurationEditor();

		if (is_array($workspace)) {
			// $workspace is the workspace array.
			$workspaceInfo = $workspace;
		} else {
			// $workspace is the workspace id.
			$workspaceInfo = workspace_getWorkspaceInfo($workspace);
		}
		$editor->setValue(array('configuration', 'category'), workspace_getWorkspacesDelegationCategory());
		$editor->setValue(array('configuration', 'editor'), workspace_getWorkspacesTextEditor());
		//$editor->setValue(array('configuration', 'skin'), workspace_getWorkspacesSkinName() . '/' . workspace_getWorkspacesSkinCss());
		if (workspace_getWorkspacesAutomaticConnexionValue() == 1) {
			$editor->setValue(array('configuration', 'automatic_connexion'), true);
		} else {
			$editor->setValue(array('configuration', 'automatic_connexion'), false);
		}

		$editor->setValue(array('configuration', 'showSignoffButton'), workspace_getWorkspacesShowSignoffButton());

		if (workspace_getWorkspacesDisableEmailValue() == 1) {
			$editor->setValue(array('configuration', 'disable_email'), true);
		} else {
			$editor->setValue(array('configuration', 'disable_email'), false);
		}

		$editor->setValue(array('configuration', 'date_format'), workspace_getWorkspacesDateFormat());
		$editor->setValue(array('configuration', 'time_format'), workspace_getWorkspacesTimeFormat());

//		$editor->setHiddenValue('workspace[id]', $workspaceInfo['id'] .'');
//		$editor->setValue(array('workspace', 'group'), $workspaceInfo['group']);
//		$editor->setValue(array('workspace', 'administrator'), $workspaceInfo['administrator']);

		$page = workspace_Page()
			->setTitle(workspace_translate("Global configuration"))
			->addItemMenu('list', workspace_translate("Administration"), $this->proxy()->listWorkspaces()->url())
			->addItemMenu('configuration', workspace_translate("Global configuration"), $this->proxy()->editGlobalConfiguration()->url())
			->setCurrentItemMenu('configuration')
			->addItem($editor);

		foreach ($errorMessages as $errorMessage) {
			$page->addError($errorMessage);
		}
		return $page;
	}





	/**
	 * Displays a form to edit an existing workspace.
	 *
	 * @param int	$workspace.
	 * @return Widget_BabPage
	 */
	public function _edit($workspace = null, $errorMessages = array())
	{
		require_once dirname(__FILE__) . '/admin.ui.php';
		require_once dirname(__FILE__) . '/workspaces.php';

		$editor = workspace_WorkspaceConfigurationEditor();

		if (is_array($workspace)) {
			// $workspace is the workspace array.
			$workspaceInfo = $workspace;
		} else {
			// $workspace is the workspace id.
			$workspaceInfo = workspace_getWorkspaceInfo($workspace, true);
		}

		require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';
		$folderId = workspace_getWorkspaceRootFolderId($workspaceInfo['id']);
		$notifiedGroups = aclGetAccessGroups(BAB_FMNOTIFY_GROUPS_TBL, $folderId);
		$topicId = workspace_getWorkspaceTopicId($workspaceInfo['id']);
		$forumId = workspace_getWorkspaceForumId($workspaceInfo['id']);
		$groupId = workspace_getWorkspaceGroupId($workspaceInfo['id']);
		$comGroups = aclGetAccessGroups("bab_topicscom_groups", $topicId);
		$forumGroups = aclGetAccessGroups("bab_forumsnotify_groups", $forumId);

		$editor->setValue(array('workspace', 'name'), $workspaceInfo['name']);
		$editor->setValue(array('workspace', 'notify'), workspace_getWorkspaceFileNotifications($workspaceInfo['id']));
		$editor->setHiddenValue('workspace[id]', $workspaceInfo['id'] .'');
		$editor->setValue(array('workspace', 'group'), $workspaceInfo['group']);
		$editor->setValue(array('workspace', 'category'), $workspaceInfo['category']);
		$editor->setValue(array('workspace', 'administrator'), $workspaceInfo['administrator']);
		$editor->setValue(array('workspace', 'skin'), $workspaceInfo['skin']);
		$editor->setValue(array('workspace', 'skin_styles'), $workspaceInfo['skin_styles']);
		$editor->setValue(array('workspace', 'num_article'), $workspaceInfo['num_article']);
		$editor->setValue(array('workspace', 'num_forum'), $workspaceInfo['num_forum']);
		$editor->setValue(array('workspace', 'num_agenda'), $workspaceInfo['num_agenda']);
		$editor->setValue(array('workspace', 'num_file'), $workspaceInfo['num_file']);
		$editor->setValue(array('workspace', 'other_them'), $workspaceInfo['other_them']);
		$editor->setValue(array('workspace', 'file_size'), $workspaceInfo['file_size']);
		$editor->setValue(array('workspace', 'comment'), isset($comGroups[$groupId]));
		$editor->setValue(array('workspace', 'update_directory'), $workspaceInfo['update_directory']);
		$editor->setValue(array('workspace', 'forum_notif'), isset($forumGroups[$groupId]));
		$editor->setValue(array('workspace', 'versioning'), $workspaceInfo['versioning']);
		$editor->setValue(array('workspace', 'disabled'), $workspaceInfo['disabled']);
		$editor->setValue(array('workspace', 'notifyCalendar'), workspace_getWorkspaceCalendarNotifications($workspaceInfo['id']));
		$editor->setValue(array('workspace', 'create_users'), workspace_getWorkspaceAllowUserCreation($workspaceInfo['id']));

		$page = workspace_Page()
			->setTitle(workspace_translate('Edit the collaborative workspace'))
			->addItemMenu('list', workspace_translate('Workspaces'), $this->proxy()->listWorkspaces()->url())
			->addItemMenu('admin', workspace_translate('Administration'), $this->proxy()->edit($workspace)->url())
			->addItemMenu('administrators', workspace_translate('Administrators'), $this->proxy()->editAdministrators($workspace)->url())
			->setCurrentItemMenu('admin')
			->addItem($editor);

		foreach ($errorMessages as $errorMessage) {
			$page->addError($errorMessage);
		}

		return $page;
	}

	public function editTheme($id)
	{
	    require_once dirname(__FILE__) . '/admin.ui.php';
	    require_once dirname(__FILE__) . '/workspaces.php';

	    $App = workspace_App();
	    $W = bab_Widgets();

	    $set = $App->WorkspaceSet();

	    /**
	     * @var workspace_Workspace $workspace
	     */
	    $workspace = $set->request($set->id->is($id));

	    if(!$workspace->userIsAdministrator()){
	        throw new Exception('Access denied');
	    }

	    $workspace->setCurrentWorkspace();

	    $page = $W->BabPage();
	    $page->addClass('app-page-editor');

	    $page->setTitle($App->translate('Workspace theme editor'));

	    if(!$workspace->isThemeEditable()){
	        $page->addItem(
	            $W->Title($App->translate(sprintf('There is no compatible theme editor existing for the selected theme : %s', $workspace->getSkinName())))
            );
	        return $page;
	    }

	    $editor = $workspace->getSkinEditor();

        $page->addItem($editor);
	    return $page;
	}

	public function themeEditor($id)
	{
	    $App = workspace_App();
	    $set = $App->WorkspaceSet();

	    /**
	     * @var workspace_Workspace $workspace
	     */
	    $workspace = $set->request($set->id->is($id));
	    workspace_overrideConfigurationValues($workspace->delegation);
	    return $workspace->getSkinEditor();
	}



    /**
     *
     * @param int $workspace
     */
    protected function administrators($workspace)
    {
        $W = bab_Widgets();
        require_once dirname(__FILE__) . '/workspaces.php';

        $workspaceInfo = workspace_getWorkspaceInfo($workspace, true);

        $form = $W->Form();
        $form->setHiddenValue('tg', bab_rp('tg'));
        $form->setHiddenValue('workspace', $workspace);
        $form->setName('administrators');
        $form->setLayout($W->VBoxItems());

        $administratorIds = workspace_getWorkspaceAdministratorIds($workspace);
        $permanentAdministratorIds = workspace_getWorkspacePermanentAdministratorIds($workspace);

        $headerBox = $W->FlowItems(
            $W->Label(workspace_translate('Name'))
                ->setSizePolicy('widget-20em'),
            $W->Label(workspace_translate('Permanent'))
                ->setSizePolicy('widget-10em widget-align-center')
        );
        $form->addItem($headerBox->setSizePolicy('widget-list-element widget-strong'));

        foreach ($administratorIds as $administratorId) {
            $adminBox = $W->FlowItems(
                $W->Label(bab_getUserName($administratorId, true))
                    ->setSizePolicy('widget-20em'),
                $W->CheckBox()
                    ->setName(array($administratorId))
                    ->setValue(isset($permanentAdministratorIds[$administratorId]))
                    ->addClass('')
                    ->setSizePolicy('widget-10em widget-align-center')
            );

            $form->addItem($adminBox->setSizePolicy('widget-list-element'));
        }

        $form->addItem(
            $W->SubmitButton()
                ->setAction($this->proxy()->saveAdministrators())
                ->setLabel(workspace_translate('Save'))
        );

        return $form;
    }


    /**
     *
     * @param int $workspace
     * @param array $administrators
     * @throws Exception
     */
    public function saveAdministrators($workspace = null, $administrators = null)
    {
        require_once dirname(__FILE__) . '/admin.php';
        if (!workspace_userIsWorkspaceAdministrator()) {
            throw new Exception('Access denied');
        }

        $permanentAdministratorIds = array();
        foreach ($administrators as $administrator => $selected) {
            workspace_setUserAdministrator($administrator, $workspace);
            if ($selected) {
                $permanentAdministratorIds[$administrator] = $administrator;
            }
        }

        workspace_setWorkspacePermanentAdministratorIds($permanentAdministratorIds, $workspace);

        global $babBody;

        $babBody->addMessage(workspace_translate('Configuration saved.'));

        return true;
    }



    /**
     * Displays a form to edit an existing workspace.
     *
     * @param int	$workspace.
     * @return Widget_BabPage
     */
    public function editAdministrators($workspace = null)
    {
        $W = bab_Widgets();
        require_once dirname(__FILE__) . '/admin.ui.php';
        require_once dirname(__FILE__) . '/workspaces.php';

        workspace_BreadCrumbs::setCurrentPosition($this->proxy()->editAdministrators($workspace), workspace_translate("Remove workspace"));

        $editor = $this->administrators($workspace);
        $editor->addClass('workspace-dialog');

        if (is_array($workspace)) {
            // $workspace is the workspace array.
            $workspaceInfo = $workspace;
        } else {
            // $workspace is the workspace id.
            $workspaceInfo = workspace_getWorkspaceInfo($workspace, true);
        }

        $page = workspace_Page();

        $toolbar = workspace_Toolbar('main-toolbar');

        $newIcon = $W->Icon(workspace_translate('Add administrator'), Func_Icons::ACTIONS_USER_NEW);
        $newButton = $W->Link($newIcon, workspace_Controller()->Admin()->selectAdministrator($workspace));

        $toolbar->addItem($newButton);

        $page->addItem($toolbar);

        $page->setTitle(workspace_translate('Edit workspace administrators'))
            ->addItemMenu('list', workspace_translate('Workspaces'), $this->proxy()->listWorkspaces()->url())
            ->addItemMenu('admin', workspace_translate('Administration'), $this->proxy()->edit($workspace)->url())
            ->addItemMenu('administrators', workspace_translate('Administrators'), $this->proxy()->editAdministrators($workspace)->url())
            ->setCurrentItemMenu('administrators')
            ->addItem($editor);

        return $page;
    }

    public function managersEditor($workspaceId = null, $itemId = null)
    {
        $App = workspace_App();
        $W = bab_Widgets();

        $set = $App->WorkspaceSet();
        $workspace = $set->request($set->id->is($workspaceId));

        $view = $this->managersEditorView($workspace, $itemId);
//         $view->setAjaxAction();

        $box = $W->VBoxItems(
            $view
        );

        $box->setReloadAction($this->proxy()->managersEditor($workspaceId, $view->getId()));
        return $box;
    }

    public function managersEditorView($workspace, $itemId)
    {
        $App = workspace_App();
        $Ui = $App->Ui();
        $view = $Ui->WorkspaceManagerEditor($itemId);
        $view->setRecord($workspace);
        return $view;
    }




    /**
     *
     * @param int $workspace
     * @return Widget_BabPage
     */
    public function selectAdministrator($workspace)
    {
        $W = bab_Widgets();
        require_once dirname(__FILE__) . '/workspaces.php';
        require_once dirname(__FILE__) . '/directories.ui.php';

        $workspaceInfo = workspace_getWorkspaceInfo($workspace, true);

        $form = $W->Form();
        $frame = new workspace_BaseForm('workspace_member_select_editor');

        $userFormItem = workspace_FormField(
            workspace_translate('New administrator:'),
            $W->UserPicker()->setName(array('members', '', 'id'))
        );

        $frame->addItem($userFormItem);

        $frame->addButton(
            $W->SubmitButton('save')
            ->validate(true)
            ->setAjaxAction(workspace_Controller()->Admin()->addAdministrator())
            ->setLabel(workspace_translate('Add administrator'))
        );
        $frame->addButton(
            $W->SubmitButton('cancel')
                ->setAjaxAction(workspace_Controller()->Admin()->Cancel())
                ->setLabel(workspace_translate('Cancel'))
        );

        $form->setLayout($W->VBoxLayout())->addItem($frame);

        $form->setHiddenValue('workspace', $workspace);

        $form->setHiddenValue('tg', bab_rp('tg'));


        $form->addClass('workspace-dialog');
        $form->addClass('icon-left-16 icon-left icon-16x16');

        $page = workspace_Page()
            ->setTitle(workspace_translate('Add administrator'))
            ->addItem($form);

        return $page;
    }



    /**
     * @param array	$members		contains the user ids to add to the workspace.
     *
     * @return Widget_BabPage
     */
    public function addAdministrator($workspace = null, $members = null)
    {
        if (!bab_isUserAdministrator()) {
            throw new Exception('Access denied');
        }

        require_once dirname(__FILE__) . '/admin.php';
        $groupId = workspace_getWorkspaceGroupId($workspace);

        foreach ($members as $member) {
            // By default we add new members to the default workspace group (readers)
            try {
                if(empty($member['id'])){
                    $GLOBALS['babBody']->addError(workspace_translate('Unknown user'));
                }
                else{
                    bab_addUserToGroup($member['id'], $groupId);
                    workspace_setUserProfile($member['id'], 'administrator', $workspace);
                }
            } catch (Exception $e) {
                $GLOBALS['babBody']->addError($e->getMessage());
            }
        }
        return true;
    }

    /**
     * @param array	$members		contains the user ids to remove to the workspace.
     *
     * @return Widget_BabPage
     */
    public function removeAdministrator($workspace = null, $members = null)
    {
        if (!bab_isUserAdministrator()) {
            throw new Exception('Access denied');
        }

        require_once dirname(__FILE__) . '/admin.php';
        $groupId = workspace_getWorkspaceGroupId($workspace);
        foreach ($members as $member) {
            // By default we add new members to the default workspace group (readers)
            try {
                bab_removeUserFromGroup($member['id'], $groupId);
                workspace_removeDelegationAdmin($workspace, $member['id']);
            } catch (Exception $e) {
                $GLOBALS['babBody']->addError($e->getMessage());
            }
        }
        return true;
    }


	/**
	 * An intermediate dialog to confirm the creation/modification of the workspace
	 * with a summary information about what will be done.
	 * !!! If it's an update of the workspace, not a creation, it's go to workspace_updateWorkspace()
	 *
	 * @return Widget_BabPage
	 */
	public function confirmSave($workspace = null)
	{
		require_once dirname(__FILE__) . '/admin.php';
		require_once dirname(__FILE__) . '/admin.ui.php';
		require_once $GLOBALS['babInstallPath'] . '/utilit/userinfosincl.php';

		$name = workspace_sanitizeWorkspaceName($workspace['name']);

		if ($name === '') {
			bab_debug($workspace);
			$this->edit($workspace, array(workspace_translate('The workspace name must not be empty')));
			return;
		}

		$notify = isset($workspace['notify']) ? $workspace['notify'] : false;

		/* !!! It's an update of the workspace */
		if (isset($workspace['id'])) {
			workspace_updateWorkspace(
				$workspace['id'],
				$name,
				$notify,
				$workspace['skin'],
				$workspace['skin_styles'],
				$workspace['num_article'],
				$workspace['num_forum'],
				$workspace['num_agenda'],
				$workspace['num_file'],
				$workspace['other_them'],
				$workspace['file_size'],
				$workspace['comment'],
				$workspace['forum_notif'],
				$workspace['update_directory'],
				$workspace['versioning'],
				(bool) $workspace['disabled'],
				$workspace['notifyCalendar'],
				$workspace['category'],
			    (bool) $workspace['create_users']
			);

			bab_siteMap::clearAll(); // clear for disabled workspaces, removed from sitemap

			$this->addMessage(workspace_translate('The workspace configuration has been saved'));
			workspace_redirect($this->proxy()->listWorkspaces());
		}



		$administrator = $workspace['administrator'];
		$group = $workspace['group'];

		if ($administrator === '') {
			$administrator = $GLOBALS['BAB_SESS_USERID'];
		}
		$administratorName = implode(' ', bab_userInfos::arrName($administrator));
		if ($group === '') {
			$groupName = $name;
			require_once dirname(__FILE__) . '/workspaces.php';
			if (workspace_groupNameExist($groupName)) {
				return $this->edit($workspace, array(sprintf(workspace_translate("A group named '%s' already exists."), $groupName)));
			}
			$newGroup = true;
		} else {
			$groupName = bab_getGroupName($group, true);
			$newGroup = false;
		}

		$file_size = $workspace['file_size'];
		if( $file_size > floor($GLOBALS['babMaxTotalSize']/1048576) ){
			$file_size = floor($GLOBALS['babMaxTotalSize']/1048576);
		}
		if( $file_size > floor($GLOBALS['babMaxGroupSize']/1048576) ){
			$file_size = floor($GLOBALS['babMaxGroupSize']/1048576);
		}

		$dialog = workspace_workspaceConfirmationDialog(
			$name,
			$administratorName,
			$groupName,
			$newGroup,
			$administrator,
			$group,
			$workspace['skin'],
			$workspace['skin_styles'],
			$workspace['num_article'],
			$workspace['num_forum'],
			$workspace['num_agenda'],
			$workspace['num_file'],
			$workspace['other_them'],
			$file_size,
			$workspace['comment'],
			$workspace['forum_notif'],
			$workspace['update_directory'],
			$workspace['versioning'],
			$workspace['notifyCalendar'],
			$workspace['category'],
		    $workspace['create_users']
		);

		$page = workspace_Page();
		$page->setTitle(workspace_translate("Confirm creation of a new collaborative workspace"))
			->addItemMenu('admin', workspace_translate("Administration"), '')
			->setCurrentItemMenu('admin')
			->addItem($dialog);

		return $page;
	}


	/**
	 * Saves the workspace data and returns to the workspace list.
	 *
	 * @param array $workspace
	 * @return Widget_Action
	 */
	public function _save($workspace = null)
	{
		global $babBody;

		if (!bab_isUserAdministrator()) {
			throw new Exception('Access denied');
		}

		require_once $GLOBALS['babInstallPath'] . '/utilit/grpincl.php';
		require_once dirname(__FILE__) . '/admin.php';

		$name = $workspace['name'];
		if ($name === '') {

			$babBody->addError('The workspace name must not be empty');

			$this->edit($workspace);
			return;
		}

		$administrator = $workspace['administrator'];

		$group = $workspace['group'];
		if (!empty($group) && false === bab_isGroup($group)) {
			throw new Exception('Wrong group id');
		}

		$statusCreation = workspace_createWorkspace(
			$name,
			$administrator,
			$group,
			$workspace['skin'],
			$workspace['skin_styles'],
			$workspace['num_article'],
			$workspace['num_forum'],
			$workspace['num_agenda'],
			$workspace['num_file'],
			$workspace['other_them'],
			$workspace['file_size'],
			$workspace['comment'],
			$workspace['forum_notif'],
			$workspace['update_directory'],
			$workspace['versioning'],
			$workspace['notifyCalendar'],
			$workspace['category'],
		    $workspace['create_users']
		);
		if (!$statusCreation) {
			$babBody->addError('The workspace could not be created.');
			return;
		}

		workspace_redirect($this->proxy()->listWorkspaces());
	}


	/**
	 * Saves the workspace data and returns to the workspace list.
	 *
	 * @param array $configuration
	 */
	public function saveGlobalConfiguration($configuration = null)
	{
		global $babBody;

		if (!bab_isUserAdministrator()) {
			throw new Exception('Access denied');
		}

		workspace_setWorkspacesDelegationCategory($configuration['category']);
		workspace_setWorkspacesTextEditor($configuration['editor']);
		workspace_setWorkspacesAutomaticConnexionValue($configuration['automatic_connexion']);
		workspace_setWorkspacesDisableEmailValue($configuration['disable_email']);
		workspace_setWorkspacesShowSignoffButton($configuration['showSignoffButton']);
		workspace_setWorkspacesDateFormat($configuration['date_format']);
		workspace_setWorkspacesTimeFormat($configuration['time_format']);

		//list($skinName, $skinCss) = explode('/', $configuration['skin']);
		//workspace_setWorkspacesSkin($skinName, $skinCss);

		workspace_redirect($this->proxy()->editGlobalConfiguration());
	}



	/**
	 *
	 * @param unknown $workspace
	 * @return Widget_BabPage
	 * @throws Exception
	 */
	public function confirmDelete($workspace)
	{
		if (!bab_isUserAdministrator()) {
			throw new Exception('Access denied');
		}

		require_once dirname(__FILE__) . '/admin.ui.php';

		workspace_BreadCrumbs::setCurrentPosition($this->proxy()->listWorkspaces(), workspace_translate("Remove workspace"));

		$dialog = workspace_confirmDelete($workspace);

		$page = workspace_Page()
			->setTitle(workspace_translate("Remove workspace"))
			->addItem($dialog);

		return $page;
	}







	/**
	 *
	 * @param int $workspace
	 */
	public function delete($workspace, $moveobjects)
	{
	    global $babBody;

		if (!bab_isUserAdministrator()) {
			throw new Exception('Access denied');
		}

		require_once dirname(__FILE__) . '/admin.php';

		try {
		    workspace_removeWorkspace($workspace, !((bool) $moveobjects));
		} catch (Exception $e) {

		    $W = bab_Widgets();
		    $page = workspace_Page();

		    $page->setTitle(workspace_translate('Remove workspace'));
		    $page->addItem(
	            $W->Frame(null, $W->VBoxItems(
                    $W->Title(workspace_translate('Unable to delete workspace')),
                    $W->Label(workspace_translate($e->getMessage()))
	            )->setVerticalSpacing(1,'em'))->addClass('workspace-dialog')
		    );

		    return $page;
		}

		$babBody->addError(workspace_translate('The workspace has been deleted.'));

		workspace_redirect(workspace_BreadCrumbs::last());
	}



	/**
	 * Does nothing and return to the previous page.
	 */
	public function cancel()
	{
		return true;
	}


	/**
	 * Displays help on this page.
	 *
	 * @return Widget_Action
	 */
	public function help()
	{

	}

	public function saveWorkspaceTheme($workspace = null, $configuration = array())
	{
	    $App = workspace_App();
	    $set = $App->WorkspaceSet();

	    /**
	     * @var workspace_Workspace $workspace
	     */
	    $workspace = $set->request($set->id->is($workspace));

	    if(!$workspace->userIsAdministrator()){
	        throw new Exception('Access denied');
	    }

	    $themeConfiguration = $workspace->getSkinConfiguration();
	    foreach ($themeConfiguration as $index => $values){
	        if($values['type'] == 'color'){
	            if(isset($configuration[$index])){
	                if(strpos($configuration[$index], '#') === false){
	                    $configuration[$index] = '#'.$configuration[$index];
	                }
	                bab_Registry::set('/workspace/workspaces/'.$workspace->delegation.'/theme/'.$index, $configuration[$index]);
	            }
	        }
	        elseif($values['type'] == 'image'){
	            $imageUrl = $workspace->getImageUrl($index);
	            bab_Registry::set('/workspace/workspaces/'.$workspace->delegation.'/theme/'.$index, 'images/addons/workspace/'.$workspace->delegation.'/'.$imageUrl);
	        }
	    }

	    $this->addMessage(dashboardlte_translate('The configuration has been applied'));
	    return true;
	}

	public function resetWorkspaceTheme($workspace = null)
	{
	    $App = workspace_App();
	    $set = $App->WorkspaceSet();

	    /**
	     * @var workspace_Workspace $workspace
	     */
	    $workspace = $set->request($set->id->is($workspace));

	    if(!$workspace->userIsAdministrator()){
	        throw new Exception('Access denied');
	    }

	    $configuration = $workspace->getSkinConfiguration();
	    foreach ($configuration as $key => $values){
	        if($values['type'] == 'color'){
	            bab_Registry::set('/workspace/workspaces/'.$workspace->delegation.'/theme/'.$key, $values['default']);
	        }
	    }

	    $this->addMessage(dashboardlte_translate('The configuration has been reset'));
	    return true;
	}

	public function configureFileManager($workspace = null)
	{
	    $App = workspace_App();
	    $set = $App->WorkspaceSet();

	    /**
	     * @var workspace_Workspace $workspace
	     */
	    $workspace = $set->request($set->id->is($workspace));

	    if(!$workspace->userIsAdministrator()){
	        throw new Exception('Access denied');
	    }

	    $editor = $workspace->getFileManagerConfigurationForm();
	    $editor->isAjax = true;
	    $editor->setHiddenValue('tg', $App->controllerTg);
	    $editor->setSaveAction($this->proxy()->saveFileManagerConfiguration());
	    $editor->setName('data');
	    $editor->setValues($workspace->getFileManagerConfiguration(), array('data'));

	    return $editor;
	}

	public function saveFileManagerConfiguration($data = null)
	{
	    if(!bab_isUserAdministrator()) {
	        throw new Exception('Access denied');
	    }
	    if(!isset($data['id']) || !isset($data['version']) || !isset($data['notification'])){// || !isset($data['maxSize'])){
	        throw new Exception('Invalid data');
	    }

	    $App = workspace_App();
	    $set = $App->WorkspaceSet();

	    /* @var $workspace workspace_Workspace */
	    $workspace = $set->request($set->id->is($data['id']));
	    return $workspace->saveFileManagerConfiguration($data);
	}


    /**
     * @param string $addonName
     * @param int $workspaceId
     * @return boolean|unknown
     */
    public function editAddonConfiguration($addonName, $workspaceId)
    {
        $App = workspace_App();

        $addon = bab_functionality::get('WorkspaceAddon/' . $addonName);
        if (!$addon) {
            $this->addError(sprintf($App->translate('Cannot find configuration for workspace addon <b>%s</b>'), bab_toHtml($addonName)));
            return true;
        }

        $Ui = $App->Ui();
        $page = $Ui->Page();
        $page->setTitle(sprintf($App->translate('Configure %s'), $addon->getName()));
        $form = $addon->getConfigurationEditor($workspaceId);
        $page->addItem($form);

        return $page;
    }



    public function saveAddonConfiguration($configuration = null, $workspaceId = null, $addonName = null)
    {
        global $babBody;
        $addon = bab_functionality::get('WorkspaceAddon/' . $addonName);
        if ($addon) {
            $addon->setConfiguration($workspaceId, $configuration);
            $babBody->addMessage(sprintf(workspace_translate('Configuration for %s was successfully saved'), $addonName));
        }
        else{
            $babBody->addError(sprintf(workspace_translate('Functionality %s was not found'), $addonName));
        }
        return true;
    }

    public function save($data = null)
    {
        if(isset($data[$data['skin']]['skinStyles'])){
            $data['skinStyles'] = $data[$data['skin']]['skinStyles'];
        }
        return parent::save($data);
    }

    public function postSave(workspace_Workspace $record, $data)
    {
        $record->updateAddons($data);
    }
}
