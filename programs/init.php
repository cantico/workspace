<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';





function workspace_upgrade($version_base, $version_ini)
{
    require_once dirname(__FILE__) . '/app.php';

    $functionalities = new bab_functionalities();
    $addon = bab_getAddonInfosInstance('workspace');
    $addonPhpPath = $addon->getPhpPath();

    $functionalities->unregister('AppWorkspace');

    if ($functionalities->registerClass('Func_App_Workspace', $addonPhpPath . 'app.php')) {
        echo(bab_toHtml('Functionality "Func_App_Workspace" registered.'));
    }

    $App = workspace_App();

    bab_functionality::get('LibOrm')->initMysql();
    $mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);

    require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';

    $App->synchronizeSql('workspace_');



    global $babDB;

    $sql = 'DELETE FROM '.BAB_EVENT_LISTENERS_TBL.' WHERE addon_name = ' . $babDB->quote('workspace');

    $babDB->db_query($sql);

    bab_removeAddonEventListeners('workspace');
    $addon = bab_getAddonInfosInstance('workspace');
    $addon->addEventListener('bab_eventBeforeSiteMapCreated',	'workspace_onSiteMapItems',			'init.php');
    $addon->addEventListener('bab_eventBeforePageCreated',		'workspace_onBeforePageCreated',	'init.php', 10);
    $addon->addEventListener('bab_eventUserAttachedToGroup',	'workspace_onUserAttachedToGroup',	'init.php');

    $addon->addEventListener('bab_eventFmFile',					'workspace_onFmFile',				'notifications.php', 10);
    $addon->addEventListener('bab_eventForumPost',				'workspace_onForumPost',			'notifications.php', 10);
    $addon->addEventListener('bab_eventArticle',				'workspace_onArticleAdded',			'notifications.php', 10);
    $addon->addEventListener('bab_eventCalendarEvent',			'workspace_onCalendarEvent',		'notifications.php', 10);
    $addon->addEventListener('bab_eventDirectory',				'workspace_onDirectoryChange',		'notifications.php', 10);

    $addon->registerFunctionality('Workspace', 'functionality.class.php');
    $addon->registerFunctionality('WorkspaceAddon', 'workspaceaddon.class.php');
    $addon->registerFunctionality('Func_Ovml_Container_Workspaces', '/ovml/ocworkspaces.class.php');
    $addon->registerFunctionality('Func_Ovml_Container_WorkspaceTags', '/ovml/ocworkspacetags.class.php');
    $addon->registerFunctionality('Func_Ovml_Container_WorkspaceMenu', '/ovml/ocworkspacemenu.class.php');


    // Here we ensure that each base group of a workspace has 2 sub-groups (writers and administrators).

    require_once $GLOBALS['babInstallPath'] . 'utilit/userincl.php';
    require_once dirname(__FILE__) . '/workspaces.php';

    $workspaces = workspace_getWorkspaceList(null, true);

    foreach ($workspaces as $workspace) {

        $workspaceId = $workspace['id'];
        $workspaceInfo = workspace_getWorkspaceInfo($workspaceId, true);

        $groupId = $workspaceInfo['group'];

        // The function calls will create the writers and administrators sub-groups if they do not exist.
        $writersGroupId = workspace_getWorkspaceWritersGroupId($workspaceId);
        $administratorsGroupId = workspace_getWorkspaceAdministratorsGroupId($workspaceId);

        $categoryId = workspace_getWorkspaceCategoryId($workspaceId);
        $topicId = workspace_getWorkspaceTopicId($workspaceId);
        $directoryId = workspace_getWorkspaceDirectoryId($workspaceId);
        $forumId = workspace_getWorkspaceForumId($workspaceId);
        $calId = workspace_getWorkspaceCalendarId($workspaceId);
        $rootFolderId = workspace_getWorkspaceRootFolderId($workspaceId);

        $adminIds = workspace_getWorkspaceAdministratorIds($workspaceId);

        foreach ($adminIds as $adminId) {
            // We ensure that the workspace administrator is member of the administered group.
            if (!bab_isMemberOfGroup($groupId, $adminId)) {
                bab_addUserToGroup($adminId, $groupId);
            }
            if (!bab_isMemberOfGroup($administratorsGroupId, $adminId)) {
                bab_addUserToGroup($adminId, $administratorsGroupId);
            }
        }


        if (version_compare($version_base, '1.0.7', '<=')) {
            // Here we remove access rights erroneously attributed to ALL USERS GROUP (0).
            bab_installWindow::message(bab_toHtml(sprintf(workspace_translate('Cleaning access rights for %s'), $workspaceInfo['name'])));
            $groupId = BAB_ALLUSERS_GROUP;

            $rightsTables = array(
                BAB_DEF_TOPCATCOM_GROUPS_TBL,
                BAB_DEF_TOPCATMOD_GROUPS_TBL,
                BAB_DEF_TOPCATSUB_GROUPS_TBL,
                BAB_DEF_TOPCATVIEW_GROUPS_TBL,
                BAB_DEF_TOPCATMAN_GROUPS_TBL
            );
            foreach ($rightsTables as $rightsTable) {
                aclRemove($rightsTable, $groupId, $categoryId);
            }

            $rightsTables = array(
                BAB_TOPICSMOD_GROUPS_TBL,
                BAB_TOPICSSUB_GROUPS_TBL,
                BAB_TOPICSVIEW_GROUPS_TBL,
                BAB_TOPICSCOM_GROUPS_TBL,
                BAB_TOPICSMAN_GROUPS_TBL
            );

            foreach ($rightsTables as $rightsTable) {
                aclRemove($rightsTable, $groupId, $topicId);
            }

            $rightsTables = array(
                BAB_DBDIRVIEW_GROUPS_TBL,
                BAB_DBDIRADD_GROUPS_TBL,
                BAB_DBDIRUPDATE_GROUPS_TBL,
                BAB_DBDIRDEL_GROUPS_TBL,
                BAB_DBDIREXPORT_GROUPS_TBL
            );

            foreach ($rightsTables as $rightsTable) {
                aclRemove($rightsTable, $groupId, $directoryId);
            }

            $rightsTables = array(
                BAB_FORUMSPOST_GROUPS_TBL,
                BAB_FORUMSREPLY_GROUPS_TBL,
                BAB_FORUMSVIEW_GROUPS_TBL,
                BAB_FORUMSFILES_GROUPS_TBL,
                BAB_FORUMSMAN_GROUPS_TBL,
                BAB_FORUMSNOTIFY_GROUPS_TBL
            );

            foreach ($rightsTables as $rightsTable) {
                aclRemove($rightsTable, $groupId, $forumId);
            }

            $rightsTables = array(
                BAB_CAL_PUB_MAN_GROUPS_TBL,
                BAB_CAL_PUB_VIEW_GROUPS_TBL
            );

            foreach ($rightsTables as $rightsTable) {
                aclRemove($rightsTable, $groupId, $calId);
            }

            $rightsTables = array(
                BAB_FMUPLOAD_GROUPS_TBL,
                BAB_FMDOWNLOAD_GROUPS_TBL,
                BAB_FMUPDATE_GROUPS_TBL
            );

            foreach ($rightsTables as $rightsTable) {
                aclRemove($rightsTable, $groupId, $rootFolderId);
            }
        } elseif (version_compare($version_base, '1.1.103', '<=')) {
            // Here we ensure that all workspaces groups are associated to calendar notification access right.
            bab_installWindow::message(bab_toHtml(sprintf(workspace_translate('Adding calendar notification access rights for %s'), $workspaceInfo['name'])));
            aclAdd(BAB_CAL_PUB_NOT_GROUPS_TBL, $groupId, $calId);
            aclAdd(BAB_CAL_PUB_NOT_GROUPS_TBL, $writersGroupId, $calId);
            aclAdd(BAB_CAL_PUB_NOT_GROUPS_TBL, $administratorsGroupId, $calId);

            // Here we delete double entries with the same id_user for the same directory.
            $sql = 'SELECT id_user, COUNT(id) AS `count` FROM '.BAB_DBDIR_ENTRIES_TBL.' WHERE id_user > 0 AND id_directory='.$babDB->quote($directoryId).' GROUP BY id_user';

            $counts = $babDB->db_query($sql);
            while ($count = $babDB->db_fetch_assoc($counts)) {
                if ($count['count'] <= 1) {
                    continue;
                }
                $sql = 'SELECT id, id_user FROM '.BAB_DBDIR_ENTRIES_TBL.' WHERE id_user='.$babDB->quote($count['id_user']) . ' AND id_directory='.$babDB->quote($directoryId).' ORDER BY id ASC LIMIT 1,100 ';
                $entries = $babDB->db_query($sql);
                $ids = array();
                while ($entry = $babDB->db_fetch_assoc($entries)) {
                    $ids[$entry['id']] = $entry['id'];
                }

                bab_installWindow::message(bab_toHtml(sprintf(workspace_translate('Deleting double directory entries %s'), implode(',', $ids))));

                $sql = 'DELETE FROM '.BAB_DBDIR_ENTRIES_EXTRA_TBL.' WHERE id_entry IN(' . $babDB->quote($ids) . ')';
                $babDB->db_query($sql);

                $sql = 'DELETE FROM '.BAB_DBDIR_ENTRIES_TBL.' WHERE id_directory=' . $babDB->quote($directoryId).' AND id IN(' . $babDB->quote($ids) . ')';
                $babDB->db_query($sql);
            }

        }
    }

    //Set
    $app = workspace_App();
    $workspaceSet = $app->WorkspaceSet();
    $linkSet = $app->LinkSet();
    $tagSet = $app->TagSet();

    $synchronize = new bab_synchronizeSql();
    $synchronize->addOrmSet($workspaceSet);
    $synchronize->addOrmSet($linkSet);
    $synchronize->addOrmSet($tagSet);

    workspace_copyDelegations();

    //Portlets
    @bab_functionality::includefile('PortletBackend');
    if(!class_exists('Func_PortletBackend')) {
        bab_installWindow::message(bab_toHtml(workspace_translate('Error : functionality PortletBackend not found. Workspace portlets will not be created.')));
    }
    else{
        $addon->unregisterFunctionality('PortletBackend/Workspace');
        $addon->registerFunctionality('PortletBackend/Workspace', 'portletbackend.class.php');
    }

    return true;
}




/**
 * Automatic redirect of users after authentification if they are only in groups of workspaces
 */
function workspace_onBeforePageCreated(bab_eventBeforePageCreated $event)
{
    if (bab_isAjaxRequest()) {
        return;
    }

    require_once dirname(__FILE__) . '/workspaces.php';
    require_once dirname(__FILE__) . '/workspace.ctrl.php';


    // Check if users should be redirected to workspaces after authentification
    // It depends on a global workspace configuration and applies only to users if they are only
    // members of groups of workspaces.

    if (bab_userIsloggedin()) {
        if (!isset($_SESSION['workspace_redirectToWorkspacesPerformed'])) {
            $_SESSION['workspace_redirectToWorkspacesPerformed'] = true;
            $_SESSION['workspace_userIsOnlyInGroupsOfWorkspaces'] = false;
            if (workspace_getWorkspacesAutomaticConnexionValue() == 1) {
                if (workspace_isUserOnlyInGroupsOfWorkspaces()) {
                    $_SESSION['workspace_userIsOnlyInGroupsOfWorkspaces'] = true;
                    $homeUrl = workspace_Controller()->User()->displayHome()->url();
                    header('Location: ' . $homeUrl);
                    die;
                }
            }
        }
    }

    $tg = bab_rp('tg', null);



    if (substr($tg, 0, strlen('addon/workspace/')) === 'addon/workspace/' || $tg === 'workspace.main') {
        // This will systematically initialize the current workspace.
        $workspaceId = bab_rp('workspace', null);
        if(isset($workspaceId)){
            $App = workspace_App();
            $set = $App->WorkspaceSet();
            $workspace = $set->get($set->id->is($workspaceId));
            if($workspace){
                $delegationId = $workspace->delegation;
            }
        }
        else{
            $delegationId = workspace_getCurrentWorkspace();
        }
        $skinName = workspace_getWorkspaceSkinName($delegationId);
        $skinCss = workspace_getWorkspaceSkinCss($delegationId);
        $info = workspace_getWorkspaceInfo($delegationId);

        if ($skinName !== '') {
            bab_skin::applyOnCurrentPage($skinName, $skinCss);
        }

        $addon = bab_getAddonInfosInstance('workspace');
        $babBody = bab_getBody();
        $babBody->addStyleSheet($addon->getStylePath().'icons/icons.css');
        $babBody->addStyleSheet($addon->getStylePath().'workspace.css');

        $GLOBALS['/' . $skinName . '/global/topNavigationNode'] = 'babworkspaces' . $delegationId;
        $GLOBALS['/' . $skinName . '/global/topNavigationNode2'] = 'babworkspaces';
        $GLOBALS['/' . $skinName . '/global/subHeaderText'] = $info['name'];

        bab_Registry::set('/workspace/user/' . $GLOBALS['BAB_SESS_USERID'].'/currentWorkspace', $delegationId);

        //Set contextual menu
        workspace_overrideConfigurationValues($delegationId);
    }
    else{
        bab_Registry::set('/workspace/user/' . $GLOBALS['BAB_SESS_USERID'].'/currentWorkspace', NULL);
    }
}

function workspace_overrideConfigurationValues($delegationId)
{
    $App = workspace_App();
    $set = $App->WorkspaceSet();
    $workspace = $set->get($set->delegation->is($delegationId));
    if ($workspace) {
        $skinName = $workspace->getSkinName();
        $skinConfiguration = $workspace->getSkinConfiguration();
        foreach ($skinConfiguration as $key => $configuration) {
            if (isset($configuration['type']) && $configuration['type'] == 'color') {
                bab_Registry::override('/' . $skinName . '/' . $key, $configuration['value']);
            }
            if (isset($configuration['type']) && $configuration['type'] == 'image') {
                if (! empty($configuration['value']) && $configuration['value'] != 'images/addons/workspace/' . $workspace->delegation . '/') {
                    bab_Registry::override('/' . $skinName . '/' . $key, $configuration['value']);
                }
            }
        }
        bab_Registry::override('/' . $skinName . '/contextMenu/rootNode', 'workspace_' . $workspace->delegation);

        bab_Registry::override('/workspace/currentWorkspace', $workspace->id);
    }
}



/**
 * Automatically add a user to db directory when he is attached to a workspace group.
 *
 * @param bab_eventUserAttachedToGroup $event
 */
function workspace_onUserAttachedToGroup(bab_eventUserAttachedToGroup $event)
{
    require_once dirname(__FILE__) . '/workspaces.php';
    require_once dirname(__FILE__) . '/directories.php';

    $groupId = $event->id_group;
    $userId = $event->id_user;
    $workspaceId = workspace_getWorkspaceIdByGroupId($groupId);
    if($workspaceId){
        $directoryId = workspace_getWorkspaceDirectoryId($workspaceId);

        $entry = workspace_getDirectoryEntryByUserId($userId);

        if ($entry['photo'] instanceof bab_dirEntryPhoto) {
            $photoData = $entry['photo']->getData();
        } else {
            $photoData = '';
        }
        bab_debug('G: "' . $groupId . '" W: "' .$workspaceId. '" D:' . $directoryId);

        workspace_addDbContact($directoryId, $entry, $photoData, $userId);
    }
}


/**
 * This function is called back when the ovidentia sitemap is refreshed.
 * It will add sitemap entries for the workspace addon.
 */
function workspace_onSiteMapItems(bab_eventBeforeSiteMapCreated $event)
{
    require_once dirname(__FILE__) . '/workspaces.php';
    require_once dirname(__FILE__) . '/workspace.ctrl.php';

    bab_Functionality::includefile('Icons');

    $mergeAddonsMenu = bab_Registry::get('/core/sitemap/mergeAddonsMenu', false);
    if ($mergeAddonsMenu) {
        $adminPosition = array('root', 'DGAll', 'babAdmin');
        $userPosition = array('root', 'DGAll', 'babUser');
    } else {
        $adminPosition = array('root', 'DGAll', 'babAdmin', 'babAdminSectionAddons');
        $userPosition = array('root', 'DGAll', 'babUser', 'babUserSectionAddons');
    }

    //Add a link in the admin section to admin the workspaces
    $item = $event->createItem('workspace_admin');
    $item->setPosition($adminPosition);
    $item->setLabel(workspace_translate('Workspaces'));
    $item->setLink(workspace_Controller()->Admin()->displayList()->url(), 0);
    $item->addIconClassname(Func_Icons::APPS_SUMMARY);
    $event->addFunction($item);
    $App = workspace_App();
    $set = $App->WorkspaceSet();

    $workspaces = $set->select(
        $set->delegation->isNot(0)
        ->_AND_($set->isReadable())
    )->orderAsc($set->name);

    if ($workspaces->count() > 0) {
        $item = $event->createItem('workspace_workspaces');
        $item->setPosition($userPosition);
        $item->setLabel(workspace_translate('My workspaces'));
//        $item->setLink(workspace_Controller()->Page()->display('home')->url(), 0);
        $item->addIconClassname(Func_Icons::APPS_SUMMARY);
        $event->addFolder($item);
    }

    // Initialize the sitemap for each workspaces
    foreach ($workspaces as $index => $workspace) {
        if ($index == 0) {
            // Add a link in the user section to access the workspaces


        }
        $workspace->initializeSiteMap($event);
    }
}


function workspace_copyDelegations()
{
    global $babDB;

    $App = workspace_App();
    $workspaceSet = $App->WorkspaceSet();
    $registry = new bab_Registry();

    $nbCopied = 0;
    $workspaces = $registry->toArray('workspace/workspaces/');

    foreach ($workspaces as $key => $values) {
        if ($registry->isDirectory('workspace/workspaces/' . $key)) {
            $workspace = $workspaceSet->get($workspaceSet->delegation->is($key));
            // We make a workspace clone only if the delegation has not already been copied
            if (! isset($workspace)) {
                $res = $babDB->db_query("select * from " . BAB_DG_GROUPS_TBL . " WHERE id=" . $babDB->quote($key));
                // We get info directly from the delegation, because some datas are not in the registry
                while ($arr = $babDB->db_fetch_assoc($res)) {
                    // Copy settings
                    $workspace = $workspaceSet->newRecord();
                    $workspace->delegation = $key;
                    $workspace->name = $arr['name'];
                    $workspace->description = $arr['description'];
                    $workspace->color = $arr['color'];
                    $workspace->group = $arr['id_group'];
                    $workspace->category = $arr['iIdCategory'];
                    $workspace->skin = $values['skin'];
                    $workspace->skinStyles = $values['skin_styles'];
                    $workspace->save();

                    // Add functionalities
                    $workspaceAddons = $App->getWorkspaceAddons();
                    foreach ($workspaceAddons as $addonName) {
                        // Previously, every workspaces had a file system and a directory
                        // If filemanager and directorymanager addons are available, we try to add those functionalities to the cloned workspace
                        if ($addonName == 'FileManager' || $addonName == 'DirectoryManager') {
                            $addon = bab_functionality::get('WorkspaceAddon/' . $addonName);
                            if ($addon) {
                                $addon->setDefaultConfiguration($workspace->id);
                            }
                        }
                    }
                    $nbCopied ++;
                }
            }
        }
    }

    if ($nbCopied > 0) {
        bab_installWindow::message(bab_toHtml(sprintf(workspace_translate('%d workspace has been cloned', '%d workspaces have been cloned', $nbCopied), $nbCopied)));
    }
}
