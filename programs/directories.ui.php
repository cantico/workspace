<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/workspacewidgets.php';
require_once dirname(__FILE__) . '/workspaces.php';
require_once dirname(__FILE__) . '/directories.php';






/**
 * @return Widget_SimpleTreeview
 */
function workspace_DirectoryTreeView()
{
    require_once dirname(__FILE__) . '/files.ctrl.php';
    require_once dirname(__FILE__) . '/files.php';

    $W = bab_Widgets();

    $treeview = $W->SimpleTreeview('workspace_directory_treeview');
    $treeview->addClass(Func_Icons::ICON_LEFT_16);

    $treeview->hideToolbar()
        ->setPersistent(true);

    $allEntries = $treeview->createElement(
        'all',
        'directory',
        workspace_translate('All entries'),
        workspace_translate('All entries'),
        workspace_Controller()->Directories()->displayEntries('all')->url()
    );
//	$allEntries->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/forum.png');
    $icon = $W->Icon(workspace_translate('All entries'), Func_Icons::OBJECTS_GROUP);

    $allEntries->setItem($icon);

    $treeview->appendElement($allEntries, null);

    $memberEntries = $treeview->createElement(
        'members',
        'directory',
        workspace_translate('Workspace members'),
        workspace_translate('Workspace members'),
        workspace_Controller()->Directories()->displayEntries('members')->url()
    );
//	$memberEntries->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/folder.png');
    $icon = $W->Icon(workspace_translate('Workspace members'), Func_Icons::PLACES_FOLDER);
    $memberEntries->setItem($icon);

    $treeview->appendElement($memberEntries, 'all');

    $memberEntries = $treeview->createElement(
        'administrator',
        'directory',
        workspace_translate('Administrators'),
        workspace_translate('Workspace administrators'),
        workspace_Controller()->Directories()->displayEntries('administrator')->url()
    );
//	$memberEntries->setIcon($GLOBALS['babSkinPath'] . 'images/addons/workspace/administrator_icon.png');
    $icon = $W->Icon(workspace_translate('Workspace administrators'), 'status-workspace-administrator');
    $memberEntries->setItem($icon);

    $treeview->appendElement($memberEntries, 'members');

    $memberEntries = $treeview->createElement(
        'writer',
        'directory',
        workspace_translate('Writers'),
        workspace_translate('Workspace writers'),
        workspace_Controller()->Directories()->displayEntries('writer')->url()
    );
//	$memberEntries->setIcon($GLOBALS['babSkinPath'] . 'images/addons/workspace/writer_icon.png');
    $icon = $W->Icon(workspace_translate('Workspace writers'), 'status-workspace-writer');
    $memberEntries->setItem($icon);
    $treeview->appendElement($memberEntries, 'members');

    $memberEntries = $treeview->createElement(
        'reader',
        'directory',
        workspace_translate('Readers'),
        workspace_translate('Workspace readers'),
        workspace_Controller()->Directories()->displayEntries('reader')->url()
    );
//	$memberEntries->setIcon($GLOBALS['babSkinPath'] . 'images/addons/workspace/reader_icon.png');
    $icon = $W->Icon(workspace_translate('Workspace readers'), 'status-workspace-reader');
    $memberEntries->setItem($icon);
    $treeview->appendElement($memberEntries, 'members');

    $contactEntries = $treeview->createElement(
        'contacts',
        'directory',
        workspace_translate('Other contacts'),
        workspace_translate('Other contacts'),
        workspace_Controller()->Directories()->displayEntries('contacts')->url()
    );
//	$contactEntries->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/folder.png');
    $icon = $W->Icon(workspace_translate('Other contacts'), Func_Icons::PLACES_FOLDER);
    $contactEntries->setItem($icon);
    $treeview->appendElement($contactEntries, 'all');


    return $treeview;
}


/**
 *
 * @param array $entry
 * @param bool
 * @return Widget_Frame
 */
function workspace_detailedContactEntry(array $entry, $entryId, $userId = null, $workspaceId = null, $directoryId = null) //$isAdministrator = false)
{
    $W = bab_Widgets();

    $entryIcon = $W->Icon('');
    $entryIcon->setStockImage(Func_Icons::APPS_USERS);
    if ($entryId == null) {
        $entryId = workspace_getDirEntryFromUserId(0, $userId);
        $entry = bab_getDirEntry($entryId, BAB_DIR_ENTRY_ID);
    }
    $T = bab_functionality::get('Thumbnailer', false);
    if ($T) {
        if (isset($entry['jpegphoto']['photo'])) {
            $photo = $entry['jpegphoto']['photo'];

            if ($photo instanceof bab_dirEntryPhoto) {
                 $T->setSourceBinary($photo->getData(), $photo->lastUpdate());
            }

            $T->setBorder(1, '#cccccc');
            $imageUrl = $T->getThumbnail(58, 58);
            $entryIcon->setImageUrl($imageUrl);
        }
        /* !!! A client of Cantico have asked to not show a default image */
        //$defaultImagePath = $GLOBALS['babInstallPath'] . 'skins/ovidentia/images/addons/workspace/user_default.png';
        //$T->setSourceFile($defaultImagePath);
    }


    $givenname = $entry['givenname']['value'];
    $sn = $entry['sn']['value'];
    $email = $entry['email']['value'];
    $organization = $entry['organisationname']['value'];
    $departmentnumber = $entry['departmentnumber']['value'];
    $address = $entry['bstreetaddress']['value'];
    $postalcode = $entry['bpostalcode']['value'];
    $city = $entry['bcity']['value'];
    $country = $entry['bcountry']['value'];
    $phone = $entry['btel']['value'];
    $fax = $entry['bfax']['value'];
    $mobile = $entry['mobile']['value'];


    $fullnameLabel = $W->Label(bab_composeUserName($givenname, $sn));
    if (isset($entry['userid'])) {
        $fullnameLabel->addClass('bab-user-' . $entry['userid']);
    }
    $fullnameLabel->addClass('fn');
    $fullnameLabel = $W->Link($fullnameLabel, workspace_Controller()->Directories()->displayEntry($entryId));

    $organizationLabel = $W->Label($organization)->addClass('org');
    $departmentnumberLabel = $W->Label($departmentnumber)->addClass('org');
    $emailLabel = $W->Label(workspace_translate('Email: ') . $email)->addClass('email');
    $streetaddressLabel = $W->Label($address)->addClass('street-address');
    $postalcodeLabel = $W->Label($postalcode)->addClass('postal-code');
    $cityLabel = $W->Label($city)->addClass('locality');
    $countryLabel = $W->Label($country)->addClass('country');
    $phoneLabel = !$phone ? $W->Label() : $W->Label(workspace_translate('Phone: ') . $phone)->addClass('phone');
    $faxLabel = !$fax ? $W->Label() : $W->Label(workspace_translate('Fax: ') . $fax)->addClass('fax');
    $mobileLabel = !$mobile ? $W->Label() : $W->Label(workspace_translate('Mobile: ') . $mobile)->addClass('phone');

    $info = $W->VBoxLayout();

    $info->addItem($fullnameLabel);

    if (isset($userId)) {
        if (workspace_userIsWorkspacePermanentAdministrator($userId, $workspaceId)) {
            $info->addItem($W->Label(workspace_translate('Workspace permanent administrator')));
        } elseif (workspace_userIsWorkspaceAdministrator($userId, $workspaceId)) {
            $info->addItem($W->Label(workspace_translate('Workspace administrator')));
        }
        if (workspace_userIsWorkspaceAdministrator(null, $workspaceId)) {
            $fullnameLabel->setTitle(workspace_translate('Identifier:') . ' ' . bab_getUserNickname($userId));
        }
    }

    $info->addItem($organizationLabel);
    $info->addItem($departmentnumberLabel);
    $info->addItem(
        $W->VBoxItems(
            $streetaddressLabel,
            $W->HBoxItems(
                $postalcodeLabel,
                $cityLabel
            )->setHorizontalSpacing(0.5, 'em'),
            $countryLabel
        )->addClass('adr')
    );


    $info2 = $W->VBoxLayout();

    if (!workspace_getWorkspacesDisableEmailValue()) {
        $info2->addItem($emailLabel);
    }
    $info2->addItem($phoneLabel);
    $info2->addItem($faxLabel);
    $info2->addItem($mobileLabel);

    $entryFrame = $W->Frame()->setLayout(
        $W->VBoxItems(
            $W->HBoxItems(
                $entryIcon,
                $info
            )->setHorizontalSpacing(1, 'em'),
            $info2
        )->addClass(Func_Icons::ICON_LEFT_64, 'vcard')
     );

    return $entryFrame;
}



/**
 * Creates a frame with information about the directory entry.
 *
 * @param array	$dirEntry
 *
 * @return Widget_Frame
 */
function workspace_createFullContactEntry(array $entry)
{
    $W = bab_Widgets();
    $T = bab_functionality::get('Thumbnailer', false);

    $entryIcon = $W->Icon('');

    $entryIcon->setStockImage(Func_Icons::APPS_USERS);


    if ($T) {
        if (isset($entry['photo'])) {
            $photo = $entry['photo'];

            if ($photo instanceof bab_dirEntryPhoto) {
                 $T->setSourceBinary($photo->getData(), $photo->lastUpdate());
            }
        } else {
            $defaultImagePath = $GLOBALS['babInstallPath'] . 'skins/ovidentia/images/addons/workspace/user_default.png';
            $T->setSourceFile($defaultImagePath);
        }
        $T->setBorder(1, '#cccccc');
        $imageUrl = $T->getThumbnail(124, 124);
        $entryIcon->setImageUrl($imageUrl);
    }


    $givenname = $entry['givenname'];
    $sn = $entry['sn'];
    $organization = $entry['organisationname'];
    $departmentnumber = $entry['departmentnumber'];

    $address = $entry['bstreetaddress'];
    $postalcode = $entry['bpostalcode'];
    $city = $entry['bcity'];
    $country = $entry['bcountry'];

    $email = $entry['email'];

    $phone = $entry['btel'];
    $fax = $entry['bfax'];
    $mobile = $entry['mobile'];


    $fullnameLabel = $W->Link($W->Title($givenname . ' ' . $sn, 2)->addClass('fn'), workspace_Controller()->Directories()->displayEntry($entry['id']));
    $organizationLabel = $W->Label($organization)->addClass('org');
    $departmentnumberLabel = $W->Label($departmentnumber);

    // Postal business address
    $streetaddressLabel = $W->Label($address)->addClass('street-address');
    $postalcodeLabel = $W->Label($postalcode)->addClass('postalcode');
    $cityLabel = $W->Label($city)->addClass('locality');
    $countryLabel = $W->Label($country)->addClass('country');

    // Email
    $emailLabel = $W->Label($email)->addClass('email');

    // Phone numbers
    $phoneLabel = !$phone ? $W->Label() : $W->Label(workspace_translate('Phone: ') . $phone)->addClass('phone');
    $faxLabel = !$fax ? $W->Label() : $W->Label(workspace_translate('Fax: ') . $fax)->addClass('fax');
    $mobileLabel = !$mobile ? $W->Label() : $W->Label(workspace_translate('Mobile: ') . $mobile)->addClass('phone');

    $info = $W->VBoxItems(
        $fullnameLabel,
        $organizationLabel,
        $departmentnumberLabel,
        $W->VBoxItems(
            $streetaddressLabel,
            $W->HBoxItems(
                $postalcodeLabel,
                $cityLabel
             )->setHorizontalSpacing(0.5, 'em'),
            $countryLabel
        )->addClass('adr'),
        $emailLabel,
        $W->VBoxItems(
            $phoneLabel,
            $faxLabel,
            $mobileLabel
        )->addClass('workspace-phones')
    );

    $entryFrame = $W->Frame()->setLayout($W->VBoxLayout())
                    ->addClass('icon-top-128')->addClass('icon-128x128')->addClass('icon-top')
                    ->addClass('vcard')
                    ->addItem(
                        $W->HBoxLayout()->setHorizontalSpacing(1, 'em')
                            ->addItem($entryIcon)
                            ->addItem($info)
                    )
                    ;

    return $entryFrame;
}


/**
 * Creates a frame with information about the directory entry.
 *
 * @param array	$dirEntry
 *
 * @return Widget_Frame
 */
function workspace_createContactEntry(array $dirEntry)
{
    $W = bab_Widgets();
    $T = bab_functionality::get('Thumbnailer');

    $entryIcon = $W->Icon('');

    $entryIcon->setStockImage(Func_Icons::APPS_USERS);

    if (isset($dirEntry['jpegphoto']['photo'])) {
        $photo = $dirEntry['jpegphoto']['photo'];

        if ($T && $photo instanceof bab_dirEntryPhoto) {
             $T->setSourceBinary($photo->getData(), $photo->lastUpdate());
            $imageUrl = $T->getThumbnail(96, 48);
            $entryIcon->setImageUrl($imageUrl);
        }
    }

    $givenname = $dirEntry['givenname']['value'];
    $sn = $dirEntry['sn']['value'];
    $email = $dirEntry['email']['value'];
    $address = $dirEntry['bstreetaddress']['value'];
    $postalcode = $dirEntry['bpostalcode']['value'];
    $city = $dirEntry['bcity']['value'];
    $country = $dirEntry['bcountry']['value'];
    $phone = $dirEntry['btel']['value'];

    $fullnameLabel = $W->Title($givenname . ' ' . $sn, 5)->addClass('fn');
//	$fullnameLabel = $W->Link($W->Title($givenname . ' ' . $sn, 5)->addClass('fn'), workspace_Controller()->Directories()->displayEntry($entry['id']));
    $emailLabel = $W->Label($email)->addClass('email');
    $addressLabel = $W->Label($address)->addClass('street-address');
    $postalcodeLabel = $W->Label($postalcode)->addClass('postalcode');
    $cityLabel = $W->Label($city)->addClass('locality');
    $countryLabel = $W->Label($country)->addClass('country');
    $phoneLabel = $W->Label($phone)->addClass('phone');

    $address = 	$W->VBoxItems(
                    $addressLabel,
                    $W->HBoxItems(
                        $postalcodeLabel,
                        $cityLabel)->setHorizontalSpacing(0.5, 'em'),
                    $countryLabel
                )->addClass('adr');

    $entry = $W->Frame()->setLayout(
                    $W->HBoxItems(
                        $entryIcon,
                        $W->VBoxItems(
                            $fullnameLabel,
                            $W->VBoxItems(
                                $W->VBoxItems(
                                    $emailLabel->addClass('workspace-email'),
                                    $phoneLabel->addClass('workspace-phone')
                                ),
                                $address
                            )->setVerticalSpacing(0.5, 'em')
                        )
                    )->setHorizontalSpacing(1, 'em')
                    ->addClass('icon-left-48')->addClass('icon-48x48')->addClass('icon-left')
                    ->addClass('vcard')
                );


    return $entry;
}





/**
 * @param int	$groupId
 * @param int	$directoryId
 */
function workspace_DirectoryListView($directoryId, $groupId, $filter)
{
    global $babDB;

    require_once dirname(__FILE__) . '/directories.ctrl.php';

    $W = bab_Widgets();

    $toolbar = workspace_Toolbar('main-toolbar');

    $currentUserId = bab_getUserId();
    $workspaceId = workspace_getCurrentWorkspace();
    $autoUpdate = workspace_getWorkspaceUpdateDirectory($workspaceId);

    if (workspace_canAddEntriesToDirectory()) {
        $newEntryIcon = $W->Icon(workspace_translate('New entry'), Func_Icons::ACTIONS_USER_PROPERTIES);
        $newEntryButton = $W->Link($newEntryIcon, workspace_Controller()->Directories()->edit());

        $toolbar->addItem($newEntryButton);
    }

    if (workspace_userIsWorkspaceAdministrator()) {
        $addUserIcon = $W->Icon(workspace_translate('Add a member'), Func_Icons::ACTIONS_LIST_ADD_USER);
        $addUserButton = $W->Link($addUserIcon, workspace_Controller()->Directories()->selectMember());

        $toolbar->addItem($addUserButton);
    }

    $directoryEntryPanel = $W->Frame()->setLayout($W->VBoxLayout())->addClass('workspace-directory-entries-panel');


    // Workspaces members section

    $memberEmailAddresses = array();

    if ($filter !== 'contacts') {

        if ($filter === 'administrator') {
            $sectionTitle = workspace_translate('Workspace administrators');
        } else if ($filter === 'writer') {
            $sectionTitle = workspace_translate('Workspace writers');
        } else if ($filter === 'reader') {
            $sectionTitle = workspace_translate('Workspace readers');
        } else {
            $sectionTitle = workspace_translate('Workspace members');
        }
        $userSubPanel = $W->Section(
            $sectionTitle,
            $W->VBoxLayout(),
            3
        )->setFoldable(true);


        $userListView = $W->Frame('workspace_users_listview');
        $userListView->setLayout($W->FlowLayout()->setVerticalAlign('top'));

        $users = bab_getGroupsMembers($groupId);

        bab_Sort::asort($users, 'name', bab_Sort::CASE_INSENSITIVE);

        $userIsWorkspaceAdministrator = workspace_userIsWorkspaceAdministrator($currentUserId, $workspaceId);
        $canUpdateUsersProfile = workspace_canUpdateUsersProfile($currentUserId, $workspaceId);
        $workspacesDisableEmailValue = workspace_getWorkspacesDisableEmailValue();


        if ($users) {

            foreach ($users as $user) {

                $userProfile = workspace_getUserProfile($user['id'], $workspaceId);

                $uptodate = workspace_dirEntryIsUpToDate($user['id'], $directoryId);
                if ($autoUpdate && !$uptodate) {
                    workspace_updateUserDirectoryEntryFromPortal($user['id'], $directoryId);
                }

                if ($filter !== 'all' && $filter !== 'members' && $userProfile !== $filter) {
                    continue;
                }
                $dirEntryId = workspace_getDirEntryFromUserId($directoryId, $user['id']);
                if ($dirEntryId == null) {
                    //$dirEntry = $user;
                    $dirEntryId = workspace_getDirEntryFromUserId(0, $user['id']);
                }
                $dirEntry = bab_getDirEntry($dirEntryId, BAB_DIR_ENTRY_ID);

                if (!is_array($dirEntry)) {
                    continue;
                }

                $entry = $W->Frame(null, $W->HBoxLayout())->addClass('workspace-directory-entry');


                if (!$autoUpdate && !$uptodate) {
                    $entry->addClass('workspace-entry-updated');
                    $entry->setTitle(workspace_translate('The global directory entry is more recent than this one.'));
                }

                $entryInfo = workspace_detailedContactEntry($dirEntry, $dirEntryId, $user['id'], $workspaceId, $directoryId)
                    ->addClass('workspace-directory-entry-info');

                $entry->addItem($entryInfo);

                if (isset($userProfile)) {
                    $entry->addClass('workspace-profile-' . $userProfile);
                }
                if (workspace_userIsWorkspacePermanentAdministrator($user['id'], $workspaceId)) {
                    $entry->addClass('workspace-permanent');
                }

                $userListView->addItem($entry);

                $menu = $W->Menu()
                    ->setLayout($W->FlowLayout())
                    ->addClass(Func_Icons::ICON_LEFT_16);

                if ($dirEntryId == null) {
                    $address = '';
                } else {
                    $address = $dirEntry['bstreetaddress']['value']
                        . ', ' . $dirEntry['bpostalcode']['value']
                        . ' ' . $dirEntry['bcity']['value']
                        . ' ' . $dirEntry['bcountry']['value'];
                }


                if (workspace_canUpdateEntriesFromMember($user['id'], $currentUserId, $workspaceId)) {
                    $link = $W->Link(
                        workspace_translate('Edit directory entry'),
                        workspace_Controller()->Directories()->edit($dirEntryId, 0, $user['id'])
                    )->addClass('icon', Func_Icons::ACTIONS_USER_PROPERTIES);
                    $menu->addItem($link);

                    if (!$autoUpdate && !$uptodate && $canUpdateUsersProfile) {
                        $link = $W->Link(
                            workspace_translate('Update directory entry'),
                            workspace_Controller()->Directories()->updateFromPortal($user['id'])
                        )->addClass('icon', Func_Icons::ACTIONS_VIEW_REFRESH);
                        $menu->addItem($link);
                    }
                }
                if ($dirEntryId == null) {
                    if (bab_isEmailValid($dirEntry['email']) && !$workspacesDisableEmailValue) {
                        $link = $W->Link(
                            workspace_translate('Send an email'),
                            workspace_Controller()->Directories()->editEmail($dirEntry['email'])
                        )->addClass('icon', Func_Icons::ACTIONS_MAIL_SEND);
                        $menu->addItem($link);
                    }
                } else {
                    if (!empty($dirEntry['bcity']['value']) || !empty($dirEntry['bpostalcode']['value'])) {
                        $link = $W->Link(
                            workspace_translate('Locate on map'),
                            workspace_Controller()->Directories()->locate($address)
                        )->addClass('icon', Func_Icons::ACTIONS_EDIT_FIND);
                        $menu->addItem($link->setOpenMode(Widget_Link::OPEN_POPUP));
                    }
                    if (bab_isEmailValid(trim($dirEntry['email']['value']))) {
                        $link = $W->Link(
                            workspace_translate('Send an email'),
                            workspace_Controller()->Directories()->editEmail(trim($dirEntry['email']['value']))
                        )->addClass('icon', Func_Icons::ACTIONS_MAIL_SEND);
                        $menu->addItem($link);
                    }
                }

                if ($userIsWorkspaceAdministrator && !workspace_userIsWorkspacePermanentAdministrator($user['id'], $workspaceId)) {
                    if ($user['id'] != $GLOBALS['BAB_SESS_USERID']) {

                        $message = sprintf(
                            workspace_translate('Remove \'%s\' from the workspace members?'),
                            bab_composeUserName($dirEntry['givenname']['value'], $dirEntry['sn']['value'])
                        );

                        $link = $W->Link(
                            workspace_translate('Remove from workspace'),
                            workspace_Controller()->Directories()->removeMember($user['id'])
                        )->addClass('icon', Func_Icons::ACTIONS_LIST_REMOVE_USER)
                        ->setConfirmationMessage($message);
                        $menu->addItem($link);

                        if ($userProfile !== 'administrator') {
                            $link = $W->Link(
                                workspace_translate('Set administrator profile'),
                                workspace_Controller()->Directories()->setProfile($user['id'], 'administrator')
                            )->addClass('icon', 'status-workspace-administrator');
                            $menu->addItem($link);
                        }

                        if ($userProfile !== 'writer') {
                            $link = $W->Link(
                                workspace_translate('Set writer profile'),
                                workspace_Controller()->Directories()->setProfile($user['id'], 'writer')
                            )->addClass('icon', 'status-workspace-writer');
                            $menu->addItem($link);
                        }

                        if ($userProfile !== 'reader') {
                            $link = $W->Link(
                                workspace_translate('Set reader profile'),
                                workspace_Controller()->Directories()->setProfile($user['id'], 'reader')
                            )->addClass('icon', 'status-workspace-reader');
                            $menu->addItem($link);
                        }

                    }
                }

                $entry->addItem($menu);
                if ($dirEntryId == null) {
                    if (!empty($dirEntry['email'])) {
                        $memberEmailAddresses[$dirEntry['email']] = $dirEntry['email'];
                    }
                } else {
                    if (!empty($dirEntry['email']['value'])) {
                        $memberEmailAddresses[$dirEntry['email']['value']] = $dirEntry['email']['value'];
                    }
                }
            }
        }
        $userSubPanel->addItem($userListView->setSizePolicy(Widget_SizePolicy::MAXIMUM));

        $directoryEntryPanel->addItem($userSubPanel);

        switch ($filter) {
            case 'reader':
                $sendEmailLabel = workspace_translate('Send an email to all readers');
                break;
            case 'writer':
                $sendEmailLabel = workspace_translate('Send an email to all writers');
                break;
            case 'administrator':
                $sendEmailLabel = workspace_translate('Send an email to all administrators');
                break;
            case 'all':
            case 'members':
            default:
                $sendEmailLabel = workspace_translate('Send an email to all members');
                break;
        }
         $sendEmailIcon = $W->Icon($sendEmailLabel, Func_Icons::ACTIONS_MAIL_SEND);
         $sendEmailButton = $W->Link($sendEmailIcon, workspace_Controller()->Directories()->editEmailForSelectedMembers($filter));


        //$W->Link($sendEmailIcon, workspace_Controller()->Directories()->editEmail($recipients));


        if (!$workspacesDisableEmailValue) {
            $toolbar->addItem($sendEmailButton);
        }
    }


    // Directory contact entries section

    if ($filter === 'contacts' || $filter === 'all') {
        $contactSubPanel = $W->Section(
            workspace_translate('Other contacts'),
            $W->VBoxLayout(),
            3
        )->setFoldable(true);

        $contactlistView = $W->Frame('workspace_entries_listview')
            ->setLayout($W->FlowLayout());

        include_once $GLOBALS['babInstallPath'].'utilit/searchapi.php';
        $realm = bab_Search::getRealm('bab_SearchRealmDirectories');
        $realm->setAccessRightsVerification(false);
        $realm->setDirectory($directoryId);

        $criteria = $realm->getDefaultCriteria();

        $dirEntries = array();
        $sortedEntries = array();

        foreach($realm->search($criteria) as $record) {
            $dirEntries[$record->id] = array();
            foreach ($realm->getFields() as $field) {
                if ($field->searchable()) {
                    $fieldname = $field->getName();
                    $dirEntries[$record->id][$fieldname] = array(
                        'name' => $field->getDescription(),
                        'value' => $record->$fieldname
                    );
                    $sortedEntries[$record->id] = bab_composeUserName($record->givenname, $record->sn);
                }
            }
        }

        bab_Sort::natcasesort($sortedEntries, bab_Sort::CASE_INSENSITIVE);


        foreach ($sortedEntries as $dirEntryId => $fullname) {

            //// Just to ensure the entry is not associated to a user.
            $sql = '
                SELECT e.id, e.id_user
                FROM `bab_dbdir_entries` e
                WHERE e.id_directory IN(' .  $babDB->quote($directoryId) . ') AND e.id=' .  $babDB->quote($dirEntryId);

            $entries = $babDB->db_query($sql);
            if ($entries) {
                $entry =  $babDB->db_fetch_assoc($entries);
                if ($entry['id_user']) {
                    continue;
                }
            }
            ////

            $dirEntry = bab_admGetDirEntry($dirEntryId, BAB_DIR_ENTRY_ID, workspace_getWorkspaceDirectoryId($workspaceId));
            $entry = $W->Frame()->setLayout(
                $W->HBoxItems(
                    workspace_detailedContactEntry($dirEntry, $dirEntryId, null, $workspaceId)
                        ->addClass('workspace-directory-entry-info')
                )
            );
            $entry->addClass('workspace-directory-entry');


            $contactlistView->addItem($entry);

            $menu = $W->Menu()
                    ->setLayout($W->FlowLayout())
                    ->addClass(Func_Icons::ICON_LEFT_16);

            $address = $dirEntry['bstreetaddress']['value']
                 . ', ' . $dirEntry['bpostalcode']['value']
                 . ' ' . $dirEntry['bcity']['value']
                 . ' ' . $dirEntry['bcountry']['value'];

            if (!empty($dirEntry['bcity']['value']) || !empty($dirEntry['bpostalcode']['value'])) {
                $link = $W->Link(
                    workspace_translate('Locate on map'),
                    workspace_Controller()->Directories()->locate($address)
                )->addClass('icon', Func_Icons::ACTIONS_EDIT_FIND)
                ->setOpenMode(Widget_Link::OPEN_POPUP);
                $menu->addItem($link);
            }
            if (bab_isEmailValid($dirEntry['email']['value'])) {
                $link = $W->Link(
                    workspace_translate('Send an email'),
                    workspace_Controller()->Directories()->editEmail($dirEntry['email']['value'])
                )->addClass('icon', Func_Icons::ACTIONS_MAIL_SEND);
                $menu->addItem($link);
            }
            if (workspace_canUpdateEntriesFromDirectory($directoryId)) {
                $link = $W->Link(
                    workspace_translate('Edit directory entry'),
                    workspace_Controller()->Directories()->edit($dirEntryId)
                )->addClass('icon', Func_Icons::ACTIONS_USER_PROPERTIES);
                $menu->addItem($link);
            }
            if (workspace_canDeleteEntriesFromDirectory($directoryId)) {
                $link = $W->Link(
                    workspace_translate('Delete directory entry'),
                    workspace_Controller()->Directories()->delete($dirEntryId)
                )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE);
                $link->setConfirmationMessage(workspace_translate('Are you sure you want to delete this directory entry?'));
                $menu->addItem($link);
            }

            $entry->addItem($menu);
        }
        $contactSubPanel->addItem($contactlistView);

        $directoryEntryPanel->addItem($contactSubPanel);
    }



    $treeview = workspace_DirectoryTreeView();
    $treeview->addClass('workspace-resizable-panel');
    $treeview->setSizePolicy('workspace-side-panel minimum');
//	$treeview->setSizePolicy(Widget_SizePolicy::MINIMUM);

    $treeview->highlightElement($filter);

    $splitview = $W->Frame('workspace_directory_splitview', $W->HBoxLayout()->addClass('expand'))
                            ->addItem($treeview)
                            ->addItem($directoryEntryPanel)
                            ;

    $explorer = $W->Frame('directory_explorer')
                        ->setLayout($W->VBoxLayout())
                        ->addItem($toolbar)
//						->addItem($locationbar)
                        ->addItem($splitview)
                        ;
    return $explorer;
}




define('WORKSPACE_HOME_ADDRESS', 'h');
define('WORKSPACE_BUSINESS_ADDRESS', 'b');
/**
 *
 * @return Widget_Frame
 */
function workspace_addressEditor($addressType = WORKSPACE_BUSINESS_ADDRESS)
{
    $W = bab_Widgets();
    $addressFrame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(0.5, 'em'));

    $postalCodeInput = $W->SuggestPostalCode()->setSize(8)->setName($addressType . 'postalcode');
    $cityInput = $W->SuggestPlaceName()->setSize(40)->setName($addressType . 'city');
    $countryInput = $W->LineEdit()->setSize(20)->setName($addressType . 'country');

    $postalCodeInput->setRelatedPlaceName($cityInput);
    $cityInput->setRelatedPostalCode($postalCodeInput);

    $streetFormItem = workspace_FormField(workspace_translate('Street:'),
                            $W->LineEdit()->setSize(60)->setName($addressType . 'streetaddress'));
    $postalcodeFormItem = workspace_FormField(workspace_translate('Postal code:'),
                            $postalCodeInput);
    $cityFormItem = workspace_FormField(workspace_translate('City:'),
                            $cityInput);
    $stateFormItem = workspace_FormField(workspace_translate('State:'),
                            $W->LineEdit()->setSize(20)->setName($addressType . 'state'));
    $countryFormItem = workspace_FormField(workspace_translate('Country:'),
                            $countryInput);

    $addressFrame->addItem($streetFormItem);
    $addressFrame->addItem($W->HBoxItems($postalcodeFormItem, $cityFormItem)->setHorizontalSpacing(1, 'em'));
    $addressFrame->addItem($W->HBoxItems($stateFormItem, $countryFormItem)->setHorizontalSpacing(1, 'em'));

    return $addressFrame;
}


/**
 * @return Widget_Form
 */
function workspace_DirectoryEntryEditor($userId = null)
{
    $W = bab_Widgets();

    $form = $W->Form();
    $frame = new workspace_BaseForm('directory_entry_editor');
    $frame->setName('entry')
        ->addClass('workspace-dialog');

    $photoFormItem =  workspace_FormField(
        workspace_translate('Photo:'),
        $imagePicker = $W->ImagePicker()
            ->setId('workspace-photo')
            ->oneFileMode(true)
            ->setEncodingMethod(null)
            ->setDimensions(128, 128)
            ->setName('photo')
    );
    // empty temporary image path
    //$imagePicker->cleanup();


    $firstnameFormItem = workspace_FormField(
        workspace_translate('First name:'),
        $W->LineEdit()
            ->setSize(20)
            ->setName('givenname')
            ->setMandatory(true, workspace_translate('The first name must not be empty'))
    );
    $lastnameFormItem = workspace_FormField(
        workspace_translate('Last name:'),
        $W->LineEdit()->setSize(30)->setName('sn')
            ->setMandatory(true, workspace_translate('The last name must not be empty'))
    );

    $organizationFormItem = workspace_FormField(workspace_translate('Company:'), $W->LineEdit()->setSize(50)->setName('organisationname'));
    $departmentnumberFormItem = workspace_FormField(workspace_translate('Department:'), $W->LineEdit()->setSize(50)->setName('departmentnumber'));

    $phoneFormItem = workspace_FormField(workspace_translate('Phone:'), $W->LineEdit()->setSize(20)->setName('btel'));
    $faxFormItem = workspace_FormField(workspace_translate('Fax:'), $W->LineEdit()->setSize(20)->setName('bfax'));
    $mobileFormItem = workspace_FormField(workspace_translate('Mobile:'), $W->LineEdit()->setSize(20)->setName('mobile'));
    $emailFormItem = workspace_FormField(
        workspace_translate('Email:'),
        $emailLineEdit = $W->LineEdit()->setSize(60)->setName('email')
    );

    if (isset($userId)) {
        $emailLineEdit->setMandatory(true, workspace_translate('The email address must not be empty'));
    }

    $nameSection = $W->Section(
        workspace_translate('Contact information'),
        $W->VBoxItems(
            $W->HBoxItems(
                $firstnameFormItem,
                $lastnameFormItem
            )->setHorizontalSpacing(1, 'em'),
            $organizationFormItem,
            $departmentnumberFormItem,
            $W->FlowItems(
                $phoneFormItem,
                $mobileFormItem,
                $faxFormItem
            )->setHorizontalSpacing(1, 'em'),
            $emailFormItem
        )->setVerticalSpacing(0.5, 'em'),
        4
    )->setFoldable(true);//->addClass('compact');

    $addressSection = $W->Section(
        workspace_translate('Address'),
        workspace_addressEditor(WORKSPACE_BUSINESS_ADDRESS),
        4
    )->setFoldable(true);//->addClass('compact');

    $profileSection = $W->Section(
        workspace_translate('User profile'),
        $W->RadioSet()
            ->setHorizontalView(false)
            ->setName('profile')
            ->addOption('reader', workspace_translate('Reader'))
            ->addOption('writer', workspace_translate('Writer'))
            ->addOption('administrator', workspace_translate('Administrator')),
        4
    )->addClass('compact');


    $leftBox = 	$W->VBoxItems(
        $photoFormItem
    )->setVerticalSpacing(2, 'em')->addClass('icon-top-128 icon-128x128 icon-top');

    // We only propose to edit the entry's user profile if the entry is associated
    // to a user (not a simple contact entry) and the currently logged user has the
    // right to update this profile.
    if (isset($userId) && workspace_canUpdateUsersProfile()) {
        $leftBox->addItem($profileSection);
    }

    $rightBox = $W->VBoxItems(
        $nameSection,
        $addressSection
    )->setVerticalSpacing(2, 'em');

    $frame->addItem(
        $W->HBoxItems(
            $leftBox,
            $rightBox
        )->setHorizontalSpacing(4, 'em')
    );


    $frame->addButton(
        $W->SubmitButton('save')
            ->validate(true)
            ->setLabel(workspace_translate("Save entry"))
            ->setAction(workspace_Controller()->Directories()->save())
    )
    ->addButton(
        $W->SubmitButton('cancel')
            ->setLabel(workspace_translate("Cancel"))
            ->setAction(workspace_Controller()->Directories()->cancel())
    );

    $form->setLayout($W->VBoxLayout())->addItem($frame);

//	$form->setValues($entry);
    workspace_setSelfPageHiddenFields($form);

    return $form;
}







/**
 *
 * @param string	$recipients		A string of comma-separated email addresses.
 * @return Widget_Form
 */
function workspace_emailEditor($recipients)
{
    $W = bab_Widgets();

    $form = $W->Form();
    $frame = new workspace_BaseForm('email_editor');
    $frame->setName('email')
            ->addClass('workspace-dialog');

    $recipients = implode(',', $recipients);

    $recipientsFormItem = workspace_FormField(
        workspace_translate('Recipients:'),
        $W->TextEdit()
            ->setName('recipients')
            ->setLines(1)
            ->setColumns(100)
            ->setMandatory(true, workspace_translate('The recipients field must not be empty'))
    );
    $subjectFormItem = workspace_FormField(
        workspace_translate('Subject:'),
        $W->LineEdit()
            ->setName('subject')
            ->setSize(100)
            ->setMandatory(true, workspace_translate('The message subject must not be empty'))
    );
    $bodyFormItem = workspace_FormField(
        workspace_translate('Body:'),
        $W->SimpleHtmlEdit()
            ->setName('body')
            ->setLines(10)
            ->setColumns(100)
    );

    $frame->addItem($recipientsFormItem);
    $frame->addItem($subjectFormItem);
    $frame->addItem($bodyFormItem);


    $frame->addButton(
        $W->SubmitButton('send')
            ->validate(true)
            ->setLabel(workspace_translate('Send email'))
            ->setAction(workspace_Controller()->Directories()->sendEmail())
    );
    $frame->addButton(
        $W->SubmitButton('cancel')
            ->setLabel(workspace_translate('Cancel'))
            ->setAction(workspace_Controller()->Directories()->cancel())
    );

    $form->setLayout($W->VBoxLayout())->addItem($frame);

    workspace_setSelfPageHiddenFields($form);

    $form->setValue(array('email', 'recipients'), $recipients);

    return $form;
}





/**
 * Returns a form allowing the selection of an existing ovidentia user.
 *
 * @see workspace_memberSelector
 * @return widget_Form
 */




/**
 * Returns a form allowing the selection of an existing ovidentia user.
 *
 * @see workspace_memberSelector
 * @return widget_Form
 */
function workspace_memberSelectEditor()
{
    $W = bab_Widgets();

    $form = $W->Form();
    $frame = new workspace_BaseForm('workspace_member_select_editor');


    $nbUsers = 0;

    $userFormItem = workspace_FormField(
        workspace_translate('New member:'),
        $W->UserPicker()->setName(array('members', $nbUsers, 'id'))
    );

    $frame->addItem($userFormItem);

    $notifyFormItem = workspace_FormField(
        workspace_translate('Notify user'),
        $W->CheckBox()->setName('notifyUser')
    );

    $frame->addItem($notifyFormItem);

    $frame->addButton(
        $W->SubmitButton('save')
            ->validate(true)
            ->setAction(workspace_Controller()->Directories()->addMembers())
            ->setLabel(workspace_translate('Add user'))
    );
    $frame->addButton(
        $W->SubmitButton('cancel')
            ->setAction(workspace_Controller()->Directories()->Cancel())
            ->setLabel(workspace_translate('Cancel'))
    );

    $form->setLayout($W->VBoxLayout())->addItem($frame);

    $form->setHiddenValue('tg', bab_rp('tg'));

    return $form;
}




/**
 * Returns a form to edit information about a workspace member.
 *
 * @see workspace_memberSelector
 *
 * @return Widget_Form
 */
function workspace_memberEditor()
{
    $W = bab_Widgets();

    $form = $W->Form();
    $frame = new workspace_BaseForm('workspace_member_create_editor');
    $frame->addClass('workspace-dialog');

    $entryIcon = $W->Icon('')->setId('directory_entry_editor_icon');

    $entryIcon->setStockImage(Func_Icons::APPS_USERS);

//     $photoFormItem = workspace_FormField(
//         workspace_translate('Photo:'),
//         $W->Uploader()
//             ->setAcceptedMimeTypes('image/png,image/jpeg,image/gif')
//             ->setName('photo')
//             ->addClass('small')
//             ->setPreviewImageItem($entryIcon)
//     );

    $photoFormItem =  workspace_FormField(
        workspace_translate('Photo:'),
        $imagePicker = $W->ImagePicker()
            ->setId('workspace-photo')
            ->oneFileMode(true)
            ->setEncodingMethod(null)
            ->setDimensions(128, 128)
            ->setName('photo')
        );


    $firstnameFormItem = workspace_FormField(
        workspace_translate('First name:'),
        $W->LineEdit()->setSize(20)->setName('givenname')
            ->setMandatory(true, workspace_translate("The first name must not be empty"))
    );
    $lastnameFormItem = workspace_FormField(
        workspace_translate('Last name:'),
        $W->LineEdit()->setSize(30)->setName('sn')
            ->setMandatory(true, workspace_translate("The last name must not be empty"))
    );

    $organizationFormItem = workspace_FormField(
        workspace_translate('Company:'),
        $W->LineEdit()->setSize(50)->setName('organisationname')
    );

    $departmentnumberFormItem = workspace_FormField(
        workspace_translate('Department:'),
        $W->LineEdit()->setSize(50)->setName('departmentnumber')
    );


    $phoneFormItem = workspace_FormField(
        workspace_translate('Phone:'),
        $W->LineEdit()->setSize(20)->setName('btel')
    );
    $faxFormItem = workspace_FormField(
        workspace_translate('Fax:'),
        $W->LineEdit()->setSize(20)->setName('bfax')
    );
    $mobileFormItem = workspace_FormField(
        workspace_translate('Mobile:'),
        $W->LineEdit()->setSize(20)->setName('mobile')
    );
    $emailFormItem = workspace_FormField(
        workspace_translate('Email:'),
        $W->LineEdit()->setSize(60)->setName('email')
            ->setMandatory(true, workspace_translate('The email address must not be empty'))
    );

    $nicknameFormItem = workspace_FormField(
        workspace_translate('Identifier:'),
        $W->LineEdit()->setSize(20)->setName('identifier')
            ->setAutoComplete(false)
            ->setMandatory(true, workspace_translate('The identifier must not be empty'))
    );
    $passwordFormItem = workspace_FormField(
        workspace_translate('Password:'),
        $W->LineEdit()->setSize(20)->setName('password')
            ->setAutoComplete(false)
            ->obfuscate(true)
            ->setMandatory(true, workspace_translate('The password must not be empty'))
    );
    $password2FormItem = workspace_FormField(
        workspace_translate('Password (check):'),
        $W->LineEdit()->setSize(20)->setName('password2')
            ->setAutoComplete(false)
            ->obfuscate(true)
            ->setMandatory(true, workspace_translate('The password check must not be empty'))
    );
    $notifyFormItem = workspace_FormField(
        workspace_translate('Notify user'),
        $W->CheckBox()->setName('notifyuser')
    );
    $sendPwdFormItem = workspace_FormField(
        workspace_translate('Send password with email'),
        $W->CheckBox()->setName('sendpwd')
    );



    $nameSection = $W->Section(
        workspace_translate('Contact information'),
        $W->VBoxItems(
            $W->HBoxItems(
                $firstnameFormItem,
                $lastnameFormItem
            )->setHorizontalSpacing(1, 'em'),
            $organizationFormItem,
            $departmentnumberFormItem,
            $W->FlowItems(
                $phoneFormItem,
                $mobileFormItem,
                $faxFormItem
            )->setHorizontalSpacing(1, 'em'),
            $emailFormItem
        )->setVerticalSpacing(0.5, 'em'),
        4
    )->setFoldable(true);



    $addressSection = $W->Section(
        workspace_translate('Address'),
        workspace_addressEditor(WORKSPACE_BUSINESS_ADDRESS),
        4
    )->setFoldable(true);



    $credentialsSection = $W->Section(
        workspace_translate('Credentials'),
        $W->VBoxItems(
            $nicknameFormItem,
            $passwordFormItem,
            $password2FormItem,
            $notifyFormItem,
            $sendPwdFormItem
        )->setVerticalSpacing(0.5, 'em'),
        4
    )->setName('member')->addClass('compact');


    $leftBox = 	$W->VBoxItems(
        $entryIcon,
        $photoFormItem,
        $credentialsSection
    )->setVerticalSpacing(2, 'em')->addClass('icon-top-128 icon-128x128 icon-top');


    $rightBox = $W->Frame()->setLayout(
        $W->VBoxItems(
            $nameSection,
            $addressSection
        )->setVerticalSpacing(2, 'em')
    );

    $frame->addItem(
        $W->HBoxItems(
            $leftBox,
            $rightBox->setName('entry')
        )->setHorizontalSpacing(4, 'em')
    );

    $frame->addButton(
        $W->SubmitButton('create')
            ->validate(true)
            ->setAction(workspace_Controller()->Directories()->createMember())
            ->setLabel(workspace_translate('Create and add user'))
    );
    $frame->addButton(
        $W->SubmitButton('cancel_create')
            ->setAction(workspace_Controller()->Directories()->Cancel())
            ->setLabel(workspace_translate('Cancel'))
    );

    $form->setLayout($W->VBoxLayout())->addItem($frame);

    $form->setHiddenValue('tg', bab_rp('tg'));

    return $form;
}





/**
 * Displays a page to either select a existing user or create a new one.
 *
 * @param string	$openPanel 		One of 'member_selector_tab' or 'member_editor_tab'
 *
 * @see workspace_memberSelectEditor
 * @see workspace_memberEditor
 *
 * @return Widget_Tabs
 */
function workspace_memberSelector($openPanel = 'member_selector_tab')
{
    $W = bab_Widgets();

    $tabs = $W->Tabs();

    $memberSelectEditor = workspace_memberSelectEditor();
    $memberSelectEditor->addClass('workspace-dialog', Func_Icons::ICON_LEFT_16);
    $tabs->addTab(
        workspace_translate('Select an existing user'),
        $memberSelectEditor, 1, 'member_selector_tab'
    );

    if (workspace_canCreateUsers()) {
        $memberEditor = workspace_memberEditor()->setId('member_editor');
        $memberEditor->addClass('workspace-dialog');
        $tabs->addTab(
            workspace_translate('Create a new user'),
            $memberEditor, 2, 'member_editor_tab'
        );
        $tabs->setSelectedTab($openPanel);
    }

    $tabs->addClass('workspace-dialog');

    return $tabs;
}

