<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/workspaceselector.php';





/**
 * Checks whether the current user is one of the workspace administrators.
 *
 * @param int		$userId
 * @param int		$workspaceId
 *
 * @return bool
 */
function workspace_userIsWorkspaceAdministrator($userId = null, $workspaceId = null)
{
    static $userIsWorkspaceAdministrator = null;
    if (!isset($userId)) {
        $userId = bab_getUserId();
    }
    if (!isset($workspaceId)) {
        $workspaceId = bab_getCurrentUserDelegation();
    }

    if (!isset($userIsWorkspaceAdministrator)) {
        $userIsWorkspaceAdministrator = array();
    }
    $cacheKey = $userId . ',' . $workspaceId;
    if (!isset($userIsWorkspaceAdministrator[$cacheKey])) {
        $ids = workspace_getWorkspaceAdministratorIds($workspaceId);
        $userIsWorkspaceAdministrator[$cacheKey] = array_key_exists($userId, $ids);
    }
	return $userIsWorkspaceAdministrator[$cacheKey];
}



/**
 * Checks whether the current user is one of the workspace permanent administrators.
 *
 * @param int		$userId
 * @param int		$workspaceId
 *
 * @return bool
 */
function workspace_userIsWorkspacePermanentAdministrator($userId = null, $workspaceId = null)
{
    static $userIsWorkspacePermanentAdministrator = null;
    if (!isset($userId)) {
        $userId = bab_getUserId();
    }
    if (!isset($workspaceId)) {
        $workspaceId = bab_getCurrentUserDelegation();
    }

    if (!isset($userIsWorkspacePermanentAdministrator)) {
        $userIsWorkspaceAdministrator = array();
    }
    $cacheKey = $userId . ',' . $workspaceId;
    if (!isset($userIsWorkspacePermanentAdministrator[$cacheKey])) {
        $ids = workspace_getWorkspacePermanentAdministratorIds($workspaceId);
        $userIsWorkspacePermanentAdministrator[$cacheKey] = array_key_exists($userId, $ids);
    }
    return $userIsWorkspacePermanentAdministrator[$cacheKey];
}




/**
 * Returns workspaces accessible to the user.
 *
 * @return array
 */
function workspace_getWorkspaceList($userId = null, $admin = false)
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/delegincl.php';

	$workspaces = array();

	if ($admin == true) {
		$delegations = workspace_getAdminDelegations($userId);
	} else {
		$delegations = bab_getUserVisiblesDelegations($userId);
	}

	$registry = bab_getRegistryInstance();

	foreach ($delegations as $delegation) {
		if (empty($delegation['id'])) {
			continue;
		}

		$registry->changeDirectory('/workspace/workspaces/');
		
		if ($registry->isDirectory($delegation['id'])) {
		    
		    if (!$admin && workspace_isWorkspaceDisabled($delegation['id'])){
		        continue;
		    }
		    
		    $workspaces[$delegation['id']] = array(
		        'id' => $delegation['id'],
		        'name' => $delegation['name'],
		        'description' => $delegation['description'],
		        'disable' => workspace_isWorkspaceDisabled($delegation['id'])
		        
		    );
		}
	}

	return $workspaces;
}




/**
 *
 * Returns the full list of worksapce if the user is admin or the visible delegation
 */
function workspace_getAdminDelegations($userId = null)
{
	global $babDB;
	if (!isset($userId)) {
		$userId = bab_getUserId();
	}
	if (bab_isMemberOfGroup(BAB_ADMINISTRATOR_GROUP, $userId)) {
		return bab_getDelegationsFromResource($babDB->db_query("select * from ".BAB_DG_GROUPS_TBL." order by name asc"));
	} else {
		return bab_getUserVisiblesDelegations($userId);
	}
}




/**
 * Returns information about the specified workspace.
 *
 * @return array
 */
function workspace_getWorkspaceInfo($workspaceId, $admin = false)
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/delegincl.php';

	if ($admin == true) {
		$delegations = workspace_getAdminDelegations();
	} else {
		$delegations = bab_getUserVisiblesDelegations();
	}

	$registry = bab_getRegistryInstance();



	foreach ($delegations as $delegation) {
		if (empty($delegation['id'])) {
			continue;
		}

		$registry->changeDirectory('/workspace/workspaces/');
		if ($workspaceId == $delegation['id'] && $registry->isDirectory($delegation['id'])) {

			$info = array(
				'id' => $delegation['id'],
				'name' => $delegation['name'],
				'description' => $delegation['description'],
				'group' => workspace_getWorkspaceGroupId($delegation['id']),
				'category' => workspace_getWorkspaceCategory($workspaceId),
				'categoryName' => workspace_getWorkspaceCategoryName($workspaceId),
				'skin' => workspace_getWorkspaceSkinName($workspaceId),
				'skin_styles' => workspace_getWorkspaceSkinCss($workspaceId),
				'num_article' => workspace_getWorkspaceArticleNum($workspaceId),
				'num_forum' => workspace_getWorkspaceForumNum($workspaceId),
				'num_agenda' => workspace_getWorkspaceAgendaNum($workspaceId),
				'num_file' => workspace_getWorkspaceFileNum($workspaceId),
				'other_them' => workspace_getWorkspaceOtherThem($workspaceId),
				'file_size' => workspace_getWorkspaceFileSize($workspaceId),
				'update_directory' => workspace_getWorkspaceUpdateDirectory($workspaceId),
				'disabled' => workspace_isWorkspaceDisabled($workspaceId),
				'versioning' => workspace_getWorkspaceFileVersioning($workspaceId)
			);

			$administratorIds = workspace_getWorkspaceAdministratorIds($delegation['id']);
			if (count($administratorIds) > 0) {
				$info['administrator'] = current($administratorIds);
			}
			return $info;
		}
	}

	return null;
}




/**
 * Returns the specified (or current if not specified) workspace's associated group.
 *
 * @return int		or null on error
 */
function workspace_getWorkspaceGroupId($workspaceId = null)
{
	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}
	$delegationsInfo = bab_getDelegationById($workspaceId);
	if (list(,$delegationInfo) = each($delegationsInfo)) {
		return $delegationInfo['id_group'];
	}
	return null;
}

/**
 * Returns the specified (or current if not specified) delegation category
 */
function workspace_getWorkspaceCategory($workspaceId = null)
{
	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}
	global $babDB;
	$req = 'SELECT iIdCategory as category FROM '.BAB_DG_GROUPS_TBL.'
			WHERE id='.$babDB->quote($workspaceId);
	$res = $babDB->db_query($req);
	$arr = $babDB->db_fetch_assoc($res);

	return $arr['category'];
}

/**
 * Returns the specified (or current if not specified) delegation category
 */
function workspace_getWorkspaceCategoryName($workspaceId = null)
{
	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}
	global $babDB;
	$req = 'SELECT iIdCategory as category FROM '.BAB_DG_GROUPS_TBL.'
			WHERE id='.$babDB->quote($workspaceId);
	$res = $babDB->db_query($req);
	$arr = $babDB->db_fetch_assoc($res);


	$categoryName = '';
	if ($arr['category'] != 0) {
		$res = $babDB->db_query('SELECT * FROM '.BAB_DG_CATEGORIES_TBL.' WHERE id='.$babDB->quote($arr['category']));
		if($cat = $babDB->db_fetch_assoc($res)) {
			$categoryName = $cat['name'];
		}
	}

	return $categoryName;
}





/**
 * Returns the specified workspace id.
 *
 * @return int		or null on error
 */
function workspace_getWorkspaceIdByGroupId($groupeId)
{
	$workspacesId = false;
	require_once $GLOBALS['babInstallPath'] . 'utilit/delegincl.php';

	if($workspacesId === false){
		$registry = bab_getRegistry();
		$registry->changeDirectory('/workspace/workspaces/');
		while($workspaceId = $registry->fetchChildDir()){
			$workspacesId[] = str_replace('/', '', $workspaceId);
		}
	}

	if ($workspacesId) {
		foreach($workspacesId as $workspaceId){
			$workspaceGroupeId = workspace_getWorkspaceGroupId($workspaceId);
			if($groupeId == $workspaceGroupeId){
				return $workspaceId;
			}
		}
	}

	return false;
}

function wokspace_isExist($delegationId)
{
	$registry = bab_getRegistryInstance();
	return $registry->isDirectory('/workspace/workspaces/'.$delegationId);
}

function wokspace_isDisabled($workspaceId)
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace/workspaces/'.$workspaceId);
	return $registry->getValue('disabled', false);
}




/**
 * Recreates admin groups and associated rights with workspace items.
 *
 * @return int		the id of the admin group created or null on error.
 */
function workspace_createAdminGroup($workspaceId)
{
	require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';

	$groupId = workspace_getWorkspaceGroupId($workspaceId);
	if ($groupId == null || $workspaceId == null) {
		return null;
	}
	$administratorsGroupId = bab_createGroup('administrators', '', 0, $groupId);

	$categoryId = workspace_getWorkspaceCategoryId($workspaceId);

	$administratorsRightsTables= array(
		BAB_DEF_TOPCATCOM_GROUPS_TBL,
		BAB_DEF_TOPCATMOD_GROUPS_TBL,
		BAB_DEF_TOPCATSUB_GROUPS_TBL,
		BAB_DEF_TOPCATVIEW_GROUPS_TBL
	);
	foreach ($administratorsRightsTables as $rightsTable) {
	    aclAdd($rightsTable, $administratorsGroupId, $categoryId);
	}

	$topicId = workspace_getWorkspaceTopicId($workspaceId);

	$administratorsRightsTables = array(
		BAB_TOPICSMAN_GROUPS_TBL,
		BAB_TOPICSMOD_GROUPS_TBL,
		BAB_TOPICSSUB_GROUPS_TBL,
		BAB_TOPICSVIEW_GROUPS_TBL
	);

	foreach ($administratorsRightsTables as $rightsTable) {
		aclAdd($rightsTable, $administratorsGroupId, $topicId);
	}

	$directoryId = workspace_getWorkspaceDirectoryId($workspaceId);

	$administratorsRightsTables = array(
		BAB_DBDIRVIEW_GROUPS_TBL,
		BAB_DBDIRADD_GROUPS_TBL,
		BAB_DBDIRUPDATE_GROUPS_TBL,
		BAB_DBDIRDEL_GROUPS_TBL,
		BAB_DBDIREXPORT_GROUPS_TBL,
		BAB_DBDIRIMPORT_GROUPS_TBL,
		BAB_DBDIRBIND_GROUPS_TBL,
		BAB_DBDIRUNBIND_GROUPS_TBL,
		BAB_DBDIREMPTY_GROUPS_TBL
	);

	foreach ($administratorsRightsTables as $rightsTable) {
		aclAdd($rightsTable, $administratorsGroupId, $directoryId);
	}

	$forumId = workspace_getWorkspaceForumId($workspaceId);

	$administratorsRightsTables = array(
		BAB_FORUMSMAN_GROUPS_TBL,
		BAB_FORUMSPOST_GROUPS_TBL,
		BAB_FORUMSREPLY_GROUPS_TBL,
		BAB_FORUMSVIEW_GROUPS_TBL,
		BAB_FORUMSFILES_GROUPS_TBL
	);

	foreach ($administratorsRightsTables as $rightsTable) {
		aclAdd($rightsTable, $administratorsGroupId, $forumId);
	}

	$calId = workspace_getWorkspaceCalendarId($workspaceId);

	$administratorsRightsTables = array(
		BAB_CAL_PUB_MAN_GROUPS_TBL,
		BAB_CAL_PUB_VIEW_GROUPS_TBL
	);

	foreach ($administratorsRightsTables as $rightsTable) {
		aclAdd($rightsTable, $administratorsGroupId, $calId);
	}

	$rootFolderId = workspace_getWorkspaceRootFolderId($workspaceId);

	$administratorsRightsTables = array(
		BAB_FMUPLOAD_GROUPS_TBL,
		BAB_FMDOWNLOAD_GROUPS_TBL,
		BAB_FMUPDATE_GROUPS_TBL,
		BAB_FMMANAGERS_GROUPS_TBL
	);

	foreach ($administratorsRightsTables as $rightsTable) {
		aclAdd($rightsTable, $administratorsGroupId, $rootFolderId);
	}

	return $administratorsGroupId;
}




/**
 * Recreates writer groups and associated rights with workspace items.
 *
 * @return int		the id of the writer group created or null on error.
 */
function workspace_createWriterGroup($workspaceId)
{
	require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';

	$groupId = workspace_getWorkspaceGroupId($workspaceId);
	if ($groupId == null || $workspaceId == null) {
		return null;
	}
	$writerGroupId = bab_createGroup('writers', '', 0, $groupId);

	$categoryId = workspace_getWorkspaceCategoryId($workspaceId);

	$writersRightsTables = array(
		BAB_DEF_TOPCATCOM_GROUPS_TBL,
		BAB_DEF_TOPCATMOD_GROUPS_TBL,
		BAB_DEF_TOPCATSUB_GROUPS_TBL,
		BAB_DEF_TOPCATVIEW_GROUPS_TBL
	);
	foreach ($writersRightsTables as $rightsTable) {
	    aclAdd($rightsTable, $writerGroupId, $categoryId);
	}

	$topicId = workspace_getWorkspaceTopicId($workspaceId);

	$writersRightsTables = array(
		BAB_TOPICSMOD_GROUPS_TBL,
		BAB_TOPICSSUB_GROUPS_TBL,
		BAB_TOPICSVIEW_GROUPS_TBL
	);

	foreach ($writersRightsTables as $rightsTable) {
		aclAdd($rightsTable, $writerGroupId, $topicId);
	}

	$directoryId = workspace_getWorkspaceDirectoryId($workspaceId);

	$writersRightsTables = array(
		BAB_DBDIRVIEW_GROUPS_TBL,
		BAB_DBDIRADD_GROUPS_TBL,
		BAB_DBDIRUPDATE_GROUPS_TBL,
		BAB_DBDIRDEL_GROUPS_TBL,
		BAB_DBDIREXPORT_GROUPS_TBL
	);

	foreach ($writersRightsTables as $rightsTable) {
		aclAdd($rightsTable, $writerGroupId, $directoryId);
	}

	$forumId = workspace_getWorkspaceForumId($workspaceId);

	$writersRightsTables = array(
		BAB_FORUMSPOST_GROUPS_TBL,
		BAB_FORUMSREPLY_GROUPS_TBL,
		BAB_FORUMSVIEW_GROUPS_TBL,
		BAB_FORUMSFILES_GROUPS_TBL
	);

	foreach ($writersRightsTables as $rightsTable) {
		aclAdd($rightsTable, $writerGroupId, $forumId);
	}

	$calId = workspace_getWorkspaceCalendarId($workspaceId);

	$writersRightsTables = array(
		BAB_CAL_PUB_MAN_GROUPS_TBL,
		BAB_CAL_PUB_VIEW_GROUPS_TBL
	);

	foreach ($writersRightsTables as $rightsTable) {
		aclAdd($rightsTable, $writerGroupId, $calId);
	}

	$rootFolderId = workspace_getWorkspaceRootFolderId($workspaceId);

	$writersRightsTables = array(
		BAB_FMUPLOAD_GROUPS_TBL,
		BAB_FMDOWNLOAD_GROUPS_TBL,
		BAB_FMUPDATE_GROUPS_TBL
	);

	foreach ($writersRightsTables as $rightsTable) {
		aclAdd($rightsTable, $writerGroupId, $rootFolderId);
	}

	return $writerGroupId;
}




/**
 * Returns the specified (or current if not specified) workspace's administrators group.
 *
 * @return int		or null on error
 */
function workspace_getWorkspaceAdministratorsGroupId($workspaceId = null)
{
	$groupId = workspace_getWorkspaceGroupId($workspaceId);
	$groups = bab_getGroups($groupId, false);

	foreach ($groups['name'] as $groupKey => $groupName) {
		if ($groupName === 'administrators') {
			return $groups['id'][$groupKey];
		}
	}
	if( $groupId != null ){
		if (!isset($workspaceId)) {
			$workspaceId = workspace_getCurrentWorkspace();
		}
		return workspace_createAdminGroup($workspaceId);
	}

	return null;
}




/**
 * Returns the specified (or current if not specified) workspace's writers group.
 *
 * @return int		or null on error
 */
function workspace_getWorkspaceWritersGroupId($workspaceId = null)
{
	$groupId = workspace_getWorkspaceGroupId($workspaceId);
	$groups = bab_getGroups($groupId, false);

	foreach ($groups['name'] as $groupKey => $groupName) {
		if ($groupName === 'writers') {
			return $groups['id'][$groupKey];
		}
	}
	if ($groupId != null) {
		if (!isset($workspaceId)) {
			$workspaceId = workspace_getCurrentWorkspace();
		}
		return workspace_createWriterGroup($workspaceId);
	}

	return null;
}




/**
 * Returns the profile (administrator, writer or reader) for the specified user.
 * @param int   $userId
 * @param int   $workspaceId
 *  @return string	'administrator', 'writer' or 'reader' or false
 */
function workspace_getUserProfile($userId = null, $workspaceId = null)
{
    if (!isset($userId)) {
        $userId = bab_getUserId();
    }

	$groupId = workspace_getWorkspaceGroupId($workspaceId);
	if (!bab_isMemberOfGroup($groupId, $userId)) {
		return false;
	}

	if (workspace_userIsWorkspaceAdministrator($userId, $workspaceId)) {
		return 'administrator';
	}

	$writersGroupId = workspace_getWorkspaceWritersGroupId($workspaceId);
	if (bab_isMemberOfGroup($writersGroupId, $userId)) {
		return 'writer';
	}

	return 'reader';
}










/**
 * Returns the specified (or current if not specified) workspace's root folder id.
 *
 * @return int		or null on error
 */
function workspace_getWorkspaceRootFolderId($workspaceId = null)
{
	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}

	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace/workspaces/' . $workspaceId);
	$folderId = $registry->getValue('rootFolderId');

	return $folderId;
}




/**
 *
 * @return string
 */
function workspace_getWorkspaceRootFolderName($workspaceId = null)
{
	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}

	require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';
	require_once $GLOBALS['babInstallPath'] . 'utilit/fileincl.php';

	$folderId = workspace_getWorkspaceRootFolderId($workspaceId);
	$groupId = workspace_getWorkspaceGroupId($workspaceId);

	$oFmFolderSet	= new BAB_FmFolderSet();
	$oId			= $oFmFolderSet->aField['iId'];
	$oIdDgOwner		= $oFmFolderSet->aField['iIdDgOwner'];

	$oCriteria = $oIdDgOwner->in($workspaceId);
	$oCriteria = $oCriteria->_and($oId->in($folderId));
	$oFmFolderSet->select($oCriteria);

	while (null !== ($oFmFolder = $oFmFolderSet->next())) {
		return $oFmFolder->getName();
	}

	return '';
}





/**
 * Returns the specified (or current if not specified) workspace's calendar id.
 *
 * @return int		or null on error
 */
function workspace_getWorkspaceCalendarId($workspaceId = null, $checkAccess = false)
{
	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}

	$registry = bab_getRegistry();
	$registry->changeDirectory('/workspace/workspaces/' . $workspaceId);
	$calendarId = $registry->getValue('calendarId');
	if ($checkAccess) {
		require_once $GLOBALS['babInstallPath'] . '/utilit/calincl.php';
		if (!is_null($calendarId) && !bab_isCalendarAccessValid($calendarId)) {
			return null;
		}
	}
	return $calendarId;
}




/**
 * Returns the specified (or current if not specified) workspace's directory id.
 *
 * @return int		or null on error
 */
function workspace_getWorkspaceDirectoryId($workspaceId = null)
{
	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}

	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace/workspaces/' . $workspaceId);
	$directoryId = $registry->getValue('directoryId');

	return $directoryId;
}




/**
 * Returns the specified (or current if not specified) workspace's topic id.
 *
 * @return int		or null on error
 */
function workspace_getWorkspaceTopicId($workspaceId = null)
{
	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}

	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace/workspaces/' . $workspaceId);
	$topicId = $registry->getValue('topicId');

	return $topicId;
}




/**
 * Returns the specified (or current if not specified) workspace's topic category id.
 *
 * @return int		or null on error
 */
function workspace_getWorkspaceCategoryId($workspaceId = null)
{
	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}

	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace/workspaces/' . $workspaceId);
	$categoryId = $registry->getValue('categoryId');

	return $categoryId;
}




/**
 * Returns the specified (or current if not specified) workspace's forum id.
 *
 * @return int		or null on error
 */
function workspace_getWorkspaceForumId($workspaceId = null)
{
	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}

	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace/workspaces/' . $workspaceId);
	$forumId = $registry->getValue('forumId');

	return $forumId;
}




/**
 * Checks if the specified (or current if not specified) workspace is
 * configured to send notifications to members on file uploads.
 *
 * @return bool		or null on error
 */
function workspace_getWorkspaceFileNotifications($workspaceId = null)
{
	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}

	require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';
	require_once $GLOBALS['babInstallPath'] . 'utilit/fileincl.php';

	$folderId = workspace_getWorkspaceRootFolderId($workspaceId);
	$groupId = workspace_getWorkspaceGroupId($workspaceId);
	$notifiedGroups = aclGetAccessGroups(BAB_FMNOTIFY_GROUPS_TBL, $folderId);

	if (!isset($notifiedGroups[$groupId])) {
		return false;
	}

	$oFmFolderSet	= new BAB_FmFolderSet();
	$oId			= $oFmFolderSet->aField['iId'];
	$oIdDgOwner		= $oFmFolderSet->aField['iIdDgOwner'];

	$oCriteria = $oIdDgOwner->in($workspaceId);
	$oCriteria = $oCriteria->_and($oId->in($folderId));
	$oFmFolderSet->select($oCriteria);

	while (null !== ($oFmFolder = $oFmFolderSet->next())) {
		return ($oFmFolder->getFileNotify() == 'Y');
	}
}


/**
 * Checks if the specified (or current if not specified) workspace is
 * configured to do versioning of files.
 *
 * @return bool		or null on error
 */
function workspace_getWorkspaceFileVersioning($workspaceId = null)
{
	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}

	require_once $GLOBALS['babInstallPath'] . 'utilit/fileincl.php';

	$folderId = workspace_getWorkspaceRootFolderId($workspaceId);

	$oFmFolderSet	= new BAB_FmFolderSet();
	$oId			= $oFmFolderSet->aField['iId'];
	$oIdDgOwner		= $oFmFolderSet->aField['iIdDgOwner'];

	$oCriteria = $oIdDgOwner->in($workspaceId);
	$oCriteria = $oCriteria->_and($oId->in($folderId));
	$oFmFolderSet->select($oCriteria);

	while (null !== ($oFmFolder = $oFmFolderSet->next())) {
		return ($oFmFolder->getVersioning() == 'Y');
	}
}


/**
 * Sets if the specified (or current if not specified) workspace is
 * configured to allow people to comment article.
 */
function workspace_setWorkspaceComments($comment = false, $workspaceId){

	require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';

	$groupId = workspace_getWorkspaceGroupId($workspaceId);
	$topicId = workspace_getWorkspaceTopicId($workspaceId);

	if ($comment) {
		aclAdd("bab_topicscom_groups", $groupId + BAB_ACL_GROUP_TREE, $topicId);
	} else {
		aclRemove("bab_topicscom_groups", $groupId + BAB_ACL_GROUP_TREE, $topicId);
	}
}




/**
 * Sets if the specified (or current if not specified) workspace is
 * configured to allow people to comment article.
 */
function workspace_setWorkspaceForumNotifications($forum = false, $workspaceId){

	require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';

	$groupId = workspace_getWorkspaceGroupId($workspaceId);
	$forumId = workspace_getWorkspaceForumId($workspaceId);

	if ($forum) {
		aclAdd("bab_forumsnotify_groups", $groupId + BAB_ACL_GROUP_TREE, $forumId);
	} else {
		aclRemove("bab_forumsnotify_groups", $groupId + BAB_ACL_GROUP_TREE, $forumId);
	}
}




/**
 * Sets if the specified (or current if not specified) workspace is
 * configured to send notifications to members on file uploads.
 */
function workspace_setWorkspaceFileNotifications($notify = false, $workspaceId = null)
{
	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}

	require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';
	require_once $GLOBALS['babInstallPath'] . 'utilit/fileincl.php';

	$groupId = workspace_getWorkspaceGroupId($workspaceId);
	$folderId = workspace_getWorkspaceRootFolderId($workspaceId);

	if ($notify) {
		aclAdd(BAB_FMNOTIFY_GROUPS_TBL, $groupId + BAB_ACL_GROUP_TREE, $folderId);
	} else {
		aclRemove(BAB_FMNOTIFY_GROUPS_TBL, $groupId + BAB_ACL_GROUP_TREE, $folderId);
	}

	$oFmFolderSet	= new BAB_FmFolderSet();
	$oId			= $oFmFolderSet->aField['iId'];
	$oIdDgOwner		= $oFmFolderSet->aField['iIdDgOwner'];

	$oCriteria = $oIdDgOwner->in($workspaceId);
	$oCriteria = $oCriteria->_and($oId->in($folderId));
	$oFmFolderSet->select($oCriteria);

	while (null !== ($oFmFolder = $oFmFolderSet->next())) {
		$oFmFolder->setFileNotify($notify ? 'Y' : 'N');
		$oFmFolder->save();
	}
}



/**
 * Sets if the specified (or current if not specified) workspace is
 * configured to do versioning of files.
 */
function workspace_setWorkspaceFileVersioning($versioning = false, $workspaceId = null)
{
	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}

	require_once $GLOBALS['babInstallPath'] . 'utilit/fileincl.php';

	$folderId = workspace_getWorkspaceRootFolderId($workspaceId);

	$oFmFolderSet	= new BAB_FmFolderSet();
	$oId			= $oFmFolderSet->aField['iId'];
	$oIdDgOwner		= $oFmFolderSet->aField['iIdDgOwner'];

	$oCriteria = $oIdDgOwner->in($workspaceId);
	$oCriteria = $oCriteria->_and($oId->in($folderId));
	$oFmFolderSet->select($oCriteria);

	while (null !== ($oFmFolder = $oFmFolderSet->next())) {
		$oFmFolder->setVersioning($versioning ? 'Y' : 'N');
		$oFmFolder->save();
	}
}



function workspace_getWorkspaceAdministratorIds($workspaceId = null)
{
	global $babDB;
	static $administratorsByWorkspace = null;

	if (!isset($administratorsByWorkspace)) {
	    $administratorsByWorkspace = array();
	}
	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}
	if (!isset($administratorsByWorkspace[$workspaceId])) {
    	$administratorIds = array();
    	$res = $babDB->db_query('SELECT id_user FROM '.BAB_DG_ADMIN_TBL.' WHERE id_dg='.$babDB->quote($workspaceId));
    	while ($administrator = $babDB->db_fetch_array($res)) {
    		$administratorIds[$administrator['id_user']] = $administrator['id_user'];
    	}
    	$administratorsByWorkspace[$workspaceId] = $administratorIds;
	}
	return $administratorsByWorkspace[$workspaceId];
}




/**
 *
 * @param int $workspaceId
 * @return int[]
 */
function workspace_getWorkspacePermanentAdministratorIds($workspaceId = null)
{
    if (!isset($workspaceId)) {
    	$workspaceId = workspace_getCurrentWorkspace();
    }

    $permanentAdministratorIds = array();

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/workspace/workspaces/' . $workspaceId);
    $ids = explode(',', $registry->getValue('permanentAdministrators'));
    foreach ($ids as $id) {
        $permanentAdministratorIds[$id] = $id;
    }
    return $permanentAdministratorIds;
}





/**
 *
 * @param int[] $permanentAdministratorIds
 * @param int   $workspaceId
 * @return 0|1|2
 */
function workspace_setWorkspacePermanentAdministratorIds($permanentAdministratorIds, $workspaceId = null)
{
    if (!isset($workspaceId)) {
        $workspaceId = workspace_getCurrentWorkspace();
    }

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/workspace/workspaces/' . $workspaceId);
    return $registry->setKeyValue('permanentAdministrators', implode(',', $permanentAdministratorIds));
}


/**
 * Checks if the user is allowed to create new user accounts.
 * @param int   $workspaceId
 * @return bool
 */
function workspace_getWorkspaceAllowUserCreation($workspaceId = null)
{
    if (!isset($workspaceId)) {
        $workspaceId = workspace_getCurrentWorkspace();
    }
    global $babDB;

    $req = 'SELECT users FROM ' . BAB_DG_GROUPS_TBL . ' WHERE id=' . $babDB->quote($workspaceId);

    $dgGroups = $babDB->db_query($req);
    while ($dgGroup = $babDB->db_fetch_assoc($dgGroups)) {
        return $dgGroup['users'] == 'Y';
    }

    return false;
}




/**
 * Defines if the user is allowed to create new user accounts.
 * @param int   $workspaceId
 * @param bool  $allow
 */
function workspace_setWorkspaceAllowUserCreation($allow, $workspaceId = null)
{
    if (!isset($workspaceId)) {
        $workspaceId = workspace_getCurrentWorkspace();
    }
    global $babDB;

    $req = 'UPDATE ' . BAB_DG_GROUPS_TBL . ' SET users=' . $babDB->quote($allow ? 'Y' : 'N')
    . ' WHERE id=' . $babDB->quote($workspaceId);

    $babDB->db_query($req);
}




/**
 * Returns the current user workspace.
 *
 * @return int
 */
function workspace_getCurrentWorkspace()
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/delegincl.php';

	$workspaces = workspace_getWorkspaceList();
	if (empty($workspaces)) {
	    return null;
	}

	$workspaceId = bab_getCurrentUserDelegation();

	if (array_key_exists($workspaceId, $workspaces)) {
		return $workspaceId;
	}

	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace/user/' . $GLOBALS['BAB_SESS_USERID']);
	$workspaceIds = array_keys($workspaces);
	$workspaceId = array_pop($workspaceIds);
	$workspaceId = $registry->getValue('lastWorkspace', $workspaceId);
	if (array_key_exists($workspaceId, $workspaces)) {
	    bab_setCurrentUserDelegation($workspaceId);
		return $workspaceId;
	}

	$firstWorkspace = array_shift($workspaces);

	return $firstWorkspace['id'];
}




/**
 * Sets the current user workspace.
 */
function workspace_setCurrentWorkspace($workspaceId)
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/delegincl.php';
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace/user/' . $GLOBALS['BAB_SESS_USERID']);
	$registry->setKeyValue('lastWorkspace', $workspaceId);

	bab_setCurrentUserDelegation($workspaceId);
}




/**
 *
 */
function workspace_workspaceExist($workspaceId)
{
	if (empty($workspaceId)) {
		return false;
	}
	$registry = bab_getRegistryInstance();
	return $registry->isDirectory('/workspace/workspaces/'.$workspaceId);
}




/**
 * Checks the existence of the group name in the workspaces parent group.
 */
function workspace_groupNameExist($groupName)
{
	$workspacesParentGroup = workspace_getWorkspacesParentGroup();

	$groups = bab_getGroups($workspacesParentGroup, false);
	if (in_array($groupName, $groups['name'])) {
		return true;
	}
	return false;
}


/**
 * Return the path to the specified workspace trash folder.
 *
 * @param int $workspaceId
 * @return string
 */
function workspace_getWorkspaceTrashPath($workspaceId)
{
	return 'DG' . $workspaceId . '/.trash';
}




/**
 * @return int		The id of the workspaces parent group.
 */
function workspace_getWorkspacesParentGroup()
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace');
	$group = $registry->getValue('parentGroup', BAB_REGISTERED_GROUP);

	return $group;
}



/**
 * @param int $baseGroup      The based group id for workspaces groups.
 */
function workspace_setWorkspacesParentGroup($parentGroup)
{
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/workspace');
    $registry->setKeyValue('parentGroup', $parentGroup);
}




/**
 * @return string	The name of the workspace skin.
 */
function workspace_getWorkspaceSkinName($workspaceId)
{
	if(workspace_workspaceExist($workspaceId)){
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/workspace/workspaces/'.$workspaceId);
		$group = $registry->getValue('skin', 'workspace');

		return $group;
	}
	return 'workspace';
}




/**
 * @return string		The name of the workspace skin css file.
 */
function workspace_getWorkspaceSkinCss($workspaceId)
{
	if(workspace_workspaceExist($workspaceId)){
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/workspace/workspaces/'.$workspaceId);
		$css = $registry->getValue('skin_styles', 'blue.css');

		return $css;
	}
	return 'blue.css';
}




/**
 * @return string		The number of article to be displayed in the right section of the home page
 */
function workspace_getWorkspaceArticleNum($workspaceId)
{
	if(workspace_workspaceExist($workspaceId)){
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/workspace/workspaces/'.$workspaceId);
		$num = $registry->getValue('num_article', '3');

		return $num;
	}
	return 3;
}




/**
 * @return string		The number of forum post to be displayed in the right section of the home page
 */
function workspace_getWorkspaceForumNum($workspaceId)
{
	if(workspace_workspaceExist($workspaceId)){
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/workspace/workspaces/'.$workspaceId);
		$num = $registry->getValue('num_forum', '3');

		return $num;
	}
	return 3;
}




/**
 * @return string		The number of event agenda to be displayed in the right section of the home page
 */
function workspace_getWorkspaceAgendaNum($workspaceId)
{
	if(workspace_workspaceExist($workspaceId)){
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/workspace/workspaces/'.$workspaceId);
		$num = $registry->getValue('num_agenda', '3');

		return $num;
	}
	return 3;
}




/**
 * @return string		The number of file to be displayed in the right section of the home page
 */
function workspace_getWorkspaceFileNum($workspaceId)
{
	if(workspace_workspaceExist($workspaceId)){
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/workspace/workspaces/'.$workspaceId);
		$num = $registry->getValue('num_file', '3');

		return $num;
	}
	return 3;
}




/**
 * @return bool		Display or not the other them section in the right section of the home page
 */
function workspace_getWorkspaceOtherThem($workspaceId)
{
	if(workspace_workspaceExist($workspaceId)){
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/workspace/workspaces/'.$workspaceId);
		$num = $registry->getValue('other_them', false);

		return $num;
	}
	return false;
}




/**
 * @return int		Max file size
 */
function workspace_getWorkspaceFileSize($workspaceId)
{
	if(workspace_workspaceExist($workspaceId)){
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/workspace/workspaces/'.$workspaceId);
		$num = $registry->getValue('file_size', 0);

		return $num;
	}
	return 0;
}




/**
 * @return bool		True if ovidentia global directory entries should be updated when workspace directory entry is updated
 */
function workspace_getWorkspaceUpdateDirectory($workspaceId)
{
	if (workspace_workspaceExist($workspaceId)){
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/workspace/workspaces/'.$workspaceId);
		$value = $registry->getValue('update_directory', false);

		return $value;
	}
	return false;
}




/**
 * Checks if the specified (or current if not specified) workspace is
 * configured to send notifications to members on calendar events.
 *
 * @param int       $workspaceId
 * @return bool		or null on error
 */
function workspace_getWorkspaceCalendarNotifications($workspaceId = null)
{
    if (!isset($workspaceId)) {
        $workspaceId = workspace_getCurrentWorkspace();
    }

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/workspace/workspaces/'.$workspaceId);
    $value = $registry->getValue('notifyCalendar', false);
    return $value;
}



/**
 * Configured notifications to members on calendar events for the specified workspace.
 *
 * @param bool      $notifyCalendar
 * @param int       $workspaceId
 * @return 1|2|3
 */
function workspace_setWorkspaceCalendarNotifications($notifyCalendar, $workspaceId = null)
{
    if (!isset($workspaceId)) {
        $workspaceId = workspace_getCurrentWorkspace();
    }

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/workspace/workspaces/'.$workspaceId);
    return $registry->setKeyValue('notifyCalendar', $notifyCalendar);
}



/**
 * Test if a workspace is disabled
 * @param int $workspaceId
 * @return bool
 */
function workspace_isWorkspaceDisabled($workspaceId)
{
	if (workspace_workspaceExist($workspaceId)){
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/workspace/workspaces/'.$workspaceId);
		$value = $registry->getValue('disabled', false);

		return $value;
	}
	return false;
}


/**
 * Tests if the sign off button should be displayed
 *
 * @return bool
 */
function workspace_getWorkspacesShowSignoffButton()
{
	$registry = bab_getRegistryInstance();
    $registry->changeDirectory('/workspace');
    $value = $registry->getValue('showSignoffButton', false);

	return $value;
}

/**
 * Sets if the sign off button should be deisplayed for this workspace.
 *
 * @param bool  $show
 */
function workspace_setWorkspacesShowSignoffButton($show)
{
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/workspace');
    $registry->setKeyValue('showSignoffButton', $show);
}


/**
 * @return int		The id of the workspaces delegation category.
 */
function workspace_getWorkspacesDelegationCategory()
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace');
	$category = $registry->getValue('delegationCategory', 0);

	return $category;
}




/**
 * @param int	$categoryId		The id of the workspaces delegation category.
 */
function workspace_setWorkspacesDelegationCategory($categoryId)
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace');
	$registry->setKeyValue('delegationCategory', $categoryId);
}





/**
 * @return string		The type of text editor (plain/rich/wysiwyg) to be used in workspaces.
 */
function workspace_getWorkspacesTextEditor()
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace');
	$category = $registry->getValue('textEditor', 'rich');

	return $category;
}




/**
 * @param string	$editor		The type of text editor (plain/rich/wysiwyg) to be used in workspaces.
 */
function workspace_setWorkspacesTextEditor($editor)
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace');
	$registry->setKeyValue('textEditor', $editor);
}




/**
 * @return string		The value of option Automatic Connexion to be used in workspaces, return 0 or 1.
 */
function workspace_getWorkspacesAutomaticConnexionValue() {
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace');
	$category = $registry->getValue('automatic_connexion', 0);

	return $category;
}



/**
 * @param string	$value		0 or 1. The value of option Automatic Connexion to be used in workspaces.
 */
function workspace_setWorkspacesAutomaticConnexionValue($value)
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace');
	$registry->setKeyValue('automatic_connexion', $value);
}




/**
 * @return string		The value of option disable Email to be used in workspaces, return 0 or 1.
 */
function workspace_getWorkspacesDisableEmailValue() {
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace');
	$category = $registry->getValue('disable_email', 0);

	return $category;
}




/**
 * @param string	$value		0 or 1. The value of option Disable Email to be used in workspaces.
 */
function  workspace_setWorkspacesDisableEmailValue($value)
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace');
	$registry->setKeyValue('disable_email', $value);
}




/**
* Replacement object
* This object replace all $XXXX() in html string
* @since 6.4.0
*/
class workspace_replace {

	var $ext_url;
	var $ignore_macro = array();




	function _var(&$txt, $var, $new)
	{
		$txt = preg_replace('/'  .preg_quote($var, '/') . '/', $new, $txt);
	}


	function _make_link($url, $text, $popup = 0, $url_popup = false, $classname = false)
	{
		if (isset($this->ext_url)) {
			$url = $GLOBALS['babUrlScript']."?tg=login&cmd=detect&referer=".urlencode($url);
			$popup = 0;
		}
		if ($classname !== false) {
			$classname = 'class="' . $classname . '"';
		}
		$url = ($popup == 1 || $popup == true) && $url_popup != false ? $url_popup : $url;
		if ($popup == 1 || $popup === true) {
			return '<a ' . $classname . ' href="'.bab_toHtml($url).'" onclick="bab_popup(this.href);return false;">'.$text.'</a>';
		} else if ($popup == 2) {
			return '<a ' . $classname . ' target="_blank" href="'.bab_toHtml($url).'">'.$text.'</a>';
		} else {
			return '<a ' . $classname . ' href="'.bab_toHtml($url).'">'.$text.'</a>';
		}
	}

	/**
	 * @param	string	$macro			ex : OVML
	 */
	function addIgnoreMacro($macro)
	{
		$this->ignore_macro[$macro] = 1;
	}

	/**
	 * @param	string	$macro			ex : OVML
	 */
	function removeIgnoreMacro($macro)
	{
		unset($this->ignore_macro[$macro]);
	}

	/**
	 * Test ignored macro, a macro is ignored if the test is done more than 5 time
	 * @param	string	$macro			ex : OVML
	 * @return	boolean
	 */
	function isMacroIgnored($macro, $params)
	{
		static $ignore_stack = array();

		if (isset($this->ignore_macro[$macro])) {

			if (isset($ignore_stack[$macro.$params])) {
				$ignore_stack[$macro.$params]++;
			} else {
				$ignore_stack[$macro.$params] = 1;
			}

			return $ignore_stack[$macro.$params] > 5;
		}

		return false;
	}



	/**
	 * external links for email
	 * @param	string	&$txt
	 */
	function email(&$txt)
	{
		$this->ext_url = true;
		$this->ref($txt);
		unset($this->ext_url);
	}


	/**
	 * replace macro in string
	 * @param string &$txt
	 */
	function ref(&$txt)
	{
	global $babBody, $babDB;

	self::ovidentia_ref($txt);

	$reg = "/\\\$([A-Z]*?)\((.*?)\)/";
	$matches = preg_match_all($reg, $txt, $m);
	if (preg_match_all($reg, $txt, $m)) {
		for ($k = 0; $k < count($m[1]); $k++ ) {
			if (!$this->isMacroIgnored($m[1][$k], $m[2][$k])) {
				$var = $m[0][$k];
				$varname = $m[1][$k];
				$param = explode(',',$m[2][$k]);

				if (count($param) > 0) {
					switch ($varname) {
						case 'ARTICLEPOPUP':
							$popup = true;
							// No break !
						case 'ARTICLE':
							$title_topic = count($param) > 1 ? trim($param[0],'"') : false;
							$title_object = count($param) > 1 ? trim($param[1],'"') : trim($param[0],'"');
							if (!isset($popup)) {
								$popup = false;
							}
							if ($title_topic) {
								$res = $babDB->db_query("select a.id,a.id_topic,a.title,a.restriction from ".BAB_TOPICS_TBL." t, ".BAB_ARTICLES_TBL." a where t.category='".$babDB->db_escape_string($title_topic)."' AND a.id_topic=t.id AND a.title='".$babDB->db_escape_string($title_object)."'");
								if ($res && $babDB->db_num_rows($res) > 0) {
									$arr = $babDB->db_fetch_array($res);
								} else {
									$title_topic = false;
								}
							}
							if (!$title_topic) {
								$res = $babDB->db_query("select id,id_topic,title,restriction from ".BAB_ARTICLES_TBL." where title LIKE '%".$babDB->db_escape_like($title_object)."%'");
								if ($res && $babDB->db_num_rows($res) > 0) {
									$arr = $babDB->db_fetch_array($res);
								}
							}
							if (bab_isAccessValid(BAB_TOPICSVIEW_GROUPS_TBL, $arr['id_topic']) && bab_articleAccessByRestriction($arr['restriction'])) {
								$title_object = $this->_make_link($GLOBALS['babUrlScript']."?tg=articles&idx=More&article=".$arr['id']."&topics=".$arr['id_topic'],$title_object,$popup,$GLOBALS['babUrlScript']."?tg=articles&idx=viewa&topics=".$arr['id_topic']."&article=".$arr['id']);
							}
							workspace_replace::_var($txt, $var, $title_object);
							break;

						case 'ARTICLEID':
							if (!is_numeric($param[0])) {
								break;
							}
							$id_object = $param[0];
							$title_object = isset($param[1]) ? $param[1] : '';
							$popup = isset($param[2]) ? $param[2] : false;
							$connect = isset($param[3]) ? $param[3] : false;

							$sql = '
									SELECT article.*, category.id_dgowner
									FROM ' . BAB_ARTICLES_TBL . ' AS article
										LEFT JOIN ' . BAB_TOPICS_TBL . ' AS topic ON topic.id = article.id_topic
										LEFT JOIN ' . BAB_TOPICS_CATEGORIES_TBL . ' AS category ON category.id = topic.id_cat
									WHERE article.id = ' . $babDB->quote($id_object);

							$res = $babDB->db_query($sql);
							if ($res && $babDB->db_num_rows($res) > 0) {
								$arr = $babDB->db_fetch_array($res);
								$title_object = empty($title_object) ? $arr['title'] : $title_object;

								if ($arr['id_dgowner'] == workspace_getCurrentWorkspace()) {
									$title_object = $this->_make_link($GLOBALS['babUrlScript'].'?tg=addon/workspace/main&idx=articles:show&article=' . $arr['id'], $title_object);
								} else if (bab_isAccessValid(BAB_TOPICSVIEW_GROUPS_TBL, $arr['id_topic']) && ($arr['restriction'] == '' || bab_articleAccessByRestriction($arr['restriction']))) {
									$title_object = $this->_make_link($GLOBALS['babUrlScript']."?tg=articles&idx=More&article=".$arr['id']."&topics=".$arr['id_topic'],$title_object,$popup,$GLOBALS['babUrlScript']."?tg=articles&idx=viewa&topics=".$arr['id_topic']."&article=".$arr['id'], 'bab-external bab-article-'.$arr['id']);
								} else if (!$GLOBALS['BAB_SESS_LOGGED'] && $connect) {
									$title_object = $this->_make_link($GLOBALS['babUrlScript']."?tg=login&cmd=detect&referer=".urlencode($GLOBALS['babUrlScript']."?tg=articles&idx=More&article=".$arr['id']."&topics=".$arr['id_topic']),$title_object,0,false,'bab-article-'.$arr['id'], 'bab-external');
								}
							}
							workspace_replace::_var($txt, $var, $title_object);
							break;

						case 'ARTICLEFILEID':
							$id_object = $param[0];
							$title_object = isset($param[1]) ? $param[1] : '';
							$res = $babDB->db_query("select aft.*, at.id_topic, at.restriction from ".BAB_ART_FILES_TBL." aft left join ".BAB_ARTICLES_TBL." at on aft.id_article=at.id where aft.id='".$babDB->db_escape_string($id_object)."'");
							if ($res && $babDB->db_num_rows($res) > 0) {
								$arr = $babDB->db_fetch_array($res);
								if (bab_isAccessValid(BAB_TOPICSVIEW_GROUPS_TBL, $arr['id_topic']) && ($arr['restriction'] == '' || bab_articleAccessByRestriction($arr['restriction']))) {
									$title_object = empty($title_object) ? (empty($arr['description'])? $arr['name']: $arr['description']) : $title_object;
									$title_object = $this->_make_link($GLOBALS['babUrlScript']."?tg=articles&idx=getf&topics=".$arr['id_topic']."&idf=".$arr['id'],$title_object);
								}
							}
							workspace_replace::_var($txt,$var,$title_object);
							break;

						case 'CONTACT':
							$title_object = $param[0].' '.$param[1];
							$res = $babDB->db_query("select * from ".BAB_CONTACTS_TBL." where  owner='".$babDB->db_escape_string($GLOBALS['BAB_SESS_USERID'])."' and firstname LIKE '%".$babDB->db_escape_string($param[0])."%' and lastname LIKE '%".$babDB->db_escape_like($param[1])."%'");
							if( $res && $babDB->db_num_rows($res) > 0)
								{
								$arr = $babDB->db_fetch_array($res);
								$title_object = $this->_make_link($GLOBALS['babUrlScript'].'?tg=contact&idx=modify&item='.$arr['id'].'&bliste=0',$title_object,true);
								}
							workspace_replace::_var($txt,$var,$title_object);
							break;

						case 'CONTACTID':
							$id_object = $param[0];
							$title_object = isset($param[1]) ? $param[1] : '';
							$res = $babDB->db_query("select * from ".BAB_CONTACTS_TBL." where  owner='".$babDB->db_escape_string($GLOBALS['BAB_SESS_USERID'])."' and id= '".$babDB->db_escape_string($id_object)."'");
							if( $res && $babDB->db_num_rows($res) > 0)
								{
								$arr = $babDB->db_fetch_array($res);
								$title_object = empty($title_object) ? bab_composeUserName($arr['firstname'],$arr['lastname']) : $title_object;
								$title_object = $this->_make_link($GLOBALS['babUrlScript'].'?tg=contact&idx=modify&item='.$arr['id'].'&bliste=0',$title_object,true);
								}
							workspace_replace::_var($txt,$var,$title_object);
							break;

						case 'DIRECTORYID':
							$id_object = trim($param[0]);
							$title_object = isset($param[1]) ? $param[1] : '';
							$res = $babDB->db_query("select id,sn,givenname,id_directory from ".BAB_DBDIR_ENTRIES_TBL." where id= '".$babDB->db_escape_string($id_object)."'");
							if( $res && $babDB->db_num_rows($res) > 0)
								{
								$arr = $babDB->db_fetch_array($res);
								if( $arr['id_directory'] == 0  )
									{
									$iddir = isset($param[2]) ? trim($param[2]): '' ;
									}
								else
									{
									$iddir = $arr['id_directory'];
									}

								if ( $iddir && bab_isAccessValid(BAB_DBDIRVIEW_GROUPS_TBL, $iddir))
									{
									$title_object = empty($title_object) ? bab_composeUserName($arr['sn'],$arr['givenname']) : $title_object;
									$title_object = $this->_make_link($GLOBALS['babUrlScript']."?tg=directory&idx=ddbovml&directoryid=".((int) $iddir)."&userid=".$arr['id'],$title_object,true);
									}
								}
							workspace_replace::_var($txt,$var,$title_object);
							break;

						case 'FAQ':
							$title_object = $param[1];
							$res = $babDB->db_query("select * from ".BAB_FAQCAT_TBL." where category='".$babDB->db_escape_string($param[0])."'");
							if( $res && $babDB->db_num_rows($res) > 0)
								{
								$arr = $babDB->db_fetch_array($res);
								if(bab_isAccessValid(BAB_FAQCAT_GROUPS_TBL, $arr['id']))
									{
									$req = "select * from ".BAB_FAQQR_TBL." where question='".$babDB->db_escape_string($param[1])."'";
									$res = $babDB->db_query($req);
									if( $res && $babDB->db_num_rows($res) > 0)
										{
										$arr = $babDB->db_fetch_array($res);
										$title_object = $this->_make_link($GLOBALS['babUrlScript']."?tg=faq&idx=viewpq&idcat=".$arr['idcat']."&idq=".$arr['id'],$title_object,true);
										}
									}
								}
							workspace_replace::_var($txt,$var,$title_object);
							break;

						case 'FAQID':
							$id_object = (int) $param[0];
							$title_object = isset($param[1]) ? $param[1] : '';
							$popup = isset($param[2]) ? $param[2] : false;
							$res = $babDB->db_query("select * from ".BAB_FAQQR_TBL." where id='".$babDB->db_escape_string($id_object)."'");
							if( $res && $babDB->db_num_rows($res) > 0)
								{
								$arr = $babDB->db_fetch_array($res);
								if(bab_isAccessValid(BAB_FAQCAT_GROUPS_TBL, $arr['idcat']))
									{
									$title_object = empty($title_object) ? $arr['question'] : $title_object;
									$title_object = $this->_make_link($GLOBALS['babUrlScript']."?tg=faq&idx=listq&item=".$arr['idcat']."&idscat=".$arr['id_subcat']."&idq=".$id_object."#".$id_object,$title_object,$popup,$GLOBALS['babUrlScript']."?tg=faq&idx=viewpq&idcat=".$arr['idcat']."&idq=".$id_object);

									}
								}
							workspace_replace::_var($txt,$var,$title_object);
							break;

						case 'FILE':
							$id_object = (int) $param[0];
							$title_object = isset($param[1]) ? $param[1] : '';
							include_once $GLOBALS['babInstallPath']."utilit/fileincl.php";
							$res = $babDB->db_query("select * from ".BAB_FILES_TBL." where id='".$babDB->db_escape_string($id_object)."' and state='' and confirmed='Y'");
							if( $res && $babDB->db_num_rows($res) > 0)
								{
								$arr = $babDB->db_fetch_array($res);
								if (bab_isAccessFileValid($arr['bgroup'], $arr['id_owner']))
									{
									$title_object = empty($title_object) ? $arr['name'] : $title_object;
									if( bab_getFileContentDisposition() == '')
										{
										$inl = empty($GLOBALS['files_as_attachment']) ? '&inl=1' : '';
										}
									else
										{
										$inl ='';
										}

										$sPath = removeEndSlah($arr['path']);
										$title_object = $this->_make_link($GLOBALS['babUrlScript']."?tg=fileman&sAction=getFile".$inl."&id=".$arr['id_owner']."&gr=".$arr['bgroup']."&path=".urlencode($sPath)."&file=".urlencode($arr['name']).'&idf='.$arr['id'],$title_object,2,false,'bab-file-' . $arr['id']);
									}
								}
							workspace_replace::_var($txt,$var,$title_object);
							break;

						case 'FOLDER':
							$id_object = (int) $param[0];
							$path_object = isset($param[1]) ? $param[1] : '';
							$title_object = isset($param[2]) ? $param[2] : '';

							$res = $babDB->db_query("select id,folder from ".BAB_FM_FOLDERS_TBL." where id='".$babDB->db_escape_string($id_object)."' and active='Y'");
							if( $res && $babDB->db_num_rows($res) > 0)
							{
								$arr = $babDB->db_fetch_array($res);
								require_once $GLOBALS['babInstallPath'].'utilit/fileincl.php';

								$oFmFolder = BAB_FmFolderHelper::getFmFolderById($arr['id']);
								if (!is_null($oFmFolder))
								{
									$oOwnerFmFolder = null;
									$sPath = $oFmFolder->getName() . ((mb_strlen(trim($path_object)) > 0 ) ? '/' . $path_object : '');

									$iOldDelegation = bab_getCurrentUserDelegation();
									bab_setCurrentUserDelegation($oFmFolder->getDelegationOwnerId());

									BAB_FmFolderHelper::getInfoFromCollectivePath($sPath, $oFmFolder->getId(), $oOwnerFmFolder);

									bab_setCurrentUserDelegation($iOldDelegation);

									$title_object = empty($title_object) ? $arr['folder'] : $title_object;
									if ($oFmFolder->getDelegationOwnerId() == workspace_getCurrentWorkspace()) {
										$title_object = $this->_make_link(workspace_Controller()->Files()->browse('DG' . workspace_getCurrentWorkspace() . '/' . $sPath)->url(), $title_object);
									} else if(!is_null($oOwnerFmFolder) && (bab_isAccessValid(BAB_FMDOWNLOAD_GROUPS_TBL, $oOwnerFmFolder->getId()) || bab_isAccessValid(BAB_FMMANAGERS_GROUPS_TBL, $oOwnerFmFolder->getId()))) {
										$title_object = $this->_make_link($GLOBALS['babUrlScript']."?tg=fileman&idx=list&id=".$arr['id']."&gr=Y&path=".urlencode($sPath),$title_object);
									}

								}
							}
							workspace_replace::_var($txt,$var,$title_object);
							break;

						case 'LINKPOPUP':
							$url_object = $param[0];
							$title_object = isset($param[1]) ? $param[1] : $url_object;
							$popup = isset($param[2]) ? $param[2] : 2;
							$title_object = $this->_make_link($GLOBALS['babUrlScript']."?tg=link&idx=popup&url=".urlencode($url_object),$title_object, $popup);
							workspace_replace::_var($txt,$var,$title_object);
							break;

						case 'VAR':
							$title_object = $param[0];
							switch($title_object)
								{
								case "BAB_SESS_USERID":
								case "BAB_SESS_NICKNAME":
								case "BAB_SESS_USER":
								case "BAB_SESS_FIRSTNAME":
								case "BAB_SESS_LASTNAME":
								case "BAB_SESS_EMAIL":
									$title_object = $GLOBALS[$title_object];
									break;
								case "babslogan":
								case "adminemail":
								case "adminname":
									$title_object = $babBody->babsite[$title_object];
									break;
								default:
									$title_object = '';
									break;
								}
							workspace_replace::_var($txt,$var,$title_object);
							break;

						case 'OVML':
							$args = array();
							if( ($cnt = count($param)) > 1 )
							{
								for( $i=1; $i < $cnt; $i++)
								{
									$tmp = explode('=', $param[$i]);
									if( is_array($tmp) && count($tmp) == 2 )
										{
										$args[trim($tmp[0])] = trim($tmp[1], '"');
										}
								}
							}

							workspace_replace::_var($txt,$var,preg_replace("/\\\$OVML\(.*\)/","",trim(bab_printOvmlTemplate($param[0], $args))));
							break;
						}
					}
				}
			else
				{
				workspace_replace::_var($txt,$m[1][$k],'');
				}
			}
		}
	}





	/**
	 * Replace reference in HTML string
	 *
	 * this function can replace two type of tag :
	 * 	<a href="ovidentia:///articles/article/12">blabla</a>
	 *  <img src="ovml_placeholder.jpg" longdesc="ovidentia:///ovml/file/example.html" />
	 *
	 *  in the fist cas, the href attribute will be replaced by the target URL from the getUrl() method
	 *  in the second case, the img tag will be replaced by the targeted html replacement string from the getDescription() method
	 *
	 *  @see IReferenceDescription::getUrl()
	 *  @see IReferenceDescription::getDescription()
	 *
	 */
	private static function ovidentia_ref(&$html)
	{
		 $html = preg_replace_callback('/<(?P<tag>a|img)[^>]+(?:href|longdesc)="(?P<reference>ovidentia:\/\/[\w\/\.\-\?&=%]+)"[^>]*(?:>(?P<linkcontent>[^<]+)<\/a>|>)/', array('workspace_replace', 'ovrefreplace'), $html);
	}

	/**
	 * @see bab_replace::ovidentia_ref
	 * @param array $match
	 * @return unknown_type
	 */
	private static function ovrefreplace(Array $match)
	{
		require_once $GLOBALS['babInstallPath'] . 'utilit/reference.class.php';

		// sometimes the string keys are missing because of a bug, workaround :
		$match['tag'] = $match[1];
		$match['reference'] = $match[2];
		if (isset($match[3])) {
			$match['linkcontent'] = $match[3];
		}


		$ref = explode('?', $match['reference']);
		try {
			$reference = new bab_Reference($ref[0]);
		} catch(Exception $e) {
			return sprintf('<span style="color:red">%s</span>', bab_toHtml($e->getMessage()));
		}


		$refDesc = bab_Reference::getReferenceDescription($reference);


		if (!isset($refDesc) || !($refDesc instanceof IReferenceDescription))
		{
			return sprintf('<span style="color:red">%s</span>', bab_toHtml(sprintf(bab_translate('Missing target API for %s'), $match['reference'])));
		}

		if (isset($ref[1])) {
			parse_str($ref[1], $arr);
			$refDesc->setParameters($arr);
		}


		// We check for links to folders: if they are links to folders inside the workspace
		// we redirect to the page in the workspace. Otherwise, we go to ovidentia file manager.
		if (substr($ref[0], 0, strlen('ovidentia:///files/folder/')) === 'ovidentia:///files/folder/') {
			$path = $refDesc->getReference()->getObjectId();
			if (substr($path, 0, 2) !== 'DG') {
				$path = 'DG' . workspace_getCurrentWorkspace() . '/' . $path;
				$refUrl = workspace_Controller()->Files()->browse($path)->url();
			} else {
				$pathElements = explode('/', $path);
				$dg = array_shift($pathElements);
				$delegationId = substr($dg, 2);
				if ($delegationId == workspace_getCurrentWorkspace()) {
					$refUrl = workspace_Controller()->Files()->browse($path)->url();
				} else {
					$refUrl = $refDesc->getUrl();
				}
			}

		} else {

		    try {
			    $refUrl = $refDesc->getUrl();
		    } catch(Exception $e) {
			    return sprintf('<span style="text-decoration: line-through" title="%s (%s)">'. $match['linkcontent'] . '</span>', bab_toHtml($e->getMessage()), $match['reference']);
		    }

		}



		try {
			$access = $refDesc->isAccessValid();
		} catch(Exception $e) {
			return sprintf('<span style="color:red">%s</span>', bab_toHtml($e->getMessage()));
		}

		if ('a' === $match['tag']) {
			if ($access) {
				return str_replace($match['reference'], bab_toHtml($refUrl), $match[0]);
			} else {
				return $match['linkcontent'];
	 		}
		}

		if ('img' === $match['tag']) {
			if ($access) {
				return $refDesc->getDescription();
			} else {
				return '';
			}
		}

		return $match[0];
	}
}




require_once $GLOBALS['babInstallPath'] . 'utilit/template.php';

class workspace_NotificationTemplate extends bab_Template
{

	public $title;
	public $message;

	public $workspaceLabel;
	public $workspaceName;

	public $dateLabel;
	public $date;

	public $workspaceUrl;

	public function __construct($title, $message, $workspaceInfo)
	{
		$this->message = $message;
		$this->title = $title;

		$this->workspaceLabel = workspace_translate('Workspace');
		$this->workspaceName = $workspaceInfo['name'];
		$this->workspaceUrl = bab_toHtml($GLOBALS['babUrl'] . workspace_Controller()->User()->displayHome($workspaceInfo['id'])->url());

		$this->dateLabel = workspace_translate('Date');
		$this->date = bab_strftime(mktime());
	}
}




class workspace_FileNotificationTemplate extends workspace_NotificationTemplate
{
	public $folderLabel;
	public $fileLabel;
	public $authorLabel;

	public $folderName;
	public $fileName;
	public $authorName;

	public $folderUrl;
	public $fileUrl;

	public function __construct($title, $message, $workspaceInfo, BAB_FolderFile $file)
	{
		parent::__construct($title, $message, $workspaceInfo);

		$fmPath = 'DG' . $file->getDelegationOwnerId() . '/' . $file->getPathname();
		$fmPathname = 'DG' . $file->getDelegationOwnerId() . '/' . $file->getPathname() . $file->getName();

		$this->folderLabel = workspace_translate('Folder');
		$this->folderName = $file->getPathname();
		$this->folderUrl = bab_toHtml($GLOBALS['babUrl'] . workspace_Controller()->Files()->browse($fmPath)->url());

		$this->fileLabel = workspace_translate('File');
		$this->fileName = $file->getName();
		$this->fileUrl = bab_toHtml($GLOBALS['babUrl'] . workspace_Controller()->Files()->displayFile($fmPathname)->url());

		$this->authorLabel = workspace_translate('Uploaded by');
		$userInfo = bab_getUserInfos($file->getModifierId());
		$this->authorName = bab_composeUserName($userInfo['givenname'], $userInfo['sn']);
	}
}



/*TODO
class workspace_DirectoryNotificationTemplate extends workspace_NotificationTemplate
{
	public $directoryLabel;
	public $directoryName;

	public $directoryUrl;

	public function __construct($title, $message, $workspaceInfo, $dirid)
	{
		parent::__construct($title, $message, $workspaceInfo);

		$this->authorName = $event->getPostAuthor();

		$this->directoryLabel = workspace_translate('Directory');
		$this->directoryName = $event->getThreadTitle();
		$this->threadUrl = bab_toHtml($GLOBALS['babUrl'] . workspace_Controller()->Forum()->listPosts($event->getThreadId())->url());

		if ($event instanceof bab_eventForumAfterPostAdd) {
			$this->postLabel = workspace_translate('Post');
			$this->postName = bab_getdb;
			$this->postUrl = bab_toHtml($GLOBALS['babUrl'] . workspace_Controller()->Forum()->listPosts($event->getThreadId(), $event->getPostId())->url());
			$this->authorLabel = workspace_translate('Posted by');
		} else {
			$this->postLabel = false;
			$this->authorLabel = workspace_translate('Created by');
		}

	}
}*/




class workspace_ForumNotificationTemplate extends workspace_NotificationTemplate
{
	public $threadLabel;
	public $postLabel;
	public $authorLabel;

	public $threadName;
	public $postName;
	public $authorName;

	public $threadUrl;
	public $postUrl;

	public function __construct($title, $message, $workspaceInfo, bab_eventForumPost $event)
	{
		parent::__construct($title, $message, $workspaceInfo);

		$this->authorName = $event->getPostAuthor();

		$this->threadLabel = workspace_translate('Thread');
		$this->threadName = $event->getThreadTitle();
		$this->threadUrl = bab_toHtml($GLOBALS['babUrl'] . workspace_Controller()->Forum()->listPosts($event->getThreadId(), null, $workspaceInfo['id'])->url());

		if ($event instanceof bab_eventForumAfterPostAdd) {
		    $post = workspace_getPost($event->getPostId());
            $this->postLabel = workspace_translate('Post');
			$this->postName = $post['subject'];
			$this->postUrl = bab_toHtml($GLOBALS['babUrl'] . workspace_Controller()->Forum()->listPosts($event->getThreadId(), $event->getPostId(), $workspaceInfo['id'])->url());
			$this->authorLabel = workspace_translate('Posted by');
		} else {
			$this->postLabel = false;
			$this->authorLabel = workspace_translate('Created by');
		}

	}
}




class workspace_ArticleNotificationTemplate extends workspace_NotificationTemplate
{
	public $articleLabel;
	public $authorLabel;

	public $articleName;
	public $authorName;

	public $articleUrl;

	public function __construct($title, $message, $workspaceInfo, $author, $articleId)
	{
		parent::__construct($title, $message, $workspaceInfo);

		$this->authorName = $author;

		$article = workspace_getArticle($articleId);
		$this->articleLabel = workspace_translate('Article');
		$this->articleName = $article['title'];
		$this->articleUrl = bab_toHtml($GLOBALS['babUrl'] . workspace_Controller()->Articles()->show($articleId, $workspaceInfo['id'])->url());
		$this->authorLabel = workspace_translate('Published by');

	}
}




class workspace_CalendarNotificationTemplate extends workspace_NotificationTemplate
{
	public $eventLabel;
	public $authorLabel;

	public $eventName;
	public $authorName;

	public $eventUrl;

	public function __construct($title, $message, $workspaceInfo, $period)
	{
		parent::__construct($title, $message, $workspaceInfo);


		$this->eventName = $period->getProperty('SUMMARY');
		$this->eventDescription = $period->getProperty('DESCRIPTION');
		$this->eventLocation = $period->getProperty('LOCATION');
		$this->begin = bab_longDate($period->ts_begin);
		$this->end = bab_longDate($period->ts_end);

		$this->authorName = $author;

		$this->eventLabel = workspace_translate('Event');
		$this->locationLabel = workspace_translate('Location');
		$this->descriptionLabel = workspace_translate('Description');
		$this->eventUrl = bab_toHtml($GLOBALS['babUrl'] . workspace_Controller()->Calendars()->display(workspace_CtrlCalendars::TYPE_WORKWEEK, BAB_DateTime::fromTimeStamp($period->ts_begin)->getIsoDate(), $workspaceInfo['id'])->url());

	}
}




/**
 * Notify file recipient for one file by email
 * when a file has been uploaded or a file has been updated
 *
 * @param BAB_FolderFile 	$file		Filename
 * @param Array 			$users		users to notify
 * @param array				$workspaceInfo
 * @param string			$title		title displayed in email
 * @param string			$message	message displayed in email
 * @param bool				$bnew		true = new file uploaded | false = the file has been updated
 *
 * @return bool
 */
function workspace_fileNotifyMembers(BAB_FolderFile $file, $users, $workspaceInfo, $title, $message = '', $bnew = true)
{
	global $babBody, $BAB_SESS_USER, $BAB_SESS_EMAIL, $babAdminEmail, $babInstallPath;
	include_once $babInstallPath . 'utilit/mailincl.php';
	include_once $babInstallPath . 'admin/acl.php';

	require_once dirname(__FILE__) . '/workspace.ctrl.php';

	$mail = bab_mail();
	if ($mail == false) {
		return;
	}
	$mailBCT = 'mail'.$babBody->babsite['mail_fieldaddress'];
	$clearBCT = 'clear'.$babBody->babsite['mail_fieldaddress'];

	$mail->mailFrom($babAdminEmail, $GLOBALS['babAdminName']);

	if ($bnew) {
		$mail->mailSubject(sprintf(workspace_translate('New file in workspace %s'), $workspaceInfo['name']));
	} else {
		$mail->mailSubject(sprintf(workspace_translate('A file has been updated in workspace %s'), $workspaceInfo['name']));
	}

	$addonInfo = bab_getAddonInfosInstance('workspace');
	$addonTemplatePath = $addonInfo->getRelativePath();

	$fileNotificationTemplate = new workspace_FileNotificationTemplate($title, $message, $workspaceInfo, $file);

	$message = bab_printTemplate($fileNotificationTemplate, $addonTemplatePath.'email.html', 'file_html');
	$messagetxt = bab_printTemplate($fileNotificationTemplate, $addonTemplatePath.'email.html', 'file_text');

	$message = $mail->mailTemplate($message);

	$mail->mailBody($message, 'html');
	$mail->mailAltBody($messagetxt);


	$arrusers = array();
	$count = 0;
	$result = true;
	foreach($users as $id => $arr) {
		if (count($arrusers) == 0 || !isset($arrusers[$id])) {
			$arrusers[$id] = $id;
			$mail->$mailBCT($arr['email'], $arr['name']);
			$count++;
		}

		if ($count == $babBody->babsite['mail_maxperpacket']) {
			$result = $mail->send();
			$mail->$clearBCT();
			$mail->clearTo();
			$count = 0;

			if (!$result) {
				return false;
			}
		}
	}

	if ($count > 0) {
		$result = $mail->send();
		$mail->$clearBCT();
		$mail->clearTo();
		$count = 0;
	}

	if (!$result) {
		return false;
	}

	return true;
}



/*TODO
function workspace_directoryNotifyMembers($directory, $users, $workspaceInfo, $title, $message = '')
{
	global $babBody, $BAB_SESS_USER, $BAB_SESS_EMAIL, $babAdminEmail, $babInstallPath;
	include_once $babInstallPath . 'utilit/mailincl.php';
	include_once $babInstallPath . 'admin/acl.php';

	require_once dirname(__FILE__) . '/workspace.ctrl.php';

	$mail = bab_mail();
	if ($mail == false) {
		return;
	}
	$mailBCT = 'mail'.$babBody->babsite['mail_fieldaddress'];
	$clearBCT = 'clear'.$babBody->babsite['mail_fieldaddress'];

	$mail->mailFrom($babAdminEmail, $GLOBALS['babAdminName']);

	$mail->mailSubject($title);

	$addonInfo = bab_getAddonInfosInstance('workspace');
	$addonTemplatePath = $addonInfo->getRelativePath();

	$fileNotificationTemplate = new workspace_FileNotificationTemplate($title, $message, $workspaceInfo, $file);

	$message = bab_printTemplate($fileNotificationTemplate, $addonTemplatePath.'email.html', 'file_html');
	$messagetxt = bab_printTemplate($fileNotificationTemplate, $addonTemplatePath.'email.html', 'file_text');

	$mail->mailBody($message, 'html');
	$mail->mailAltBody($messagetxt);


	$arrusers = array();
	$count = 0;
	$result = true;
	foreach($users as $id => $arr) {
		if (count($arrusers) == 0 || !isset($arrusers[$id])) {
			$arrusers[$id] = $id;
			$mail->$mailBCT($arr['email'], $arr['name']);
			$count++;
		}

		if ($count == $babBody->babsite['mail_maxperpacket']) {
			$result = $mail->send();
			$mail->$clearBCT();
			$mail->clearTo();
			$count = 0;

			if (!$result) {
				return false;
			}
		}
	}

	if ($count > 0) {
		$result = $mail->send();
		$mail->$clearBCT();
		$mail->clearTo();
		$count = 0;
	}

	if (!$result) {
		return false;
	}

	return true;
}*/




/**
 * Notify file recipient for one thread/post by email
 * when a thread/post has been created
 *
 * @param string 			$author
 * @param int 				$threadId
 * @param int 				$postId
 * @param Array 			$users		users to notify
 * @param array				$workspaceInfo
 * @param string			$title		title displayed in email
 * @param string			$message	message displayed in email
 *
 * @return bool
 */
function workspace_forumNotifyMembers(bab_eventForumPost $event, $users, $workspaceInfo, $title, $message = '', $changeType = null)
{
	global $babBody, $BAB_SESS_USER, $BAB_SESS_EMAIL, $babAdminEmail, $babInstallPath;
	include_once $babInstallPath . 'utilit/mailincl.php';
	include_once $babInstallPath . 'admin/acl.php';

	require_once dirname(__FILE__) . '/workspace.ctrl.php';

	$mail = bab_mail();
	if ($mail == false) {
		return;
	}
	$mailBCT = 'mail'.$babBody->babsite['mail_fieldaddress'];
	$clearBCT = 'clear'.$babBody->babsite['mail_fieldaddress'];

	$mail->mailFrom($babAdminEmail, $GLOBALS['babAdminName']);

//	$mail->mailSubject($title);

	switch ($changeType) {
		case 'newThread':
			$mail->mailSubject(sprintf(workspace_translate('A new thread has been created in workspace %s'), $workspaceInfo['name']));
			break;
		case 'newPost':
		default:
			$mail->mailSubject(sprintf(workspace_translate('A message has been posted in workspace %s'), $workspaceInfo['name']));
			break;
	}

	$addonInfo = bab_getAddonInfosInstance('workspace');
	$addonTemplatePath = $addonInfo->getRelativePath();

	$forumNotificationTemplate = new workspace_ForumNotificationTemplate($title, $message, $workspaceInfo, $event);

//	$message = $mail->mailTemplate(bab_printTemplate($fileNotifyMembersTemplate, $addonTemplatePath.'email.html', 'file_html'));
	$message = bab_printTemplate($forumNotificationTemplate, $addonTemplatePath.'email.html', 'forum_html');
	$messagetxt = bab_printTemplate($forumNotificationTemplate, $addonTemplatePath.'email.html', 'forum_text');

	$message = $mail->mailTemplate($message);

	$mail->mailBody($message, 'html');
	$mail->mailAltBody($messagetxt);


	$arrusers = array();
	$count = 0;
	foreach ($users as $id => $arr) {
		if (count($arrusers) == 0 || !isset($arrusers[$id])) {
			$arrusers[$id] = $id;
			$mail->$mailBCT($arr['email'], $arr['name']);
			$count++;
		}

		if ($count == $babBody->babsite['mail_maxperpacket']) {
			$result = $mail->send();
			$mail->$clearBCT();
			$mail->clearTo();
			$count = 0;

			if (!$result) {
				return false;
			}
		}
	}

	if ($count > 0) {
		$result = $mail->send();
		$mail->$clearBCT();
		$mail->clearTo();
		$count = 0;
		if (!$result) {
            return false;
	   }
	}


	return true;
}






/**
 * Notify file recipient for one thread/post by email
 * when a thread/post has been created
 *
 * @param string 			$author
 * @param int 				$articleId
 * @param Array 			$users			Users to notify.
 * @param array				$workspaceInfo
 * @param string			$title			Title displayed in email.
 * @param string			$message		Message displayed in email.
 * @param string    		$changeType		Type of change. One of 'newArticle' or 'updatedArticle'.
 *
 * @return bool
 */
function workspace_articleNotifyMembers($author, $articleId, $users, $workspaceInfo, $title, $message = '', $changeType = null)
{
	global $babBody, $BAB_SESS_USER, $BAB_SESS_EMAIL, $babAdminEmail, $babInstallPath;
	include_once $babInstallPath . 'utilit/mailincl.php';
	include_once $babInstallPath . 'admin/acl.php';

	require_once dirname(__FILE__) . '/workspace.ctrl.php';

	$mail = bab_mail();
	if ($mail == false) {
		return;
	}
	$mailBCT = 'mail'.$babBody->babsite['mail_fieldaddress'];
	$clearBCT = 'clear'.$babBody->babsite['mail_fieldaddress'];

	$mail->mailFrom($babAdminEmail, $GLOBALS['babAdminName']);

//	$mail->mailSubject($title);

	switch ($changeType) {
		case 'updatedArticle':
			$mail->mailSubject(sprintf(workspace_translate('An article has been edited in workspace %s'), $workspaceInfo['name']));
			break;
		case 'newArticle':
		default:
			$mail->mailSubject(sprintf(workspace_translate('An article has been published in workspace %s'), $workspaceInfo['name']));
			break;
	}

	$addonInfo = bab_getAddonInfosInstance('workspace');
	$addonTemplatePath = $addonInfo->getRelativePath();

	$articleNotificationTemplate = new workspace_ArticleNotificationTemplate($title, $message, $workspaceInfo, $author, $articleId);

	$message = bab_printTemplate($articleNotificationTemplate, $addonTemplatePath.'email.html', 'article_html');
	$messagetxt = bab_printTemplate($articleNotificationTemplate, $addonTemplatePath.'email.html', 'article_text');

	$message = $mail->mailTemplate($message);

	$mail->mailBody($message, 'html');
	$mail->mailAltBody($messagetxt);


	$arrusers = array();
	$count = 0;
	foreach($users as $id => $arr) {
		if (count($arrusers) == 0 || !isset($arrusers[$id])) {
			$arrusers[$id] = $id;
			$mail->$mailBCT($arr['email'], $arr['name']);
			$count++;
		}

		if ($count == $babBody->babsite['mail_maxperpacket']) {
			$result = $mail->send();
			$mail->$clearBCT();
			$mail->clearTo();
			$count = 0;

			if (!$result) {
				return false;
			}
		}
	}

	if ($count > 0) {
		$result = $mail->send();
		$mail->$clearBCT();
		$mail->clearTo();
		$count = 0;
	}

	if (!$result) {
		return false;
	}

	return true;
}






/**
 * Notify file recipient for one thread/post by email
 * when a thread/post has been created
 *
 * @param string 			$author
 * @param int 				$articleId
 * @param Array 			$users		users to notify
 * @param array				$workspaceInfo
 * @param string			$title		title displayed in email
 * @param string			$message	message displayed in email
 *
 * @return bool
 */
function workspace_calendarNotifyMembers($period, $users, $workspaceInfo, $title, $message = '')
{
	global $babBody, $BAB_SESS_USER, $BAB_SESS_EMAIL, $babAdminEmail, $babInstallPath;
	include_once $babInstallPath . 'utilit/mailincl.php';
	include_once $babInstallPath . 'admin/acl.php';

	require_once dirname(__FILE__) . '/workspace.ctrl.php';

	$mail = bab_mail();
	if ($mail == false) {
		return;
	}
	$mailBCT = 'mail'.$babBody->babsite['mail_fieldaddress'];
	$clearBCT = 'clear'.$babBody->babsite['mail_fieldaddress'];

	$mail->mailFrom($babAdminEmail, $GLOBALS['babAdminName']);

	$mail->mailSubject($title);

	$addonInfo = bab_getAddonInfosInstance('workspace');
	$addonTemplatePath = $addonInfo->getRelativePath();

	$calendarNotificationTemplate = new workspace_CalendarNotificationTemplate($title, $message, $workspaceInfo, $period);

	$message = bab_printTemplate($calendarNotificationTemplate, $addonTemplatePath.'email.html', 'calendar_html');
	$messagetxt = bab_printTemplate($calendarNotificationTemplate, $addonTemplatePath.'email.html', 'calendar_text');

	$message = $mail->mailTemplate($message);

	$mail->mailBody($message, 'html');
	$mail->mailAltBody($messagetxt);


	$arrusers = array();
	$count = 0;
	foreach($users as $id => $arr) {
		if (count($arrusers) == 0 || !isset($arrusers[$id])) {
			$arrusers[$id] = $id;
			$mail->$mailBCT($arr['email'], $arr['name']);
			$count++;
		}

		if ($count == $babBody->babsite['mail_maxperpacket']) {
			$result = $mail->send();
			$mail->$clearBCT();
			$mail->clearTo();
			$count = 0;

			if (!$result) {
				return false;
			}
		}
	}

	if ($count > 0) {
		$result = $mail->send();
		$mail->$clearBCT();
		$mail->clearTo();
		$count = 0;
	}

	if (!$result) {
		return false;
	}

	return true;
}







/**
 *
 * @param int $workspaceId
 * @param int $userId
 */
function workspace_notifyNewMember($workspaceId, $userId)
{
    global $babAdminEmail, $babInstallPath;
    include_once $babInstallPath . 'utilit/mailincl.php';
    include_once $babInstallPath . 'admin/acl.php';

    require_once dirname(__FILE__) . '/workspace.ctrl.php';

    $mail = bab_mail();
    if ($mail == false) {
        return;
    }

    $userEmail = bab_getUserEmail($userId);
    $userName = bab_getUserName($userId, true);


    $workspaceInfo = workspace_getWorkspaceInfo($workspaceId, true);

    $title = sprintf(
        workspace_translate('You have been invited to workspace %s'),
        $workspaceInfo['name']
    );
    $message = sprintf(
        workspace_translate('__NEW_MEMBER_MESSAGE_HTML__'),
        bab_toHtml($workspaceInfo['name']),
        bab_toHtml($GLOBALS['babUrl'] . workspace_Controller()->User()->displayHome($workspaceInfo['id'])->url())
    );
    $messagetxt = sprintf(
        workspace_translate('__NEW_MEMBER_MESSAGE_TXT__'),
        $workspaceInfo['name'],
        $GLOBALS['babUrl'] . workspace_Controller()->User()->displayHome($workspaceInfo['id'])->url()
    );

    $addonInfo = bab_getAddonInfosInstance('workspace');
    $addonTemplatePath = $addonInfo->getRelativePath();
    $tpl = new workspace_NotificationTemplate('', $message, $workspaceInfo);

    $message = bab_printTemplate($tpl, $addonTemplatePath.'email.html', 'simple_html');
    $messagetxt = bab_printTemplate($tpl, $addonTemplatePath.'email.html', 'simple_text');

    $message = $mail->mailTemplate($message);

    $mail->mailFrom($babAdminEmail, $GLOBALS['babAdminName']);
    $mail->mailSubject($title);
    $mail->mailBody($message, 'html');
    $mail->mailAltBody($messagetxt);

    $mail->mailTo($userEmail, $userName);

    $result = $mail->send();

    if (!$result) {
        return false;
    }

    return true;
}




/**
 * @return string		The date format.
 */
function workspace_getWorkspacesDateFormat()
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace');
	$dateFormat = $registry->getValue('dateFormat', '%d %j %M %Y');

	return $dateFormat;
}


/**
 * @return string		The time format.
 */
function workspace_getWorkspacesTimeFormat()
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace');
	$dateFormat = $registry->getValue('timeFormat', '%H:%i');

	return $dateFormat;
}




/**
 * @param string	$dateFormat		The date format.
 */
function workspace_setWorkspacesDateFormat($dateFormat)
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace');
	$registry->setKeyValue('dateFormat', $dateFormat);
}



/**
 * @param string	$timeFormat		The time format.
 */
function workspace_setWorkspacesTimeFormat($timeFormat)
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace');
	$registry->setKeyValue('timeFormat', $timeFormat);
}


