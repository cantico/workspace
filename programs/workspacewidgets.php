<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';


bab_Functionality::includefile('Widgets');
bab_functionality::get('Widgets')->includePhpClass('Widget_VBoxLayout');



/**
 *
 * @param string	$labelText
 * @param string	$inputWidget
 * @param string	$descriptionText
 * @return Widget_Item
 */
function workspace_FormField($labelText, Widget_InputWidget $inputWidget, $descriptionText = null)
{
	$W = bab_Widgets();

	$formField = $W->VBoxLayout()->setVerticalSpacing(0.2, 'em');

	if ($inputWidget instanceof Widget_CheckBox) {
		$formField->addItem(
			$W->HBoxItems(
				$inputWidget,
				$W->Label($labelText)->setAssociatedWidget($inputWidget)
			)->setVerticalAlign('middle')
		);
	} else {
		$formField->addItem($W->Label($labelText)->setAssociatedWidget($inputWidget));
		$formField->addItem($inputWidget);
	}
	if (isset($descriptionText)) {
		$formField->addItem($W->Label($descriptionText)->addClass('workspace-form-description'));
	}

	return $formField;
}




/**
 * An a empty form.
 * @return workspace_BaseForm
 */
function workspace_BaseForm($id = null)
{
	return new workspace_BaseForm($id);
}

/**
 * An a empty form with a button box.
 */
class workspace_BaseForm extends Widget_Frame
{

	private $outerLayout = null;
	private $innerLayout = null;
	private $buttonRow = null;

	public function __construct($id)
	{
		parent::__construct($id);

		bab_functionality::get('Widgets')->includePhpClass('Widget_VBoxLayout');
//		$this->addClass('BabLoginMenuBackground');

		$this->outerLayout = Widget_VBoxLayout()
								->addClass('workspace-form-layout')
								->setVerticalSpacing(2, 'em');
		$this->setLayout($this->outerLayout);

		$this->innerLayout = Widget_VBoxLayout()
								->setVerticalSpacing(1, 'em');

		$this->outerLayout->addItem($this->innerLayout);

		bab_functionality::get('Widgets')->includePhpClass('Widget_HBoxLayout');
		$this->buttonRow = Widget_HBoxLayout()
							->setHorizontalSpacing(1, 'em')
							->addClass('workspace-form-button-row');

		$this->outerLayout->addItem($this->buttonRow);
	}

	/**
	 * Adds an item to the top part of the form.
	 *
	 * @return workspace_BaseForm
	 */
	public function addItem(Widget_Displayable_Interface $item = null)
	{
		$this->innerLayout->addItem($item);

		return $this;
	}

	/**
	 * Adds a button to button box of the form.
	 *
	 * @return workspace_BaseForm
	 */
	public function addButton(Widget_SubmitButton $button)
	{
		$this->buttonRow->addItem($button);

		return $this;
	}

}




/**
 * An a empty form with a button box.
 */
class workspace_BaseList extends Widget_VBoxLayout
{
    /**
     * {@inheritDoc}
     * @see Widget_VBoxLayout::addItem()
     */
	public function addItem(Widget_Displayable_Interface $item = null, $position = null)
	{
		$item->setSizePolicy('widget-list-element');
		parent::addItem($item, $position);
	}

}





function workspace_setSelfPageHiddenFields(Widget_Form $form)
{
	$W = bab_functionality::get('Widgets');
	$canvas = $W->HtmlCanvas();
	$context = array_keys($_POST + $_GET);
	$fields = $form->getFields();

	$fnames = array();
	foreach($fields as $f) {
		$htmlname = $canvas->getHtmlName($f->getFullName());
		if (!empty($htmlname)) {
			$fnames[] = $htmlname;
		}
	}

	$context = array_diff($context, $fnames);

	foreach($context as $fieldname) {
		$value = bab_rp($fieldname);
		if (!is_array($value)) {
			$form->setHiddenValue($fieldname, $value);
		}
	}
}


/**
 *
 * @return Widget_Form
 */
function workspace_workspaceSelectorForm()
{
	require_once dirname(__FILE__) . '/workspaces.php';
	require_once $GLOBALS['babInstallPath'] . '/utilit/delegincl.php';

	$W = bab_functionality::get('Widgets');

	$workspaces = workspace_getWorkspaceList();

	$select = $W->Select();
	$select->setName('workspace');
	foreach ($workspaces as $workspace) {
		$select->addOption($workspace['id'], $workspace['name']);
	}

	$select->setValue(bab_getCurrentUserDelegation());

	$form = $W->Form();
	$form->addClass('workspace-selector');
	$form->setLayout($W->HBoxLayout())
		->addItem($select)
		->addItem($W->SubmitButton(workspace_translate("Go"))->setAction(workspace_Controller()->User()->selectWorkspace()))
		;

	workspace_setSelfPageHiddenFields($form);

	return $form;
}


function workspace_workspaceSelector()
{
	require_once dirname(__FILE__) . '/workspaces.php';
	require_once $GLOBALS['babInstallPath'] . '/utilit/delegincl.php';

	$W = bab_functionality::get('Widgets');

	$workspaces = workspace_getWorkspaceList();

	foreach ($workspaces as $workspace) {
		if ($workspace['id'] == bab_getCurrentUserDelegation()) {
			$select = $W->Link($workspace['name'], workspace_Controller()->User()->listWorkspaces());
		}
	}

	return $select;
}


