<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';





/**
 * Instanciates the App factory.
 *
 * @return Func_App_Workspace
 */
function workspace_App()
{
    return bab_Functionality::get('App/Workspace');
}

function workspace_Controller()
{
    $App = workspace_App();
    return $App->Controller();
}


function workspace_errorHandler($errno, $errstr, $errfile, $errline)
{
	switch ($errno) {
		case E_ERROR:
		case E_USER_ERROR:
			$errorstr = "ERROR :\n";
			$errorlevel = DBG_ERROR;
			break;

		case E_WARNING:
		case E_USER_WARNING:
			$errorstr = "WARNING :\n";
			$errorlevel = DBG_WARNING;
			break;

		case E_NOTICE:
		case E_USER_NOTICE:
			$errorstr = "NOTICE :\n";
			$errorlevel = DBG_INFO;
			break;

		case E_STRICT:
			return; // On ne traite pas les E_STRICT
			$errorstr = "STRICT :\n";
			$errorlevel = DBG_INFO;
			break;

		default:
			$errorstr = "Erreur {$errno}\n";
			$errorlevel = DBG_ERROR;
			break;
    }

	$errorstr .= "<span style=\"font-weight:normal;\">{$errstr}\n"
				. "{$errfile}</span>\n"
				. "Ligne {$errline}\n";

	if (function_exists('bab_debug')) {
		bab_debug($errorstr, $errorlevel, 'PHP Errors');
	} else {
//		echo '<pre>'.$errorstr.'</pre>';
	}
	if (isset($babInstallPath)) {
		include_once $babInstallPath . 'utilit/devtools.php';
	}
	if (function_exists('bab_debug_print_backtrace')) {
//	bab_debug_print_backtrace();
	}
}


//set_error_handler('workspace_errorHandler');



define('WORKSPACE_PREFIX',		'workspace');



/**
 * This exception is thrown when the system tries to perform
 * an operation on an object that the user is not allowed to.
 */
class workspace_AccessException extends Exception
{
}



/**
 * Translates the string.
 *
 * @param string $str
 * @return string
 */
function workspace_translate($str, $str_plurals = null, $number = null)
{
    if ($translate = bab_functionality::get('Translate/Gettext'))
    {
        /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('workspace');

        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}





// /**
//  * Translates the string.
//  *
//  * @param string $str
//  * @return string
//  */
// function workspace_translate($str)
// {
// 	$translation = bab_translate($str, 'workspace');

// 	return $translation;
// }

/**
 * Translates all the string in an array and returns a new array.
 *
 * @param array $arr
 * @return array
 */
function workspace_translateArray($arr)
{
	$newarr = $arr;

	foreach ($newarr as &$str) {
		$str = workspace_translate($str);
	}
	return $newarr;
}


/**
 * Instanciates the widget factory.
 *
 * @return Func_Widgets
 */
function workspace_WidgetFactory()
{
	$W = bab_Functionality::get('Widgets');
	assert('$W instanceof Func_Widgets; /* Could not instanciate the Func_Widgets functionality. */');

	// Includes definitions of stock icons.
	$W->includePhpClass('Widget_Icon');

	return $W;
}





/**
 *
 * @param int $workspaceId
 * @return string
 */
function workspace_reference($workspaceId)
{
    return 'workspace.' . $workspaceId;
}




/**
 *
 * @param int $workspaceId
 * @return string
 */
function workspace_buttonBox()
{
    $W = bab_Widgets();
    $buttonBox = $W->FlowItems();
    $buttonBox->setHorizontalSpacing(4, 'px');
    $buttonBox->addClass(Func_Icons::ICON_LEFT_SYMBOLIC, 'widget-button-box');

    return $buttonBox;
}




function workspace_userPreview($userId, $subText = null)
{
    $W = bab_Widgets();
    $dirEntry = bab_getDirEntry($userId, BAB_DIR_ENTRY_ID_USER);

    $userName = bab_getUserName($userId, true);

    $authorIcon = $W->Icon($userName . "\n" . $subText);

    $authorIcon->setStockImage(Func_Icons::APPS_USERS);

    if (isset($dirEntry['jpegphoto']['photo'])) {
        $photo = $dirEntry['jpegphoto']['photo'];

        /* @var $T Func_Thumbnailer */
        $T = bab_functionality::get('Thumbnailer');

        if ($T && $photo instanceof bab_dirEntryPhoto) {
            $T->setSourceBinary($photo->getData(), $photo->lastUpdate());
            $T->setBorder(1, '#cccccc', 0);
            $T->setResizeMode(Func_Thumbnailer::CROP_CENTER);
            $imageUrl = $T->getThumbnail(46, 46);
//             $T->setBorder(1, '#cccccc');
//             $imageUrl = $T->getThumbnail(30, 30);
            $authorIcon->setImageUrl($imageUrl);
        }
    }

    return $authorIcon;
}


/**
 * Creates a folder in the addon's upload folder.
 *
 * @param string	$pathname	The full pathname of the new folder.
 * @return bool
 */
function workspace_createFolder($pathname)
{
	if (strncmp($pathname, $GLOBALS['babAddonUpload'], strlen($GLOBALS['babAddonUpload']))) {
		return false;
	}
	$folder = $GLOBALS['babAddonUpload'];
	if (!is_dir($folder)) {
		if (!bab_mkdir($folder)) {
			return false;
		}
	}
	$relativePath = spaces_path(substr($pathname, strlen($GLOBALS['babAddonUpload'])));
	$subfolders = explode('/', $relativePath);

	foreach ($subfolders as $subfolder) {
		$folder .= $subfolder . '/';
		if (!is_dir($folder)) {
			if (!bab_mkdir($folder)) {
				return false;
			}
		}
	}
	return true;
}





/**
 * Concats all strings in the array $subPaths to form a path.
 * @see workspace_path
 *
 * @param array $subPaths	An array of string
 */
function workspace_concatPaths($subPaths)
{
	$isRelativePath = (substr($subPaths[0], 0, 1) !== '/');

	$allElements = array();
	foreach ($subPaths as $subPath) {
		$elements = explode('/', $subPath);
		foreach ($elements as $element) {
			if ($element === '..' && count($allElements) > 0) {
				array_pop($allElements);
			} elseif ($element !== '.' && $element !== '..' && $element !== '') {
				array_push($allElements, $element);
			}
		}
	}

	$path = implode('/', $allElements);
	if (!$isRelativePath) {
		$path = '/' . $path;
	}

	return $path;
}



/**
 * Concats all strings to form a path.
 * Eg. workspace_path('a/b', 'c', 'd/e/', 'f/') => 'a/b/c/d/e/f'
 *
 * @param	string	$subPath...		On or more sub paths.
 * @return	string
 */
function workspace_path($subPath)
{
	$subPaths = func_get_args();

	$path = workspace_concatPaths($subPaths);

	return $path;
}






//function workspace_redirect($action)
//{
//	$controller = new workspace_Controller();
//	if (!($action instanceof Widget_Action)) {
//		$action = Widget_Action::fromUrl($action);
//	}
//	$controller->execute($action);
//}


/**
 * Redirects to the specified url or action.
 * This function makes the current script die.
 *
 * @param Widget_Action|string $url
 */
function workspace_redirect($url)
{
	global $babBody;



	if ($url instanceof Widget_Action) {
		$url = $url->url();
	}
	if (!empty($babBody->msgerror)) {
		$url .=  '&msgerror=' . urlencode($babBody->msgerror);
	}

	if (!empty($babBody->messages)) {
        $url .=  '&msg=' . urlencode(implode('<br />', $babBody->messages));
	}

	header('Location: ' . $url);
	die;
}





/**
 * @param string	$id
 *
 * @return Widget_Frame
 */
function workspace_Toolbar($id = null)
{
    $W = bab_Widgets();

    $toolbar = $W->Frame($id)
        ->setLayout($W->FlowLayout()->setVerticalAlign('top'))
        ->addClass('widget-toolbar', Func_Icons::ICON_LEFT_32);

    return $toolbar;
}





/**
 * @param string	$id
 *
 * @return Widget_Frame
 */
function workspace_Locationbar($id = null)
{
	$W = bab_Widgets();

	$locationbar = $W->Frame('location-toolbar')
						->setLayout($W->HBoxLayout()->setHorizontalSpacing(0.5, 'em'))
						->addClass('icon-left-16')->addClass('icon-16x16')->addClass('icon-left')
						;
	return $locationbar;
}





/**
 *
 * @param string $message
 */
function workspace_accessDenied($message)
{
	global $babBody;

	$babBody->addError($message);
	$babBody->addError('');
}





/**
 * @param	mixed	$value
 * @return	string
 */
function workspace_json_encode($a)
{
	if (is_null($a)) return 'null';
	if ($a === false) return 'false';
	if ($a === true) return 'true';
	if (is_scalar($a)) {
		if (is_float($a)) {
			// Always use "." for floats.
			return floatval(str_replace(",", ".", strval($a)));
		}

		if (is_string($a)) {
			static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
			return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
		} else {
			return $a;
		}
	}
	require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';
	if ($a instanceof BAB_DateTime) {
		/* @var $a BAB_DateTime */
		return 'new Date(Date.UTC(' . $a->getYear() . ',' . ($a->getMonth() - 1) . ',' . $a->getDayOfMonth() . ',' . $a->getHour() . ',' . $a->getMinute() . ',' . $a->getSecond() . ',0))';
	}
	if (is_object($a) && method_exists($a, 'toJson')) {
		return $a->toJson();
	}
	$isList = true;
	for ($i = 0, reset($a); $i < count($a); $i++, next($a)) {
		if (key($a) !== $i) {
			$isList = false;
			break;
		}
	}
	$result = array();
	if ($isList) {
		foreach ($a as $v) {
			$result[] = workspace_json_encode($v);
		}
		return '[' . join(',', $result) . ']';
	} else {
		foreach ($a as $k => $v) {
			$result[] = workspace_json_encode($k).':'.workspace_json_encode($v);
		}
		return '{' . join(',', $result) . '}';
	}
}





/**
 * Return true if the current user is attached only to groups of workspaces, not to groups of Ovidentia for others access.
 * This function is called by init.php for an automatic redirect of users after authentification if they are only in groups of workspaces.
 *
 * @return bool
 */
function workspace_isUserOnlyInGroupsOfWorkspaces()
{
	require_once dirname(__FILE__) . '/workspaces.php';

	/* Array contains only groups that the user must be in */
	$allGroupsOfWorkspacesAccessibleToUser = array();

	/* Groups of the current user */
	$userGroups = bab_getUserGroups();
	/* List of workspaces accessibles to the current user */
	$workspacesAccessibleToUser = workspace_getWorkspaceList();
	$idWorkspaces = array_keys($workspacesAccessibleToUser);
	foreach ($idWorkspaces as $idWorkspace) {
		/* Id of group associated to the workspace (and delegation) */
		$groupId = workspace_getWorkspaceGroupId($idWorkspace);
		$allGroupsOfWorkspacesAccessibleToUser[$groupId] = $groupId;
		/* Id of all groups descendant to the group */
		$groups = bab_getGroups($groupId, true);
		foreach ($groups['id'] as $id) {
			$allGroupsOfWorkspacesAccessibleToUser[$id] = $id;
		}
	}
	foreach ($userGroups['id'] as $idgroup) {
		if (!isset($allGroupsOfWorkspacesAccessibleToUser[$idgroup])) {
			/* The current user is in a group outside workspaces */
			return false;
		}
	}

	return true;
}





/**
 * Ensures that the workspace name does not contain cabalistic characters.
 *
 * @param string $name	The initial workspace name.
 * @return string		The sanitized workspace name.
 */
function workspace_sanitizeWorkspaceName($name)
{
	if (bab_Charset::getIso() !== bab_Charset::UTF_8) {
		$name = iconv(bab_Charset::getIso(), bab_Charset::UTF_8, $name);
		$name = html_entity_decode($name, ENT_QUOTES, bab_Charset::UTF_8);
		$name = iconv(bab_Charset::UTF_8, bab_Charset::getIso() .'//TRANSLIT', $name);
	}
	return $name;
}





/**
 * Returns the formatted date.
 *
 * @param int $time			A unix timestamp
 * @return string
 */
function workspace_formatDate($time)
{
	$format = workspace_getWorkspacesDateFormat();
	return bab_formatDate($format, $time);
}

/**
 * Returns the formatted time.
 *
 * @param int $time			A unix timestamp
 * @return string
 */
function workspace_formatTime($time)
{
	$format = workspace_getWorkspacesTimeFormat();
	return bab_formatDate($format, $time);
}

/**
 * Returns the formatted date and time.
 *
 * @param int $time			A unix timestamp
 * @return string
 */
function workspace_formatDateTime($time)
{
	$format = workspace_getWorkspacesDateFormat() . ' ' . workspace_getWorkspacesTimeFormat();
	return bab_formatDate($format, $time);
}





bab_Functionality::get('Icons');
