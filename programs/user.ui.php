<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/workspacewidgets.php';







/**
 * @return Widget_Frame
 */
function workspace_WorkspaceUserList()
{
	require_once dirname(__FILE__) . '/workspaces.php';

	$W = bab_Widgets();

	$listView = new workspace_BaseList('workspace_list');
	$listView->addClass('workspace-large-list');
	$listView->setIconStyle(48, 'left');


	$workspaces = workspace_getWorkspaceList();

	foreach ($workspaces as $workspace) {
		$workspaceRow = $W->VBoxItems(
							$W->Link($W->Icon($workspace['name'], Func_Icons::PLACES_FOLDER), workspace_Controller()->User()->selectWorkspace($workspace['id'])),
							$W->Label(substr(strip_tags($workspace['description']), 0, 80))
						);
		$listView->addItem($workspaceRow);
	}


//	$tagView = $W->Frame()->setLayout($W->VBoxLayout())
//					->addItem($W->Label('Tags'))
//					->addClass('workspace-resizable');

	$splitview = $W->Frame('workspace_article_splitview', $W->HBoxLayout()->addClass('expand'))
//							->addItem($tagView->setSizePolicy(Widget_SizePolicy::MINIMUM))
							->addItem($listView->setSizePolicy(Widget_SizePolicy::MAXIMUM))
							;

	return $splitview;
}




/**
 * @return Widget_Html
 */
function workspace_PageContent($htmlContent)
{
	$W = bab_Widgets();

	$html = $W->Html($htmlContent);

	return $html;
}
