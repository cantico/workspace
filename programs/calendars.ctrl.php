<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/controller.class.php';
require_once dirname(__FILE__) . '/page.class.php';






/**
 * This controller manages actions that can be performed on calendars.
 */
class workspace_CtrlCalendars extends workspace_Controller
{
	const TYPE_MONTH = 'month';
	const TYPE_WEEK = 'week';
	const TYPE_WORKWEEK = 'workweek';
	const TYPE_DAY = 'day';
	const TYPE_TIMELINE = 'timeline';


	/**
	 * Returns a json encoded array of events
	 *
	 * @param string	$inputDateFormat	The format of start and end parameters.
	 * @param string	$start				Start date/time
	 * @param string	$end				End date/time
	 * @param array     $workspacesIds      An array of workspace ids. Set to null to get current workspace
	 *
	 * @return Widget_Action
	 */
	public function events($inputDateFormat = 'timestamp', $start = null, $end = null, $workspacesIds = array())
	{
		require_once dirname(__FILE__) . '/calendars.php';
		require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

		if ($inputDateFormat === 'timestamp') {
			$startDate = BAB_DateTime::fromTimeStamp($start);
			$endDate = BAB_DateTime::fromTimeStamp($end);
		} else {
			$startDate = BAB_DateTime::fromIsoDateTime($start);
			$endDate = BAB_DateTime::fromIsoDateTime($end);
		}


		$calendarEvents = workspace_getCalendarEvents($startDate, $endDate, $workspacesIds);

		$jsonEvents = array();
		foreach ($calendarEvents as $calendarEvent) {
			$jsonEvents[] = workspace_Event::fromCalendarPeriod($calendarEvent);
		}

		echo workspace_json_encode($jsonEvents);

		die;
	}



	/**
	 * Sets the date to be displayed next time a calendar view is displayed.
	 *
	 * @param string $date		ISO formatted date.
	 *
	 * @return workspace_Action
	 */
	public function setDate($date = null)
	{
		if (isset($date) && is_string($date)) {
			$_SESSION['workspace'][workspace_getCurrentWorkspace()]['dateToShow'] = $date;
		}
		die;
	}



	/**
	 * Displays the calendar.
	 *
	 * @param string	$type			The type of view.
	 * @param string	$dateToShow		An ISO-formatted date that should be visible on this calendar.
	 * @param int		$workspace		If specified, will change the current workspace.
	 * @param int		$ajax			If set, the html for the calendar widget is returned as standalone.
	 *
	 * @return workspace_Action
	 */
	public function display($type = workspace_CtrlCalendars::TYPE_WORKWEEK, $dateToShow = null, $workspace = null, $ajax = null)
	{
		require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
		require_once $GLOBALS['babInstallPath'] . '/utilit/calapi.php';
		require_once dirname(__FILE__) . '/calendars.php';
		require_once dirname(__FILE__) . '/calendars.ui.php';

		workspace_BreadCrumbs::setCurrentPosition($this->proxy()->display($type, $dateToShow, $workspace), workspace_translate("Calendars"));

		if (isset($workspace)) {
			workspace_setCurrentWorkspace($workspace);
		}

		if (!isset($_SESSION['workspace_personal_calendar_visible'])) {
			$_SESSION['workspace_personal_calendar_visible'] = true;
		}
		if (!isset($_SESSION['workspace_shared_calendar_visible'])) {
			$_SESSION['workspace_shared_calendar_visible'] = true;
		}


		$W = bab_Widgets();
		$W->includeCss();

		if (!(isset($dateToShow) && is_string($dateToShow))) {
			if (!isset($_SESSION['workspace'][workspace_getCurrentWorkspace()]['dateToShow'])) {
				$dateToShow = BAB_DateTime::now();
				$dateToShow = $dateToShow->getIsoDate();
			} else {
				$dateToShow = $_SESSION['workspace'][workspace_getCurrentWorkspace()]['dateToShow'];
			}
		}
		$_SESSION['workspace'][workspace_getCurrentWorkspace()]['dateToShow'] = $dateToShow;

		$page = workspace_Page();
		$page->addItemMenu('calendars', workspace_translate('Calendars'), $this->proxy()->display($type, $dateToShow, $workspace, $ajax))
			->addItemMenu('search', workspace_translate('Search events'), $this->proxy()->search()->url())
			->setCurrentItemMenu('calendars');

		if (workspace_getSharedCalendarId() == null && workspace_getPersonalCalendarId() == null && workspace_getUserProfile($GLOBALS['BAB_SESS_USERID']) == 'reader') {
			$explorer = $W->label("");
			$page->addError(workspace_translate("You do not have access to any calendar in this workspace."));
		} elseif ($type === workspace_CtrlCalendars::TYPE_TIMELINE) {
			$explorer = workspace_timelineView();
		} else {
			$explorer = workspace_calendarView($type, $dateToShow);
		}

		if (isset($ajax)) {
			$calendarWidget = Widget_Item::getById('calendar');
			$htmlCanvas = $W->HtmlCanvas();
			die($calendarWidget->display($htmlCanvas));
		}

		$page->addItem($explorer);

		return $page;
	}


	/**
	 * Displays the calendar events search results by period.
	 *
	 * @return Widget_Action
	 */
	public function search($workspace = null, $keywords = null, $from = null, $to = null)
	{
		require_once dirname(__FILE__) . '/search.ui.php';

		workspace_BreadCrumbs::setCurrentPosition($this->proxy()->search($workspace, $keywords, $from, $to), workspace_translate('Search events'));

		$W = bab_Widgets();

		$page = workspace_Page()
					->setTitle(workspace_translate('Search results'))
					->addItemMenu('calendars', workspace_translate('Calendars'), $this->proxy()->display()->url())
					->addItemMenu('search', workspace_translate('Search events'), $this->proxy()->search()->url())
					->setCurrentItemMenu('search');

		$resultBox = workspace_calendarsSearchResultList($keywords, $from, $to);


		$resultFrame = $W->Frame()->setLayout($W->VBoxLayout()->setVerticalSpacing(1, 'em'));
		$resultFrame->addClass('workspace-dialog');

		$searchForm = $W->Form()->setLayout($W->FlowLayout()->setSpacing(2, 'em'))
			->setReadOnly()
			->addItem(
				$W->FlowItems(
					$keywordsLbl = $W->Label(workspace_translate('Keywords')),
					$W->LineEdit()->setName('keywords')->setValue($keywords)->setAssociatedLabel($keywordsLbl)
				)->setHorizontalSpacing(0.5, 'em')
			)
			->addItem(
				$W->FlowItems(
					$FromLbl = $W->Label(workspace_translate('From')),
					$W->DatePicker()->setName('from')->setValue(bab_rp('from'))->setAssociatedLabel($FromLbl),
					$ToLbl = $W->Label(workspace_translate('to')),
					$W->DatePicker()->setName('to')->setValue(bab_rp('to'))->setAssociatedLabel($ToLbl)
				)->setHorizontalSpacing(0.5, 'em')
			)
			->addItem($W->SubmitButton()->setLabel(workspace_translate('New search')))
			->setSelfPageHiddenFields();

		$resultFrame->addItem($searchForm);
		$resultFrame->addItem($resultBox);


		$page->addItem($resultFrame);

		$page->displayHtml();
	}



	/**
	 * Toggles the visibility of the specified calendar.
	 *
	 * For now only two calendars available : 'personal' and 'shared'
	 *
	 * @param string $calendar		'personal' or 'shared'
	 *
	 * @return Widget_Action
	 */
	public function toggleVisible($calendar)
	{
		switch ($calendar) {
			case 'personal':
				$_SESSION['workspace_personal_calendar_visible'] = !($_SESSION['workspace_personal_calendar_visible']);
				break;

			case 'shared':
				$_SESSION['workspace_shared_calendar_visible'] = !($_SESSION['workspace_shared_calendar_visible']);
				break;
		}
		workspace_redirect(workspace_BreadCrumbs::last());
	}



	/**
	 * Deletes the specified event.
	 *
	 * @param int	$event
	 * @return Widget_Action
	 */
	public function deleteEvent($event = null)
	{
		if (!isset($event['id'])) {
			workspace_redirect(workspace_BreadCrumbs::last());
			return;
		}

		list($calendarId, $eventId) = explode(':', $event['id']);

		$calendar = bab_getICalendars()->getEventCalendar($calendarId);

		if (!isset($calendar)) {
			die('Cannot instantiate the event calendar (' . $calendarId . ')');
		}

		$backend = $calendar->getBackend();

		$collection = $backend->CalendarEventCollection($calendar);

		$period = $backend->getPeriod($collection, $eventId);

		$backend->deletePeriod($period);

		if (isset($event['notify']) && $event['notify']) {
		    $notifyEvent = new bab_eventAfterEventDelete;
		    $notifyEvent->setPeriod($period);

		    foreach ($period->getCalendars() as $calendar) {
		        $notifyEvent->addCalendar($calendar);
		    }
		    bab_fireEvent($notifyEvent);
		}

		workspace_redirect(workspace_BreadCrumbs::last());
	}


	/**
	 * Displays a form to edit a new calendar event.
	 *
	 * @param string	$date		ISO formatted date 'YYYY-MM-DD'
	 * @param string	$time		ISO formatted time 'HH:MM'
	 * @param string	$dateEnd	ISO formatted date 'YYYY-MM-DD'
	 * @param string	$timeEnd	ISO formatted time 'HH:MM'
	 * @param string	$title
	 * @param string	$location
	 * @param string	$description
	 *
	 * @return Widget_Action
	 */
	public function addEvent($date = null, $time = null, $dateEnd = null, $timeEnd = null, $title = null, $location = null, $description = null, $calendar = null, $error = null)
	{
		require_once dirname(__FILE__) . '/calendars.ui.php';
		require_once dirname(__FILE__) . '/calendars.php';

		$W = bab_Widgets();
		$page = workspace_Page();
		$editor = workspace_eventEditor(true);
		if (isset($date)) {
			$editor->setValue(array('event', 'startdate'), $date);
		}
		if (isset($dateEnd)) {
			$editor->setValue(array('event', 'enddate'), $dateEnd);
		}
		if (isset($time)) {
			$editor->setValue(array('event', 'starttime'), $time);
			list($hours, $minutes) = explode(':', $time);
			if (!isset($timeEnd)) {
				$hours += 2;
				if ($hours >= 24) {
					$hours = 23;
					$minutes = 59;
				}
				$timeEnd = sprintf('%02d:%02d', $hours, $minutes);
			}
			$editor->setValue(array('event', 'endtime'), $timeEnd);
		}
		if (isset($title)) {
			$editor->setValue(array('event', 'title'), $title);
		}
		if (isset($location)) {
			$editor->setValue(array('event', 'location'), $location);
		}
		if (isset($description)) {
			$editor->setValue(array('event', 'description'), $description);
		}
		if (isset($calendar)) {
			$editor->setValue(array('event', 'calendar'), $calendar);
		}

		if (isset($error)) {
			$page->addError($error);
		}

		$page->setTitle(workspace_translate('Create a new event'));

		if (workspace_getPersonalCalendarId() == null && workspace_getUserProfile($GLOBALS['BAB_SESS_USERID']) == 'reader') {
			$editor = $W->Label("");
			$page->addError(workspace_translate('You cannot create events.'));
		}

		$page->addItemMenu('calendars', workspace_translate('Calendars'), '')
			 ->setCurrentItemMenu('calendars')
			 ->addItem($editor);
		$page->displayHtml();
	}


	/**
	 * Displays a form to edit an existing calendar event.
	 *
	 * @param int|array $event
	 * @return Widget_Action
	 */
	public function editEvent($event = null, $error = null)
	{
		require_once dirname(__FILE__) . '/calendars.ui.php';
		require_once dirname(__FILE__) . '/calendars.php';

		$page = workspace_Page();
		if (isset($event['event']['id']) || !is_array($event)) {
			$editor = workspace_eventEditor(false);
		} else {
			$editor = workspace_eventEditor(true);
		}

		if (!is_array($event)) {

			require_once  $GLOBALS['babInstallPath'] . '/utilit/cal.periodcollection.class.php';

			list($calendarId, $eventId) = explode(':', $event);

			$calendar = bab_getICalendars()->getEventCalendar($calendarId);

			if (!isset($calendar)) {
				die('Cannot instantiate the event calendar (' . $calendarId . ')');
			}

			$collection = new bab_CalendarEventCollection;
			$collection->setCalendar($calendar);

			$backend = $calendar->getBackend();

			$period = $backend->getPeriod($collection, $eventId);

			if (!isset($period)) {
				die('Cannot instantiate the event (' . $eventId . ')');
			}


			$start = bab_DateTime::fromICal($period->getProperty('DTSTART'));
			$end = bab_DateTime::fromICal($period->getProperty('DTEND'));

			$category = bab_getCalendarCategory($period->getProperty('CATEGORIES'));
			if (isset($category)) {
				$category = $category['id'];
			}

			$startIsoTime = $start->getIsoTime();
			$endIsoTime = $end->getIsoTime();
			$startIsoTime = substr($startIsoTime, 0, 5);
			$endIsoTime = substr($endIsoTime, 0, 5);
			$data = $period->getData();
			$editor->setValue(array('event', 'title'), $period->getProperty('SUMMARY'));
			$editor->setValue(array('event', 'description'), $data['description']);
			$editor->setValue(array('event', 'location'), $period->getProperty('LOCATION'));
			$editor->setValue(array('event', 'startdate'), $start->getIsoDate());
			$editor->setValue(array('event', 'starttime'), $startIsoTime);
			$editor->setValue(array('event', 'endtime'), $endIsoTime);
			$editor->setValue(array('event', 'enddate'), $end->getIsoDate());
			$editor->setValue(array('event', 'category'), $category);
			$editor->setHiddenValue('event[id]', $event);
		} else {
			if (isset($event['event']['id'])) {
				$editor->setHiddenValue('event[id]', $event['event']['id']);
			}
			$editor->setValues($event);
		}
		$page->setTitle(workspace_translate('Edit event'));
		if (isset($error)) {
			$page->addError($error);
		}

		$page->addItemMenu('calendars', workspace_translate('Calendars'), '')
			 ->setCurrentItemMenu('calendars')
			 ->addItem($editor);
		$page->displayHtml();
	}


	/**
	 * Moves the event by the specified offset in days and/or minutes relatively to its current start.
	 *
	 * @param int	$event				The calendar + event id (calendarId:eventId, eg.: public/2:b8512ece-9cb9-4cee-97ea-59988cea389e)
	 * @param int	$offsetDays
	 * @param int	$offsetMinutes
	 * @return Widget_Action
	 */
	public function moveEvent($event = null, $offsetDays = null, $offsetMinutes = null, $offsetDuration = null, $ajax = null)
	{

		require_once dirname(__FILE__) . '/calendars.php';
		require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
		require_once $GLOBALS['babInstallPath'] . '/utilit/calincl.php';
		require_once $GLOBALS['babInstallPath'] . '/utilit/calapi.php';

		list($calendarId, $eventId) = explode(':', $event);

		$calendar = bab_getICalendars()->getEventCalendar($calendarId);

		if (!isset($calendar)) {
			die(
			    bab_json_encode(
			        array('error' =>'Cannot instantiate the event calendar (' . $calendarId . ')')
			    )
			);
		}

		$backend = $calendar->getBackend();

		$collection = $backend->CalendarEventCollection($calendar);

		$period = $backend->getPeriod($collection, $eventId);

		if (!isset($period)) {
			die(
                bab_json_encode(
                    array('error' => 'Cannot instantiate the event (' . $eventId . ')')
                )
			);
		}
		$start = bab_DateTime::fromICal($period->getProperty('DTSTART'));
		$end = bab_DateTime::fromICal($period->getProperty('DTEND'));

		if (isset($offsetDays)) {
			$start->add($offsetDays, BAB_DATETIME_DAY);
			$end->add($offsetDays, BAB_DATETIME_DAY);
		}
		if (isset($offsetMinutes)) {
			$start->add($offsetMinutes, BAB_DATETIME_MINUTE);
			$end->add($offsetMinutes, BAB_DATETIME_MINUTE);
		}
		if (isset($offsetDuration)) {
			$end->add($offsetDuration, BAB_DATETIME_MINUTE);
		}

		$period->setProperty('DTSTART', $start->getICal());
		$period->setProperty('DTEND', $end->getICal());

		$eventArgs = array(
			'calid' => $calendarId,
			'evtid' => $eventId,
			'selected_calendars' => array($calendarId),
			'title' => $period->getProperty('SUMMARY'),
			'description' => $period->getProperty('DESCRIPTION'),
			'descriptionformat' => 'html',
			'location' => $period->getProperty('LOCATION'),
			'yearbegin' => $start->getYear(),
			'monthbegin' => $start->getMonth(),
			'daybegin' => $start->getDayOfMonth(),
			'timebegin' => $start->getHour() . ':' . $start->getMinute(),
			'yearend' => $end->getYear(),
			'monthend' => $end->getMonth(),
			'dayend' => $end->getDayOfMonth(),
			'timeend' => $end->getHour() . ':' . $end->getMinute(),
			'owner' => $GLOBALS['BAB_SESS_USERID'],
			'bfree' => true
		);

		$calendarIds = array($calendarId);
		$errorMessages = null;
		$result = bab_saveEvent($calendarIds, $eventArgs, $errorMessages, BAB_CAL_EVT_CURRENT);

		if (isset($ajax)) {
			die();
		}
		workspace_redirect(workspace_BreadCrumbs::last());
	}


	/**
	 * Saves the event and returns to the previous page.
	 *
	 * @param array $event
	 * @return Widget_Action
	 */
	public function saveEvent($event = null)
	{
		if (!is_array($event)) {
			workspace_redirect(workspace_BreadCrumbs::last());
			return;
		}
		require_once dirname(__FILE__) . '/calendars.php';
		require_once $GLOBALS['babInstallPath'] . '/utilit/calapi.php';
		require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

		$event['startDatetime'] = $event['startdate'] . ' ' .  $event['starttime'];
		if ($event['enddate'] == '') {
			$event['enddate'] =  $event['startdate'];
		}
		$event['endDatetime'] = $event['enddate'] . ' ' .  $event['endtime'];

		$dateStart = BAB_DateTime::fromIsoDateTime($event['startDatetime'].':00');
		$dateEnd = BAB_DateTime::fromIsoDateTime($event['endDatetime'].':00');

		if (BAB_DateTime::compare($dateStart, $dateEnd) == 1) {
			$event['startDatetime'] = $event['enddate'] . ' ' .  $event['endtime'];
			$event['endDatetime'] = $event['startdate'] . ' ' .  $event['starttime'];
		} elseif (BAB_DateTime::compare($dateStart, $dateEnd) == 0) {
			$this->editEvent($_POST, workspace_translate('The event must include at last one minute.'));
			return;
		}
		$event['creatorId'] = $GLOBALS['BAB_SESS_USERID'];



		if (isset($event['calendar'])) {

			$calendarId = ($event['calendar'] === 'public') ?
				workspace_getSharedCalendarId() :
				workspace_getPersonalCalendarId();
			$eventArgs = array(
				'selected_calendars' => array($calendarId)
			);

		} elseif (isset($event['id'])) {

			list($calendarId, $eventId) = explode(':', $event['id']);
			$eventArgs = array(
				'calid' => $calendarId,
				'evtid' => $eventId,
				'selected_calendars' => array($calendarId),
			);

		} else {
			/// ERROR
		}

		$eventArgs += array(
			'title' => $event['title'],
			'description' => $event['description'],
			'descriptionformat' => 'html',
			'location' => $event['location'],
			'yearbegin' => $dateStart->getYear(),
			'monthbegin' => $dateStart->getMonth(),
			'daybegin' => $dateStart->getDayOfMonth(),
			'timebegin' => $dateStart->getHour() . ':' . $dateStart->getMinute(),
			'yearend' => $dateEnd->getYear(),
			'monthend' => $dateEnd->getMonth(),
			'dayend' => $dateEnd->getDayOfMonth(),
			'timeend' => $dateEnd->getHour() . ':' . $dateEnd->getMinute(),
			'owner' => $GLOBALS['BAB_SESS_USERID'],
			'color' => str_replace('public', '', $calendarId) == $calendarId ? 'DCC7F2' : 'C7DCF2',
			'category' => $event['category'],
		    'groupe-notif' => $event['notify'],
			'bfree' => true
		);

		$calendarIds = array($calendarId);
		$errorMessages = null;

		if (isset($event['id'])) {
			$result = bab_saveEvent($calendarIds, $eventArgs, $errorMessages, BAB_CAL_EVT_CURRENT);
		} else {
			$result = bab_saveEvent($calendarIds, $eventArgs, $errorMessages);
		}

		if (!$result) {
			$this->editEvent($_POST, 'Erreur : ' . $errorMessages);
			return;
		}

		workspace_redirect(workspace_BreadCrumbs::last());
	}


	/**
	 * Does nothing and return to the previous page.
	 *
	 * @return Widget_Action
	 */
	public function cancel()
	{
		workspace_redirect(workspace_BreadCrumbs::last());
	}


	/**
	 * Displays help on this page.
	 *
	 * @return Widget_Action
	 */
	public function help()
	{

	}
}
