<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */

/* @var $App Func_App_Workspace */
$App = bab_functionality::get('App/Workspace');
$App->includeRecordSet();



/**
 * Workspaces can be associated to one domain.
 *
 * @property ORM_PkField            $id
 * @property ORM_StringField        $name
 * @property workspace_CategorySet	$parent
 *
 */
class workspace_CategorySet extends app_RecordSet
{
    public function __construct()
    {
        parent::__construct(workspace_App());

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name')
                ->setDescription('Name')
        );

        $this->hasOne('parent', 'workspace_CategorySet');

        $this->hasMany('workspaces', 'workspace_WorkspaceSet', 'category');
    }
}


/**
 * A workspace can be associated to one domain.
 *
 * @property int                $id
 * @property string             $name
 * @property workspace_Category $parent
 */
class workspace_Category extends app_Record
{
}
