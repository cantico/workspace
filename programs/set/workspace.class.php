<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */
require_once WORKSPACE_PHP_PATH . 'workspaces.php';
include_once WORKSPACE_PHP_PATH . 'workspaceevents.php';

/* @var $App Func_App_Workspace */
$App = bab_functionality::get('App/Workspace');
$App->includeRecordSet();

/**
 * A class that contains information about a workspace.
 *
 * @property ORM_PkField                $id
 * @property ORM_IntField               $delegation
 * @property ORM_StringField            $name
 * @property ORM_TextField              $description
 * @property ORM_DateField              $createdOn
 * @property ORM_DateField              $archivedDate       Date after which the workspace is considered archived
 * @property ORM_DateTimeField          $lastActivity       Last activity
 * @property ORM_BoolField              $isEphemeral
 * @property workspace_CategorySet      $category
 * @property ORM_StringField            $color
 * @property ORM_ImageField             $logo
 * @property ORM_GroupField             $group
 * @property ORM_BoolField              $isArchived
 * @property ORM_StringField            $skin
 * @property ORM_StringField            $skinStyles
 *
 * @method workspace_Workspace      newRecord()
 * @method workspace_Workspace      get()
 * @method workspace_Workspace      request()
 * @method workspace_Workspace[]    select()
 */
class workspace_WorkspaceSet extends app_RecordSet
{

    /**
     *
     */
    public function __construct()
    {
        parent::__construct(workspace_App());

        $this->setDescription('Workspace');

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_IntField('delegation')
                ->setDescription('Delegation'),
            ORM_StringField('name')
                ->setDescription('Name'),
            ORM_StringField('color')
                ->setDescription('Color'),
            ORM_ImageField('logo')
                ->setDescription('Logo'),
            ORM_TextField('description')
                ->setDescription('Description'),
            ORM_DateField('createdOn')
                ->setDescription('Creation date'),
            ORM_DateField('archivedDate')
                ->setDescription('Archived date'),
            ORM_DateTimeField('lastActivity')
                ->setDescription('Last activity'),
            ORM_GroupField('group')
                ->setDescription('Group of users who can access the workspace'),
            ORM_DateField('archivedDate')
                ->setDescription('The date to archive the workspace if the workspace is ephemeral, or the date the workspace has been archived'),
            ORM_StringField('skin')
                ->setDescription('The name of the skin module to use'),
            ORM_StringField('skinStyles')
                ->setDescription('The name of the style sheet to use')
        );

        $this->addFields(
            ORM_CriterionOperation(
                'isArchived',
                $this->all(
                    $this->archivedDate->isNot('0000-00-00'),
                    $this->archivedDate->lessThanOrEqual(date('Y-m-d'))
                )
            )
        );



        $this->hasOne('category', 'workspace_CategorySet');
    }



    public function isCreatable()
    {
        return true;
    }

    /**
     * @return ORM_Criteria
     */
    public function isReadable($userId = null)
    {
        $delegationIds = array();
        $userDelegations = bab_getUserVisiblesDelegations($userId);
        foreach($userDelegations as $userDelegation){
            $delegationIds[] = $userDelegation['id'];
        }
        return $this->delegation->in($delegationIds);
    }


    /**
     * @return ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
    }

    /**
     * @return ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }

    public function save(workspace_Workspace $workspace)
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

        if(!empty($workspace->name)){
            $isCreation = $workspace->delegation == 0 ? true : false;
            $now = BAB_DateTime::now();

            //Create the differents workspace functionalities based on the form
            //Delegation
            if($isCreation){
                $workspace->createdOn = $now->getIsoDate();
                $workspace->lastActivity = $now->getIsoDateTime();
                $workspace->createDelegation();
                bab_Registry::set('/workspace/workspaces/'.$workspace->delegation.'/name', $workspace->name);
            }
            else{
                //Update
                $savedWorkspace = $this->get($workspace->id);
                //It's possible savedWorkspace->createdOn is not defined if the workspace come from a cloning (can come from a first install)
                if($savedWorkspace && $savedWorkspace->createdOn){
                    //Changing the group
                    if($savedWorkspace->group != $workspace->group){
                        require_once $GLOBALS['babInstallPath'] . '/utilit/grpincl.php';
                        if (!bab_isGroup($workspace->group)) {
                            throw new Exception('Invalid group id');
                        }
                        $workspace->setDelegationGroup($workspace->group);
                    }
                }
                else{
                    $workspace->createdOn = $now->getIsoDate();
                }
                $workspace->lastActivity = $now->getIsoDateTime();

                $event = new workspace_eventWorkspaceUpdated();
            }
        }
        $workspace->skin = empty($workspace->skin) ? 'workspace' : $workspace->skin;
        $workspace->skinStyles = empty($workspace->skinStyles) ? 'blue.css' : $workspace->skinStyles;

        bab_Registry::set('/workspace/workspaces/'.$workspace->delegation.'/skin', $workspace->skin);
        bab_Registry::set('/workspace/workspaces/'.$workspace->delegation.'/skin_styles', $workspace->skinStyles);

        $workspace->initializePortlets();

        if(isset($event)){
            $event->setWorkspace($workspace);
            bab_fireEvent($event);
        }

        return parent::save($workspace);
    }
}


/**
 * A class that contains information about a budget.
 *
 * @property int                    $id
 * @property int                    $delegation
 * @property string                 $name
 * @property string                 $description
 * @property string                 $archivedDate       Date after which the workspace is considered archived
 * @property string                 $lastActivity
 * @property string                 $createdOn
 * @property workspace_Category     $category
 * @property string                 $color
 * @property string                 $logo
 * @property int                    $group
 * @property bool                   $isArchived
 * @property string                 $skin
 * @property string                 $skinStyles
 */
class workspace_Workspace extends app_Record
{
    /**
     * Checks if the record is readable by the current user.
     * @param int       $userId userId to check, or null for current user
     * @since 1.0.21
     * @return bool
     */
    public function isReadable($userId = null)
    {
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->_AND_($set->id->is($this->id)))->count() == 1;
    }


    /**
     * Checks if the record is updatable by the current user.
     * @since 1.0.21
     * @return bool
     */
    public function isUpdatable()
    {
        $set = $this->getParentSet();
        return $set->select($set->isUpdatable()->_AND_($set->id->is($this->id)))->count() == 1;
    }

    /**
     * Checks if the record is deletable by the current user.
     * @since 1.0.21
     * @return bool
     */
    public function isDeletable()
    {
        $set = $this->getParentSet();
        return $set->select($set->isDeletable()->_AND_($set->id->is($this->id)))->count() == 1;
    }


    /**
     * @return bool
     */
    public function isArchived()
    {
        $set = $this->getParentSet();
        return $set->select($set->isArchived()->_AND_($set->id->is($this->id)))->count() == 1;
    }

    /**
     * Get the upload path for files related to this contact.
     *
     * @return bab_Path
     */
    public function getUploadPath()
    {
        if (!isset($this->id)) {
            return null;
        }

        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

        $path = new bab_Path(bab_getAddonInfosInstance('workspace')->getUploadPath());
        $path->push(get_class($this));
        $path->push($this->id);
        return $path;
    }


    /**
     * Get the path for the photo folder
     * this method can be used with the image picker widget
     *
     * @return bab_Path
     */
    public function getLogoFolderPath()
    {
        $addon = bab_getAddonInfosInstance('workspace');

        $ovidentiapath = realpath('.');

        $path = new bab_Path($ovidentiapath);
        $path->push($this->getLogoFolderRelativePath());
        if (!$path->isDir()) {
            $path->createDir();
        }
        return $path;
    }

    public function getLogoFolderRelativePath()
    {
        $addon = bab_getAddonInfosInstance('workspace');
        $path = new bab_Path('images',  $addon->getRelativePath(), $this->id);
        $path->push('logo');
        return $path;
    }


    /**
     * Return the full path of the photo file
     * this method return null if there is no logo to display
     *
     * @return bab_Path | null
     */
    public function getLogoPath()
    {
        $path = $this->getLogoFolderPath();

        if (!isset($path)) {
            return null;
        }

        $W = bab_Widgets();
        $uploaded = $W->ImagePicker()->getFolderFiles($path);

        if (!isset($uploaded)) {
            return null;
        }

        foreach ($uploaded as $file) {
            return $file->getFilePath();
        }

        return null;
    }

    /**
     * Return the relative path of the photo file
     * this method return null if there is no logo to display
     *
     * @return bab_Path | null
     */
    public function getRelativeLogoPath()
    {
        $path = $this->getLogoFolderRelativePath();

        if (!isset($path)) {
            return null;
        }

        $W = bab_Widgets();
        $uploaded = $W->ImagePicker()->getFolderFiles($path);

        if (!isset($uploaded)) {
            return null;
        }

        foreach ($uploaded as $file) {
            return $file->getFilePath();
        }

        return null;
    }

    /**
     *
     * @param int				$width
     * @param int				$height
     * @param bool				$border
     * @return Widget_Image
     */
    function logo($width, $height, $border = false)
    {
        $W = bab_Widgets();
        $T = @bab_functionality::get('Thumbnailer');

        $image = $W->Image();

        if ($T) {
            $logo = $this->getLogoPath();
            if (!empty($logo)) {
                $T->setSourceFile($logo->toString());
                $T->setResizeMode(Func_Thumbnailer::KEEP_ASPECT_RATIO);
            } else {
                $addon = bab_getAddonInfosInstance('workspace');
                $T->setSourceFile($addon->getStylePath() . 'images/logo-default.png');
            }

            if ($border) {
                $padWidth = min(array(2, round(min(array($width, $height)) / 24)));
                $T->setBorder(1, '#cccccc', $padWidth, '#ffffff');
            }
            $imageUrl = $T->getThumbnail($width, $height);
            $image->setUrl($imageUrl);
        }

        if ($this->name) {
            $image->setTitle($this->name);
        }

//        $image->addClass('crm-organization-image crm-element-image small');

        return $image;
    }


    /**
     * Checks whether the workspace is disabled
     *
     * @TODO There is two function isDisabled : wokspace_isDisabled() and workspace_isWorkspaceDisabled()
     * @return bool
     */
    public function isDisabled()
    {
        return workspace_isWorkspaceDisabled($this->delegation);
    }

    /**
     * Returns information about the specified workspace.
     *
     * @return array
     */
    public function getInfos($admin = false)
    {
        return workspace_getWorkspaceInfo($this->delegation, $admin);
    }

    /**
     * Notify file recipient for one thread/post by email
     * when a thread/post has been created
     *
     * @param string 			$author
     * @param int 				$articleId
     * @param Array 			$users			Users to notify.
     * @param string			$title			Title displayed in email.
     * @param string			$message		Message displayed in email.
     * @param string    		$changeType		Type of change. One of 'newArticle' or 'updatedArticle'.
     *
     * @return bool
     */
    public function articleNotifyMembers($author, $articleId, $users, $title, $message = '', $changeType = null)
    {
        $workspaceInfo = $this->getInfos();
        return workspace_articleNotifyMembers($author, $articleId, $users, $workspaceInfo, $title, $message = '', $changeType = null);
    }

    /**
     * Notify file recipient for one thread/post by email
     * when a thread/post has been created
     *
     * @param string 			$author
     * @param int 				$articleId
     * @param Array 			$users		users to notify
     * @param string			$title		title displayed in email
     * @param string			$message	message displayed in email
     *
     * @return bool
     */
    public function calendarNotifyMembers($period, $users, $title, $message = '')
    {
        $workspaceInfo = $this->getInfos();
        return workspace_calendarNotifyMembers($period, $users, $workspaceInfo, $title, $message = '');
    }

    /**
     * Recreates admin groups and associated rights with workspace items.
     *
     * @return int		the id of the admin group created or null on error.
     */
    public function createAdminGroup()
    {
        return workspace_createAdminGroup($this->delegation);
    }

    /**
     * Recreates writer groups and associated rights with workspace items.
     *
     * @return int		the id of the writer group created or null on error.
     */
    public function createWriterGroup()
    {
        return workspace_createWriterGroup($this->delegation);
    }

    /**
     * Notify file recipient for one file by email
     * when a file has been uploaded or a file has been updated
     *
     * @param BAB_FolderFile 	$file		Filename
     * @param Array 			$users		users to notify
     * @param array				$workspaceInfo
     * @param string			$title		title displayed in email
     * @param string			$message	message displayed in email
     * @param bool				$bnew		true = new file uploaded | false = the file has been updated
     *
     * @return bool
     */
    public function fileNotifyMembers(BAB_FolderFile $file, $users, $title, $message = '', $bnew = true)
    {
        $workspaceInfo = $this->getInfos();
        return workspace_fileNotifyMembers($file, $users, $workspaceInfo, $title, $message = '', $bnew);
    }

    /**
     * Notify file recipient for one thread/post by email
     * when a thread/post has been created
     *
     * @param string 			$author
     * @param int 				$threadId
     * @param int 				$postId
     * @param Array 			$users		users to notify
     * @param array				$workspaceInfo
     * @param string			$title		title displayed in email
     * @param string			$message	message displayed in email
     *
     * @return bool
     */
    public function forumNotifyMembers(bab_eventForumPost $event, $users, $title, $message = '', $changeType = null)
    {
        $workspaceInfo = $this->getInfos();
        return workspace_forumNotifyMembers($event, $users, $workspaceInfo, $title, $message = '', $changeType);
    }

    /**
     * Returns the profile (administrator, writer or reader) for the specified user.
     * @param int   $userId
     *  @return string	'administrator', 'writer' or 'reader' or false
     */
    public function getUserProfile($userId = null)
    {
        return workspace_getUserProfile($userId = null, $this->delegation);
    }

    public function getAdministratorIds()
    {
        return workspace_getWorkspaceAdministratorIds($this->delegation);
    }

    /**
     * Returns the workspace's administrators group.
     *
     * @return int		or null on error
     */
    public function getAdministratorsGroupId()
    {
        return workspace_getWorkspaceAdministratorsGroupId($this->delegation);
    }

    /**
     * @return string		The number of event agenda to be displayed in the right section of the home page
     */
    public function getAgendaNum()
    {
        return workspace_getWorkspaceAgendaNum($this->delegatione);
    }

    /**
     * Checks if the user is allowed to create new user accounts.
     * @param int   $workspaceId
     * @return bool
     */
    public function getAllowUserCreation()
    {
        return workspace_getWorkspaceAllowUserCreation($this->delegation);
    }

    /**
     * @return string		The number of article to be displayed in the right section of the home page
     */
    public function getArticleNum()
    {
        return workspace_getWorkspaceArticleNum($this->delegation);
    }

    /**
     * Returns the workspace's calendar id.
     *
     * @return int		or null on error
     */
    public function getCalendarId($checkAccess = false)
    {
        return workspace_getWorkspaceCalendarId($this->delegation, $checkAccess);
    }

    /**
     * Checks if the workspace is
     * configured to send notifications to members on calendar events.
     *
     * @param int       $workspaceId
     * @return bool		or null on error
     */
    public function getCalendarNotifications()
    {
        return workspace_getWorkspaceCalendarNotifications($this->delegation);
    }

    /**
     * Returns the workspace category
     */
    public function getCategory()
    {
        return workspace_getWorkspaceCategory($this->delegation);
    }

    /**
     * Returns the workspace's topic category id.
     *
     * @return int		or null on error
     */
    public function getCategoryId()
    {
        return workspace_getWorkspaceCategoryId($this->delegation);
    }

    /**
     * Returns the workspace category name
     */
    public function getCategoryName()
    {
        return workspace_getWorkspaceCategoryName($this->delegation);
    }

    /**
     * Returns the workspace's directory id.
     *
     * @return int		or null on error
     */
    public function workspace_getWorkspaceDirectoryId()
    {
        return workspace_getWorkspaceDirectoryId($this->delegation);
    }

    /**
     * Checks if the workspace is
     * configured to send notifications to members on file uploads.
     *
     * @return bool		or null on error
     */
    public function getFileNotifications()
    {
        return workspace_getWorkspaceFileNotifications($this->delegation);
    }

    /**
     * @return string		The number of file to be displayed in the right section of the home page
     */
    public function getFileNum()
    {
        return workspace_getWorkspaceFileNum($this->delegation);
    }

    /**
     * @return int		Max file size
     */
    public function getFileSize()
    {
        return workspace_getWorkspaceFileSize($this->delegation);
    }

    /**
     * Checks if the workspace is
     * configured to do versioning of files.
     *
     * @return bool		or null on error
     */
    public function getFileVersioning()
    {
        return workspace_getWorkspaceFileVersioning($this->delegation);
    }

    /**
     * Returns the workspace's forum id.
     *
     * @return int		or null on error
     */
    public function getForumId()
    {
        return workspace_getWorkspaceForumId($this->delegation);
    }

    /**
     * @return string		The number of forum post to be displayed in the right section of the home page
     */
    public function getForumNum()
    {
        return workspace_getWorkspaceForumNum($this->delegation);
    }

    /**
     * Returns the workspace's associated group.
     *
     * @return int		or null on error
     */
    public function getGroupId()
    {
        return $this->group;
    }

    /**
     * @return bool		Display or not the other them section in the right section of the home page
     */
    public function getOtherThem()
    {
        return workspace_getWorkspaceOtherThem($this->delegation);
    }

    /**
     *
     * @param int $workspaceId
     * @return int[]
     */
    public function getPermanentAdministratorIds()
    {
        return workspace_getWorkspacePermanentAdministratorIds($this->delegation);
    }

    /**
     * Returns the workspace's root folder id.
     *
     * @return int		or null on error
     */
    public function getRootFolderId()
    {
        return workspace_getWorkspaceRootFolderId($this->delegation);
    }

    /**
     *
     * @return string
     */
    public function workspace_getWorkspaceRootFolderName()
    {
        return workspace_getWorkspaceRootFolderName($this->delegation);
    }

    /**
     * @return string		The name of the workspace skin css file.
     */
    public function getSkinCss()
    {
        return workspace_getWorkspaceSkinCss($this->delegation);
    }

    /**
     * @return string	The name of the workspace skin.
     */
    public function getSkinName()
    {
        return workspace_getWorkspaceSkinName($this->delegation);
    }

    /**
     * Returns the workspace's topic id.
     *
     * @return int		or null on error
     */
    public function getTopicId()
    {
        return workspace_getWorkspaceTopicId($this->delegation);
    }

    /**
     * Return the path to the specified workspace trash folder.
     *
     * @param int $workspaceId
     * @return string
     */
    public function getTrashPath()
    {
        return workspace_getWorkspaceTrashPath($this->delegation);
    }

    /**
     * @return bool		True if ovidentia global directory entries should be updated when workspace directory entry is updated
     */
    public function getUpdateDirectory()
    {
        return workspace_getWorkspaceUpdateDirectory($this->delegation);
    }

    /**
     * Returns the workspace's writers group.
     *
     * @return int		or null on error
     */
    public function getWritersGroupId()
    {
        return workspace_getWorkspaceWritersGroupId($this->delegation);
    }

    /**
     *
     * @param int $userId
     */
    public function notifyNewMember($userId)
    {
        return workspace_notifyNewMember($this->delegation, $userId);
    }

    /**
     * Sets the current user workspace.
     */
    public function setCurrentWorkspace()
    {
        return workspace_setCurrentWorkspace($this->delegation);
    }

    /**
     * Defines if the user is allowed to create new user accounts.
     * @param bool  $allow
     */
    public function workspace_setWorkspaceAllowUserCreation($allow)
    {
        return workspace_setWorkspaceAllowUserCreation($allow, $this->delegation);
    }

    /**
     * Configured notifications to members on calendar events for the workspace.
     *
     * @param bool      $notifyCalendar
     * @return 1|2|3
     */
    public function workspace_setWorkspaceCalendarNotifications($notifyCalendar)
    {
        return workspace_setWorkspaceCalendarNotifications($notifyCalendar, $this->delegation);
    }

    /**
     * Sets if the workspace is
     * configured to allow people to comment article.
     */
    public function setComments($comment = false)
    {
        return workspace_setWorkspaceComments($comment, $this->delegation);
    }

    /**
     * Sets if the workspace is
     * configured to send notifications to members on file uploads.
     */
    public function setFileNotifications($notify = false)
    {
        return workspace_setWorkspaceFileNotifications($notify, $this->delegation);
    }

    /**
     * Sets if the workspace is
     * configured to do versioning of files.
     */
    public function setFileVersioning($versioning = false)
    {
        return workspace_setWorkspaceFileVersioning($versioning, $this->delegation);
    }

    /**
     * Sets if the workspace is
     * configured to allow people to comment article.
     */
    public function setForumNotifications($forum = false)
    {
        return workspace_setWorkspaceForumNotifications($forum, $this->delegation);
    }

    /**
     *
     * @param int[] $permanentAdministratorIds
     * @return 0|1|2
     */
    public function setPermanentAdministratorIds($permanentAdministratorIds)
    {
        return workspace_setWorkspacePermanentAdministratorIds($permanentAdministratorIds, $this->delegation);
    }

    /**
     * Checks whether the current user is one of the workspace administrators.
     *
     * @param int		$userId
     *
     * @return bool
     */
    public function userIsAdministrator($userId = null)
    {
        return workspace_userIsWorkspaceAdministrator($userId, $this->delegation);
    }


    /**
     * Checks whether the current user is one of the workspace permanent administrators.
     *
     * @param int		$userId
     *
     * @return bool
     */
    public function userIsPermanentAdministrator($userId = null)
    {
        return workspace_userIsWorkspacePermanentAdministrator($userId, $this->delegation);
    }

    public function getTags()
    {
        $App = workspace_App();
        $tagSet = $App->TagSet();

        $links = $tagSet->selectLinkedTo($this);
        $tags = array();
        foreach($links as $link){
            if($link->type == 'hasPersonalTag'){
                if($link->createdBy == bab_getUserId()){
                    $tags[] = $link->targetId;
                }
                continue;
            }
            $tags[] = $link->targetId;
        }

        return $tags;
    }

    public function getDelegationGroupId()
    {
        global $babDB;
        if($this->delegation == 0){
            return null;
        }
        //Select the delegation group id associated to the workspace
        $res = $babDB->db_query('SELECT id_group FROM '.BAB_DG_GROUPS_TBL.' WHERE id='.$babDB->quote($this->delegation));
        if ($babDB->db_num_rows($res) > 0) {
            //Not found
            return null;
        }
        if($res['id_group'] == 0){
            return null;
        }

        return $res['id_group'];
    }

    /**
     * Return an array populated with the managers user id
     * @return array    Array of user id
     */
    public function getManagers()
    {
        $managers = array();
        $managers[] = bab_getUserId();

        return $managers;
    }

    public function createDelegation()
    {
        global $babDB;

        $res = $babDB->db_query('SELECT * FROM ' . BAB_DG_GROUPS_TBL . ' WHERE name=' . $babDB->quote($this->name));
        if ($babDB->db_num_rows($res) > 0) {
            throw new Exception('The delegation name already exists.');
        }

        $req1 = '(name, description, color, battach';
        $req2 = '(' .$babDB->quote($this->name). ', ' . $babDB->quote($this->description). ', ' . $babDB->quote($this->color) . ', ' . $babDB->quote('Y');

        $group = $this->getGroupId();

        if (empty($group)) {

            // If the group is not specified, we create one with the same name as the workspace.

            if (workspace_groupNameExist($this->name)) {
                throw new Exception('Group name already exists');
            }

            $parentGroup = workspace_getWorkspacesParentGroup();
            $group = bab_createGroup($this->name, '', 0, $parentGroup);
            $this->group = $group;

        } else {
            require_once $GLOBALS['babInstallPath'] . '/utilit/grpincl.php';
            if (!bab_isGroup($group)) {
                throw new Exception('Invalid group id');
            }
        }

        $req1 .= ',iIdCategory ';
        $req2 .= ', ' . $babDB->quote(0);

        $req1 .= ',id_group )';
        $req2 .= ', ' . $group . ' )';
        $babDB->db_query('INSERT INTO ' . BAB_DG_GROUPS_TBL . ' ' . $req1 . ' VALUES ' . $req2);
        $delegationId = $babDB->db_insert_id();

        //  The readers group is groupId
        // We try to create the 'writers' and 'administrators' sub-group if they do not already exist.
        $writersGroup = bab_groupIsChildOf($group, 'writers');
        if ($writersGroup === false) {
           bab_createGroup('writers', '', 0, $group);
        }

        $administratorsGroup = bab_groupIsChildOf($group, 'administrators');
        if ($administratorsGroup === false) {
            $administratorsGroupId = bab_createGroup('administrators', '', 0, $group);
        } else {
            $administratorsGroupId = $administratorsGroup['id'];
        }

        // Set the workspace administrator
        $managers = $this->getManagers();
        foreach ($managers as $manager){
            $res = $babDB->db_query('SELECT COUNT(*)
                                FROM ' . BAB_DG_ADMIN_TBL . '
                                WHERE id_dg=' . $babDB->quote($delegationId) . '
                                    AND id_user=' . $babDB->quote($manager));
            list($n) = $babDB->db_fetch_array($res);
            if ($n == 0) {
                //Ensure the user is not already in the list
                $babDB->db_query('INSERT INTO
                        ' . BAB_DG_ADMIN_TBL . ' (id_dg, id_user)
                        VALUES (' . $babDB->quote($delegationId) . ',' . $babDB->quote($manager) . ')');
            }

            // We ensure that the workspace administrator is member of the administered group
            bab_addUserToGroup($manager, $group);
            bab_addUserToGroup($manager, $administratorsGroupId);
        }

        $this->delegation = $delegationId;
        return $delegationId;
    }

    /**
     * Update the workspace lastActivity value with the value given in parameter. If no value given, the last activity will be the present date and time
     * @param BAB_DateTime $lastActivity
     * @return boolean
     */
    public function updateLastActivity(BAB_DateTime $lastActivity = null)
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
        if(!isset($lastActivity)){
            $lastActivity = BAB_DateTime::now();
        }
        $this->lastActivity = $lastActivity->getIsoDateTime();
        return $this->save();
    }

    /**
     * Archive the workspace
     */
    public function archive()
    {
        $this->archivedDate = date('Y-m-d');
        $this->save();
    }

    public function setDelegationGroup($groupId)
    {
        global $babDB;

        if(!bab_isGroup($groupId)){
            return false;
        }

        //Update delegation group
        $sql = 'UPDATE '.BAB_DG_GROUPS_TBL.' SET id_group='.$babDB->quote($groupId).' WHERE id='.$babDB->quote($this->delegation);
        $babDB->db_query($sql);
    }

    public function getSkinConfiguration()
    {
        $infos = $this->getInfos();
        $skinName = $infos['skin'];
        $addon = bab_getAddonInfosInstance($skinName);
        if($addon){
            $skinName = $addon->getName();
            $skinNameParts = explode('_', $skinName);
            $funcName = '';
            foreach($skinNameParts as $part){
                $funcName.= ucfirst($part);
            }
            $func = @bab_functionality::includefile($funcName);
            if($func && method_exists($func, 'getConfiguration')){
                $configuration = $func::getConfiguration();
                foreach ($configuration as $key => $values){
                    $values['value'] = bab_Registry::get('workspace/workspaces/'.$this->delegation.'/theme/'.$key, $values['value']);
                    $configuration[$key] = $values;
                }
                return $configuration;
            }
        }
        return array();
    }

    public function getSkinEditor()
    {
        $App = workspace_App();
        $infos = $this->getInfos();
        $skinName = $infos['skin'];
        $addon = bab_getAddonInfosInstance($skinName);
        if($addon){
            $skinName = $addon->getName();
            $skinNameParts = explode('_', $skinName);
            $funcName = '';
            foreach($skinNameParts as $part){
                $funcName.= ucfirst($part);
            }
            $func = @bab_functionality::includefile($funcName);
            if($func && method_exists($func, 'getEditor')){
                /**
                 * @var Widget_Form $editor
                 */
                $saveAction = $App->Controller()->Admin()->saveWorkspaceTheme($this->id);
                $resetAction = $App->Controller()->Admin()->resetWorkspaceTheme($this->id);
                $editor = $func::getEditor(
                    $saveAction,
                    $resetAction,
                    $App->Controller()->Admin()->themeEditor($this->id, $saveAction, $resetAction)
                );
                $editor->setHiddenValue('workspace', $this->id);
                return $editor;
            }
        }
        return null;
    }

    function getImagePath($directory)
    {
        $addon = bab_getAddonInfosInstance('workspace');

        $ovidentiapath = realpath('.');

        $uploadPath = new bab_Path($ovidentiapath, 'images',  $addon->getRelativePath(), $this->delegation, $directory);
        if (!$uploadPath->isDir()) {
            $uploadPath->createDir();
        }

        return $uploadPath;
    }

    function getImageUrl($imageName, $baseFolder = '')
    {
        $W = bab_Widgets();

        $imagesPath = $this->getImagePath($imageName);
        try {
            $imagesPath->deleteDir();
        } catch (bab_FolderAccessRightsException $e) { }
        $imagesPath->createDir();

        $imagePicker = $W->ImagePicker()
        ->setName($imageName . 'Image');
        if ($files = $imagePicker->getTemporaryFiles()) {
            foreach ($files as $file) {
                rename($file->getFilePath()->toString(), $imagesPath->toString() . '/' . $file->getFileName());
            }
        }

        $imageUrl = '';
        foreach ($imagesPath as $imagePath) {
            $imageFilename = basename($imagePath->toString());
            $imageFilename = str_replace("'", "%27", $imageFilename);
            $imageUrl = $baseFolder . $imageName . '/' .  $imageFilename;
            break;
        }

        return $imageUrl;
    }

    public function isThemeEditable()
    {
        $skinConfiguration = $this->getSkinConfiguration();
        if(count($skinConfiguration) > 0){
            return true;
        }
        return false;
    }

    /**
     * Instanciate the portlets containers of the workspace for the differents pages
     * @return bool
     */
    public function initializePortlets()
    {
        @bab_functionality::includefile('PortletBackend');
        if(!class_exists('Func_PortletBackend')) {
            // functionality PortletBackend not found
            return false;
        }

        //PAGE TITLE, SHARED ON ALL WORKSPACE PAGES
        if (Func_PortletBackend::isEmptyContainer('workspace_'.$this->id.'_top'))
        {
            Func_PortletBackend::addPortlet(
                'Workspace',
                'workspaceTitle',
                'workspace_'.$this->id.'_top',
                0,
                array(
                    '_blockTitleType' => 'custom',
                    '_blockTitle' => '',
                    'workspaceId' => $this->id
                )
            );
        }
    }

    /**
     * Instanciate the sitemap for the workspace based on its configured functionalities
     * @param bab_eventBeforeSiteMapCreated $event
     */
    public function initializeSiteMap(bab_eventBeforeSiteMapCreated $event)
    {
        bab_Functionality::includefile('Icons');

        $mergeAddonsMenu = bab_Registry::get('/core/sitemap/mergeAddonsMenu', false);
        if ($mergeAddonsMenu) {
            $userPosition = array('root', 'DGAll', 'babUser');
        } else {
            $userPosition = array('root', 'DGAll', 'babUser', 'babUserSectionAddons');
        }

        $userPosition[] = 'workspace_workspaces';


        //Select workspace
        $item = $event->createItem('workspace_' . $this->delegation);
        $item->setPosition($userPosition);
        $item->setLabel(bab_toHtml($this->name));
        $item->setLink(workspace_Controller()->Page()->display('home', $this->id)->url(), 0);
        $item->addIconClassname(Func_Icons::APPS_SUMMARY);
        $event->addFolder($item);

        $workspacePosition = $userPosition;
        $workspacePosition[] = 'workspace_' . $this->delegation;

        //Home
        $item = $event->createItem('workspace_home' . $this->delegation);
        $item->setPosition($workspacePosition);
        $item->setLabel(workspace_translate('Home page'));
        $item->setLink(workspace_Controller()->Page()->display('home', $this->id)->url(), 0);
        $item->addIconClassname(Func_Icons::ACTIONS_GO_HOME);
        $event->addFunction($item);



        //Addons
        $App = workspace_App();
        $workspaceAddons = $App->getWorkspaceAddons();
        foreach ($workspaceAddons as $addonName) {
            if ($this->hasFunctionality($addonName)) {
                $addon = bab_functionality::get('WorkspaceAddon/' . $addonName);
                if ($addon) {
                    $item = $event->createItem('workspace_' . $addonName . $this->delegation);
                    $item->setPosition($workspacePosition);
                    $item->setLabel($addon->getName());
                    $item->setLink(workspace_Controller()->Page()->display($addonName, $this->id)->url(), 0);
                    $item->addIconClassname($addon->getIconClassName());
                    $event->addFunction($item);
                }
            }
        }

        //Configuration
        if (bab_isUserAdministrator()) {
            $item = $event->createItem('workspace_configuration' . $this->delegation);
            $item->setPosition($workspacePosition);
            $item->setLabel(workspace_translate('Configuration'));
            $item->setLink(workspace_Controller()->Admin()->edit($this->id)->url(), 0);
            $item->addIconClassname(Func_Icons::CATEGORIES_PREFERENCES_OTHER);
            $event->addFunction($item);
        }
    }

    public function updateAddons($data = array())
    {
        bab_Registry::set('/workspace/workspaces/'.$this->delegation.'/name', $data['name']);
        bab_Registry::set('/workspace/workspaces/'.$this->delegation.'/skin', $data['skin']);
        bab_Registry::set('/workspace/workspaces/'.$this->delegation.'/skin_styles', $data['skinStyles']);
        foreach ($data['addons'] as $addonName => $isWorkspaceEnabled){
            bab_Registry::set('/workspace/workspaces/'.$this->delegation.'/addons/'.$addonName.'/isWorkspaceEnabled', $isWorkspaceEnabled);
            if($isWorkspaceEnabled){
                $addon = bab_functionality::get('WorkspaceAddon/' . $addonName);
                if ($addon) {
                    $addon->applyConfiguration($this->id);
                }
            }
        }
    }

    /**
     * Returns whether or not the workspace has the functionality available
     * @param string $functionalityPath Path to the functionality (Ex : DirectoryManager, FileManager, ... )
     * @return bool
     */
    public function hasFunctionality($functionalityPath)
    {
        return bab_Registry::get('/workspace/workspaces/' . $this->delegation . '/addons/' . $functionalityPath . '/isWorkspaceEnabled', false) == 1; //Force returned value to be a boolean
    }


    /**
     * Returns the list of enabled WorkspaceAddon names.
     * For example:
     *  Array
     *  (
     *      [0] => Calendar
     *      [1] => DirectoryManager
     *      [2] => FileManager
     *      [3] => TaskManager
     *  )
     *
     * @return string[]
     */
    public function getFunctionalities()
    {
        $App = workspace_App();
        $workspaceAddons = $App->getWorkspaceAddons();
        $functionalities = array();
        foreach ($workspaceAddons as $addonName){
            if ($this->hasFunctionality($addonName)){
                $functionalities[] = $addonName;
            }
        }
        return $functionalities;
    }
}
