<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */

/* @var $App Func_App_Workspace */
$App = bab_functionality::get('App/Workspace');
$App->includeRecordSet();

/**
 * A class used to store tags.
 *
 * @property    ORM_StringField    $label
 * @property    ORM_BoolField      $checked
 *
 * @method workspace_Tag                  get(mixed $criteria)
 * @method workspace_Tag                  request(mixed $criteria)
 * @method workspace_Tag[]|\ORM_Iterator  select(\ORM_Criteria $criteria)
 * @method workspace_Tag                  newRecord()
 *
 * @method Func_App_Workspace App()
 */
class workspace_TagSet extends app_RecordSet
{
    public function __construct()
    {
        parent::__construct(workspace_App());
        
        $this->setPrimaryKey('id');
        
        $this->App = workspace_App();

        $this->setDescription('Tag');

        $this->addFields(
            ORM_StringField('label')
                ->setDescription('Label')
        );
    }


    /**
     * Returns an iterator of tags associated to the specified object,
     * optionally filtered on the specified link type.
     *
     * @return ORM_Iterator
     */
    public function selectLinkedTo(ORM_Record $source)
    {
        $linkType = array('hasTag', 'hasPersonalTag');
        $linkSet = $this->App->LinkSet();
        if (is_array($source) || ($source instanceof Iterator)) {
            return $linkSet->selectForSources($source, $this->getRecordClassName(), $linkType);
        } else {
            return $linkSet->selectForSource($source, $this->getRecordClassName(), $linkType);
        }
    }


    /**
     * Replaces all links to $oldTagId by links to $newTagId.
     *
     * @return workspace_TagSet
     */
    public function replaceLinks($oldTagId, $newTagId, $linkType = array('hasTag', 'hasPersonalTag'))
    {
        $linkSet = $this->App->LinkSet();

        $links = $linkSet->select(
            $linkSet->targetClass->is($this->App->TagClassName())
               ->_AND_($linkSet->targetId->is($oldTagId))
               ->_AND_($linkSet->type->is($linkType))
        );

        foreach ($links as $link) {
            $link->targetId = $newTagId;
            $link->save();
        }

        return $this;
    }

    /**
     * @return ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }


    /**
     * @return ORM_Criteria
     */
    public function isUpdatable()
    {
        if ($this->App->Access()->administer()) {
            return $this->all();
        }
        return $this->none();
    }

    /**
     * @return ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }


    /**
     * {@inheritDoc}
     * @see ORM_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return $this->App->Access()->administer();
    }
    
    /**
     * Returns an iterator of tags associated to the specified object,
     * optionally filtered on the specified link type.
     *
     * @return ORM_Iterator
     */
    public function selectFor(ORM_Record $source)
    {
        $linkType = array('hasTag', 'hasPersonalTag');
        $linkSet = $this->App->LinkSet();
        return $linkSet->selectForSource($source, $this->getRecordClassName(), $linkType);
    }
    
}



/**
 * @property    string     $label
 * @property    string     $description
 * @property    bool       $checked
 *
 * @method Func_App_Workspace App()
 */
class workspace_Tag extends app_Record
{
    
    public $App = null;
    
    public function __construct(workspace_TagSet $parentSet)
    {
        parent::__construct($parentSet);
        $this->App = workspace_App();
    }
    /**
     * {@inheritDoc}
     * @see ORM_Record::getRecordTitle()
     */
    public function getRecordTitle()
    {
        return $this->label;
    }


    /**
     * Replaces all links this tag by links to the one specified.
     *
     * @param workspace_Tag|int	$tag	A tag object or id.
     *
     * @return workspace_Tag
     */
    public function replaceBy($tag)
    {
        if ($tag instanceof workspace_Tag) {
            $tag = $tag->id;
        }
        $this->getParentSet()->replaceLinks($this->id, $tag);
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isLinkedTo(ORM_Record $source, $linkType = null)
    {
        $linkSet = $this->App->LinkSet();
        
        $criteria =	$linkSet->sourceClass->is(get_class($source))
        ->_AND_($linkSet->sourceId->is($source->id))
        ->_AND_($linkSet->targetClass->is(get_class($this)))
        ->_AND_($linkSet->targetId->is($this->id));
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $links = $linkSet->select($criteria);
        
        $isLinked = ($links->count() > 0);
        
        $linkSet->__destruct();
        unset($linkSet);
        
        return $isLinked;
    }
    
    
    
    /**
     * @return bool
     */
    public function isSourceOf(ORM_Record $target, $linkType = null)
    {
        $linkSet = $this->App->LinkSet();
        
        $criteria =	$linkSet->targetClass->is(get_class($target))
        ->_AND_($linkSet->targetId->is($target->id))
        ->_AND_($linkSet->sourceClass->is(get_class($this)))
        ->_AND_($linkSet->sourceId->is($this->id));
        if (isset($linkType)) {
            if (is_array($linkType)) {
                $criteria = $criteria->_AND_($linkSet->type->in($linkType));
            } else {
                $criteria = $criteria->_AND_($linkSet->type->is($linkType));
            }
        }
        $links = $linkSet->select($criteria);
        
        $isLinked = ($links->count() > 0);
        
        $linkSet->__destruct();
        unset($linkSet);
        
        return $isLinked;
    }
    
    
    
    /**
     * Link record to $source
     *
     * @return ORM_Record
     */
    public function linkTo(ORM_Record $source, $linkType = '')
    {
        $linkSet = $this->App->LinkSet();
        /* @var $link workspace_Link */
        $link = $linkSet->newRecord();
        $link->sourceClass = get_class($source);
        $link->sourceId = $source->id;
        $link->targetClass = get_class($this);
        $link->targetId = $this->id;
        $link->type = $linkType;
        $link->save();
        
        $link->__destruct();
        unset($link);
        
        $linkSet->__destruct();
        unset($linkSet);
        
        return $this;
    }
    
    
    
    /**
     * Unlink record from $source
     *
     * @return ORM_Record
     */
    public function unlinkFrom(ORM_Record $source, $linkType = null)
    {
        $linkSet = $this->App->LinkSet();
        
        $linkSet->deleteLink($source, $this, $linkType);
        
        return $this;
    }
}
