<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/admin.php';
require_once dirname(__FILE__).'/workspaces.php';

/**
 *
 */
class Func_Workspace extends bab_functionality
{
	/**
	 *
	 */
	public function getWorkspaceInfo($id = null)
	{
		if ($id === null) {
			$id = workspace_getCurrentWorkspace();
		}

		return workspace_getWorkspaceInfo($id, true);
	}
	
	
	/**
	 *
	 */
	public function getWorkspaceCalendar($id = null)
	{
		if ($id === null) {
			$id = workspace_getCurrentWorkspace();
		}

		return workspace_getWorkspaceCalendarId($id);
	}
	
	
	/**
	 *
	 */
	public function getWorkspaceCategory($id = null)
	{
		if ($id === null) {
			$id = workspace_getCurrentWorkspace();
		}

		return workspace_getWorkspaceCategory($id);
	}
	
	
	/**
	 *
	 */
	public function getWorkspaces()
	{
		return workspace_getWorkspaceList(null, true);
	}

	public function createWorkspace($name, $administrator = null, $groupId = null, $skin = null, $skin_styles = null, $num_article = null, $num_forum = null, $num_agenda = null, $num_file = null, $other_them = null, $file_size = null, $comment = null, $forum_notif = null, $update_directory = null, $versioning = null, $notifyCalendar = null, $category = 0)
	{
		$group;
		if (!empty($group) && false === bab_isGroup($group)) {
			throw new Exception('Wrong group id');
			return false;
		}

		$workspace = workspace_createWorkspace(
			$name,
			$administrator,
			$groupId,
			$skin,
			$skin_styles,
			$num_article,
			$num_forum,
			$num_agenda,
			$num_file,
			$other_them,
			$file_size,
			$comment,
			$forum_notif,
			$update_directory,
			$versioning,
		    $notifyCalendar,
		    $category
		);

		if (!$workspace) {
			return false;
		}

		bab_siteMap::clearAll(); // clear for disabled workspaces, removed from sitemap
		return $workspace;
	}

	/**
	 *
	 */
	public function updateWorkspace($id, $name)
	{
		//@TODO
	}


	/**
	 * @param int    $id Workspace id
	 */
	public function disableWorkspace($id)
	{
		global $babDB;
		$res = $babDB->db_query('SELECT * FROM '.BAB_DG_GROUPS_TBL.'
		                   	 	 WHERE id='.$babDB->quote($id));
		if( $babDB->db_num_rows($res) != 1) {
		    return false;
		}

		$registry = bab_getRegistryInstance();
        $registry->changeDirectory('/workspace/workspaces/' . $id);

		$registry->setKeyValue('disabled', true);

		return true;
	}


	/**
	 * @param int    $id Workspace id
	 */
	public function enableWorkspace($id)
	{
		global $babDB;
		$res = $babDB->db_query('SELECT * FROM '.BAB_DG_GROUPS_TBL.'
		                   	 	 WHERE id='.$babDB->quote($id));
		if( $babDB->db_num_rows($res) != 1) {
		    return false;
		}

		$registry = bab_getRegistryInstance();
        $registry->changeDirectory('/workspace/workspaces/' . $id);

		$registry->setKeyValue('disabled', false);

		return true;
	}


	/**
	 * @param int    $id Workspace id
	 */
	public function getWorksapceMembers($id)
	{
		$directoryId = workspace_getWorkspaceDirectoryId($id);
		$groupId = workspace_getWorkspaceGroupId($id);
		$users = bab_getGroupsMembers($groupId);
		$autoUpdate = workspace_getWorkspaceUpdateDirectory($id);

		$return = array();

		if ($users) {
	        foreach ($users as $user) {
	            $uptodate = workspace_dirEntryIsUpToDate($user['id'], $directoryId);
	            if ($autoUpdate && !$uptodate) {
	                workspace_updateUserDirectoryEntryFromPortal($user['id'], $directoryId);
	            }

	            $dirEntryId = workspace_getDirEntryFromUserId($directoryId, $user['id']);
	            if ($dirEntryId == null) {
	                $dirEntry = $user;
	            } else {
	                $dirEntry = bab_getDirEntry($dirEntryId, BAB_DIR_ENTRY_ID);
	            }

	            if (!is_array($dirEntry)) {
	                continue;
	            }

				$return[$user['id']]['id'] = $user['id'];

	            $profile = workspace_getUserProfile($user['id'], $id);
	            if (isset($profile)) {
	                $return[$user['id']]['profile'] = $profile;
	            }

				$return[$user['id']]['uptodate'] = true;
	            if (!$autoUpdate && !$uptodate) {
	                $return[$user['id']]['uptodate'] = false;
	            }

				foreach($dirEntry as $k => $info) {
					if(is_array($info)) {
						$return[$user['id']][$k] = $info['value'];
					} else {
						$return[$user['id']][$k] = $info;
					}
				}
	        }
        }

		return $return;
	}

	/**
	 *
	 * @param int    $workspace Workspace id
	 * @param string $profile   'reader', 'writer' or 'administrator'.
	 * @param int    $user      The user id.
	 *
	 */
	public function addMember($workspace, $profile, $user)
	{
		$groupId = workspace_getWorkspaceGroupId($workspace);

		bab_addUserToGroup($user, $groupId);
		/*if ($notifyUser == '1') {
		    workspace_notifyNewMember($workspace, $user);
		}*/

		workspace_setUserProfile($user, $profile, $workspace);

		return true;
	}

	/**
	 *
	 * @param int    $workspace Workspace id
	 * @param int    $user      The user id.
	 *
	 */
	public function removeMember($workspace, $user)
	{
		$groupId = workspace_getWorkspaceGroupId($workspace);
		$administratorsGroupId = workspace_getWorkspaceAdministratorsGroupId($workspace);
		$writersGroupId = workspace_getWorkspaceWritersGroupId($workspace);

		bab_removeUserFromGroup($user, $groupId);
		bab_removeUserFromGroup($user, $administratorsGroupId);
		bab_removeUserFromGroup($user, $writersGroupId);

		if (workspace_userIsWorkspaceAdministrator($user, $workspace)) {
			workspace_removeDelegationAdmin($workspace, $user);
		}

		// We also delete the corresponding directory entry in the workspace directory.
		$directoryId = workspace_getWorkspaceDirectoryId($workspace);
		$dirEntryId = workspace_getDirEntryFromUserId($directoryId, $user);
		workspace_deleteDbContact($dirEntryId);

		return true;
	}

	/**
	 *
	 * @param int		$workspace Workspace id
	 * @param int		$user      The user id.
	 * @return string	$profile   'reader', 'writer' or 'administrator'.
	 *
	 */
	public function getUserProfile($workspace, $user)
	{
		return workspace_getUserProfile($user, $workspace);
	}




	/**
	 *
	 * @param int    $workspace Workspace id
	 * @param int    $user      The user id.
	 * @param string $profile   'reader', 'writer' or 'administrator'.
	 *
	 */
	public function setUserProfile($workspace, $user, $profile)
	{
		workspace_setUserProfile($user, $profile, $workspace);

		return true;
	}
	
	/**
	 * Get Group
	 *
	 * @return int	Id of the current parent groupe
	 */
	public function getWorkspacesParentGroup()
	{
		return workspace_getWorkspacesParentGroup();
	}
	
	/**
	 * @param int    $workspace Workspace id
	 *
	 * Get Group
	 *
	 * @return int	Id of the specified workspace writer groupe
	 */
	public function getWorkspaceWriterGroup($workspace)
	{
		return workspace_getWorkspaceWritersGroupId($workspace);
	}
	
	/**
	 * @param int    $workspace Workspace id
	 *
	 * Get Group
	 *
	 * @return int	Id of the specified workspace admin groupe
	 */
	public function getWorkspaceAdminGroup($workspace)
	{
		return workspace_getWorkspaceAdministratorsGroupId($workspace);
	}
}