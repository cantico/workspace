<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/workspaces.php';

require_once $GLOBALS['babInstallPath'] . 'utilit/forumincl.php';
include_once $GLOBALS['babInstallPath'] . 'utilit/delincl.php';



/**
 *
 * @return array
 */
function workspace_getForumThreads($includeInactive = true, $includeActive = true)
{
	global $babDB;

	$forumId = workspace_getWorkspaceForumId();

	$threads = array();

	$sql = 'SELECT thread.*, post.author, post.id_author, post.subject, post.message
			FROM '.BAB_THREADS_TBL.' thread LEFT JOIN '.BAB_POSTS_TBL.' post ON thread.post=post.id
			WHERE forum=' . $babDB->quote($forumId);

	if (!$includeInactive) {
        $sql .= ' AND active=' . $babDB->quote('Y');
	}
	if (!$includeActive) {
	    $sql .= ' AND active=' . $babDB->quote('N');
	}

	$sql .= ' ORDER BY thread.date DESC';
	$res = $babDB->db_query($sql);

	while ($thread = $babDB->db_fetch_assoc($res)) {
		$threads[$thread['id']] = $thread;
	}

	return $threads;
}


/**
 * @param int	$threadId
 * @return array
 */
function workspace_getThreadPosts($threadId)
{
	global $babDB;

	$posts = array();

	$sql = 'SELECT *
			FROM '.BAB_POSTS_TBL.'
			WHERE id_thread='.$babDB->quote($threadId).'
			ORDER BY date ASC';

	$res = $babDB->db_query($sql);

	while ($post = $babDB->db_fetch_assoc($res)) {
		$posts[] = $post;
	}

	return $posts;
}


/**
 * @param int	$threadId
 * @return array
 */
function workspace_getThread($threadId)
{
	global $babDB;

	$sql = 'SELECT *
			FROM ' . BAB_THREADS_TBL . '
			WHERE id='.$babDB->quote($threadId);

	$res = $babDB->db_query($sql);

	$thread = $babDB->db_fetch_assoc($res);

	return $thread;
}



/**
 * Returns the number of posts on the specified forum thread.
 *
 * @param int	$threadId
 * @return int
 */
function workspace_getNbPosts($threadId)
{
	global $babDB;

	$sql = 'SELECT COUNT(*) AS nb
			FROM ' . BAB_POSTS_TBL . '
			WHERE id_thread='.$babDB->quote($threadId);

	$res = $babDB->db_query($sql);

	$posts = $babDB->db_fetch_assoc($res);

	return $posts['nb'];
}



/**
 * @param int	$postId
 * @return array
 */
function workspace_getPost($postId)
{
	global $babDB;

	$sql = 'SELECT *
			FROM ' . BAB_POSTS_TBL . '
			WHERE id='.$babDB->quote($postId);

	$res = $babDB->db_query($sql);

	$post = $babDB->db_fetch_assoc($res);

	return $post;
}


/**
 * Updates the thread after one of its post has been added or modified.
 *
 * @param int $threadId
 */
function workspace_updateThreadLastPost($threadId)
{
	global $babDB;

	$sql = 'SELECT id FROM ' . BAB_POSTS_TBL . '
			WHERE id_thread=' . $babDB->quote($threadId) . '
			ORDER BY date DESC
			LIMIT 1';
	$res = $babDB->db_query($sql);
	$lastPost = $babDB->db_fetch_assoc($res);

	if ($lastPost) {
		$sql = 'UPDATE '.BAB_THREADS_TBL. '
					SET lastpost=' . $babDB->quote($lastPost['id']) . '
					WHERE id = '.$babDB->quote($threadId);
		$res = $babDB->db_query($sql);
	}
}


/**
 * @param string	$subject
 * @param string	$message
 * @param int		$parentPostId
 * @param in		$postId
 * @return bool
 */
function workspace_savePost($subject, $message , $parentPostId = null, $postId = null)
{
	global $babDB;

	$starterId = $GLOBALS['BAB_SESS_USERID'];
	$name = $GLOBALS['BAB_SESS_USER'];
	$confirmed = 'Y';

	if (!isset($postId)) {

		$parentPost = workspace_getPost($parentPostId);
		if (!$parentPost) {
			return false;
		}

		$threadId = $parentPost['id_thread'];

		$sql = 'INSERT INTO '.BAB_POSTS_TBL.' (id_thread, id_parent, date, subject, message, id_author, author, confirmed, date_confirm)
				VALUES (' . $babDB->quote($threadId) . ', ' . $babDB->quote($parentPostId) . ', NOW(), ' . $babDB->quote($subject) . ', ' . $babDB->quote($message) . ', ' . $babDB->quote($starterId) . ', '. $babDB->quote($name) . ', ' . $babDB->quote($confirmed) . ', NOW())';

		$res = $babDB->db_query($sql);
		$postId = $babDB->db_insert_id();

		require_once $GLOBALS['babInstallPath'] . 'utilit/eventforum.php';
		$event = new bab_eventForumAfterPostAdd();

		$event->setForum($forumId);
		$event->setThread($threadId, $subject);
		$event->setPost($postId, $name, true);

		bab_fireEvent($event);

	} else {

		$post = workspace_getPost($postId);
		if (!$post) {
			return false;
		}

		$threadId = $post['id_thread'];

		$sql = 'UPDATE '.BAB_POSTS_TBL.' SET subject=' . $babDB->quote($subject). ', message=' . $babDB->quote($message). ', dateupdate=NOW()
				WHERE id=' . $babDB->quote($postId);

		$res = $babDB->db_query($sql);

	}

	workspace_updateThreadLastPost($post['id_thread']);

	return true;
}



/**
 * Deletes the specified forum thread.
 *
 * @param int		$threadId
 */
function workspace_deleteThread($threadId)
{
	if (!workspace_canDeleteThread($threadId)) {
		return false;
	}

	$thread = workspace_getThread($threadId);

	$forumId = $thread['forum'];

	bab_deleteThread($forumId, $threadId);

	return true;
}




/**
 * Closes the specified forum thread.
 *
 * @param int		$threadId
 */
function workspace_closeThread($threadId)
{
    if (!workspace_canCloseThread($threadId)) {
        return false;
    }

    bab_closeThread($threadId);
    return true;
}




/**
 * Opens the specified forum thread.
 *
 * @param int		$threadId
 */
function workspace_openThread($threadId)
{
    if (!workspace_canCloseThread($threadId)) {
        return false;
    }

    bab_openThread($threadId);
    return true;
}

/**
 * Creates or updates a forum thread.
 * If $id is provided, the corresponding thread is updated.
 *
 * @param string	$title
 * @param string	$body
 * @param int		$id		or null to create a new thread
 */
function workspace_saveThread($subject, $message, $threadId = null)
{
	global $babDB;

	$forumId = workspace_getWorkspaceForumId();

	$starterId = $GLOBALS['BAB_SESS_USERID'];
	$name = $GLOBALS['BAB_SESS_USER'];

	$notifyme = 'N';
	$confirmed = 'Y';

	if (!isset($threadId)) {

		$sql = 'INSERT INTO '.BAB_THREADS_TBL.' (forum, date, notify, starter) VALUES
				(' .$babDB->quote($forumId). ', NOW(), ' . $babDB->quote($notifyme) . ', ' . $babDB->quote($starterId). ')';
		$res = $babDB->db_query($sql);
		$threadId = $babDB->db_insert_id();

		$sql = 'INSERT INTO '.BAB_POSTS_TBL.' (id_thread, date, subject, message, id_author, author, confirmed, date_confirm)
					VALUES (' .$babDB->quote($threadId). ', NOW(), ' . $babDB->quote($subject). ', ' . $babDB->quote($message). ', ' . $babDB->quote($starterId). ', '. $babDB->quote($name) . ', ' . $babDB->quote($confirmed). ', NOW())';
		$res = $babDB->db_query($sql);
		$postId = $babDB->db_insert_id();

		$sql = 'UPDATE '.BAB_THREADS_TBL. '
					SET lastpost='.$babDB->quote($postId).',post='.$babDB->quote($postId).'
				WHERE id = '.$babDB->quote($threadId);
		$res = $babDB->db_query($sql);

		require_once $GLOBALS['babInstallPath'] . 'utilit/eventforum.php';
		$event = new bab_eventForumAfterThreadAdd();

		$event->setForum($forumId);
		$event->setThread($threadId, $subject);
		$event->setPost($postId, $name, true);

		bab_fireEvent($event);

	} else {

		$arr = $babDB->db_fetch_assoc($babDB->db_query('SELECT post FROM '.BAB_THREADS_TBL.' WHERE id=' . $babDB->quote($threadId)));

		if (!$arr) {
			return false;
		}
		$postId = $arr['post'];
		$sql = 'UPDATE '.BAB_POSTS_TBL.'
					SET
						subject=' . $babDB->quote($subject). ',
						message=' . $babDB->quote($message). ',
						dateupdate=NOW()
				WHERE id=' . $babDB->quote($postId);

		$res = $babDB->db_query($sql);
	}

	return true;
}



/**
 * Deletes the specified forum post.
 *
 * @param int	$postId		The post id.
 */
function workspace_deletePost($postId)
{
	global $babDB;

	if (!workspace_canDeletePost($postId)) {
		return false;
	}


	$post = workspace_getPost($postId);
	$thread = workspace_getThread($post['id_thread']);

	$forumId = $thread['forum'];

	if ($post['id_parent'] == 0) {
		/* if it's the only post in the thread, delete the thread also */
		bab_deleteThread($forumId, $post['id_thread']);
	} else {
		bab_deletePostFiles($forumId, $postId);
		$sql = 'DELETE FROM ' . BAB_POSTS_TBL . ' WHERE id = ' . $babDB->quote($postId);
		$babDB->db_query($sql);
		workspace_updateThreadLastPost($post['id_thread']);
	}

}




/**
 * Checks whether the current user can delete the specified forum post.
 *
 * @param int $postId
 * @return bool
 */
function workspace_canDeletePost($postId)
{
	global $BAB_SESS_USERID, $babDB;
	$post = workspace_getPost($postId);

	if ($post['id_author'] != $BAB_SESS_USERID) {
		return false;
	}

	// We do not allow to delete posts which are parent to other posts.
	$sql = 'SELECT id FROM ' . BAB_POSTS_TBL . '
			WHERE id_parent=' . $babDB->quote($postId) . '
			LIMIT 1';

	$res = $babDB->db_query($sql);
	if ($babDB->db_fetch_assoc($res)) {
		return false;
	}

	// Check acl on the post's forum.
	$thread = workspace_getThread($post['id_thread']);
	if (!bab_isAccessValid(BAB_FORUMSMAN_GROUPS_TBL, $thread['forum'])) {
		return false;
	}

	return true;
}



/**
 * Checks whether the current user can create a thread in the specified forum.
 *
 * @param int $forumId		The current workspace forum if not specified.
 * @return bool
 */
function workspace_canCreateThread($forumId = null)
{
	if (!isset($forumId)) {
		$forumId = workspace_getWorkspaceForumId();
	}
	return workspace_userIsWorkspaceAdministrator()
			|| bab_isAccessValid(BAB_FORUMSPOST_GROUPS_TBL, $forumId);
}



/**
 * Checks whether the current user can delete the specified forum thread.
 *
 * @param int $threadId
 * @return bool
 */
function workspace_canDeleteThread($threadId)
{
	$thread = workspace_getThread($threadId);

	return workspace_userIsWorkspaceAdministrator()
			|| bab_isAccessValid(BAB_FORUMSMAN_GROUPS_TBL, $thread['forum']);
}


/**
 * Checks whether the current user can delete the specified forum thread.
 *
 * @param int $threadId
 * @return bool
 */
function workspace_canCloseThread($threadId)
{
	$thread = workspace_getThread($threadId);

	return workspace_userIsWorkspaceAdministrator()
			|| bab_isAccessValid(BAB_FORUMSMAN_GROUPS_TBL, $thread['forum']);
}


/**
 * Checks whether the current user can view the specified forum thread.
 *
 * @param int $threadId
 * @return bool
 */
function workspace_canViewThread($threadId)
{
	$thread = workspace_getThread($threadId);

	return workspace_userIsWorkspaceAdministrator()
			|| bab_isAccessValid(BAB_FORUMSVIEW_GROUPS_TBL, $thread['forum']);
}


/**
 * Checks whether the current user can reply in the specified forum thread.
 *
 * @param int $threadId
 * @return bool
 */
function workspace_canReplyInThread($threadId)
{
	$thread = workspace_getThread($threadId);

	return workspace_userIsWorkspaceAdministrator()
			|| bab_isAccessValid(BAB_FORUMSREPLY_GROUPS_TBL, $thread['forum']);
}


/**
 * Checks whether the current user can view the specified forum threads / posts.
 *
 * @param int $forumId	The current workspace forum if not specified.
 * @return bool
 */
function workspace_canViewForumThreads($forumId = null)
{
	if (!isset($forumId)) {
		$forumId = workspace_getWorkspaceForumId();
	}
	return workspace_userIsWorkspaceAdministrator()
			|| bab_isAccessValid(BAB_FORUMSVIEW_GROUPS_TBL, $forumId);
}




/**
 * @property string	$subject		The post subject
 * @property string	$message		The post content
 */
class workspace_Thread
{
	private $loaded;

}



/**
 * @property string	$subject		The post subject
 * @property string	$message		The post content
 */
class workspace_Post
{
	private $loaded;

	private $id = null;
	private $id_thread = null;
	private $id_parent = null;
	private $date = null;
	private $date_confirm = null;
	private $dateupdate = null;
	private $subject = null;
	private $message = null;
	private $confirmed = null;
	private $id_author = null;
	private $author = null;


	private $id_forum = null;


	/**
	 * @param int	$id
	 */
	public function __construct($post = null)
	{
		if (is_array($post)) {
			$this->id = $post['id'];
			$this->id_thread = $post['id_thread'];
			$this->id_parent = $post['id_parent'];
			$this->date = $post['date'];
			$this->date_confirm = $post['date_confirm'];
			$this->dateupdate = $post['dateupdate'];
			$this->id_author = $post['id_author'];
			$this->author = $post['author'];
			$this->subject = $post['subject'];
			$this->message = $post['message'];
			$this->confirmed = $post['confirmed'];
			$this->loaded = true;
		} else {
			$this->id = $post;
			$this->loaded = false;
		}
	}



	/**
	 *
	 */
	private function load()
	{
		if ($this->loaded) {
			return;
		}

		global $babDB;

		$sql = 'SELECT `id_thread`, `id_parent`, `date`, `dateupdate`, `date_confirm`, `id_author`, `author`, `subject`, `message`, `confirmed`
				FROM ' . BAB_POSTS_TBL . '
				WHERE id=' . $babDB->quote($this->id);

		$res = $babDB->db_query($sql);

		$post = $babDB->db_fetch_assoc($res);
		is_null($this->id_thread)
					&& $this->id_thread = $post['id_thread'];
		is_null($this->id_parent)
					&& $this->id_parent = $post['id_parent'];
		is_null($this->date)
					&& $this->date = $post['date'];
		is_null($this->date_confirm)
					&& $this->confirmationDate = $post['date_confirm'];
		is_null($this->dateupdate)
					&& $this->dateupdate = $post['dateupdate'];
		is_null($this->id_author)
					&& $this->id_author = $post['id_author'];
		is_null($this->author)
					&& $this->author = $post['author'];
		is_null($this->subject)
					&& $this->subject = $post['subject'];
		is_null($this->message)
					&& $this->message = $post['message'];
		is_null($this->confirmed)
					&& $this->confirmed = $post['confirmed'];

		$this->loaded = true;
	}


	public function getId()
	{
		return $this->id;
	}

	public function getThread()
	{
		$this->load();
		return $this->id_thread;
	}

	public function getParent()
	{
		$this->load();
		return $this->id_parent;
	}

	public function getForum()
	{
		$this->load();
		if (!isset($this->id_forum)) {
			$thread = workspace_getThread($this->id_thread);
			$this->id_forum = (int)$thread['forum'];
		}
		return $this->id_forum;
	}

	public function getSubject()
	{
		$this->load();
		return $this->subject;
	}

	public function getMessage()
	{
		$this->load();
		return $this->message;
	}

	public function getCreationDate()
	{
		$this->load();
		return $this->date;
	}

	public function getModificationDate()
	{
		$this->load();
		return $this->dateupdate;
	}

	public function getConfirmationDate()
	{
		$this->load();
		return $this->date_confirm;
	}

	public function getAuthor()
	{
		$this->load();
		return $this->id_author;
	}

	public function getAuthorName()
	{
		$this->load();
		return $this->author;
	}

	public function getConfirmed()
	{
		$this->load();
		return ($this->confirmed == 'Y');
	}


	public function __get($propertyName)
	{
		$this->load();

		$method = 'get' . $propertyName;
		if (method_exists($this, $method)) {
			return $this->$method();
		}
	}


	public function __set($propertyName, $value)
	{
		$this->$propertyName = $value;
	}



	public function save()
	{
		global $babDB;


		if (!isset($this->id)) {

			$authorId = $this->getAuthor();
			$authorName = $this->getAuthorName();

			if (!isset($authorId)) {
				$authorId = $GLOBALS['BAB_SESS_USERID'];
				$authorName = $GLOBALS['BAB_SESS_USER'];
			}
			$confirmed = 'Y';
			$parentPost = new workspace_Post($this->getParent());

			$this->id_thread = $parentPost->getThread();

			$confirmed = ($this->getConfirmed() ? 'Y' : 'N');

			$sql = 'INSERT INTO '.BAB_POSTS_TBL.' (id_thread, id_parent, date, subject, message, id_author, author, confirmed, date_confirm)
					VALUES (' . $babDB->quote($this->id_thread) . ', ' . $babDB->quote($this->getParent()) . ', NOW(), ' . $babDB->quote($this->getSubject()) . ', ' . $babDB->quote($this->getMessage()) . ', ' . $babDB->quote($authorId) . ', '. $babDB->quote($authorName) . ', ' . $babDB->quote($confirmed) . ', NOW())';

			$res = $babDB->db_query($sql);
			$this->id = $babDB->db_insert_id();

			$thread = workspace_getThread($this->id_thread);
			$this->id_forum = (int)$thread['forum'];

			$p = workspace_getPost($thread['post']);
			$threadTitle = $p['subject'];

			require_once $GLOBALS['babInstallPath'] . 'utilit/eventforum.php';
			$event = new bab_eventForumAfterPostAdd();

			$event->setForum($this->getForum());
			$event->setThread($this->id_thread, $threadTitle);
			$event->setPost($this->id, $authorName, true);

			bab_fireEvent($event);

		} else {

			$sql = 'UPDATE '.BAB_POSTS_TBL.'
						SET
							subject=' . $babDB->quote($this->getSubject()). ',
							message=' . $babDB->quote($this->getMessage()). ',
							dateupdate=NOW()
					WHERE id=' . $this->getId();

			$res = $babDB->db_query($sql);

		}

		workspace_updateThreadLastPost($this->getThread());

		return true;
	}
}

