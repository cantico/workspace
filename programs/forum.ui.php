<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/workspacewidgets.php';





/**
 * @return Widget_Form
 */
function workspace_ThreadEditor()
{
	$W = bab_Widgets();

	$form = $W->Form();
	$frame = new workspace_BaseForm('thread_editor');
	$frame->setName('thread')
			  ->addClass('workspace-dialog');

	$titleFormItem = workspace_FormField(
	    workspace_translate('Title:'),
		$W->LineEdit()
			->setName('title')
			->setSize(80)
			->setMandatory(true, workspace_translate("The thread title must not be empty"))
    );

	switch (workspace_getWorkspacesTextEditor()) {
	    case 'plain':
	        $bodyEditor = $W->TextEdit('bab_post_body');
	        break;
	    case 'wysiwyg':
	        $bodyEditor = $W->BabHtmlEdit('bab_post_body');
	        break;
	    case 'rich':
	    default:
	        $bodyEditor = $W->SimpleHtmlEdit('bab_post_body');
	        break;
	}

	$bodyFormItem = workspace_FormField(
	    workspace_translate('Body:'),
		$bodyEditor
			->setName('body')
			->setLines(10)
			->setColumns(100)
			->setMandatory(true, workspace_translate("The thread body must not be empty"))
    );

	$frame->addItem($titleFormItem);
	$frame->addItem($bodyFormItem);


	$frame->addButton(
	    $W->SubmitButton('save')
			->validate(true)
			->setLabel(workspace_translate("Save"))
			->setAction(workspace_Controller()->Forum()->saveThread())
	)
	->addButton(
	    $W->SubmitButton('cancel')
			->setLabel(workspace_translate("Cancel"))
			->setAction(workspace_Controller()->Forum()->cancel())
	);

	$form->setLayout($W->VBoxLayout())->addItem($frame);

	workspace_setSelfPageHiddenFields($form);

	return $form;
}




function workspace_postDisplay($postId)
{
	$W = bab_Widgets();

	$post = workspace_getPost($postId);

	/* @var $replaceFactory workspace_replace */
	$replaceFactory = bab_getInstance('workspace_replace');
	$replaceFactory->ref($post['message']);

	$frame = $W->Frame(null, $W->VBoxLayout())
		->addClass('workspace-dialog')
		->addItem($W->Title($post['subject'], 3))
		->addItem(
		$W->Html($post['message'])
	);

	return $frame;
}


/**
 * @return Widget_Form
 */
function workspace_postEditor($parentPostId = null)
{
	$W = bab_Widgets();

	$form = $W->Form();
	$frame = new workspace_BaseForm('post_editor');
	$frame->setName('post')
		  ->addClass('workspace-dialog');

	if (isset($parentPostId)) {
		$parentPost = workspace_getPost($parentPostId);
		$title = 'Re: ' . $parentPost['subject'];
	} else {
		$title = '';
	}
	$titleFormItem = workspace_FormField(
	    workspace_translate('Title:'),
		$W->LineEdit()
            ->setName('title')
            ->setSize(80)->setName('title')
            ->setMandatory(true, workspace_translate("The post title must not be empty"))
    );

	switch (workspace_getWorkspacesTextEditor()) {
	    case 'plain':
	        $bodyEditor = $W->TextEdit('bab_post_body');
	        break;
	    case 'wysiwyg':
	        $bodyEditor = $W->BabHtmlEdit('bab_post_body');
	        break;
	    case 'rich':
	    default:
	        $bodyEditor = $W->SimpleHtmlEdit('bab_post_body');
	        break;
	}

	$bodyFormItem = workspace_FormField(
	    workspace_translate('Body:'),
		$bodyEditor
            ->setName('body')
			->setLines(10)
			->setColumns(100)
			->setMandatory(true, workspace_translate("The post body must not be empty"))
    );

	$frame->addItem($titleFormItem);
	$frame->addItem($bodyFormItem);


	$frame->addButton(
	    $W->SubmitButton('save')
			->validate(true)
            ->setLabel(workspace_translate("Save"))
            ->setAction(workspace_Controller()->Forum()->savePost())
    )
	->addButton(
	    $W->SubmitButton('cancel')
            ->setLabel(workspace_translate("Cancel"))
			->setAction(workspace_Controller()->Forum()->cancel())
    );

	$form->setLayout($W->VBoxLayout())->addItem($frame);

	workspace_setSelfPageHiddenFields($form);

	$form->setValue(array('post', 'title'), $title);

	return $form;
}





/**
 * @return Widget_Form
 */
function workspace_PostPreview(array $post)
{
	$W = bab_Widgets();

	$form = $W->Form();
	$frame = new workspace_BaseForm('post_preview');
	$frame->setName('Post')
			->addClass('workspace-post-preview');

	$title = $W->Title($post['title'], 4);
	$body = $W->RichText($post['body']);

	$frame->addItem($title);
	$frame->addItem($body);


	$frame->addButton(
	    $W->SubmitButton('OK')
			->setLabel(workspace_translate("OK"))
			->setAction(workspace_Controller()->Forum()->editPost())
	);

	$form->setLayout($W->VBoxLayout())->addItem($frame);

	workspace_setSelfPageHiddenFields($form);

	$form->setHiddenValues('post', $post);

	return $form;
}



/**
 * @return Widget_Frame
 */
function workspace_threadList($inactive = true, $active = true)
{
	require_once dirname(__FILE__) . '/forum.php';

	$W = bab_Widgets();

	$listView = new workspace_BaseList('thread_list');

	$listView->addClass('workspace-list');

	$forumId = workspace_getWorkspaceForumId();
	$threads = workspace_getForumThreads($inactive, $active);

	$nbThreads = 0;
	foreach ($threads as $thread) {

		$nbThreads++;

		$userId = $thread['id_author'];
		$authorIcon = workspace_userPreview($userId, workspace_formatDateTime(bab_mktime($thread['date'])));

		$nbPosts = workspace_getNbPosts($thread['id']);
		$repliesText = sprintf(workspace_translate('%d reply', '%d replies', $nbPosts - 1), $nbPosts - 1);

		if ($nbPosts > 1) {
			$lastPost = workspace_getPost($thread['lastpost']);
			$repliesItem = $W->VBoxItems(
			    $W->Link(
                    $repliesText,
                    workspace_Controller()->Forum()->listPosts($thread['id'], $thread['lastpost'])
                ),
			    $W->Label(sprintf(workspace_translate('Last on %s'), workspace_formatDateTime(bab_mktime($lastPost['date']))))
            );
		} else {
			$repliesItem = $W->Label($repliesText);
		}
		$repliesItem->addClass('widget-small');

		$buttonBox = workspace_buttonBox();

		if (workspace_canViewThread($thread['id'])) {
			$buttonBox->addItem(
			    $W->Link(
			        workspace_translate('Show full thread'),
			        workspace_Controller()->Forum()->listPosts($thread['id'])
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_ARROW_RIGHT)
            );
		}
		if ($thread['id_author'] == $GLOBALS['BAB_SESS_USERID']) {
			$buttonBox->addItem(
			    $W->Link(
			        workspace_translate('Edit'),
			        workspace_Controller()->Forum()->editThread($thread['id'])
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
            );
		}
		if (workspace_canCloseThread($thread['id'])) {
		    if (bab_isForumThreadOpen($forumId, $thread['id'])) {
    		    $buttonBox->addItem(
    		        $W->Link(
    		            workspace_translate('Close thread'),
    		            workspace_Controller()->Forum()->closeThread($thread['id'])
    		        )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DIALOG_CANCEL)
    		    );
		    } else {
		        $buttonBox->addItem(
		            $W->Link(
		                workspace_translate('Re-open'),
		                workspace_Controller()->Forum()->openThread($thread['id'])
		            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_ARROW_RIGHT)
		        );
		    }
		}
		if (workspace_canDeleteThread($thread['id'])) {
			$buttonBox->addItem(
			    $W->Link(
			        workspace_translate('Delete'),
                    workspace_Controller()->Forum()->deleteThread($thread['id'])
                )->setConfirmationMessage(workspace_translate('Are you sure you want to delete this forum thread?'))
			    ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE)
            );
		}

		/* @var $replaceFactory workspace_replace */
		$replaceFactory = bab_getInstance('workspace_replace');
		$replaceFactory->ref($thread['message']);


		$row = $W->HBoxItems(
			$W->VBoxItems(
				$authorIcon->setSizePolicy(Func_Icons::ICON_LEFT_48)
                    ->addClass('widget-15em'),
				$repliesItem
			),
			$W->VBoxItems(
				$W->VBoxItems(
					$W->Link(
					    $W->Label($thread['subject'])->addClass('title', 'widget-strong'),
					    workspace_Controller()->Forum()->listPosts($thread['id'])
                    ),
					$W->Html($thread['message'])->addClass('body')
				)->addClass('workspace-content-summary'),
				$buttonBox
			)
		)->setHorizontalSpacing(1, 'em');
		$listView->addItem($row);
	}

	if ($nbThreads < 1) {
	    if ($inactive) {
	        $title = workspace_translate('There are no closed threads in this forum');
    		$row = $W->VBoxItems(
    			$W->Title($title, 4)
    		);
	    } else {
	        $title = workspace_translate('There are no open threads in this forum');
    		$row = $W->VBoxItems(
    			$W->Title($title, 4),
    			$W->Label(workspace_translate('You can add a new one by clicking \'New thread\' in the toolbar above.'))
    		);
	    }
		$listView->addItem($row->addClass('workspace-empty-list-notice'));
	}


	return $listView;
	$splitview = $W->Frame('workspace_forum_splitview', $W->HBoxLayout()->addClass('expand'))
        ->addItem($listView->setSizePolicy(Widget_SizePolicy::MAXIMUM));

	return $splitview;
}




/**
 * Returns a list containing the list of posts in a specified thread. One of the thread posts
 * may be selected to appear highlighted.
 *
 * @param int	$threadId			The thread to display
 * @param int	$selectedPostId		The selected post in the thread that will be highlighted
 * @return Widget_Frame
 */
function workspace_postList($threadId, $selectedPostId)
{
	require_once dirname(__FILE__) . '/forum.php';

	$W = bab_Widgets();
	$T = bab_functionality::get('Thumbnailer');


	$listView = new workspace_BaseList('post_list');

	$listView->addClass('workspace-list');


	$posts = workspace_getThreadPosts($threadId);

	$nbPosts = 0;
	foreach ($posts as $post) {

		$nbPosts++;
		if ($nbPosts === 1) {
			$threadTitle = $W->Title($post['subject'], 1)->addClass('title');
		}



		$userId = $post['id_author'];

		$authorIcon = workspace_userPreview($userId, workspace_formatDateTime(bab_mktime($post['date'])));

// 		$dirEntry = bab_getDirEntry($userId, BAB_DIR_ENTRY_ID_USER);
// 		$authorIcon = $W->Icon($post['author'] . "\n" . workspace_formatDateTime(bab_mktime($post['date'])));

// 		$authorIcon->setStockImage(Func_Icons::APPS_USERS);


// 		if (isset($dirEntry['jpegphoto']['photo'])) {
// 			$photo = $dirEntry['jpegphoto']['photo'];

// 			if ($T && $photo instanceof bab_dirEntryPhoto) {
// 				$T->setSourceBinary($photo->getData(), $photo->lastUpdate());
// 				$T->setBorder(1, '#cccccc');
// 				$imageUrl = $T->getThumbnail(30, 30);
// 				$authorIcon->setImageUrl($imageUrl);
// 			}
// 		}


		$buttonBox = workspace_buttonBox();

		if (workspace_canReplyInThread($threadId)) {
			$buttonBox->addItem(
			    $W->Link(
			        workspace_translate('Reply'),
			        workspace_Controller()->Forum()->reply($post['id'])
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_GO_NEXT)
            );
		}
		if ($post['id_author'] == $GLOBALS['BAB_SESS_USERID']) {
			$buttonBox->addItem(
			    $W->Link(
			        workspace_translate('Edit'),
			        workspace_Controller()->Forum()->editPost($post['id'])
                )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
            );
		}
		if (workspace_canDeletePost($post['id'])) {
			$buttonBox->addItem(
			    $W->Link(
			        workspace_translate('Delete'),
			        workspace_Controller()->Forum()->deletePost($post['id'])
                )->setConfirmationMessage(workspace_translate('Are you sure you want to delete this forum post?'))
			    ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE)
            );
		}

		/* @var $replaceFactory workspace_replace */
		$replaceFactory = bab_getInstance('workspace_replace');
		$replaceFactory->ref($post['message']);

		$row = $W->HBoxItems(
    		$authorIcon->setSizePolicy(Func_Icons::ICON_LEFT_48)
			    ->addClass('widget-15em'),
			$W->VBoxItems(
				$W->Label($post['subject'])->addClass('title', 'widget-strong'),
				$W->Html($post['message'])->addClass('body'),
			    $buttonBox
			)->setVerticalSpacing(4, 'px')
			->setSizePolicy(Widget_SizePolicy::MAXIMUM)
			->addClass('workspace-content-summary')
		)->setHorizontalSpacing(1, 'em');

		if (isset($selectedPostId) && $post['id'] == $selectedPostId) {
			$row->addClass('workspace-highlighted');
		}
		$listView->addItem($row->setId('workspace_post_' . $post['id']));
	}

	$vbox = $W->VBoxItems($threadTitle, $listView);

	return $vbox;
}

