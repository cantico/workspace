<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

/* @var $App Func_App_Workspace */
$App = bab_functionality::get('App/Workspace');


$App->includeUi();


class workspace_Ui extends app_Ui
{

    /**
     * @var Func_App_Workspace
     */
    private $app = null;

        /**
     * @param Func_App $app
     */
    public function __construct()
    {
        $this->setApp(workspace_App());
    }
    
    /**
	 * (non-PHPdoc)
	 * @see programs/app_Object_Interface::setApp()
	 */
    public function setApp(Func_App $app = null)
    {
        $this->app = $app;
        return $this;
    }

    /**
     * @return Func_App_Workspace
     */
    public function App()
    {
        return $this->app;
    }



    /**
     * Includes workspace ui objects.
     */
    public function includeWorkspace()
    {
        require_once 'workspace.ui.php';
    }


//     /**
//      * @param workspace_Workspace $workspace
//      * @return workspace_WorkspaceCardFrame
//      */
//     public function WorkspaceCardFrame(workspace_Workspace $workspace)
//     {
//         $this->includeAddress();
//         return new workspace_WorkspaceCardFrame($this->App());
//     }

//     /**
//      *
//      * @param workspace_Workspace $workspace
//      * @return workspace_WorkspaceFullFrame
//      */
//     public function WorkspaceFullFrame(workspace_Workspace $workspace)
//     {
//         $this->includeTask();
//         return new workspace_WorkspaceFullFrame($this->App(), $task);
//     }

    /**
     * Workspace table view
     * @return workspace_WorkspaceTableView
     */
    public function WorkspaceTableView()
    {
        $this->includeWorkspace();
        return new workspace_WorkspaceTableView($this->App());
    }
    
    public function WorkspaceManagerEditor($itemId = null)
    {
        $this->includeWorkspace();
        $editor = new workspace_WorkspaceManagerEditor($this->App(), $itemId);
        return $editor;
    }


    /**
     * Workspace table view
     * @return workspace_WorkspaceCardsView
     */
    public function WorkspaceCardsView()
    {
        $this->includeWorkspace();
        return new workspace_WorkspaceCardsView($this->App());
    }


    /**
     * Workspace card frame
     * @return workspace_WorkspaceCardFrame
     */
    public function WorkspaceCardFrame(ORM_Record $record)
    {
        $this->includeWorkspace();
        return new workspace_WorkspaceCardFrame($this->App(), $record);
    }

    /**
     * Workspace editor
     * @return workspace_WorkspaceEditor
     */
    public function WorkspaceEditor()
    {
        $this->includeWorkspace();
        $editor = new workspace_WorkspaceEditor($this->App());
        return $editor;
    }
    
    public function WorkspaceFileManagerConfigurationEditor()
    {
        $this->includeWorkspace();
        $editor = new workspace_WorkspaceFileManagerConfigurationEditor($this->App());
        return $editor;
    }
    
    public function TagsEditor()
    {
        $this->includeWorkspace();
        $this->includeTag();
        $editor = new workspace_TagsEditor($this->App());
        return $editor;
    }
    
    /**
     * @return workspace_SuggestTag
     */
    public function SuggestTag($id = null)
    {
        /* @var $App Func_App_Workspace */
        $App = $this->App();
        require_once $App->getPhpPath(). 'widgets/suggesttag.class.php';
        $suggest = new workspace_SuggestTag($App, $id);
        $suggest->setSuggestAction($App->Controller()->Tag()->suggest(), 'search');
        return $suggest;
    }
    
    /**
     * Includes Tag Ui helper functions definitions.
     */
    public function includeTag()
    {
        $App = $this->App();
        require_once $App->getUiPath() . 'tag.ui.php';
    }
}
