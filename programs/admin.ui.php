<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/workspacewidgets.php';




function workspace_labelledField($label, $field)
{
	$W = bab_Widgets();

	$labelledField = $W->FlowItems(
		$W->Label($label)
			->colon()
			->addClass('widget-10em workspace-bold'),
		$field
	)->setHorizontalSpacing(2, 'em');

	return $labelledField;
}


function workspace_workspaceConfirmationDialog($workspaceName, $adminName, $groupName, $newGroup, $adminId, $groupId, $skin, $skinstyles, $num_article, $num_forum, $num_agenda, $num_file, $other_them, $file_size, $comment, $forum_notif, $update_directory, $versioning, $notifyCalendar, $category, $allowUserCreation)
{
	$W = bab_Widgets();

	$form = $W->Form();
	$frame = new workspace_BaseForm('workspace_editor');
	$frame->setName('workspace');
	$frame->addClass('workspace-dialog');

	if ($file_size == 0) {
		$maximumCapacity = workspace_translate('No limitation (same as system limits)');
	} else {
		$maximumCapacity = sprintf(workspace_translate('%s MB'), $file_size);
	}

	if ($newGroup) {
		$groupName .= ' ' . workspace_translate('(will be created)');
	}
	$titleFormItem = $W->Title(workspace_translate('You are about to create a new workspace:'), 3);

	global $babDB;
	$categoryName = '';
	if ($category != 0) {
		$res = $babDB->db_query('SELECT * FROM '.BAB_DG_CATEGORIES_TBL.' WHERE id='.$babDB->quote($category));
		if($cat = $babDB->db_fetch_assoc($res)) {
			$categoryName = $cat['name'];
		}
	}

	$workspaceInfoItem = $W->VBoxItems(
		workspace_labelledField(workspace_translate('Workspace name'), $W->Label($workspaceName)),
		workspace_labelledField(workspace_translate('Administrator'), $W->Label($adminName)),
		workspace_labelledField(workspace_translate('Group'), $W->Label($groupName)),
		workspace_labelledField(workspace_translate('Category'), $W->Label($categoryName)),
		workspace_labelledField(workspace_translate('Graphic theme'), $W->Label($skin.' / '.$skinstyles)),
		workspace_labelledField(workspace_translate('Maximum capacity'), $W->Label($maximumCapacity))
	)->setVerticalSpacing(8, 'px');



	$frame
		->addItem($titleFormItem)
		->addItem($workspaceInfoItem);


	$frame->addButton(
		$W->SubmitButton('save')
			->validate(true)
			->setLabel(workspace_translate('Create workspace'))
			->setAction(workspace_Controller()->Admin()->save())
	);
	$frame->addButton(
		$W->SubmitButton('cancel')
			->setLabel(workspace_translate('Cancel'))
			->setAction(workspace_Controller()->Admin()->cancel())
	);

    $form->setLayout($W->VBoxLayout())->addItem($frame);

    $form->setHiddenValues(
        'workspace',
        array(
            'name' => $workspaceName,
            'administrator' => $adminId,
            'group' => $groupId,
            'category' => $category,
            'skin' => $skin,
            'skin_styles' => $skinstyles,
            'num_article' => $num_article,
            'num_forum' => $num_forum,
            'num_agenda' => $num_agenda,
            'num_file' => $num_file,
            'other_them' => $other_them,
            'file_size' => $file_size,
            'comment' => $comment,
            'forum_notif' => $forum_notif,
            'update_directory' => $update_directory,
            'versioning' => $versioning,
            'notifyCalendar' => $notifyCalendar,
            'create_users' => $allowUserCreation
        )
    );
    workspace_setSelfPageHiddenFields($form);

	return $form;
}




/**
 * @return Widget_Form
 */
function workspace_WorkspaceGlobalConfigurationEditor()
{
	$W = bab_Widgets();

	$form = $W->Form();
	$frame = new workspace_BaseForm('workspace_editor');
	$frame->setName('configuration');
	$frame->addClass('workspace-dialog')
			->addClass('icon-left-16 icon-left icon-16x16');


	$categorySelect = $W->Select();

	global $babDB;
	$res = $babDB->db_query('SELECT * FROM '.BAB_DG_CATEGORIES_TBL.' ORDER BY name, description');

	$categorySelect->addOption(0, '');
	while ($cat = $babDB->db_fetch_assoc($res)) {
		$description = '';
		if($cat['description']) {
			$description =  ' - ' . $cat['description'];
		}
		$categorySelect->addOption($cat['id'], $cat['name'] . $description);
	}

	$categoryFormItem = workspace_FormField(
		workspace_translate('Delegation category:'),
		$categorySelect->setName('category'),
		workspace_translate('This category will be the default one selected on the creation form.')
	);
	$wysiwygFormItem = workspace_FormField(
		workspace_translate('Text editor:'),
		$W->Select()
			->addOption('plain', workspace_translate('Plain text editor'))
			->addOption('rich', workspace_translate('Rich text editor'))
			->addOption('wysiwyg', workspace_translate('Default ovidentia wysiwyg editor'))
			->setName('editor')
	);
	$automaticConnexionFormItem = workspace_FormField(
		workspace_translate('Activate automatic connection in workspaces'),
		$W->CheckBox()
			->setName('automatic_connexion'),
		workspace_translate('Functioning: after authentication on Ovidentia, the users only part of groups of workspaces will be redirect on workspaces')
	);
	$disableEmailFormItem = workspace_FormField(
		workspace_translate('Disable email addresses'),
		$W->CheckBox()
			->setName('disable_email')
	);

	$showSignoffButtonItem = workspace_FormField(
	    workspace_translate("The 'leave workspace' button should be replaced by a 'sign off' button"),
	    $W->CheckBox()
	       ->setName('showSignoffButton'),
	    workspace_translate('When clicking this button, users will be disconnected from the site')
	);


	$dateFormatFormItem = workspace_FormField(
		workspace_translate('Date format:'),
		$W->LineEdit()
			->setSize(20)
			->setName('date_format')
			->setTitle(workspace_translate('_DATE_FORMAT_HELP_'))
	);
	$timeFormatFormItem = workspace_FormField(
		workspace_translate('Time format:'),
		$W->LineEdit()
			->setSize(10)
			->setName('time_format')
			->setTitle(workspace_translate('_DATE_FORMAT_HELP_'))
	);



	$frame
		->addItem($categoryFormItem)
		->addItem($wysiwygFormItem)
		->addItem($automaticConnexionFormItem)
		->addItem($showSignoffButtonItem)
		->addItem($disableEmailFormItem)
		->addItem($dateFormatFormItem)
		->addItem($timeFormatFormItem);

	$frame->addButton(
		$W->SubmitButton('save')
			->validate(true)
			->setAction(workspace_Controller()->Admin()->saveGlobalConfiguration())
			->setLabel(workspace_translate('Save configuration'))
	);
	$frame->addButton(
		$W->SubmitButton('cancel')
			->setAction(workspace_Controller()->Admin()->Cancel())
		  	->setLabel(workspace_translate('Cancel'))
	);

	$form->setLayout($W->VBoxLayout())->addItem($frame);

	$form->setHiddenValue('tg', 'addon/workspace/main');

	return $form;
}




/**
 * @return Widget_Form
 */
function workspace_WorkspaceConfigurationEditor($creationMode = false)
{
	$W = bab_Widgets();

	$App = workspace_App();

	$form = $W->Form();

	$generalFrame = $W->Frame(
		'workspace_general_editor',
		$W->VBoxLayout()->setVerticalSpacing(2, 'em')
	);
	$generalFrame->setName('workspace');


	$titleFormItem = workspace_FormField(
		workspace_translate('Workspace name:'),
		$W->RegExpLineEdit()
            ->setRegExp('^[^\\/:\*\!\?\"\<\>\|]+$')
            ->setSubmitMessage(workspace_translate('The workspace name must not contain the following characters  \ / : * ! ? " < > |'))
			->setName('name')
			->setSize(80)
			->setMandatory(true, workspace_translate("The workspace name must not be empty")),
	    workspace_translate('The workspace name must not contain the following characters  \ / : * ! ? " < > |')
	);
	$disabledFormItem = workspace_FormField(
		workspace_translate('This workspace is archived'),
		$W->CheckBox()
			->setName('disabled')
	);

	global $babDB;
	$res = $babDB->db_query('SELECT * FROM '.BAB_DG_CATEGORIES_TBL.' ORDER BY name, description');

	$categorySelect = $W->Select();
	$categorySelect->addOption(0, '');
	while ($cat = $babDB->db_fetch_assoc($res)) {
		$description = '';
		if($cat['description']) {
			$description =  ' - ' . $cat['description'];
		}
		$categorySelect->addOption($cat['id'], $cat['name'] . $description);
	}

	$categorySelect->setValue(workspace_getWorkspacesDelegationCategory());

	$categoryFormItem = workspace_FormField(
		workspace_translate('Category:'),
		$categorySelect->setName('category')
	);


	$versioningFormItem = workspace_FormField(
		workspace_translate('Activate file versioning'),
		$W->CheckBox()
			->setName('versioning')
	);

	if ($creationMode) {
		$adminFormItem = workspace_FormField(
			workspace_translate('Administrator:'),
			$W->UserPicker()->setName('administrator'),
			workspace_translate("If you do not select a user, you will become the new workspace administrator")
		);
		$groupFormItem = workspace_FormField(
			workspace_translate('Group:'),
			$W->GroupPicker()->setName('group')->setValue(''),
			workspace_translate("If you do not select a group, a new one with the same name as the workspace will be created")
		);
		$notifyFormItem = null;
	} else {
// 		$adminFormItem = workspace_FormField(
// 			workspace_translate('Administrator:'),
// 			$W->UserPicker()
//                 ->setDisplayMode()
//                 ->setName('administrator')
// 		);
	    $adminFormItem = null;
		$groupFormItem = workspace_FormField(
			workspace_translate('Group:'),
			$W->GroupPicker()
                ->setDisplayMode()
                ->setName('group')->setValue('')
		);
		$notifyFormItem = workspace_FormField(
			workspace_translate('Notify workspace members when a file is uploaded'),
			$W->CheckBox()
				->setName('notify')
		);
	}

	$notifyForumFormItem = workspace_FormField(
		workspace_translate('Notify workspace members when a reply is post on a topic forum'),
		$W->CheckBox()
			->setName('forum_notif')
	);
	$commentsFormItem = workspace_FormField(
		workspace_translate('Enable comments on articles of this workspace'),
		$W->CheckBox()
			->setName('comment')
	);
	$createUsersFormItem = workspace_FormField(
		workspace_translate('Allow workspace administrators to create new users'),
		$W->CheckBox()
			->setName('create_users')
	);
	$updateDirectoryFormItem = workspace_FormField(
		workspace_translate('Update workspace directory entry when global directory entry is updated'),
		$W->CheckBox()
			->setName('update_directory')
	);

	$notifyCalendarFormItem = workspace_FormField(
	    workspace_translate('Notify workspace members when a calendar event is modified'),
	    $W->CheckBox()
	    ->setName('notifyCalendar')
	    );


	/* List of skins */
	include_once $GLOBALS['babInstallPath'].'utilit/skinincl.php';
	$skins = array();
	$skinsTmp = bab_skin::getList(false);
	foreach ($skinsTmp as $skin) {
		/* List of styles of the skin */
		$stylestmp = $skin->getStyles();
		$tabcles = array_keys($stylestmp);
		$styles = array();
		for ($j=0;$j<=count($tabcles)-1;$j++) {
			$styles[] = $tabcles[$j];
		}
		$skins[] = array('name' => $skin->getName(), 'styles' => $styles);
	}

	/* Skin selector : see workspace.js of javascript code associated */
	$select = $W->Select();
	$select->setName('skin')->setId('workspace_configadmin_skinselector')->setValue('workspace');
	$select->addOption('workspace', 'workspace '.workspace_translate('(default theme)'));
	foreach ($skins as $skin) {
		if ($skin['name'] == 'workspace') {
			$select->addOption('workspace', 'workspace '.workspace_translate('(default theme)'));
		} else {
			$select->addOption($skin['name'], $skin['name']);
		}
	}
	$select->addOption('', workspace_translate('None - same as portal'));
	$skinSelection = workspace_FormField(
		workspace_translate('Graphic theme:'),
		$select
	);
	/* Styles selector */
	$selectCss = $W->Select();
	$selectCss->setName('skin_styles')->setId('workspace_configadmin_stylesselector')->setValue('blue.css');
	foreach ($skins as $skin) {
		foreach ($skin['styles'] as $style) {
			$selectCss->addOption($style, $style, $skin['name']);
		}
	}
	$selectCss->addOption('', '', '');
	$skinSelection2 = workspace_FormField(
		workspace_translate('CSS styles associated:'),
		$selectCss
	);


	$lblRightSection = $W->Label(workspace_translate('Number of elements to display in right section of the home page.'));

	$numBox = $W->VBoxItems(
		$lblRightSection,
		$W->HBoxItems(
			$W->HBoxItems(
				$lbl = $W->Label(workspace_translate('Articles'))->colon(),
				$W->Select()->addOption(1,1)->addOption(2,2)->addOption(3,3)->addOption(4,4)->addOption(5,5)->addOption(6,6)->addOption(7,7)->addOption(8,8)->addOption(9,9)->addOption(10,10)
					->setAssociatedLabel($lbl)->setName('num_article')
			)->setVerticalAlign('middle')->setHorizontalSpacing(1, 'em'),
			$W->HBoxItems(
				$lbl = $W->Label(workspace_translate('Posts'))->colon(),
				$W->Select()->addOption(1,1)->addOption(2,2)->addOption(3,3)->addOption(4,4)->addOption(5,5)->addOption(6,6)->addOption(7,7)->addOption(8,8)->addOption(9,9)->addOption(10,10)
					->setAssociatedLabel($lbl)->setName('num_forum')
			)->setVerticalAlign('middle')->setHorizontalSpacing(1, 'em'),
			$W->HBoxItems(
				$lbl = $W->Label(workspace_translate('Events'))->colon(),
				$W->Select()->addOption(1,1)->addOption(2,2)->addOption(3,3)->addOption(4,4)->addOption(5,5)->addOption(6,6)->addOption(7,7)->addOption(8,8)->addOption(9,9)->addOption(10,10)
					->setAssociatedLabel($lbl)->setName('num_agenda')
			)->setVerticalAlign('middle')->setHorizontalSpacing(1, 'em'),
			$W->HBoxItems(
				$lbl = $W->Label(workspace_translate('Files'))->colon(),
				$W->Select()->addOption(1,1)->addOption(2,2)->addOption(3,3)->addOption(4,4)->addOption(5,5)->addOption(6,6)->addOption(7,7)->addOption(8,8)->addOption(9,9)->addOption(10,10)
					->setAssociatedLabel($lbl)->setName('num_file')
			)->setVerticalAlign('middle')->setHorizontalSpacing(1, 'em')
		)->setHorizontalSpacing(3, 'em')
	)->setVerticalSpacing(0.3, 'em');


	$otherThemItem = workspace_FormField(
		workspace_translate('Display other topics of the same delegation in the right section of the home page'),
		$W->CheckBox()
			->setName('other_them')
	);



	$capacity_max = floor($GLOBALS['babMaxTotalSize']/1048576);
	if( $capacity_max > floor($GLOBALS['babMaxGroupSize']/1048576) ){
		$capacity_max = floor($GLOBALS['babMaxGroupSize']/1048576);
	}
	$capacityItem = $W->VBoxItems(
		$W->Label(workspace_translate('Maximum capacity'). ": " . "(" . workspace_translate("Maximum capacity") . " " . $capacity_max . " Mo)"),
		$W->HBoxItems(
			$W->LineEdit()
				->setSize(10)
				->setName('file_size')
				->setValue(0),
			$W->Label("Mo"),
			$W->Label("(" . workspace_translate("0 = unlimited") . ")")
		)->setHorizontalSpacing(0.2,'em')->setVerticalAlign('middle')
	)->setVerticalSpacing(0.2, 'em');

	$generalFrame
		->addItem(
			$W->VBoxItems(
				$titleFormItem,
				$disabledFormItem,
				$adminFormItem,
				$groupFormItem,
				$categoryFormItem
			)->setVerticalSpacing(1, 'em')
		)
		->addItem(
			$W->Section(
				workspace_translate('File manager'),
				$W->VBoxItems(
					$notifyFormItem,
					$versioningFormItem,
					$capacityItem
				)->setVerticalSpacing(1, 'em')
			)
		)
		->addItem(
			$W->Section(
				workspace_translate('Forum'),
				$W->VBoxItems(
					$notifyForumFormItem
				)->setVerticalSpacing(1, 'em')
			)
		)
		->addItem(
			$W->Section(
				workspace_translate('Articles'),
				$W->VBoxItems(
					$commentsFormItem
				)->setVerticalSpacing(1, 'em')
			)
		)
		->addItem(
			$W->Section(
				workspace_translate('Directory'),
				$W->VBoxItems(
					$updateDirectoryFormItem,
				    $createUsersFormItem
				)->setVerticalSpacing(1, 'em')
			)
		)
		->addItem(
			$W->Section(
				workspace_translate('Calendar'),
				$W->VBoxItems(
					$notifyCalendarFormItem
				)->setVerticalSpacing(1, 'em')
			)
		)
		->addItem(
			$W->Section(
				workspace_translate('Appearance'),
				$W->VBoxItems(
					$W->FlowItems(
						$skinSelection,
						$skinSelection2
					)->setVerticalAlign('top')
					->setHorizontalSpacing(2, 'em'),
					$numBox,
					$otherThemItem
				)->setVerticalSpacing(1, 'em')
			)
		);



	$frame = new workspace_BaseForm('workspace_editor');
	$frame->addClass('workspace-dialog');

	$frame->addItem($generalFrame);

	$frame->addButton(
		$W->SubmitButton('save')
			->validate(true)
			->setAction($App->Controller()->Admin()->confirmSave())
	        ->setLabel($App->translate('Save workspace'))
	);
	$frame->addButton(
		$W->SubmitButton('cancel')
			->setAction(workspace_Controller()->Admin()->Cancel())
			->setLabel(workspace_translate('Cancel'))
	);

	$form->setLayout($W->VBoxLayout())->addItem($frame);

	$form->setHiddenValue('tg', bab_rp('tg'));

	return $form;
}





/**
 * @return Widget_Frame
 */
function workspace_WorkspaceAdminList()
{
	require_once dirname(__FILE__) . '/workspaces.php';

	$I = bab_Functionality::get('Icons');
	$W = bab_Widgets();
	$T = bab_functionality::get('Thumbnailer');


	$toolbar = workspace_Toolbar('main-toolbar');

	$newIcon = $W->Icon(workspace_translate('New workspace'), Func_Icons::ACTIONS_USER_GROUP_NEW);
	$newButton = $W->Link($newIcon, workspace_Controller()->Admin()->add());

	$toolbar->addItem($newButton);


	$listView = new workspace_BaseList('workspace_list');
	$listView->addClass('workspace-list');

	$archivedListView = $W->Section(
		workspace_translate('Archived workspaces'),
		new workspace_BaseList('workspace_archived_list')
	);
	$archivedListView->addClass('workspace-list');


	$workspaces = workspace_getWorkspaceList(null, true);


	foreach ($workspaces as $workspace) {

		$isArchived = workspace_isWorkspaceDisabled($workspace['id']);
		$administratorIds = workspace_getWorkspaceAdministratorIds($workspace['id']);

		$adminIcons = $W->VBoxItems()->setVerticalSpacing(2, 'px');
		foreach ($administratorIds as $administratorId) {
			$dirEntry = bab_admGetDirEntry($administratorId, BAB_DIR_ENTRY_ID_USER);

			$givenname = $dirEntry['givenname']['value'];
			$sn = $dirEntry['sn']['value'];
			$adminIcon = $W->Icon($givenname . ' ' . $sn);

			$adminIcon->setStockImage(Func_Icons::APPS_USERS);

			if (isset($dirEntry['jpegphoto']['photo'])) {
				$photo = $dirEntry['jpegphoto']['photo'];

				if ($T && $photo instanceof bab_dirEntryPhoto) {
					$T->setSourceBinary($photo->getData(), $photo->lastUpdate());
					$T->setBorder(1, '#cccccc', 1);
					$imageUrl = $T->getThumbnail(16, 16);
					$adminIcon->setImageUrl($imageUrl);
				}
			}
			$adminIcons->addItem($adminIcon);
		}


		$editLink = $W->Link(workspace_translate('Edit workspace'), workspace_Controller()->Admin()->edit($workspace['id']));
		$editLink->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)->setSizePolicy(Widget_SizePolicy::MINIMUM);

		$deleteLink = $W->Link(workspace_translate('Delete workspace'), workspace_Controller()->Admin()->confirmDelete($workspace['id']));
		$deleteLink->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE)->setSizePolicy(Widget_SizePolicy::MINIMUM);

		$buttonBox = $W->FlowItems(
		    $editLink,
		    $deleteLink
		)->addClass(Func_Icons::ICON_LEFT_16)->setHorizontalSpacing(0.5, 'em');


		$row = $W->Frame()->addItem(
			$W->FlowItems(
				$W->VBoxItems(
					$W->Label($workspace['name'])->addClass('title'),
					$buttonBox
				)->setVerticalSpacing(1, 'em')->setSizePolicy('widget-80pc'),
				$adminIcons->addClass(Func_Icons::ICON_LEFT_24)
			     ->setSizePolicy('widget-20pc')
			)->setHorizontalSpacing(1, 'em')
		    ->setVerticalAlign('top')
		);

		if ($isArchived) {
			$archivedListView->addItem($row);
		} else {
			$listView->addItem($row);
		}

	}



	$splitview = $W->Frame(
		'workspace_workspaces_splitview',
		$W->VBoxItems(
			$listView,
			$archivedListView->setFoldable(true, true)
		)->addClass('expand')
	);

	$explorer = $W->Frame('directory_explorer')
		->setLayout($W->VBoxLayout())
		->addItem($toolbar)
		->addItem($splitview);

	return $explorer;
}



















/**
 * @param	int		$workspaceId
 * @return Widget_Frame
 */
function workspace_confirmDelete($workspaceId)
{
	require_once dirname(__FILE__) . '/workspaces.php';

	$I = bab_Functionality::get('Icons');
	$W = bab_Widgets();

	list($delegation) = bab_getDelegationById($workspaceId);

	$dialog = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'))->addClass('workspace-dialog');
	$dialog->addItem($W->Title($delegation['name']));
	$dialog->addItem($W->Label(workspace_translate("Are you sure you want to remove this workspace?")));



	$buttonBox = $W->HBoxItems()->addClass('icon-left-16')->addClass('icon-16x16')->addClass('icon-left')->setHorizontalSpacing(1, 'em');
	$dialog->addItem($buttonBox);

	$cancelIcon = $W->Icon(workspace_translate('Cancel'), Func_Icons::ACTIONS_DIALOG_CANCEL);
	$cancelLink = $W->Link($cancelIcon, workspace_Controller()->Admin()->cancel());
	$cancelLink->addClass('widget-actionbutton');

	$editIcon = $W->Icon(workspace_translate('Yes, move all objects out of the workspace'), Func_Icons::ACTIONS_DIALOG_OK);
	$moveLink = $W->Link($editIcon, workspace_Controller()->Admin()->delete($workspaceId, 1));
	$moveLink->addClass('widget-actionbutton');

	$deleteIcon = $W->Icon(workspace_translate('Yes, delete all objects'), Func_Icons::ACTIONS_DIALOG_OK);
	$deleteLink = $W->Link($deleteIcon, workspace_Controller()->Admin()->delete($workspaceId, 0));
	$deleteLink->addClass('widget-actionbutton');

	$buttonBox->addItem($cancelLink);
	$buttonBox->addItem($deleteLink);
	$buttonBox->addItem($moveLink);

	return $dialog;

}
