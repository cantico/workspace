<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/portlets/forum.class.php';
require_once dirname(__FILE__).'/portlets/files.class.php';
require_once dirname(__FILE__).'/portlets/calendars.class.php';
require_once dirname(__FILE__).'/portlets/articles.class.php';
require_once dirname(__FILE__).'/portlets/workspacetitle.class.php';


bab_functionality::includeOriginal('PortletBackend');

class Func_PortletBackend_Workspace extends Func_PortletBackend 
{
    public function getDescription()
    {
        return workspace_translate('Workspace Portlets');
    }
    
    
    public function select($category = null)
    {
        return array(
            'forum'             => $this->portlet_Forum(),
            'files'             => $this->portlet_Files(),
            'calendars'         => $this->portlet_Calendars(),
            'articles'          => $this->portlet_Articles(),
            'workspaceTitle'    => $this->portlet_WorkspaceTitle()
        );
    }

    public function portlet_Forum()
    {
        return new workspace_PortletDefinition_Forum();
    }       
    
    public function portlet_Files()
    {
        return new workspace_PortletDefinition_Files();
    }     
    
    public function portlet_Calendars()
    {
        return new workspace_PortletDefinition_Calendars();
    }    
    
    public function portlet_Articles()
    {
        return new workspace_PortletDefinition_Articles();
    }   
    
    public function portlet_WorkspaceTitle()
    {
        return new workspace_PortletDefinition_WorkspaceTitle();
    }  
    
    
    /**
     * (non-PHPdoc)
     * @see Func_PortletBackend::getConfigurationActions()
     */
    public function getConfigurationActions()
    {
        return array();
    }
}