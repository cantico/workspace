<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/controller.class.php';
require_once dirname(__FILE__) . '/page.class.php';
require_once dirname(__FILE__) . '/workspaces.php';
require_once dirname(__FILE__) . '/directories.php';

class StatusPageTemplate extends bab_Template
{
	var $statusList;
	var $t_mail_sent;
	var $t_mail_error;
	var $t_server_message;

	function StatusPageTemplate($statusList)
	{
		$this->statusList = $statusList;

		$this->t_mail_sent = bab_translate("Mail sent");
		$this->t_mail_error = bab_translate("Mail error");
		$this->t_server_message = bab_translate("Server response: ");
	}


	function nextStatus()
	{
		if (list(,$this->status) = each($this->statusList)) {
			return true;
		}
		reset($this->statusList);
		return false;
	}
}



/**
 * This controller manages actions that can be performed on directories.
 */
class workspace_CtrlDirectories extends workspace_Controller
{
	/**
	 * Displays the directories entries.
	 *
	 * @param string $filter    'all', 'members', 'reader', 'writer' or administrator'.
	 * @param string $sort      Currently unused (optional).
	 * @param int    $workspace The workspace id (optional).
	 *
	 * @return workspace_Action
	 */
	public function displayEntries($filter = 'all', $sort = null, $workspace = null)
	{
		require_once dirname(__FILE__) . '/directories.ui.php';

		workspace_BreadCrumbs::setCurrentPosition($this->proxy()->displayEntries(), workspace_translate('Directory entries'));

		if (isset($workspace)) {
			workspace_setCurrentWorkspace($workspace);
		}
		$workspaceId = workspace_getCurrentWorkspace();

		$directoryId = workspace_getWorkspaceDirectoryId($workspaceId);
		$groupId = workspace_getWorkspaceGroupId($workspaceId);

		$page = workspace_Page()
				->addItemMenu('directory_entries', workspace_translate('Directory entries'), '')
				->setCurrentItemMenu('directory_entries');

		if (!workspace_canBrowseDirectory($directoryId)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You must be logged in to view this directory.'));
			$GLOBALS['babBody']->addError(workspace_translate('Access denied'));
		} else {
			$directoryListView = workspace_DirectoryListView($directoryId, $groupId, $filter);
			$page->addItem($directoryListView);
		}

		return $page;
	}





	/**
	 * Displays a page containing full information about the specified
	 * contact entry.
	 *
	 * @param int $entry The id of the contact entry to display.
	 *
	 * @return Widget_Action
	 */
	public function displayEntry($entry)
	{
		require_once dirname(__FILE__) . '/directories.ui.php';

		$page = workspace_Page();

		$workspaceId = workspace_getCurrentWorkspace();
		$directoryId = workspace_getWorkspaceDirectoryId(workspaceId);

		$dirEntry = workspace_getDirectoryEntry($entry, $directoryId);
		if (!isset($dirEntry)) {
		    $dirEntry = workspace_getDirectoryEntry($entry, 0);
		}

		$entryFrame = workspace_createFullContactEntry($dirEntry);
		$entryFrame->addClass('workspace-dialog')->addClass('workspace-detailed-directory-entry');
		$page->setTitle(workspace_translate('Contact detail'));

		$page->addItemMenu('directory_entries', workspace_translate('Directory entries'), '')
			 ->setCurrentItemMenu('directory_entries')
			 ->addItem($entryFrame);

		return $page;
	}




	/**
	 * Displays a form to select a user to add to the workspace group.
	 *
	 * @return Widget_Action
	 */
	public function selectMember($entry = null, $member = null)
	{
		require_once dirname(__FILE__) . '/directories.ui.php';

		$page = workspace_Page();



		if (is_array($entry)) {
			$selector = workspace_memberSelector('member_editor_tab');
			$editor = widget_Item::getById('member_editor');
			$editor->setValues($entry, array('entry'));
			$editor->setValues($member, array('member'));

		} else {
			$selector = workspace_memberSelector('member_selector');
		}
		$page->setTitle(workspace_translate('Add a member to the workspace'));

		$page->addItemMenu('directory_entries', workspace_translate('Directory entries'), '')
			 ->setCurrentItemMenu('directory_entries')
			 ->addItem($selector);

		return $page;
	}




	/**
	 * @param array	$members		contains the user ids to add to the workspace.
	 *
	 * @return Widget_Action
	 */
	public function addMembers($members = null, $notifyUser = false)
	{
		if (!workspace_userIsWorkspaceAdministrator()) {
			throw new Exception('Access denied');
		}

		$workspaceId = workspace_getCurrentWorkspace();
		$groupId = workspace_getWorkspaceGroupId();

		foreach ($members as $member) {
			// By default we add new members to the default workspace group (readers)
			try {
				bab_addUserToGroup($member['id'], $groupId);
				if ($notifyUser == '1') {
				    workspace_notifyNewMember($workspaceId, $member['id']);
				}
			} catch (Exception $e) {
				$GLOBALS['babBody']->addError($e->getMessage());
			}
		}
		workspace_redirect(workspace_BreadCrumbs::last());
	}



    /**
     * @return Widget_Action
     */
    public function createMember($member = null, $entry = null)
{
        global $babBody;
        if (!workspace_canCreateUsers()) {
            throw new Exception('Access denied');
        }

        $error = null;
        $userId = workspace_createUser($member, $entry, $error);

        if (!$userId) {
            $babBody->addError($error);
            return $this->selectMember($entry, $member);
        }

        $W = bab_Widgets();
        $photoData = '';
        $files = $W->ImagePicker()->getTemporaryFiles('photo');
        if (isset($files)) {
            foreach ($files as $filePickerItem) {
                /* @var $filePickerItem Widget_FilePickerItem */
                $photoData = file_get_contents($filePickerItem->getFilePath()->toString());
                break;
            }
        }

        $workspaceId =  workspace_getCurrentWorkspace();
        $directoryId = workspace_getWorkspaceDirectoryId($workspaceId);

        $entry['id'] = workspace_getDirEntryFromUserId($directoryId, $userId);
        workspace_updateDbContact($entry, $photoData);

        workspace_redirect(workspace_BreadCrumbs::last());
    }





	/**
	 * Removes the specified user from workspace group.
	 *
	 * @param int	$user		The user id.
	 */
	public function removeMember($user)
	{
		if (!workspace_userIsWorkspaceAdministrator()) {
			throw new Exception('Access denied');
		}
		require_once dirname(__FILE__) . '/admin.php';
		$groupId = workspace_getWorkspaceGroupId();
		$administratorsGroupId = workspace_getWorkspaceAdministratorsGroupId();
		$writersGroupId = workspace_getWorkspaceWritersGroupId();

		bab_removeUserFromGroup($user, $groupId);
		bab_removeUserFromGroup($user, $administratorsGroupId);
		bab_removeUserFromGroup($user, $writersGroupId);

		$workspaceId = workspace_getCurrentWorkspace();
		if (workspace_userIsWorkspaceAdministrator($user, $workspaceId)) {
			workspace_removeDelegationAdmin($workspaceId, $user);
		}

		// We also delete the corresponding directory entry in the workspace directory.
		$directoryId = workspace_getWorkspaceDirectoryId();
		$dirEntryId = workspace_getDirEntryFromUserId($directoryId, $user);
		workspace_deleteDbContact($dirEntryId);

		workspace_redirect(workspace_BreadCrumbs::last());
	}





	/**
	 * Updates the workspace directory entry with information from ovidentia directory.
	 *
	 * @param int $userId
	 */
	public function updateFromPortal($userId = null)
	{
		$directoryId = workspace_getWorkspaceDirectoryId();
		workspace_updateUserDirectoryEntryFromPortal($userId, $directoryId);

		workspace_redirect($this->proxy()->displayEntries());
	}





    /**
     * Displays a form to edit an existing or a new directory entry.
     *
     * @param	array|id	$entry		Entry id or array with keys : 'id', 'givenname', 'sn', 'organizationname'...
     * @param	int			$readonly	0 : editable, 1 : readonly
     * @param	int			$userId
     * @return Widget_Action
     */
    public function edit($entry = null, $readonly = 0, $userId = null)
    {
        $W = bab_Widgets();
        if(!workspace_canUpdateEntriesFromMember($userId) && $userId != null){
            global $babBody;
            $babBody->addError(workspace_translate('Access denied'));
            workspace_redirect(workspace_BreadCrumbs::last());
        }
        require_once dirname(__FILE__) . '/directories.ui.php';

        $page = workspace_Page();

        if (!is_null($entry)  && !is_array($entry)) {
            // $entry was the entry id.
            $entry = workspace_getDirectoryEntry($entry);
        }

        if (is_null($entry) && isset($userId)) {
            // no entry specified but a $userId is provided => we fetch the user's directory entry.
            $entry = workspace_getDirectoryEntryByUserId($userId);
        }

        $editor = workspace_DirectoryEntryEditor($userId);

        if (isset($userId)) {
            $editor->setHiddenValue('entry[id_user]', $userId);

            $profile = workspace_getUserProfile($userId);

            if (isset($profile)) {
                $editor->setValue('entry/profile', $profile);
            }
        }

        if (is_array($entry) && (isset($entry['id']) || isset($entry['userid']))) {
            $page->setTitle(workspace_translate('Edit directory entry'));
            $editor->setValues($entry, array('entry'));
            if (isset($entry['id'])) {
                $editor->setHiddenValue('entry[id]', $entry['id']);
            }

            /* @var $photoImagePicker Widget_ImagePicker */
            $photoImagePicker = Widget_Item::getById('workspace-photo');

            if (isset($entry['photo']) && $entry['photo'] instanceof bab_dirEntryPhoto) {
                $photoImagePicker->importFileData($entry['photo']->getData(), 'photo.jpg');
            } else {
                $photoImagePicker->importFileData('', 'photo.jpg');
                try {
                    $photoImagePicker->getFolder()->deleteDir();
                } catch (Exception $e) { }
                $photoImagePicker->getFolder()->createDir();
            }

        } else {
            $page->setTitle(workspace_translate('Create a new directory entry'));
        }

        if ($readonly) {
            $editor->setDisplayMode();
        }
        $page->addItemMenu('directory_entries', workspace_translate('Directory entries'), '')
            ->setCurrentItemMenu('directory_entries')
            ->addItem($editor);

        return $page;
    }





	/**
	 * Displays a form to edit an email addressed to workspace members correponding to the filter.
	 *
	 * @param string $filter 'all', 'members', 'reader', 'writer' or administrator'.
	 */
	public function editEmailForSelectedMembers($filter)
	{
		$emailAddresses = workspace_getMembersEmailAddresses($filter);


		$recipients = implode(',', $emailAddresses);

		return $this->editEmail($recipients);
	}




	/**
	 * Displays a form to edit an email addressed to the specified recipients.
	 *
	 * @param string|array $recipients An array or comma-separated string of email addresses.
	 *
	 * @return Widget_Action
	 */
	public function editEmail($recipients)
	{
		require_once dirname(__FILE__) . '/directories.ui.php';

		$page = workspace_Page();

		if (!is_array($recipients)) {
			$recipients = explode(',', $recipients);
		}

		$editor = workspace_emailEditor($recipients);
		$page->setTitle(workspace_translate("Send an email"));

		$page->addItemMenu('directory_entries', workspace_translate("Directory entries"), '')
			 ->setCurrentItemMenu('directory_entries')
			 ->addItem($editor);

		return $page;
	}





	/**
	 * Returns a status page showing the status list.
	 *
	 * @param array $statusList
	 * @return string		The html content of the page.
	 */
	private function emailStatusPage($statusList)
	{
		$statusPageTemplate = new StatusPageTemplate($statusList);
		return bab_printTemplate($statusPageTemplate, 'mail.html', 'statusPage');
	}





	/**
	 * Sends an email.
	 *
	 * @param array $email An array with following keys: 'subject', 'body', 'recipients'.
	 */
	public function sendEmail($email = null)
	{
		require_once dirname(__FILE__) . '/workspacewidgets.php';

		$W = bab_Widgets();

		$mailToRecipients = explode(',', $email['recipients']);
		$mailBccRecipients = array();

		$mail = new workspace_MailDispatcher();
		$mail->setData($email['subject'], $email['body']);
		$mail->mailDestArray($mailToRecipients, 'mailTo');
		$mail->mailDestArray($mailBccRecipients, 'mailBcc');
		$mailStatus = $mail->send();

		$page = workspace_Page();

		if ($mailStatus) {
			$page->setTitle(workspace_translate("Email message sent successfully"));
		} else {
			$page->setTitle(workspace_translate("Problems to send email message"));
		}


		$statusPage = $this->emailStatusPage($mail->status);

		$form = $W->Form(null, $W->VBoxLayout())->addClass('workspace-dialog');

		$page->addItemMenu('directory_entries', workspace_translate("Directory entries"), '')
			 ->setCurrentItemMenu('directory_entries')
			 ->addItem($form
				 ->addItem($W->Html($statusPage))
				 ->addItem($W->SubmitButton('ok')
								->setLabel(workspace_translate("OK"))
								->setAction(workspace_Controller()->Directories()->cancel())
					)
				)
			;
		workspace_setSelfPageHiddenFields($form);

		return $page;
	}





    /**
     * Saves entry information in the workspace directory.
     *
     * @param array $entry
     */
    public function save($entry = null)
    {
        global $babBody;
        global $babDB;
        $W = bab_Widgets();

        $photoData = '';
        $files = $W->ImagePicker()->getTemporaryFiles('photo');
        if (isset($files)) {
            foreach ($files as $filePickerItem) {
                /* @var $filePickerItem Widget_FilePickerItem */
                $photoData = file_get_contents($filePickerItem->getFilePath()->toString());
                break;
            }
        }

        $workspaceId = workspace_getCurrentWorkspace();
        $directoryId = workspace_getWorkspaceDirectoryId($workspaceId);

        $userId = isset($entry['id_user']) ? $entry['id_user'] : 0;

        if (isset($entry['id'])) {
            $res = $babDB->db_query('SELECT id_user
                FROM ' . BAB_DBDIR_ENTRIES_TBL . '
                WHERE id='.$babDB->quote($entry['id'])
            );
            while ($arr = $babDB->db_fetch_array($res)) {
                $userId = $arr['id_user'];
            }

            if (!workspace_canUpdateEntriesFromMember($userId) && $userId != 0) {
                $babBody->addError(workspace_translate('Access denied'));
                workspace_redirect(workspace_BreadCrumbs::last());
            }

            if (workspace_canUpdateEntriesFromDirectory($directoryId)) {
                try {
                    workspace_updateDbContact($entry, $photoData);
                } catch (Exception $e) {
                    $babBody->addError($e->getMessage());
                }
            }
        } else {
            if (workspace_canAddEntriesToDirectory($directoryId)) {
                workspace_addDbContact($directoryId, $entry, $photoData, $userId);
            }
        }
        if ($userId && isset($entry['profile'])) {
            require_once dirname(__FILE__) . '/admin.php';
            workspace_setUserProfile($userId, $entry['profile'], $workspaceId);
        }

        workspace_redirect($this->proxy()->displayEntries());
    }




	/**
	 * @param string $address
	 */
	public function locate($address)
	{
		// Google map url.
		$mapUrl = 'http://maps.google.fr/maps?f=q&source=s_q&hl=fr&geocode=&ie=' .bab_charset::getIso() .'&z=16&iwloc=addr&q=' . urlencode($address);

		// Openstreetmap url.
//		$mapUrl = 'http://gazetteer.openstreetmap.org/namefinder/?find=' . urlencode($address);
		workspace_redirect($mapUrl);
		die;
	}




	/**
	 * Promotes the specified user to administrator status.
	 *
	 * @param int     $user   The user id.
	 * @param string $profile 'reader', 'writer' or 'administrator'.
	 */
	public function setProfile($user, $profile)
	{
		if (!workspace_userIsWorkspaceAdministrator()) {
			throw new Exception('Access denied');
		}
		require_once dirname(__FILE__) . '/admin.php';
		$workspaceId = workspace_getCurrentWorkspace();
		workspace_setUserProfile($user, $profile, $workspaceId);
		workspace_redirect(workspace_BreadCrumbs::last());
	}





	/**
	 * Deletes the specified directory entry.
	 *
	 * @return Widget_Action
	 */
	public function delete($entry)
	{
		if (!workspace_canDeleteEntriesFromDirectory()) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You must be logged in to delete this entry.'));
			$GLOBALS['babBody']->addError(workspace_translate('Access denied'));
			workspace_redirect(workspace_BreadCrumbs::last());
			return;
		}

		workspace_deleteDbContact($entry);

		workspace_redirect(workspace_BreadCrumbs::last());
	}




	/**
	 * Does nothing and return to the previous page.
	 *
	 * @return Widget_Action
	 */
	public function cancel()
	{
		workspace_redirect(workspace_BreadCrumbs::last());
	}




	/**
	 * Displays help on this page.
	 *
	 * @return Widget_Action
	 */
	public function help()
	{

	}
}
