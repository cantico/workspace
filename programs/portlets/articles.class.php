<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/../functions.php';



class workspace_PortletDefinition_Articles implements portlet_PortletDefinitionInterface
{

    /**
     * @var bab_addonInfos $addon
     */
    protected $addon;


    public function __construct()
    {
        $this->addon = bab_getAddonInfosInstance('workspace');
    }


    public function getId()
    {
        return 'articles';
    }

    public function getName()
    {
        return workspace_translate('Workspace articles');
    }


    public function getDescription()
    {
        return workspace_translate('Display the last articles of the current workspace.');
    }


    public function getPortlet()
    {
        return new workspace_Portlet_Articles();
    }



    public function getPreferenceFields()
    {
        $preferenceFields = array();
        
        $workspaces = workspace_getWorkspaceList();
        $options = array();
        foreach ($workspaces as $workspace){
            $options[] = array(
                'value' => $workspace['id'],
                'label' => $workspace['name']
            );
        }
        
        $preferenceFields[] = array(
            'label' => workspace_translate('Workspaces'),
            'name' => 'workspaceIds',
            'type' => 'multiselect',
            'options' => $options,
            'description' => workspace_translate('If none specified, then the current workspace will be used')
        );
        
        $preferenceFields[] = array(
            'label' => workspace_translate('Number of articles to display'),
            'name' => 'articleNumber',
            'type' => 'int',
            'description' => sprintf(workspace_translate('Default is %d'), 4)
        );
        
        $preferenceFields[] = array(
            'label' => workspace_translate('Number of characters to display'),
            'name' => 'strlen',
            'type' => 'int',
            'description' => sprintf(workspace_translate('Default is %d'), 100)
        );

        return $preferenceFields;
    }


    /**
     * Returns the widget rich icon URL.
     * 128x128 ?
     *
     * @return string
     */
    public function getRichIcon()
    {
        return $this->addon->getIconPath();
    }


    /**
     * Returns the widget icon URL.
     * 16x16 ?
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->addon->getIconPath();
    }

    /**
     * Get thumbnail URL
     * max 120x60
     */
    public function getThumbnail()
    {
        return $this->addon->getIconPath();
    }

    public function getConfigurationActions()
    {
        return array();
    }
}





class workspace_Portlet_Articles extends Widget_Item implements portlet_PortletInterface
{
    private $portletId = null;
    private $currentWorkspace = null;
    private $workspaceIds = array();
    private $articleNumber = 4;
    private $strlen = 100;

    /**
     * Instanciates the widget factory.
     *
     * @return Func_Widgets
     */
    function Widgets()
    {
        $jquery = bab_functionality::get('jquery');
        $jquery->includeCore();
        $jquery->includeUi();
        $GLOBALS['babBody']->addStyleSheet($jquery->getStyleSheetUrl());
        if ($icons = @bab_functionality::get('Icons')) {
            $icons->includeCss();
        }

        $W = bab_Functionality::get('Widgets');
        $W->includePhpClass('Widget_Icon');
        return $W;
    }


    /**
     */
    public function __construct()
    {
        $W = $this->Widgets();
        $this->currentWorkspace = bab_Registry::get('/workspace/user/' . $GLOBALS['BAB_SESS_USERID'].'/currentWorkspace');
        $this->item = $W->VBoxItems();
    }


    public function getName()
    {
        return get_class($this);
    }


    public function getPortletDefinition()
    {
        return new workspace_PortletDefinition_Articles();
    }


    /**
     * receive current user configuration from portlet API
     */
    public function setPreferences(array $configuration)
    {
        foreach ($configuration as $name => $value) {
            if($name == 'articleNumber'){
                $value = $value <= 0 ? 4 : $value;
            }
            if($name == 'strlen'){
                $value = $value <= 0 ? 100 : $value;
            }
            $this->setPreference($name, $value);
        }
    }



    public function setPreference($name, $value)
    {
        $this->$name = $value;
    }


    public function setPortletId($id)
    {
        $this->portletId = $id;
    }





    /**
     * @param Widget_Canvas	$canvas
     * @ignore
     */
    public function display(Widget_Canvas $canvas)
    {
        $W = $this->Widgets();
        if(empty($this->workspaceIds)){
            if(empty($this->currentWorkspace)){
                $label = $W->Label(workspace_translate('This page is not part of a workspace, and there is no workspace selected in the portlet configuration.'))->addClass('noWorkspaceSelected');
                $display = $label->display($canvas);
                return $display;
            }
            $this->workspaceIds = array($this->currentWorkspace);
        }
        
        $ovml = '';
        $dateFormat = workspace_getWorkspacesDateFormat();
        $timeFormat = $dateFormat.' '.workspace_getWorkspacesTimeFormat();
        foreach ($this->workspaceIds as $workspaceId){
            $workspaceTopicId = bab_Registry::get('/workspace/workspaces/'.$workspaceId.'/topicId');
            $ovml .= '<OCRecentArticles last="'.$this->articleNumber.'" topicid="'.$workspaceTopicId.'" >
                    <!--<OVArticleDate date="'.$dateFormat.'" saveas="tmpdate"> -->
                    <OCIfNotEqual expr1="<OVRecentArticles_date>" expr2="<OVtmpdate>" c2>
                    <dt class="widget-small"><OVtmpdate saveas="RecentArticles_date"></dt>
                    </OCIfNotEqual c2>
                    <dd class="widget-list-element">
                        <h4><a href="index.php?tg=addon/workspace/main&amp;idx=articles.show&amp;article=<OVArticleId>"><OVArticleTitle></a> <em>|</em> <span class="widget-small"><OVArticleDate date="'.$timeFormat.'"></span></h4>
                        <p><OVArticleHead striptags="1" strlen="'.$this->strlen.',..."></p>
                    </dd>
                </OCRecentArticles>';
        }

        $layout = $W->Html(bab_printOvml($ovml, array()));
        $display = $layout->display($canvas);

        return $display;
    }
}



