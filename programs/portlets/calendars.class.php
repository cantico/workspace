<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/../functions.php';
require_once dirname(__FILE__) . '/../workspaces.php';



class workspace_PortletDefinition_Calendars implements portlet_PortletDefinitionInterface
{

    /**
     * @var bab_addonInfos $addon
     */
    protected $addon;


    public function __construct()
    {
        $this->addon = bab_getAddonInfosInstance('workspace');
    }


    public function getId()
    {
        return 'calendars';
    }

    public function getName()
    {
        return workspace_translate('Workspace calendars');
    }


    public function getDescription()
    {
        return workspace_translate('Display the calendars of the current workspace.');
    }


    public function getPortlet()
    {
        return new workspace_Portlet_Calendars();
    }



    /**
     * @return array
     */
    public function getPreferenceFields()
    {
        $preferenceFields = array();
        
        $workspaces = workspace_getWorkspaceList();
        $options = array();
        foreach ($workspaces as $workspace){
            $options[] = array(
                'value' => $workspace['id'],
                'label' => $workspace['name']
            );
        }
        
        $preferenceFields[] = array(
            'label' => workspace_translate('Workspaces'),
            'name' => 'workspaceIds',
            'type' => 'multiselect',
            'options' => $options,
            'description' => workspace_translate('If none specified, then the current workspace will be used')
        );

        return $preferenceFields;
    }


    /**
     * Returns the widget rich icon URL.
     * 128x128 ?
     *
     * @return string
     */
    public function getRichIcon()
    {
        return $this->addon->getIconPath();
    }


    /**
     * Returns the widget icon URL.
     * 16x16 ?
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->addon->getIconPath();
    }

    /**
     * Get thumbnail URL
     * max 120x60
     */
    public function getThumbnail()
    {
        return $this->addon->getIconPath();
    }

    public function getConfigurationActions()
    {
        return array();
    }
}





class workspace_Portlet_Calendars extends Widget_Item implements portlet_PortletInterface
{
    private $portletId = null;
    private $currentWorkspace = null;
    private $workspaceIds = array();

    /**
     * Instanciates the widget factory.
     *
     * @return Func_Widgets
     */
    function Widgets()
    {
        $jquery = bab_functionality::get('jquery');
        $jquery->includeCore();
        $jquery->includeUi();
        $GLOBALS['babBody']->addStyleSheet($jquery->getStyleSheetUrl());
        if ($icons = @bab_functionality::get('Icons')) {
            $icons->includeCss();
        }

        $W = bab_Functionality::get('Widgets');
        $W->includePhpClass('Widget_Icon');
        return $W;
    }


    /**
     */
    public function __construct()
    {
        $W = $this->Widgets();
        $this->currentWorkspace = bab_Registry::get('/workspace/user/' . $GLOBALS['BAB_SESS_USERID'].'/currentWorkspace');
        $this->item = $W->VBoxItems();
    }


    public function getName()
    {
        return get_class($this);
    }


    public function getPortletDefinition()
    {
        return new workspace_PortletDefinition_Calendars();
    }


    /**
     * receive current user configuration from portlet API
     */
    public function setPreferences(array $configuration)
    {
        foreach ($configuration as $name => $value) {
            $this->setPreference($name, $value);
        }
    }



    public function setPreference($name, $value)
    {
        $this->$name = $value;
    }


    public function setPortletId($id)
    {
        $this->portletId = $id;
    }





    /**
     * @param Widget_Canvas	$canvas
     * @ignore
     */
    public function display(Widget_Canvas $canvas)
    {
        $W = $this->Widgets();        
        if(empty($this->workspaceIds)){
            if(empty($this->currentWorkspace)){
                $label = $W->Label(workspace_translate('This page is not part of a workspace, and there is no workspace selected in the portlet configuration.'))->addClass('noWorkspaceSelected');
                $display = $label->display($canvas);
                return $display;
            }
            $this->workspaceIds = array($this->currentWorkspace);
        }
        
        $calendar = $this->getCalendar();
        
        $display = $calendar->display($canvas);
        return $display;
    }
    
    private function getCalendar()
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
        
        $W = bab_Widgets();
        
        $calendar = $W->FullCalendar('calendar-'.$this->portletId);
        $calendar->setFirstDayOfWeek(1)->setSizePolicy(Widget_SizePolicy::MAXIMUM);
        $calendar->setView(Widget_FullCalendar::VIEW_MONTH);
        
        $calendar->fetchPeriods(workspace_Controller()->Calendars()->events('timestamp', null, null, $this->workspaceIds));    ///////////////////
        
        return $calendar;
    }

}



