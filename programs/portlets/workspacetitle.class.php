<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/../functions.php';



class workspace_PortletDefinition_WorkspaceTitle implements portlet_PortletDefinitionInterface
{

    /**
     * @var bab_addonInfos $addon
     */
    protected $addon;


    public function __construct()
    {
        $this->addon = bab_getAddonInfosInstance('workspace');
    }


    public function getId()
    {
        return 'workspaceTitle';
    }

    public function getName()
    {
        return workspace_translate('Workspace title');
    }


    public function getDescription()
    {
        return workspace_translate('Display the current workspace title');
    }


    public function getPortlet()
    {
        return new workspace_Portlet_WorkspaceTitle();
    }



    /**
     * @return array
     */
    public function getPreferenceFields()
    {
        $App = workspace_App();
        $set = $App->WorkspaceSet();
        $preferenceFields = array();

        $userWorkspaces = $set->select($set->isReadable());
        $options = array(
            array(
                'value' => '',
                'label' => ''
            )
        );
        foreach ($userWorkspaces as $userWorkspace) {
            $options[] = array(
                'value' => $userWorkspace->id,
                'label' => $userWorkspace->name
            );
        }

        $preferenceFields[] = array(
            'label' => workspace_translate('Workspaces'),
            'name' => 'workspaceId',
            'type' => 'list',
            'options' => $options
        );

        return $preferenceFields;
    }


    /**
     * Returns the widget rich icon URL.
     * 128x128 ?
     *
     * @return string
     */
    public function getRichIcon()
    {
        return $this->addon->getIconPath();
    }


    /**
     * Returns the widget icon URL.
     * 16x16 ?
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->addon->getIconPath();
    }

    /**
     * Get thumbnail URL
     * max 120x60
     */
    public function getThumbnail()
    {
        return $this->addon->getIconPath();
    }

    public function getConfigurationActions()
    {
        return array();
    }
}





class workspace_Portlet_WorkspaceTitle extends Widget_Item implements portlet_PortletInterface
{
    private $portletId = null;
    private $workspaceId = null;

    /**
     * Instanciates the widget factory.
     *
     * @return Func_Widgets
     */
    function Widgets()
    {
        $jquery = bab_functionality::get('jquery');
        $jquery->includeCore();
        $jquery->includeUi();
        $GLOBALS['babBody']->addStyleSheet($jquery->getStyleSheetUrl());
        if ($icons = @bab_functionality::get('Icons')) {
            $icons->includeCss();
        }

        $W = bab_Functionality::get('Widgets');
        $W->includePhpClass('Widget_Icon');
        return $W;
    }


    /**
     */
    public function __construct()
    {
        $W = $this->Widgets();
        $this->item = $W->VBoxItems();

        $this->currentWorkspace = bab_Registry::get('/workspace/currentWorkspace');
    }


    public function getName()
    {
        return get_class($this);
    }


    public function getPortletDefinition()
    {
        return new workspace_PortletDefinition_WorkspaceTitle();
    }


    /**
     * receive current user configuration from portlet API
     */
    public function setPreferences(array $configuration)
    {
        foreach ($configuration as $name => $value) {
            $this->setPreference($name, $value);
        }
    }



    public function setPreference($name, $value)
    {
        $this->$name = $value;
    }


    public function setPortletId($id)
    {
        $this->portletId = $id;
    }





    /**
     *
     * @param Widget_Canvas $canvas
     * @ignore
     *
     */
    public function display(Widget_Canvas $canvas)
    {
        $W = $this->Widgets();
        if (empty($this->workspaceId)) {
            $this->workspaceId = $this->currentWorkspace;

//             $label = $W->Label(workspace_translate('There is no workspace selected in the portlet configuration.'))->addClass('noWorkspaceSelected');
//             $display = $label->display($canvas);
//             return $display;
        }

        $app = workspace_App();
        $set = $app->WorkspaceSet();
        $set->category();

        $workspace = $set->get($set->id->is($this->workspaceId));

        $workspaceInfos = $workspace->getInfos();
        $delegationInfos = bab_getDelegationById($workspace->delegation);

        $template = new bab_Template();

        $skinName = workspace_getWorkspaceSkinName($workspace->delegation);
        $skin = bab_getAddonInfosInstance($skinName);

        $ovml = $template->_loadTemplate($skin->getTemplate('workspaceTitle.ovml'), '');
        if (! $ovml) {
            $skin = bab_getAddonInfosInstance('workspace');
            $ovml = $template->_loadTemplate($skin->getTemplate('workspaceTitle.ovml'), '');
        }

        $variables =  array(
            'id'                => $workspace->delegation,
            'name'              => $workspace->name,
            'description'       => $workspace->description,
            'group'             => $workspace->group,
            'category'          => $workspace->category,
            'categoryName'      => $workspace->category->name,
            'skin'              => $workspace->skin,
            'skin_styles'       => $workspace->skinStyles,
            'num_article'       => $workspaceInfos['num_article'],
            'num_forum'         => $workspaceInfos['num_forum'],
            'num_agenda'        => $workspaceInfos['num_agenda'],
            'num_file'          => $workspaceInfos['num_file'],
            'other_them'        => $workspaceInfos['other_them'],
            'file_size'         => $workspaceInfos['file_size'],
            'update_directory'  => $workspaceInfos['update_directory'],
            'disabled'          => $workspaceInfos['disabled'],
            'versioning'        => $workspaceInfos['versioning'],
            'workspaceId'       => $workspace->id,
        );

        if (isset($delegationInfos[0])) {
            $variables['categoryColor'] = $delegationInfos[0]['color'];
        }

        $layout = $W->Html(bab_printOvml($ovml, $variables));
        $display = $layout->display($canvas);

        return $display;
    }
}



