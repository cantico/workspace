<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/workspaces.php';
require_once $GLOBALS['babInstallPath'].'utilit/artapi.php';



function workspace_getCategoryId()
{
	require_once $GLOBALS['babInstallPath'] . 'utilit/ovmlapi.php';

	$currentWorkspaceId = workspace_getCurrentWorkspace();
	
	$App = workspace_App();
	$set = $App->WorkspaceSet();
	$workspace = $set->request($set->id->is($currentWorkspaceId));

	$articles = array();
	$O = new bab_ovmlAPI();
	$args = array(
	    'delegationid' => $workspace->delegation
	);
	$ovmlArticleCategories = $O->getContainer('ArticleCategories', $args);

	if ($ovmlArticleCategories->getNext($ovmlArticleCategories)) {
		return $ovmlArticleCategories->CategoryId;
	}

	return null;
}

/**
 *
 * @return int		The current delegation topic id
 */
function workspace_getTopicId()
{
	require_once $GLOBALS['babInstallPath'] . 'utilit/ovmlapi.php';
	require_once $GLOBALS['babInstallPath'] . 'utilit/delegincl.php';

	$categoryId = workspace_getCategoryId();

	if (is_null($categoryId)) {
		return null;
	}

	$articles = array();
	$O = new bab_ovmlAPI();
	$args = array(
					'categoryid' => $categoryId,
					'delegationid' => bab_getCurrentUserDelegation()
	);
	$ovmlArticleTopics = $O->getContainer('ArticleTopics', $args);
	if ($ovmlArticleTopics->getNext($ovmlArticleTopics)) {
		return $ovmlArticleTopics->TopicId;
	}

	return null;
}


function workspace_getArticle($articleId)
{
	global $babDB;

	$articles = workspace_getArticles($articleId);

	if ($articles) {
		$article = $babDB->db_fetch_assoc($articles);

		return $article;
	}
	return null;
}



/**
 * Returns an array of topics id and name in the workspace delegation.
 *
 * @param int $workspaceId
 * @return array  array(topicId => topicName)
 */
function workspace_getAllWorkspaceTopics($workspaceId = null)
{
	global $babDB;

	if (!isset($workspaceId)) {
		$workspaceId = workspace_getCurrentWorkspace();
	}
	
	$App = workspace_App();
	$set = $App->WorkspaceSet();
	$workspace = $set->request($set->id->is($workspaceId));

	$sql = '
		SELECT topic.id, topic.category FROM ' . BAB_TOPICS_TBL . ' AS topic
		LEFT JOIN ' . BAB_TOPICS_CATEGORIES_TBL . ' AS category ON category.id = topic.id_cat
		WHERE category.id_dgowner IN (' . $workspace->delegation . ')';


	$topicIds = array();

	$topics = $babDB->db_query($sql);
	while ($topic = $babDB->db_fetch_assoc($topics)) {
		$topicIds[$topic['id']] = $topic['category'];
	}

	return $topicIds;
}


/**
* Returns an array of topics id and name in the workspace delegation where
* the user can submit articles.
*
* @param int $workspaceId
* @return array  array(topicId => topicName)
*/
function workspace_getSubmitableTopics($workspaceId = null)
{
	$topics = workspace_getAllWorkspaceTopics($workspaceId);

	$publishableTopics = array();
	foreach ($topics as $topicId => $topicName) {
		if (workspace_canCreateArticle($topicId)) {
			$publishableTopics[$topicId] = $topicName;
		}
	}

	return $publishableTopics;
}




function workspace_getArticlesIds($articles = null, $topics = null, $delegations = null, $archive = false, $minRating = null)
{
	global $babBody, $babDB;

	$joins = array();
	$where = array();

	if (isset($articles)) {
		$where[] = 'article.id IN (' . $babDB->quote($articles) . ')';
	}

	$topview = bab_getUserIdObjects(BAB_TOPICSVIEW_GROUPS_TBL);

	if (isset($topics)) {
		$topics = array_intersect(array_keys($topview), $topics);
	} else {

		$topics = array_keys($topview);
	}
	$where[] = 'article.id_topic IN (' . $babDB->quote($topics) . ')';

	$where[] = 'archive = ' . $babDB->quote($archive ?  'Y' : 'N');

	if (isset($delegations)) {
		$joins[] = BAB_TOPICS_TBL . ' topic ON topic.id = article.id_topic';
		$joins[] = BAB_TOPICS_CATEGORIES_TBL . ' category ON category.id = topic.id_cat ';
		$where[] = 'category.id_dgowner IN (' . $babDB->quote($delegations) . ')';
	}

	if (isset($minRating)) {
		$joins[] = BAB_COMMENTS_TBL . ' comment ON comment.id_article = article.id AND comment.article_rating > 0';
	}

	$where[] = '(article.date_publication = ' . $babDB->quote('0000-00-00 00:00:00') . ' OR article.date_publication <= NOW())';

	$sql = '
			SELECT
				article.id,
				article.restriction';
	if (isset($minRating)) {
		$sql .=	',
				AVG(comment.article_rating) AS average_rating,
				COUNT(comment.article_rating) AS nb_ratings
				';
	}

	$sql .= '
			FROM ' . BAB_ARTICLES_TBL . ' AS article';

	if (!empty($joins)) {
		$sql .= '
				LEFT JOIN ' . implode('
				LEFT JOIN ', $joins);
	}

	if (!empty($where)) {
		$sql .= '
			WHERE ' . implode('
				AND ', $where);
	}

	if (!is_numeric($minRating)) {
		 $ratingGroupBy = 'GROUP BY article.id';
	} else {
		 $ratingGroupBy = 'GROUP BY article.id HAVING average_rating >= ' . $babDB->quote($minRating);
	};

	$sql .= '
		' . $ratingGroupBy;

	require_once $GLOBALS['babInstallPath'] . 'utilit/userincl.php';
	$articles = $babDB->db_query($sql);

	$articleIds = array();
	while ($article = $babDB->db_fetch_assoc($articles)) {
		if ($article['restriction'] == '' || bab_articleAccessByRestriction($arr['restriction'])) {
			$articleIds[] = $article['id'];
		}
	}

	return $articleIds;
}








function workspace_getArticles($articles = null, $topics = null, $delegations = null, $archive = false, $minRating = null)
{
	global $babDB;
	
	if(isset($delegations)){
	    $App = workspace_App();
	    $set = $App->WorkspaceSet();
	    $workspace = $set->request($set->id->is($delegations));
	    
	    $delegations = $workspace->delegation;
	}

	$articleIds = workspace_getArticlesIds($articles, $topics, $delegations, $archive, $minRating);

	if (count($articleIds) > 0) {

		if (!is_numeric($minRating)) {
			 $ratingGroupBy = 'GROUP BY article.id';
		} else {
			 $ratingGroupBy = 'GROUP BY article.id HAVING average_rating >= ' . $babDB->quote($minRating);
		};

		$sql = '
			SELECT
				article.*,
				COUNT(article_file.id) AS nfiles';
		if (isset($minRating)) {
			$sql .=	',
				AVG(comment.article_rating) AS average_rating,
				COUNT(comment.article_rating) AS nb_ratings
				';
		}

		$sql .= '
			FROM ' . BAB_ARTICLES_TBL . ' AS article
				LEFT JOIN ' . BAB_ART_FILES_TBL . ' AS article_file ON article.id = article_file.id_article';
		if (isset($minRating)) {
			$sql .= '
				LEFT JOIN ' . BAB_COMMENTS_TBL . ' comment ON comment.id_article = article.id AND comment.article_rating > 0';
		}
		$sql .=	'
			WHERE article.id IN (' . $babDB->quote($articleIds) . ')
			' . $ratingGroupBy . '
			ORDER BY article.date DESC';

		$res = $babDB->db_query($sql);
		return $res;
	}
	return null;
}



/**
 * Creates or updates an article.
 * If $id is provided, the corresponding article is updated.
 *
 * @param string	$title
 * @param string	$body
 * @param int		$id			The id of the article to save or null to create a new article.
 * @param int		$topicId	A topic id or null for default workspace topic.
 *
 * @return int|null The created/updated article id or null if not created/updated.
 *
 * @throws workspace_AccessException
 */
function workspace_saveArticle($title, $body, $id = null, $topicId = null)
{
    $draftId = null;

    if (is_null($id)) {
        if (!isset($topicId)) {
            $topicId = workspace_getTopicId();
        }
        if (!workspace_canCreateArticle($topicId)) {
            throw new workspace_AccessException();
        }
        $draftId = workspace_createArticleDraft($title, $body, $topicId, $id);

    } else {
        if (!workspace_canEditArticle($id)) {
            throw new workspace_AccessException();
        }
        $article = workspace_getArticle($id);
        if ($article && isset($article['id_topic'])) {
            $topicId = $article['id_topic'];

            $draftId = workspace_createArticleDraft($title, $body, $topicId, $id);
        }
    }

    $articleId = null;
    if ($draftId) {
        bab_submitArticleDraft($draftId, $articleId);
    }

    return $articleId;
}



/**
 * Creates a draft for the specified article.
 *
 * @param $title
 * @param $body
 * @param $topicId
 * @param $articleId
 * @param $articleArr
 * @return int			The id of the created draft.
 */
function workspace_createArticleDraft($title, $body, $topicId, $articleId = null, array $articleArr = array())
{
    global $babDB;

    $head = $body;
    $body = '';

    $arrdefaults = array(
        'id_author' => $GLOBALS['BAB_SESS_USERID'],
        'lang' => $GLOBALS['babLanguage'],
        'date_submission'=> '0000-00-00 00:00:00',
        'date_archiving'=> '0000-00-00 00:00:00',
        'date_publication'=> '0000-00-00 00:00:00',
        'hpage_private'=> 'N',
        'hpage_public'=> 'N',
        'notify_members'=> 'N',
        'update_datemodif'=> 'N',
        'restriction' => ''
    );

    foreach ($arrdefaults as $k => $v) {
        if (isset($articleArr[$k])) {
            $arrdefaults[$k] = $articleArr[$k];
        }
    }

    if (empty($arrdefaults['id_author'])) {
        $res = $babDB->db_query('SELECT id
            FROM '.BAB_USERS_LOG_TBL.'
            WHERE sessid='.$babDB->quote(session_id()).' AND id_user=0');
        if ($res && $babDB->db_num_rows($res) == 1) {
            $arr = $babDB->db_fetch_array($res);
            $anonymousId = $arr['id'];
        } else {
            return 0;
        }
    } else {
        $anonymousId = 0;
    }

    $arrdefaults['title'] = $title;
    $arrdefaults['body'] = $body;
    $arrdefaults['head'] = $head;
    $arrdefaults['id_topic'] = $topicId;
    if ($articleId) {
        $arrdefaults['id_article'] = $articleId;
    }
    $arrdefaults['id_anonymous'] = $anonymousId;

    $babDB->db_query('INSERT INTO '.BAB_ART_DRAFTS_TBL.' ('.implode(',', array_keys($arrdefaults)).')
    						VALUES ('.$babDB->quote($arrdefaults).')');
    $draftId = $babDB->db_insert_id();

    require_once $GLOBALS['babInstallPath']."utilit/imgincl.php";

    $ar = array();
    $head = imagesReplace($head, $draftId . '_draft_', $ar);
    $body = imagesReplace($body, $draftId . '_draft_', $ar);

    $babDB->db_query('UPDATE '.BAB_ART_DRAFTS_TBL.'
        SET date_creation=NOW(), date_modification=NOW(), body=' . $babDB->quote($body) . ', head=' . $babDB->quote($head) . '
        WHERE id='.$babDB->quote($draftId));

    return $draftId;
}



/**
 * Deletes the specified article.
 *
 * @param int $articleId
 * @throws workspace_AccessException
 */
function workspace_deleteArticle($articleId)
{
	require_once $GLOBALS['babInstallPath'].'utilit/delincl.php';

	if (!workspace_canDeleteArticle($articleId)) {
		throw new workspace_AccessException();
	}
	bab_confirmDeleteArticle($articleId);
}




///////////////////////////
// Access control functions
///////////////////////////

/**
 * Checks whether the user is allowed to delete the specified article.
 *
 * @param int	$article
 * @param int	$userId		If not specified or null, check is done for the currently logged user.
 * @return bool
 */
function workspace_canDeleteArticle($article, $userId = null)
{
	// In a workspace only the workspace admins are allowed to delete articles.
	if (!workspace_userIsWorkspaceAdministrator($userId)) {
		return false;
	}

	// Here we check standard ovidentia permissions.
	if (!is_array($article)) {
		$article = workspace_getArticle($article);
	}
	if (!isset($userId)) {
		$userId = '';
	}
	if ($article) {
		return bab_isAccessValid(BAB_TOPICSMAN_GROUPS_TBL, $article['id_topic'], $userId);
	}
	return false;
}




/**
 * Checks whether the user is allowed to edit the specified article.
 *
 * @param int	$article	The article id.
 * @param int	$userId		If not specified or null, check is done for the currently logged user.
 * @return bool
 */
function workspace_canEditArticle($article, $userId = null)
{
	if (!is_array($article)) {
		$article = workspace_getArticle($article);
	}
	if (!isset($userId)) {
		$userId = '';
	}
	if ($article && isset($article['id_topic'])) {
		return bab_isAccessValid(BAB_TOPICSMOD_GROUPS_TBL, $article['id_topic'], $userId);
	}
	return false;
}




/**
 * Checks whether the user is allowed to view the specified article.
 *
 * @param int	$article	The article id.
 * @param int	$userId		If not specified or null, check is done for the currently logged user.
 * @return bool
 */
function workspace_canViewArticle($article, $userId = null)
{
	if (!is_array($article)) {
		$article = workspace_getArticle($article);
	}
	if (!isset($userId)) {
		$userId = '';
	}
	if ($article && isset($article['id_topic'])) {
		return bab_isAccessValid(BAB_TOPICSVIEW_GROUPS_TBL, $article['id_topic'], $userId);
	}
	return false;
}




/**
 * Checks whether the user is allowed to create (submit) new articles in the specified topic.
 *
 * @param int	$topic		If not specified or null, check is done for the current workspace's article topic.
 * @param int	$userId		If not specified or null, check is done for the currently logged user.
 * @return bool
 */
function workspace_canCreateArticle($topic = null, $userId = null)
{
	if (!isset($userId)) {
		$userId = '';
	}
	if (!isset($topic)) {
		$topic = workspace_getWorkspaceTopicId();
	}
	if ($topic) {
		return bab_isAccessValid(BAB_TOPICSSUB_GROUPS_TBL, $topic, $userId);
	}
	return false;
}



function workspace_canCommentTopic($topicId)
{
	return bab_isAccessValid(BAB_TOPICSCOM_GROUPS_TBL, $topicId);
}
