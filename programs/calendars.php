<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/workspaces.php';
include_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';


/**
 * @return int		The current workspace calendar id
 */
function workspace_getSharedCalendarId()
{
	$delegationId = workspace_getCurrentWorkspace();

	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/workspace/workspaces/' . $delegationId);

	$calendarId = $registry->getValue('calendarId');

	return 'public/' . $calendarId;
}




/**
 * @return int		The current user's personal calendar id
 */
function workspace_getPersonalCalendarId()
{
	$personalCalendar = bab_getIcalendars()->getPersonalCalendar();

	if (!$personalCalendar) {
		return null;
	}
	return $personalCalendar->getUrlIdentifier();
}



/**
 *
 * @return array
 */
function workspace_getCalendarCategories()
{
	require_once $GLOBALS['babInstallPath'].'utilit/calapi.php';

	static $eventCategories = null;
	if (!isset($eventCategories)) {
		$eventCategories = array();
		$categories = bab_calGetCategories();
		foreach ($categories as $category) {
			$eventCategories[$category['name']] = $category;
		}
	}
	return $eventCategories;
}




/**
 * Returns periods between $start and $end.
 *
 * @param bab_DateTime $start
 * @param bab_DateTime $end
 * @param array     $workspacesIds      An array of workspace ids. Set to null to get current workspace
 * @throws Exception
 *
 * @return bab_UserPeriods
 */
function workspace_getCalendarEvents(bab_DateTime $start, bab_DateTime $end, $workspacesIds = array())
{
	include_once $GLOBALS['babInstallPath'].'utilit/cal.userperiods.class.php';


	$userPeriods = new bab_UserPeriods($start, $end);

	$factory = bab_getInstance('bab_PeriodCriteriaFactory');
	/* @var $factory bab_PeriodCriteriaFactory */

	$criteria = $factory->Collection(
		array(
			'bab_NonWorkingDaysCollection',
			'bab_VacationPeriodCollection',
			'bab_CalendarEventCollection',
			'bab_InboxEventCollection'
		)
	);

	if (!isset($_SESSION['workspace_personal_calendar_visible'])) {
		$_SESSION['workspace_personal_calendar_visible'] = true;
	}
	if (!isset($_SESSION['workspace_shared_calendar_visible'])) {
		$_SESSION['workspace_shared_calendar_visible'] = true;
	}

	if ($_SESSION['workspace_personal_calendar_visible'] == true) {
		$personalCalendarId = workspace_getPersonalCalendarId();
		if (!empty($personalCalendarId)) {
			$calids[] = $personalCalendarId;
		}
	}
	if ($_SESSION['workspace_shared_calendar_visible'] == true) {
		$sharedCalendarId = workspace_getSharedCalendarId();
		if (!empty($sharedCalendarId)) {
			$calids[] = workspace_getSharedCalendarId();
		}
	};
	
	if(!empty($workspacesIds)){
	    $registry = bab_getRegistryInstance();
	    $calids = array();
	    foreach ($workspacesIds as $workspacesId){
	        $registry->changeDirectory('/workspace/workspaces/' . $workspacesId);
	        $calids[] = 'public/' . $registry->getValue('calendarId');
	    }
	}


	$calendars = array();
	foreach($calids as $idcal) {

		$calendar = bab_getICalendars()->getEventCalendar($idcal);
		if (!$calendar) {
			throw new Exception('Calendar not found for identifier : (' . $idcal . ')');
		}
		$calendars[] = $calendar;
	}

	$criteria = $criteria->_AND_($factory->Calendar($calendars));

	$userPeriods->createPeriods($criteria);
	$userPeriods->orderBoundaries();

	return $userPeriods;
}


/**
 *
 */
function workspace_getNonWorkingDays($start, $end)
{
	require_once $GLOBALS['babInstallPath'].'utilit/calapi.php';
	require_once $GLOBALS['babInstallPath'].'utilit/mcalincl.php';

	$calendars = array();

	if ($_SESSION['workspace_personal_calendar_visible'] == true) {
		$calendars[] = workspace_getPersonalCalendarId();
	}
	if ($_SESSION['workspace_shared_calendar_visible'] == true) {
		$calendars[] = workspace_getSharedCalendarId();
	};

	$nwh = new bab_userWorkingHours(
		BAB_dateTime::fromIsoDateTime($start),
		BAB_dateTime::fromIsoDateTime($end)
	);

	$events = array();
	foreach ($calendars as $calendar) {
		$nwh->createPeriods(BAB_PERIOD_NWDAY);
		$nwh->orderBoundaries();
		$events += $nwh->getEventsBetween(bab_mktime($start), bab_mktime($end), BAB_PERIOD_NWDAY);
	}

	return $events;
}

/**
 * @param int	$eventId
 * @return array
 */
function workspace_getEvent($eventId)
{

}


/**
 * Checks whether the current user can delete the specified calendar event.
 *
 * @param int $eventId
 * @return bool
 */
function workspace_canDeleteEvent($eventId)
{
	return true;
}



/**
 *	$event['startdate'] : array('month', 'day', 'year', 'hours', 'minutes')
 *	$event['enddate'] 	: array('month', 'day', 'year', 'hours', 'minutes')
 *	$event['owner'] 	: id of the owner
 *	$event['rrule'] 	: // BAB_CAL_RECUR_DAILY, ...
 *	$event['until'] 	: array('month', 'day', 'year')
 *	$event['rdays'] 	: repeat days array(0,1,2,3,4,5,6)
 *	$event['ndays'] 	: nb days
 *	$event['nweeks'] 	: nb weeks
 *	$event['nmonths'] 	: nb weeks
 *	$event['category'] 	: id of the category
 *	$event['private'] 	: if the event is private
 *	$event['lock'] 		: to lock the event
 *	$event['free'] 		: free event
 *	$event['alert'] 	: array('day', 'hour', 'minute', 'email'=>'Y')
 * @param array	$event
 * @return bool
 */
function workspace_saveEvent($event)
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/calapi.php';

	$calendarIds = array(workspace_getSharedCalendarId());

	return bab_newEvent($calendarIds, $event);
}





/**
 *
 * @param	array		$args
 *
 *	$args['startdate'] 	: array('month', 'day', 'year', 'hours', 'minutes')
 *	$args['enddate'] 	: array('month', 'day', 'year', 'hours', 'minutes')
 *	$args['owner'] 		: id of the owner
 *	$args['rrule'] 		: // BAB_CAL_RECUR_DAILY, ...
 *	$args['until'] 		: array('month', 'day', 'year')
 *	$args['rdays'] 		: repeat days array(0,1,2,3,4,5,6)
 *	$args['ndays'] 		: nb days
 *	$args['nweeks'] 	: nb weeks
 *	$args['nmonths'] 	: nb weeks
 *	$args['category'] 	: id of the category
 *	$args['private'] 	: if the event is private
 *	$args['lock'] 		: to lock the event
 *	$args['free'] 		: free event
 *	$args['alert'] 		: array('day', 'hour', 'minute', 'email'=>'Y')
 *	$args['selected_calendars']
 *
 * @param	string		&$msgerror
 * @param	string		[$action_function]
 */


/**
 * @property string	$subject		The post subject
 * @property string	$message		The post content
 */
class workspace_Event
{
	private $loaded;
	private $id = null;
	private $creatorId = null;
	private $title = null;
	private $description = null;
	private $location = null;
	private $startDatetime = null;
	private $endDatetime = null;
	private $calendarId = null;
	private $backgroundColor = null;
	private $textColor = null;
	private $category = null;
	private $allDay = false;
	private $editable = true;

	/**
	 */
	public function __construct($event = null)
	{
		if (is_array($event)) {
			$this->id = $event['id'];
			$this->creatorId = $event['creatorId'];
			$this->title = $event['title'];
			$this->description = $event['description'];
			$this->location = $event['location'];
			$this->startDatetime = $event['startDatetime'];
			$this->endDatetime = $event['endDatetime'];
			$this->calendarId = $event['calendarId'];
			$this->backgroundColor = $event['backgroundColor'];
			$this->textColor = '#000000';
			$this->category = $event['category'];
			$this->loaded = true;
		} else {
			$this->id = $event;
			$this->loaded = false;
		}
	}

	/**
	 *
	 * @param array $event
	 * @return workspace_Event
	 */
	public static function fromBabEvent($event)
	{
		$categories = workspace_getCalendarCategories();

		$evt = new workspace_Event($event['id_event']);
		$evt->title = $event['title'];
		$evt->description = $event['description'];
		$evt->location = $event['location'];
		$evt->startDatetime = $event['begindate'];
		$evt->endDatetime = $event['enddate'];

		list(,$startTime) = explode(' ', $evt->startDatetime);
		list(,$endTime) = explode(' ', $evt->endDatetime);

//		$evt->allDay = ($startTime === '00:00:00' && ($endTime === '00:00:00' || $endTime === '23:59:59'));

		if (!empty($event['backgroundcolor'])) {
			$evt->color = '#' . $event['backgroundcolor'];
		} else if (isset($categories[$event['id_category']])) {
			$evt->color = '#' . $categories[$event['id_category']]['color'];
		} else {
			if ($event['id_calendar'] == workspace_getPersonalCalendarId()) {
				$evt->color = '#ddbbee';
			}
			if ($event['id_calendar'] == workspace_getSharedCalendarId()) {
				$evt->color = '#bbddee';
			}
		}
		$evt->category = $event['category'];
		$evt->editable = !empty($event['id_event']);
		$evt->textColor = '#000000';
		return $evt;
	}


	/**
	 *
	 * @param bab_CalendarPeriod $event
	 * @return workspace_Event
	 */
	public static function fromCalendarPeriod(bab_CalendarPeriod $event)
	{
	    $W = bab_Widgets();

		$categories = workspace_getCalendarCategories();
		include_once $GLOBALS['babInstallPath']."utilit/dateTime.php";
		$calendarObj = $event->getCollection()->getCalendar();

		if ($calendarObj != null) {
			$calendarId = $calendarObj->getUrlIdentifier();
			$eventUid = $event->getProperty('UID');
			$evt = new workspace_Event($calendarId . ':' . $eventUid);
			$evt->title = $event->getProperty('SUMMARY');
			$evt->description = $event->getProperty('DESCRIPTION');
			$evt->location = $event->getProperty('LOCATION');
			$startDatetime = $event->getProperty('DTSTART');
			$evt->startDatetime = substr($startDatetime,0,4) . '-' . substr($startDatetime,4,2) . '-' . substr($startDatetime,6,2) . 'T' .  substr($startDatetime,9,2) . ':' . substr($startDatetime,11,2) . ':'  . substr($startDatetime,13,2);
			$endDatetime = $event->getProperty('DTEND');
			$evt->endDatetime = substr($endDatetime,0,4) . '-' . substr($endDatetime,4,2) . '-' . substr($endDatetime,6,2) . 'T' .  substr($endDatetime,9,2) . ':' . substr($endDatetime,11,2) . ':'  . substr($endDatetime,13,2);

			$evt->category = $event->getProperty('CATEGORIES');

			$color = $event->getProperty('X-CTO-COLOR');
			if (isset($categories[$evt->category])) {
				$evt->color = '#' . $categories[$evt->category]['color'];
			} else if (!empty($color)) {
				$evt->color = '#' . $color;
			}

			$evt->editable = !empty($eventUid);

		} else {

			$eventArray = array();
			$eventArray['id'] = null;
			$eventArray['creatorId'] = null;
			$eventArray['title'] = $event->getProperty('SUMMARY');
			$eventArray['description'] = $event->getProperty('DESCRIPTION');
			$eventArray['location'] = $event->getProperty('LOCATION');
			$startDatetime = $event->getProperty('DTSTART');
			$eventArray['startDatetime'] = substr($startDatetime,0,4) . '-' . substr($startDatetime,4,2) . '-' . substr($startDatetime,6,2) . 'T' .  substr($startDatetime,9,2) . ':' . substr($startDatetime,11,2) . ':'  . substr($startDatetime,13,2);
			$endDatetime = $event->getProperty('DTEND');
			$eventArray['endDatetime'] = substr($endDatetime,0,4) . '-' . substr($endDatetime,4,2) . '-' . substr($endDatetime,6,2) . 'T' .  substr($endDatetime,9,2) . ':' . substr($endDatetime,11,2) . ':'  . substr($endDatetime,13,2);
			$eventArray['calendarId'] = null;
			$eventArray['backgroundColor'] = "#FFFFFF";
			$eventArray['category'] = $event->getProperty('CATEGORIES');

			$evt = new workspace_Event($eventArray);
			$evt->title = $event->getProperty('SUMMARY');
			$evt->description = $event->getProperty('DESCRIPTION');
			$evt->location = $event->getProperty('LOCATION');
			$startDatetime = $event->getProperty('DTSTART');
			$evt->startDatetime = substr($startDatetime,0,4) . '-' . substr($startDatetime,4,2) . '-' . substr($startDatetime,6,2) . 'T' .  substr($startDatetime,9,2) . ':' . substr($startDatetime,11,2) . ':'  . substr($startDatetime,13,2);
			$endDatetime = $event->getProperty('DTEND');
			$evt->endDatetime = substr($endDatetime,0,4) . '-' . substr($endDatetime,4,2) . '-' . substr($endDatetime,6,2) . 'T' .  substr($endDatetime,9,2) . ':' . substr($endDatetime,11,2) . ':'  . substr($endDatetime,13,2);

			$evt->category = $event->getProperty('CATEGORIES');

			$evt->color = '#FFFFFF';

			$evt->editable = false;
		}

		$color = $W->Color();
		$color->setHexa($evt->color);


		list(, , $l) = $color->getHSL();

		if ($l > 0.4) {
		    $evt->textColor = '#000000';
		} else {
		    $evt->textColor = '#FFFFFF';
		}


		return $evt;
	}





	public function getId()
	{
		return $this->id;
	}

	public function getTitle()
	{
		$this->load();
		return $this->title;
	}


	public function getDescription()
	{
		$this->load();
		return $this->description;
	}


	public function getLocation()
	{
		$this->load();
		return $this->location;
	}


	public function getStartDatetime()
	{
		$this->load();
		return $this->startDatetime;
	}


	public function getEndDatetime()
	{
		$this->load();
		return $this->endDatetime;
	}


	public function getCreator()
	{
		$this->load();
		return $this->creatorId;
	}


	public function getCategory()
	{
		$this->load();
		return $this->category;
	}


	public function __get($propertyName)
	{
		$this->load();

		$method = 'get' . $propertyName;
		if (method_exists($this, $method)) {
			return $this->$method();
		}
	}


	public function __set($propertyName, $value)
	{
		$this->$propertyName = $value;
	}


	/**
	 * Checks whether the current user can delete this forum post.
	 *
	 * @return bool
	 */
	public function canDelete()
	{
		return true;
	}


	/**
	 * Deletes this calendar event.
	 */
	public function delete()
	{
		if (!$this->canDelete()) {
			return false;
		}
		bab_deleteEventById($this->id);
	}


	public function load()
	{
		if ($this->loaded) {
			return;
		}

		global $babDB;

		$res = $babDB->db_query('SELECT * FROM '.BAB_CAL_EVENTS_TBL.' WHERE id=' . $babDB->quote($this->id));
		if ($res && $event = $babDB->db_fetch_assoc($res)) {
			$this->creatorId = $event['creatorId'];
			$this->title = $event['title'];
			$this->description = $event['description'];
			$this->location = $event['location'];
			$this->startDatetime = $event['start_date'];
			$this->endDatetime = $event['end_date'];
			$this->category = $event['id_cat'];
//			$this->calendarId = $event['calendarId'];
			$this->backgroundColor = $event['color'];
			$this->loaded = true;
		}
		return true;
	}


	/**
	 * Returns a json encoded version of the event.
	 *
	 * @see http://arshaw.com/fullcalendar/docs/event_data/Event_Object/
	 * @return string
	 */
	public function toJson()
	{
		$event = array(
			'id' => $this->id,
			'title' => $this->title,
			'allDay' => $this->allDay,
			'start' => str_replace(' ', 'T', $this->startDatetime),
			'end' => str_replace(' ', 'T', $this->endDatetime),
			'description' => $this->description,
			'color' => $this->color,
			'textColor' => $this->textColor,
 			'location' => $this->location,
 			'category' => $this->category,
		    'editable' => $this->editable
		);

		if ($this->editable) {
		    $event['actions'] = '<span class="actions ' . Func_Icons::ICON_LEFT_SYMBOLIC . '">'
		      . '<a href="' . workspace_Controller()->Calendars()->editEvent($this->id)->url() . '" class="icon ' . Func_Icons::ACTIONS_DOCUMENT_EDIT . '"></a>'
		      . '<script type="text/javascript">widget_baseInit(document.getElementsByTagName("body")[0]);</script>';
		}

		return workspace_json_encode($event);
	}
}
