<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once $GLOBALS['babInstallPath'] . 'utilit/filemanApi.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/pathUtil.class.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/uploadincl.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/workspaces.php';
require_once dirname(__FILE__) . '/controller.class.php';
require_once dirname(__FILE__) . '/page.class.php';
require_once dirname(__FILE__) . '/files.php';
require_once dirname(__FILE__) . '/files.ui.php';


/**
 * This controller manages actions that can be performed on files.
 */
class workspace_CtrlFiles extends workspace_Controller
{

	/**
	 *
	 * @param $path
	 * @param $workspace
	 * @return Widget_Action
	 */
	protected function rss($path = null, $workspace = null)
	{
		require_once $GLOBALS['babInstallPath'] . '/utilit/delegincl.php';

		header('Content-Type: application/rss+xml');

		echo '<?xml version="1.0" encoding="utf-8" standalone="yes"?>' . "\n";
		echo '<rss version="2.0" xmlns:media="http://search.yahoo.com/mrss/" xmlns:atom="http://www.w3.org/2005/Atom">' . "\n";
        echo '<channel>' . "\n";

        $selfUrl = $GLOBALS['babUrl'] . $this->proxy()->browse($path, $workspace)->url();
        $rssUrl = $GLOBALS['babUrl'] . $this->proxy()->browse($path, $workspace, 'rss')->url() . '&' . session_name() . '=' . session_id();
        echo '<atom:link href="' . htmlspecialchars($rssUrl) . '" rel="self" type="application/rss+xml" />' . "\n";

        echo '<link>' . htmlspecialchars($selfUrl) . '</link>' . "\n";
        echo '<title>Slideshow</title>' . "\n";
        echo '<description>Slideshow</description>' . "\n";

		$files = workspace_listFolder($path);

		$T = bab_functionality::get('Thumbnailer');

		$mediaThumbnailUrl = null;
		$mediaContentUrl = null;

		foreach ($files as $file) {

			if ($file->isDir()) {
				continue;
			}
			$fmfile = $file->getFmFile();
			$fmState = $fmfile->getState(); /* '', D(deleted) or X(cutted) */
			if ($fmState === 'D') {
				continue;
			}

			if ($T) {
				$T->setSourceFile($file->getPathname());
				$mediaThumbnailUrl = $T->getThumbnail(256, 256);
				$linkUrl = workspace_Controller()->Files()->displayFile($file->getFmPathname())->url() . '&' . session_name() . '=' . session_id();
				//			$bigUrl = $T->getThumbnail(1024, 1024);
				$mimetype = bab_getFileMimeType($file->getFilename());
				if (substr($mimetype, 0, strlen('image/')) != 'image/') {
					$mediaContentUrl = $T->getThumbnail(1024, 1024);
				} else {
					$mediaContentUrl = $linkUrl;
				}

				if (substr($mimetype, 0, strlen('image/')) === 'image/') {
					$mediaContentUrl = $linkUrl;
					$mediaType = '';
				} else if (substr($mimetype, 0, strlen('video/x-flv')) === 'video/x-flv') {
					$mediaContentUrl = $linkUrl;
					$mediaType = 'type="video/x-flv"';
				} else {
					$mediaType = '';
					$mediaContentUrl = $T->getThumbnail(1024, 1024);
				}
			}

			echo '<item>' . "\n";
			echo '<guid>' . htmlspecialchars($GLOBALS['babUrl']) . md5($file->getPathname()) . '</guid>' . "\n";
			echo '<title>' . htmlspecialchars($file->getFilename()) . '</title>' . "\n";
			echo '<link>' . htmlspecialchars($GLOBALS['babUrl'] . $linkUrl)  . '</link>' . "\n";
			if (isset($mediaThumbnailUrl)) {
				if (method_exists($file, 'getDescription')) {
					echo '<description>' . htmlspecialchars('<img src="' . $GLOBALS['babUrl'] . $mediaThumbnailUrl . '" />' . $file->getDescription()) . '</description>' . "\n";
				} else {
					echo '<description>' . htmlspecialchars('<img src="' . $GLOBALS['babUrl'] . $mediaThumbnailUrl . '" />' . $file->getFilename()) . '</description>' . "\n";
				}
				echo '<media:thumbnail url="' . htmlspecialchars($GLOBALS['babUrl'] . $mediaThumbnailUrl) . '" />' . "\n";
			} else {
				if (method_exists($file, 'getDescription')) {
					echo '<description>' . htmlspecialchars($file->getDescription()) . '</description>' . "\n";
				} else {
					echo '<description>' . htmlspecialchars($file->getFilename()) . '</description>' . "\n";
				}
			}
			if (isset($mediaContentUrl)) {
				echo '<media:content ' . $mediaType . ' url="' . htmlspecialchars($GLOBALS['babUrl'] . $mediaContentUrl) . '" />' . "\n";
			}
			echo '</item>' . "\n";
		}

        echo '</channel>' . "\n";
        echo '</rss>' . "\n";
        die();
	}




	/**
	 * Returns the workspace (delegation) id corresponding to the specified path.
	 *
	 * @return int 		The workspace id or null on error.
	 */
	protected function getWorkspaceFromPath($path)
	{
		if (substr($path, 0, 2) !== 'DG') {
			return null;
		}
		$dgend = strpos($path, '/', 2);
		$workspaceId = (int)(substr($path, 2, $dgend - 2));
		return $workspaceId;
	}




	public function configure($path = null, $workspace = null)
	{
		if (!workspace_userIsWorkspaceAdministrator()) {
			throw new workspace_AccessException();
		}

		$workspaceId = (isset($workspace) ? $workspace : workspace_getCurrentWorkspace());

		require_once dirname(__FILE__) . '/files.ui.php';
		require_once dirname(__FILE__) . '/workspaces.php';

		$editor = workspace_filesConfigurationEditor();

		$editor->setValue(array('configuration', 'notify'), workspace_getWorkspaceFileNotifications($workspaceId));

		$page = workspace_Page()
			->addItemMenu('files', workspace_translate('File manager'), $this->proxy()->browse($path, $workspace)->url())
			->addItemMenu('files_configuration', workspace_translate('Configuration'), $this->proxy()->configure($path, $workspace)->url())
			->setCurrentItemMenu('files_configuration')
			->addItem($editor);

		$page->displayHtml();
	}




	/**
	 * Saves the workspace data and returns to the workspace list.
	 *
	 * @param array $workspace
	 * @return Widget_Action
	 */
	public function saveConfiguration($configuration = null)
	{
		global $babBody;

		if (!workspace_userIsWorkspaceAdministrator()) {
			throw new workspace_AccessException();
		}

		workspace_setWorkspaceFileNotifications($configuration['notify']);

		workspace_redirect(workspace_BreadCrumbs::last());
	}




	/**
	 * Displays the content of the trash folder.
	 *
	 * @param int $workspace
	 */
	protected function displayTrash($workspace = null)
	{
		require_once dirname(__FILE__) . '/files.ui.php';

		require_once $GLOBALS['babInstallPath'] . '/utilit/delegincl.php';

		if (!isset($path)) {
		    $workspaceId = (isset($workspace) ? $workspace : workspace_getCurrentWorkspace());
		    $delegationPrefix = 'DG' . $workspaceId;
		    $pathElements = array();
		} else if (substr($path, 0, 2) === 'DG') {
		    $pathElements = explode('/', $path);
		    $delegationPrefix = array_shift($pathElements);
		    array_shift($pathElements);
		    $workspaceId = (int)(substr($delegationPrefix, 2));
		} else {
		    throw new Exception('Wrong path');
		}

		$workspaceInfo = workspace_getWorkspaceInfo($workspaceId);
		$path = $delegationPrefix . '/' . workspace_getWorkspaceRootFolderName();

		$page = workspace_Page()
			->addItemMenu('files', workspace_translate('File manager'), '');

		if (workspace_userIsWorkspaceAdministrator()) {
			$page->addItemMenu('files_configuration', workspace_translate("Configuration"), $this->proxy()->configure($path, $workspace)->url());
		}

		$page->setCurrentItemMenu('files');

		$trashPath = workspace_getWorkspaceTrashPath($workspaceId);

		$splitView = workspace_FolderSplitView($trashPath);
		$page->addItem($splitView);

		$page->displayHtml();
	}




	/**
	 * Displays the file manager.
	 *
	 * @return Widget_Action
	 */
	protected function displayFileManager($path = null, $workspace = null)
	{
		require_once dirname(__FILE__) . '/files.ui.php';

		require_once $GLOBALS['babInstallPath'] . '/utilit/delegincl.php';

		global $babBody;

		if (!isset($path)) {
			$workspaceId = (isset($workspace) ? $workspace : workspace_getCurrentWorkspace());
			$delegationPrefix = 'DG' . $workspaceId;
			$pathElements = array();
		} else if (substr($path, 0, 2) === 'DG') {
			$pathElements = explode('/', $path);
			$delegationPrefix = array_shift($pathElements);
			array_shift($pathElements);
			$workspaceId = (int)(substr($delegationPrefix, 2));
		} else {
			throw new Exception('Wrong path');
		}
		workspace_setCurrentWorkspace($workspaceId);

		$workspaceInfo = workspace_getWorkspaceInfo($workspaceId);

		$workspaceName = $workspaceInfo['name'];

		$path = $delegationPrefix . '/' . workspace_getWorkspaceRootFolderName();

		if (!empty($pathElements)) {
			$path .= '/' . implode('/', $pathElements);
		}

		$page = workspace_Page()
			->addItemMenu('files', workspace_translate('File manager'), '');

		if (workspace_userIsWorkspaceAdministrator()) {
			$page->addItemMenu('files_configuration', workspace_translate('Configuration'), $this->proxy()->configure($path, $workspace)->url());
		}

		$page->setCurrentItemMenu('files');

		if (!workspace_canBrowseFolder($path)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You should be logged in to see the content of this folder.'));
			$babBody->addError(workspace_translate('Access denied'));
			$page->displayHtml();
			return;
		}

		try {
			$splitView = workspace_FolderSplitView($path);
			$page->addItem($splitView);
		} catch (Exception $e) {
			$babBody->addError(workspace_translate('Access denied'));
			$page->displayHtml();
			return;
		}

		$page->addAlternateLink($this->proxy()->browse($path, $workspace, 'rss')->url() . '&' . session_name() . '=' . session_id());
		$page->displayHtml();
	}




	/**
	 * Displays the file manager.
	 *
	 * @return Widget_Action
	 */
	protected function displaySlideshow($path = null, $workspace = null)
	{
		require_once dirname(__FILE__) . '/files.ui.php';

		require_once $GLOBALS['babInstallPath'] . '/utilit/delegincl.php';

		if (!isset($path)) {
			$workspaceId = (isset($workspace) ? $workspace : workspace_getCurrentWorkspace());
			$delegationPrefix = 'DG' . $workspaceId;
			$pathElements = array();
		} elseif (substr($path, 0, 2) === 'DG') {
			$pathElements = explode('/', $path);
			$delegationPrefix = array_shift($pathElements);
			array_shift($pathElements);
			$workspaceId = (int)(substr($delegationPrefix, 2));
		} else {
			throw new Exception('Wrong path');
		}
		workspace_setCurrentWorkspace($workspaceId);

		$workspaceInfo = workspace_getWorkspaceInfo($workspaceId);

		$workspaceName = $workspaceInfo['name'];

		$path = $delegationPrefix . '/' . $workspaceName;
		if (!empty($pathElements)) {
			$path .= '/' . implode('/', $pathElements);
		}

		$page = workspace_Page()
			->addItemMenu('files', workspace_translate('File manager'), '');

		if (workspace_userIsWorkspaceAdministrator()) {
			$page->addItemMenu('files_configuration', workspace_translate('Configuration'), $this->proxy()->configure($path, $workspace)->url());
		}

		$page->setCurrentItemMenu('files');

		if (!workspace_canBrowseFolder($path)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You should be logged in to see the content of this folder.'));
			throw new workspace_AccessException();
		}

		$W = bab_Widgets();

		// For the Cooliris slideshow to work, the feed url must be accessible by the cooliris.com servers.
		// So we add the session id to the url to give them access to the data.
		$slideshowFeedUrl = $GLOBALS['babUrl'] . $this->proxy()->browse($path, $workspace, 'rss')->url() . '&' . session_name() . '=' . session_id();

		// For Cooliris reference see http://www.cooliris.com/developer/reference/
		$slideshowFlashvars = 'backgroundColor=#404040'
			. '&showItemEmbed=false'
			. '&showEmbed=false'
			. '&tilt=1'
			. '&feed=' . htmlspecialchars(urlencode($slideshowFeedUrl));
		$slideshowWidth = '100%';
		$slideshowHeight = '450';
		$slideshowAllowFullScreen = 'true';


		$slideshow = $W->Html('
			<object id="cooliris_wall"
				classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
				width="' . $slideshowWidth . '" height="' . $slideshowHeight . '">
				<param name="movie" value="http://apps.cooliris.com/embed/cooliris.swf" />
				<param name="allowFullScreen" value="' . $slideshowAllowFullScreen . '" />
				<param name="allowScriptAccess" value="always" />
				<param name="flashvars" value="' . $slideshowFlashvars . '" />
					<embed type="application/x-shockwave-flash"
						src="http://apps.cooliris.com/embed/cooliris.swf"
						flashvars="' . $slideshowFlashvars . '"
						width="' . $slideshowWidth . '"
						height="' . $slideshowHeight . '"
						allowFullScreen="' . $slideshowAllowFullScreen . '"
						allowScriptAccess="always"
					/>
			</object>
		');

		$toolbar = workspace_Toolbar('main-toolbar');
		$slideshowIcon = $W->Icon(workspace_translate("Exit slideshow"), Func_Icons::MIMETYPES_IMAGE_X_GENERIC);
		$slideshowButton = $W->Link($slideshowIcon, $this->proxy()->browse($path, $workspace));
		$toolbar->addItem($slideshowButton);

		$page->addItem($toolbar);
		$page->addItem($slideshow);

		$page->addAlternateLink($this->proxy()->browse($path, $workspace, 'rss')->url() . '&' . session_name() . '=' . session_id());
		$page->displayHtml();
	}




	/**
	 * Displays the file browser for the specified folder path.
	 *
	 * @param string $path		The path of the folder to browse. Eg. 'DG1/Folder/Subfolder'
	 * @param string $workspace
	 * @param string $mode		If 'rss' the folder content is return as an rss feed.
	 * @return Widget_Action
	 */
	public function browse($path = null, $workspace = null, $mode = null)
	{
		if ($mode == 'rss') {
			$this->rss($path, $workspace);
		} else if ($mode == 'slideshow') {
			$this->displaySlideshow($path, $workspace);
		} else {
			workspace_BreadCrumbs::setCurrentPosition($this->proxy()->browse($path, $workspace, $mode), workspace_translate('Files'));

			$this->displayFileManager($path, $workspace);
		}
	}




	/**
	 * Displays the file browser for the trash.
	 *
	 * @param string $workspace
	 * @param string $mode		If 'rss' the folder content is return as an rss feed.
	 * @return Widget_Action
	 */
	public function browseTrash($workspace = null, $mode = null)
	{
		if (!workspace_canViewTrash()) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You should be logged in to see the content of this folder.'));
			throw new workspace_AccessException();
		}
		workspace_BreadCrumbs::setCurrentPosition($this->proxy()->browseTrash($workspace, $mode), workspace_translate('Trash'));
		return $this->displayTrash($workspace);
	}




	/**
	 * Displays (outputs) the specified file.
	 *
	 * @param string	$pathname
	 * @param bool		$inline		true to have the file open inline or false to have it as an attachement (open outside the browser).
	 * @return Widget_Action
	 */
	public function displayFile($pathname, $inline = false)
	{
		require_once dirname(__FILE__) . '/files.ui.php';
		require_once $GLOBALS['babInstallPath'] . '/utilit/delegincl.php';

		$workspaceId = $this->getWorkspaceFromPath($pathname);
		workspace_setCurrentWorkspace($workspaceId);

		$path = dirname($pathname);
//		$pathElements = explode('/', $path);
//		array_shift($path);
//		$path = implode('/', $pathElements);
		$filename = basename($pathname);


		$page = workspace_Page();


		if (!workspace_canDownloadFileFromFolder($path)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You should be logged in to see the content of this folder.'));
			throw new workspace_AccessException();
		}

		$files = workspace_listFolder($path);
		foreach ($files as $file) {
			if ($file->getFilename() == $filename) {
				$mimetype = bab_getFileMimeType($filename);
				$size = $file->getSize();
				if (strtolower(bab_browserAgent()) == 'msie') {
					header('Cache-Control: public');
				}
				if (!empty($inline)) {
					header('Content-Disposition: inline; filename="' . $filename .'"' ."\n");
				} else {
					header('Content-Disposition: attachment; filename="' . $filename . '"' . "\n");
				}
				header('Content-Type: ' . $mimetype . "\n");
				header('Content-Length: '. $size."\n");
				readfile($file->getPathName());
				die;
			}
		}
	}




	/**
	 * Displays the form to create a new folder.
	 *
	 * @param string	$path	The path of the folder in which the new folder will be created. Eg.: 'DG1/Folder'
	 * @return Widget_Action
	 */
	public function addFolder($path)
	{
		$page = workspace_Page();

		if (!workspace_canCreateFolderInFolder($path)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You should be logged in to create folders.'));
			throw new workspace_AccessException();
		}


		$editor = workspace_FolderEditor();
		$editor->setHiddenValue('folder[path]', $path);

		$page
			->setTitle(workspace_translate("Create a new folder"))
			->addItemMenu('files', workspace_translate("Files"), '')
			->setCurrentItemMenu('files')
			->addItem($editor);
		$page->displayHtml();
	}





	/**
	 * Creates the specified folder
	 *
	 * @param array	$folder		Eg.: array('path' => 'DG1/Folder', 'name' => 'Subfolder')
	 * @return Widget_Action
	 */
	public function createFolder($folder = null)
	{
		global $babBody;

		$page = workspace_Page();

		if (!workspace_canCreateFolderInFolder($folder['path'])) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You should be logged in to create folders.'));
			throw new workspace_AccessException();
		}


		$pathname = $folder['path'] . '/' . bab_PathUtil::sanitizePathItem($folder['name']);
		$currentDirectoryPath = $folder['path'];
		$currentDirectory = new bab_Directory();

		if (!$currentDirectory->createSubdirectory($pathname)) {
			$babBody->addError(workspace_translate('There was a problem when trying to create this folder.'));
		}

		workspace_redirect($this->proxy()->browse($currentDirectoryPath));
	}





	/**
	 * Displays the form to upload a file.
	 *
	 * @param string $path	The path of the folder where the file will be uploaded. Eg.: 'DG1/Folder/Subfolder'
	 * @return Widget_Action
	 */
	public function addFile($path = '/')
	{
		$editor = workspace_FileUploadEditor();
		$editor->setHiddenValue('file[path]', $path);

		$page = workspace_Page()
			->setTitle(workspace_translate('Upload a file'))
			->addItemMenu('files', workspace_translate('Files'), '')
			->setCurrentItemMenu('files')
			->addItem($editor);
		$page->displayHtml();
	}





	/**
	 *
	 * @param string $folderPath
	 * @param string $object
	 * @param string $body
	 * @return Widget_Action
	 */
	private function sendNotificationMessage($folderPath, $object, $body)
	{
		require_once dirname(__FILE__) . '/mail.class.php';
		$groupId = workspace_getWorkspaceGroupId();

		$recipients = bab_getGroupsMembers($groupId);

		$addresses = array();
		foreach ($recipients as $recipient) {
			if (workspace_Mail::emailIsValid($recipient['email'])) {
				$addresses[] = $recipient['email'];
			}
		}

		$senderId = $GLOBALS['BAB_SESS_USERID'];

		$folderElements = explode('/', $folderPath);
		array_shift($folderElements);
		$displayedFolderPath = implode('/', $folderElements);

		$mail = new workspace_Mail();
		$mail->setData(
			$object,
			$body,
			$GLOBALS['babUrl'] . $this->proxy()->browse($folderPath)->url(),
			sprintf(workspace_translate('Access folder %s'), $displayedFolderPath),
			$senderId
		);

		$mail->mailDestArray($addresses, 'mailTo');
		$mail->send();
	}





	/**
	 * Returns the error message corresponding to the upload error code.
	 *
	 * @param int $errorCode
	 *
	 * @return string
	 */
	protected static function getUploadErrorMessage($errorCode)
	{
		switch($errorCode) {
			case UPLOAD_ERR_OK:
				break;

			case UPLOAD_ERR_INI_SIZE:
				$errorMessage = bab_translate('The uploaded file exceeds the upload_max_filesize directive.');
				break;

			case UPLOAD_ERR_FORM_SIZE:
				$errorMessage = bab_translate('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.');
				break;

			case UPLOAD_ERR_PARTIAL:
				$errorMessage = bab_translate('The uploaded file was only partially uploaded.');
				break;

			case UPLOAD_ERR_NO_FILE:
				$errorMessage = bab_translate('No file was uploaded.');
				break;

			case UPLOAD_ERR_NO_TMP_DIR: //  since PHP 4.3.10 and PHP 5.0.3
				$errorMessage = bab_translate('Missing a temporary folder.');
				break;

			case UPLOAD_ERR_CANT_WRITE: //  since php 5.1.0
				$errorMessage = bab_translate('Failed to write file to disk.');
				break;

			default :
				$errorMessage = bab_translate('Unknown File Error.');
				break;
		}
		return $errorMessage;
	}





	/**
	 * Upload files.
	 *
	 * @return Widget_Action
	 */
	public function uploadFile($file = null)
	{
		global $babBody;

		if (empty($_FILES)) {
			return;
		}
		$files = $_FILES;

		if (!is_dir($GLOBALS['babUploadPath'].'/tmp/')) {
			bab_mkdir($GLOBALS['babUploadPath'].'/tmp/');
		}

		$workspaceId = workspace_getCurrentWorkspace();
		$delegationPrefix = 'DG' . $workspaceId;
		$workspaceInfo = workspace_getWorkspaceInfo($workspaceId);
		$rootFolderName = workspace_getWorkspaceRootFolderName($workspaceId);
		$workspaceName = $workspaceInfo['name'];
		$path = $GLOBALS['babUploadPath'] . '/fileManager/collectives/' . $delegationPrefix . '/' . $rootFolderName;

		$wsFileSize = $workspaceInfo['file_size'] * 1024 * 1024;
		if ($wsFileSize == 0) {
			$wsFileSize = min(array($GLOBALS['babMaxTotalSize'], $GLOBALS['babMaxGroupSize']));
		}
		$sizeLeft = $wsFileSize - getDirSize($path);

        $fileSize = (int) $_FILES['file']['size']['file'];

		if ($fileSize > $sizeLeft) {
			$babBody->addError(workspace_translate('Upload failed, the max size of the folder is exceeded.'));
			workspace_redirect($this->proxy()->addFile($file['path'])->url());
		}

		if ($_FILES['file']['error']['file']) {
			$errorMessage = self::getUploadErrorMessage($_FILES['file']['error']['file']);
			$babBody->addError($errorMessage);
			workspace_redirect($this->proxy()->addFile($file['path'])->url());
		}

		$tmpdir = $GLOBALS['babUploadPath'].'/tmp/' . session_id();
		if (!is_dir($tmpdir)) {
			bab_mkdir($tmpdir);
		}

		$tmpfile = $tmpdir . '/' . bab_PathUtil::sanitizePathItem($_FILES['file']['name']['file']);


		if (!move_uploaded_file($_FILES['file']['tmp_name']['file'], $tmpfile)) {
			$babBody->addError(workspace_translate('Unable to move uploaded file.'));
			workspace_redirect($this->proxy()->addFile($file['path'])->url());
		}

		$currentDirectoryPath = $file['path'];
		$currentDirectory = new bab_Directory();

		$destPath = BAB_FileManagerEnv::getCollectivePath($workspaceId) . '/' . $currentDirectoryPath;
		$destPath = $GLOBALS['babUploadPath'] . '/fileManager/collectives/' . $currentDirectoryPath;

		$destPathname = $destPath . '/' . basename($tmpfile);

		if (file_exists($destPathname)) {
			return $this->handleDuplicateFile(basename($tmpfile), $currentDirectoryPath);
		}

		if (!$currentDirectory->importFile($tmpfile, $currentDirectoryPath)) {
			$babBody->addError(workspace_translate('There was a problem when trying to upload this file.'));
		}

		$workspaceInfo = workspace_getWorkspaceInfo(workspace_getCurrentWorkspace());

		$folderElements = explode('/', $currentDirectoryPath);
		array_shift($folderElements);
		$displayedFolderPath = implode('/', $folderElements);

		workspace_redirect($this->proxy()->browse($currentDirectoryPath)); // Problems with IE ??
	}





	/**
	 * Let the user choose what to do when uploading a file while a file with the
	 * same name is already present in the folder.
	 *
	 * @param string $sourceFilename
	 * @param string $destPath
	 */
	public function handleDuplicateFile($sourceFilename, $destPath)
	{
		$W = bab_Widgets();
		$page = workspace_Page();

		$workspaceId = workspace_getCurrentWorkspace();
		$versioning = workspace_getWorkspaceFileVersioning($workspaceId);

		$realDestPath = $GLOBALS['babUploadPath'] . '/fileManager/collectives/' . $destPath;
		$realDestPathname = $realDestPath . '/' . $sourceFilename;
		if (is_dir($realDestPathname)) {
			$pageTitle = sprintf(workspace_translate('A folder named %s already exists in this folder'), $sourceFilename);
		} else {
			$pageTitle = sprintf(workspace_translate('A file named %s already exists in this folder'), $sourceFilename);
		}

		$frame = new workspace_BaseForm('workspace_dialog');
		$frame->setName('configuration');
		$frame->addClass('workspace-dialog');

		$message =	$W->FlowItems(
			$W->Icon('', Func_Icons::STATUS_DIALOG_QUESTION),
			$W->VBoxItems(
				$W->Title($pageTitle, 2),
				$W->Title(workspace_translate('What would you like to do with this file?'), 3)
			)
		)
		->addClass('icon-left-48 icon-left icon-48x48')
		->setVerticalAlign('top');

		$frame->addItem($message);

		$menuLayout = $W->VBoxItems(
			$W->Link(
				workspace_translate('Cancel upload'),
				$this->proxy()->addFile($destPath)->url()
			)->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_CANCEL),
			$W->Link(
				workspace_translate('Upload with new name'),
				$this->proxy()->uploadWithNewName($sourceFilename, $destPath)->url()
			)->addClass('icon ' . Func_Icons::ACTIONS_EDIT_COPY)
		)
		->addClass('icon-left-24 icon-left icon-24x24')
		->setVerticalSpacing(1, 'em');

		if (workspace_canUpdateFileInFolder($destPath) && !is_dir($realDestPathname)) {
			$menuLayout->addItem(
				$W->Link(
					$W->Icon(workspace_translate('Replace the existing file'), Func_Icons::ACTIONS_VIEW_REFRESH),
					$this->proxy()->uploadAndReplace($sourceFilename, $destPath)->url()
				)
			);
		}
		if ($versioning && !is_dir($realDestPathname)) {
			$menuLayout->addItem(
				$W->Link(
					workspace_translate('Upload as a new version of the file'),
					$this->proxy()->uploadNewVersion($sourceFilename, $destPath)->url()
				)->addClass('icon ' . Func_Icons::ACTIONS_DOCUMENT_NEW)
			);
		}

		$frame->addItem($menuLayout);

		$page->addItem($frame);

		$page
			->setTitle($pageTitle)
			->addItemMenu('files', workspace_translate('Files'), '')
			->setCurrentItemMenu('files');
		$page->displayHtml();
	}




	/**
	 * Replace the file with the same name in the destination folder.
	 *
	 * @param string $sourceFilename Filename of the file to upload.
	 * @param string $destPath       Path to the destination folder (starting at DGxx/)
	 */
	public function uploadAndReplace($sourceFilename, $destPath)
	{
		global $babBody;

		$tmpdir = $GLOBALS['babUploadPath'].'/tmp/' . session_id();

		$oFolderFileSet = new BAB_FolderFileSet();

		$oPathName =& $oFolderFileSet->aField['sPathName'];
		$oName =& $oFolderFileSet->aField['sName'];
		$idDgOwner =& $oFolderFileSet->aField['iIdDgOwner'];

		$pathElements = explode('/', $destPath);

		$dg = array_shift($pathElements);
		$basepath = implode('/', $pathElements) . '/';

		$delegationId = workspace_getCurrentWorkspace();

		$oCriteria = $oPathName->in($basepath);
		$oCriteria = $oCriteria->_and($oName->in($sourceFilename));
		$oCriteria = $oCriteria->_and($idDgOwner->in($delegationId));
		$oFolderFile = $oFolderFileSet->get($oCriteria);
		if (is_null($oFolderFile)) {
			$babBody->addError(workspace_translate('There was a problem when trying to upload this file.'));
			workspace_redirect($this->proxy()->browse($destPath));
		}

		$isFileUpdated = saveUpdateFile(
			$oFolderFile->getId(), bab_fmFile::move($tmpdir . '/' . $sourceFilename), $sourceFilename, '', '', 'N','Y', false, false, ''
		);
		if (!$isFileUpdated) {
			$babBody->addError(workspace_translate('There was a problem when trying to upload this file.'));
			workspace_redirect($this->proxy()->browse($destPath));
		}

		workspace_redirect($this->proxy()->browse($destPath));
	}



	/**
	 * Upload the file and rename it automatically until there is no file with the same
	 * name in the destination folder.
	 *
	 * @param string $sourceFilename Filename of the file to upload.
	 * @param string $destPath       Path to the destination folder (starting at DGxx/)
	 */
	public function uploadWithNewName($sourceFilename, $destPath)
	{
		global $babBody;

		$fullDestPath = $GLOBALS['babUploadPath'] . '/fileManager/collectives/' . $destPath;

		$fullDestPathname = $fullDestPath . '/' . $sourceFilename;

		$sourceFilenameElements = explode('.', $sourceFilename);

		if (count($sourceFilenameElements) > 1) {
			$extension = '.' . array_pop($sourceFilenameElements);
			$sourceFileNameStart = implode('.', $sourceFilenameElements);
		} else {
			$extension = '';
			$sourceFileNameStart = $sourceFilename;
		}
		$fullDestPathnameStart = $fullDestPath . '/' . $sourceFileNameStart;

		$renameAttempts = 1;
		$suffix = '';
		while (file_exists($fullDestPathnameStart . $suffix . $extension)) {
			$suffix = '(' . $renameAttempts . ')';
			$renameAttempts++;
		}

		$tmpdir = $GLOBALS['babUploadPath'].'/tmp/' . session_id();

		if (!rename($tmpdir . '/' . $sourceFilename, $tmpdir . '/' . $sourceFileNameStart . $suffix . $extension)) {
			$babBody->addError(workspace_translate('There was a problem when trying to upload this file.'));
		} else {
			$currentDirectory = new bab_Directory();
			if (!$currentDirectory->importFile($tmpdir . '/' . $sourceFileNameStart . $suffix . $extension, $destPath)) {
				$babBody->addError(workspace_translate('There was a problem when trying to upload this file.'));
			}
		}
		workspace_redirect($this->proxy()->browse($destPath));
	}



	/**
	 * Upload the a new version of the file with the same name in the destination folder.
	 *
	 * @param string $sourceFilename Filename of the file to upload.
	 * @param string $destPath       Path to the destination folder (starting at DGxx/)
	 */
	public function uploadNewVersion($sourceFilename, $destPath)
	{
		global $babBody;

		require_once $GLOBALS['babInstallPath'] . 'utilit/fileincl.php';
		require_once $GLOBALS['babInstallPath'] . 'utilit/uploadincl.php';

		$workspaceId = workspace_getCurrentWorkspace();

		$pathElements = explode('/', $destPath);

		$dg = array_shift($pathElements);
		$basepath = implode('/', $pathElements) . '/';

		$oFolderFileSet = new BAB_FolderFileSet();

		$oState =& $oFolderFileSet->aField['sState'];
		$oPathName =& $oFolderFileSet->aField['sPathName'];
		$oName =& $oFolderFileSet->aField['sName'];
		$idDgOwner =& $oFolderFileSet->aField['iIdDgOwner'];

		$oCriteria = $oState->in('');
		$oCriteria = $oCriteria->_and($oPathName->in($basepath));
		$oCriteria = $oCriteria->_and($oName->in($sourceFilename));
		$oCriteria = $oCriteria->_and($idDgOwner->in($workspaceId));

		$oFolderFile = $oFolderFileSet->get($oCriteria);
		if (is_null($oFolderFile)) {
			$babBody->addError(workspace_translate('There was a problem when trying to upload this file.'));
		} else {
			$tmpdir = $GLOBALS['babUploadPath'].'/tmp/' . session_id();

			fm_lockFile($oFolderFile->getId(), '');
			fm_commitFile($oFolderFile->getId(), '', $oFolderFile->getMajorVer(), bab_fmFile::move($tmpdir . '/' . $sourceFilename));
		}

		workspace_redirect($this->proxy()->browse($destPath));
	}




	/**
	 * Displays the form to edit a folder's properties.
	 *
	 * @param string $pathname	The path of the folder. Eg.: 'DG1/Folder/Subfolder'
	 * @return Widget_Action
	 */
	public function editFolderProperties($pathname)
	{
		$page = workspace_Page();

		if (!workspace_canRenameFileInFolder($pathname)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You should be logged in to edit this file properties.'));
			throw new workspace_AccessException();
		}


		$editor = workspace_FolderPropertiesEditor();
		$editor->setHiddenValue('properties[pathname]', $pathname);
		$editor->setValue(array('properties', 'name'), basename($pathname));

		$page
			->setTitle(workspace_translate('Edit folder name'))
			->addItemMenu('files', workspace_translate('Files'), '')
			->setCurrentItemMenu('files')
			->addItem($editor);
		$page->displayHtml();
	}



	/**
	 * Displays the form to edit a file's properties.
	 *
	 * @param string $pathname	The file pathname. Eg.: 'DG1/My documents/text.odt'
	 * @return Widget_Action
	 */
	public function editFileProperties($pathname)
	{
		$page = workspace_Page();

		if (!workspace_canRenameFileInFolder($pathname)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You should be logged in to edit this file properties.'));
			throw new workspace_AccessException();
		}

		$editor = workspace_FilePropertiesEditor();
		$editor->setHiddenValue('properties[pathname]', $pathname);
		$editor->setValue(array('properties', 'name'), basename($pathname));

		$page
			->setTitle(workspace_translate('Edit filename'))
			->addItemMenu('files', workspace_translate('Files'), '')
			->setCurrentItemMenu('files')
			->addItem($editor);
		$page->displayHtml();

	}



	/**
	 * @return Widget_Action
	 */
	public function saveFolderProperties($properties = null)
	{
		global $babBody;

		$pathname = $properties['pathname'];
		$newName = bab_PathUtil::sanitizePathItem($properties['name']);
		$path = dirname($pathname);

		$babFileNameTranslationBackup = $GLOBALS['babFileNameTranslation'];
		$GLOBALS['babFileNameTranslation'] = array();

		if (!workspace_canRenameFileInFolder($path)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You should be logged in to edit this folder properties.'));
			throw new workspace_AccessException();
		}

		$directory = new bab_Directory();

		if (!$directory->renameSubDirectory($pathname, $path . '/' . $newName)) {
			$babBody->addError(workspace_translate('There was a problem when trying to rename this folder.'));
		}

		$GLOBALS['babFileNameTranslation'] = $babFileNameTranslationBackup;
		workspace_redirect($this->proxy()->browse($path));
	}



	/**
	 * @return Widget_Action
	 */
	public function saveFileProperties($properties = null)
	{
		global $babBody;


		$pathname = $properties['pathname'];
		$newName = bab_PathUtil::sanitizePathItem($properties['name']);
		$path = dirname($pathname);

		$babFileNameTranslationBackup = $GLOBALS['babFileNameTranslation'];
		$GLOBALS['babFileNameTranslation'] = array();

		if (!workspace_canRenameFileInFolder($path)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You should be logged in to edit this file properties.'));
			throw new workspace_AccessException();
		}

		$directory = new bab_Directory();

		if (!$directory->renameFile($pathname, $path . '/' . $newName)) {
			$babBody->addError(workspace_translate('There was a problem when trying to rename this file.'));
		}

		$GLOBALS['babFileNameTranslation'] = $babFileNameTranslationBackup;
		workspace_redirect($this->proxy()->browse($path));
	}



	/**
	 * Delete the specified folder.
	 *
	 * @param string $pathname	The folder pathname. Eg.: 'DG1/My documents/Folder1/SubFolder'
	 * @return Widget_Action
	 */
	public function deleteFolder($pathname)
	{
		global $babBody;

		$path = dirname($pathname);

		$babFileNameTranslationBackup = $GLOBALS['babFileNameTranslation'];
		$GLOBALS['babFileNameTranslation'] = array();

		if (!workspace_canDeleteFolderInFolder($path)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You should be logged in to delete this folder.'));
			throw new workspace_AccessException();
		}

		$directory = new bab_Directory();

		if (!$directory->deleteSubDirectory($pathname)) {
			$babBody->addError(workspace_translate('There was a problem when trying to delete this folder.'));
		}

		$GLOBALS['babFileNameTranslation'] = $babFileNameTranslationBackup;
		workspace_redirect($this->proxy()->browse($path));
	}



    /**
     * Moves the specified file to the trash.
     *
     * @param string $pathname	 The file pathname. Eg.: 'DG1/My documents/text.odt'
     * @param bool   $definitive false = move to trash, true = delete
     *
     * @return Widget_Action
     */
    public function deleteFile($pathname, $definitive = false)
    {
        global $babBody;

        $path = dirname($pathname);

        $babFileNameTranslationBackup = $GLOBALS['babFileNameTranslation'];
        $GLOBALS['babFileNameTranslation'] = array();

        if (!workspace_canDeleteFileInFolder($path)) {
            bab_userIsloggedin() || bab_requireCredential(workspace_translate('You should be logged in to delete this file.'));
            throw new workspace_AccessException();
        }

        if ($definitive && workspace_userIsWorkspaceAdministrator()) {
            $directory = new bab_Directory();
            if (!$directory->deleteFile($pathname)) {
                $babBody->addError(workspace_translate('There was a problem when trying to delete this file.'));
            }
        } else {
            if (!workspace_moveFileToTrash($pathname)) {
                $babBody->addError(workspace_translate('There was a problem when trying to put this file to the trash.'));
            }
        }

        $GLOBALS['babFileNameTranslation'] = $babFileNameTranslationBackup;
        workspace_redirect($this->proxy()->browse($path));
    }




	/**
	 * Removes the specified file from the trash and put it baxk to its original folder.
	 *
	 * @param string $pathname	The file pathname. Eg.: 'DG1/My documents/text.odt'
	 *
	 * @return Widget_Action
	 */
	public function undeleteFile($pathname)
	{
		global $babBody;

		$path = dirname($pathname);


		$babFileNameTranslationBackup = $GLOBALS['babFileNameTranslation'];
		$GLOBALS['babFileNameTranslation'] = array();

		if (!workspace_canDeleteFileInFolder($path)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You should be logged in to delete this file.'));
			throw new workspace_AccessException();
		}

		if (!workspace_removeFileFromTrash($pathname)) {
			$babBody->addError(workspace_translate('There was a problem when trying to restore this file from the trash.'));
		}

		$GLOBALS['babFileNameTranslation'] = $babFileNameTranslationBackup;
		workspace_redirect($this->proxy()->browse($path));
	}

	/**
	 * Move a file.
	 *
	 * @param string	$sourcePathname		The source file pathname. Eg.: 'DG1/My documents/text.odt'
	 * @param string	$destPath			The destination folder path. Eg.: 'DG1/My documents/Backup/text.odt'
	 * @return Widget_Action
	 */
	public function moveFile($sourcePathname, $destPath)
	{
		global $babBody;

		$workspaceId = workspace_getCurrentWorkspace();

		$trashPath = workspace_getWorkspaceTrashPath($workspaceId);

		if ($destPath === $trashPath) {
			return $this->deleteFile($sourcePathname);
		}


		$babFileNameTranslationBackup = $GLOBALS['babFileNameTranslation'];
		$GLOBALS['babFileNameTranslation'] = array();

		$path = dirname($sourcePathname);
		$filename = basename($sourcePathname);
		$destPathname = $destPath . '/' . $filename;

		if (!workspace_canMoveFile(dirname($sourcePathname), $destPath)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You should be logged in to move this file.'));
			$GLOBALS['babFileNameTranslation'] = $babFileNameTranslationBackup;
			throw new workspace_AccessException();
		}

		$directory = new bab_Directory();

		if (!$directory->renameFile($sourcePathname, $destPathname)) {
			$babBody->addError(workspace_translate('There was a problem when trying to move this file.'));
		}

		$GLOBALS['babFileNameTranslation'] = $babFileNameTranslationBackup;
		workspace_redirect($this->proxy()->browse($path));
	}



	/**
	 * Move a folder.
	 *
	 * @param string	$sourcePathname		The source folder pathname. Eg.: 'DG1/My documents/Folder1/SubFolder'
	 * @param string	$destPath			The destination folder path. Eg.: 'DG1/My documents/Folder2'
	 * @return Widget_Action
	 */
	public function moveFolder($sourcePathname, $destPath)
	{
		global $babBody;

		$path = dirname($sourcePathname);
		$filename = basename($sourcePathname);
		$destPathname = $destPath . '/' . $filename;

		$babFileNameTranslationBackup = $GLOBALS['babFileNameTranslation'];
		$GLOBALS['babFileNameTranslation'] = array();

		if (!workspace_canMoveFolder(dirname($sourcePathname), $destPath)) {
			bab_userIsloggedin() || bab_requireCredential(workspace_translate('You should be logged in to move this folder.'));
			$GLOBALS['babFileNameTranslation'] = $babFileNameTranslationBackup;
			throw new workspace_AccessException();
		}

		$directory = new bab_Directory();

		if (!$directory->renameSubDirectory($sourcePathname, $destPathname)) {
			$babBody->addError(workspace_translate('There was a problem when trying to move this folder.'));
		}

		$GLOBALS['babFileNameTranslation'] = $babFileNameTranslationBackup;
		workspace_redirect($this->proxy()->browse($path));
	}



	/**
	 * Does nothing and return to the previous page.
	 *
	 * @return Widget_Action
	 */
	public function cancel()
	{
		$controller = new workspace_Controller();
		$action = workspace_BreadCrumbs::last();
		$action = Widget_Action::fromUrl($action);
		$controller->execute($action);
//		workspace_redirect(workspace_BreadCrumbs::last());
	}



	/**
	 * Displays help on this page.
	 *
	 * @return Widget_Action
	 */
	public function help()
	{

	}
}
