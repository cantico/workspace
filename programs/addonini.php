;<?php/*

[general]
name="workspace"
version="1.2.80"
addon_type="EXTENSION"
encoding="ISO-8859-15"
mysql_character_set_database="latin1,utf8"
description="Streamlined collaborative workspace"
description.fr="Espace collaboratif"
long_description.fr="README.md"
delete=1
ov_version="8.6.0"
php_version="5.1.0"
addon_access_control="1"
author="Laurent Choulette (laurent.choulette@cantico.fr)"
icon="workspace48.png"


[addons]
widgets             ="1.0.79"
jquery              ="1.3.2"
LibFileManagement   ="0.2.14"
LibTranslate        ="1.12.0.03"
LibOrm              ="0.11.8"
portlets            ="0.23.10"
LibTimer            ="1.1.4"
libapp              ="0.0.1"


[functionalities]
Thumbnailer="Available"
jquery="Available"

; */?>
