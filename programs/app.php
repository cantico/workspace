<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

$addon_workspace = bab_getAddonInfosInstance('workspace');

if (!$addon_workspace) {
    return;
}

define('WORKSPACE_PHP_PATH', $addon_workspace->getPhpPath());
define('WORKSPACE_CTRL_PATH', WORKSPACE_PHP_PATH);
define('WORKSPACE_SET_PATH', WORKSPACE_PHP_PATH.'set/');
define('WORKSPACE_UI_PATH', WORKSPACE_PHP_PATH);
bab_functionality::includeFile('App');




/**
 * Func_App_Directorymanager
 */
class Func_App_Workspace extends Func_App
{
    
    public function __construct()
    {
        parent::__construct();
        
        $this->addonPrefix = 'workspace';
        $this->addonName = 'workspace';
        $this->classPrefix = $this->addonPrefix . '_';
        $this->controllerTg = 'addon/workspace/main';
    }
    
    public function getUiPath()
    {
        return WORKSPACE_UI_PATH;
    }
    
    public function getSetPath()
    {
        return WORKSPACE_SET_PATH;
    }
    
    public function getPhpPath()
    {
        return WORKSPACE_PHP_PATH;
    }
       
    
    /**
     * @return string
     *
     */
    public function getDescription()
    {
        return 'Workspace App';
    }
    
    
    function setTranslateLanguage($language)
    {
        parent::setTranslateLanguage($language);
        $translate = bab_functionality::get('Translate/Gettext');
        /* @var $translate Func_Translate_Gettext */
        $translate->setLanguage($language);
    }
    
    
    /**
     * Translates the string.
     *
     * @param string $str
     * @return string
     */
    function translate($str, $str_plurals = null, $number = null)
    {
        require_once WORKSPACE_PHP_PATH . 'functions.php';
        $translation = $str;
        if ($translate = bab_functionality::get('Translate/Gettext')) {
            /* @var $translate Func_Translate_Gettext */
            $translate->setAddonName('workspace');
            $translation = $translate->translate($str, $str_plurals, $number);
        }
        if ($translation === $str) {
            $translation = parent::translate($str, $str_plurals, $number);
        }
        
        return $translation;
    }
    
    
    /**
     * Includes Controller class definition.
     */
    public function includeController()
    {
        parent::includeController();
        require_once WORKSPACE_PHP_PATH . '/workspace.ctrl.php';
    }
    
    public function includeRecordController()
    {
        parent::includeRecordController();
        require_once WORKSPACE_PHP_PATH . '/record.ctrl.php';
    }
    
    /**
     * Instanciates the controller.
     *
     * @return workspace_Controller
     */
    public function Controller()
    {
        $this->includeController();
        return bab_getInstance($this->classPrefix.'Controller')->setApp($this);
    }
    
    
    /**
     * Include class workspace_Access
     */
    protected function includeAccess()
    {
        parent::includeAccess();
        require_once WORKSPACE_PHP_PATH . '/access.class.php';
    }
    
    
    
    /**
     * Include class workspace_Ui
     */
    public function includeUi()
    {
        parent::includeUi();
        require_once WORKSPACE_UI_PATH . '/ui.class.php';
    }
    
    /**
     * The workspace_Ui object propose an access to all ui files and ui objects (widgets)
     *
     * @return workspace_Ui
     */
    public function Ui()
    {
        $this->includeUi();
        return bab_getInstance('workspace_Ui');
    }
    
    public function getWorkspaceAddons()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
        $func = new bab_functionalities();
        return $func->getChildren('WorkspaceAddon');
    }
    
    /**
     * SET DEFINITION
     */
    
    //Workspace
    
    /**
     * Includes WorkspaceSet class definition.
     */
    public function includeWorkspaceSet()
    {
        require_once WORKSPACE_SET_PATH . 'workspace.class.php';
    }
    
    /**
     * @return string
     */
    public function WorkspaceClassName()
    {
        return 'workspace_Workspace';
    }
    
    /**
     * @return string
     */
    public function WorkspaceSetClassName()
    {
        return $this->WorkspaceClassName() . 'Set';
    }
    
    /**
     * @return workspace_WorkspaceSet
     */
    public function WorkspaceSet()
    {
        $this->includeWorkspaceSet();
        $className = $this->WorkspaceSetClassName();
        $set = new $className($this);
        return $set;
    }
    
    //Category
    
    /**
     * Includes CategorySet class definition.
     */
    public function includeCategorySet()
    {
        require_once WORKSPACE_SET_PATH . 'category.class.php';
    }
    
    /**
     * @return string
     */
    public function CategoryClassName()
    {
        return 'workspace_Category';
    }
    
    /**
     * @return string
     */
    public function CategorySetClassName()
    {
        return $this->CategoryClassName() . 'Set';
    }
    
    /**
     * @return workspace_CategorySet
     */
    public function CategorySet()
    {
        $this->includeCategorySet();
        $className = $this->CategorySetClassName();
        $set = new $className($this);
        return $set;
    }
    
    
    
    //Link
    
    /**
     * Includes LinkSet class definition.
     */
    public function includeLinkSet()
    {
        require_once WORKSPACE_SET_PATH . 'link.class.php';
    }
    
    /**
     * @return string
     */
    public function LinkClassName()
    {
        return 'workspace_Link';
    }
    
    /**
     * @return string
     */
    public function LinkSetClassName()
    {
        return $this->LinkClassName() . 'Set';
    }
    
    /**
     * @return workspace_LinkSet
     */
    public function LinkSet()
    {
        $this->includeLinkSet();
        $className = $this->LinkSetClassName();
        $set = new $className($this);
        return $set;
    }
    
    //Tag
    
    /**
     * Includes TagSet class definition.
     */
    public function includeTagSet()
    {
        require_once WORKSPACE_SET_PATH . 'tag.class.php';
    }
    
    /**
     * @return string
     */
    public function TagClassName()
    {
        return 'workspace_Tag';
    }
    
    /**
     * @return string
     */
    public function TagSetClassName()
    {
        return $this->TagClassName() . 'Set';
    }
    
    /**
     * @return workspace_TagSet
     */
    public function TagSet()
    {
        $this->includeTagSet();
        $className = $this->TagSetClassName();
        $set = new $className($this);
        return $set;
    }
}

