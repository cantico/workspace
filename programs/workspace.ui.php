<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


$W = bab_Widgets();

$W->includePhpClass('Widget_TableModelView');



/**
 *
 *
 * workspace_WorkspaceTableView
 */
class workspace_WorkspaceTableView extends widget_TableModelView
{
    /**
     * @var Func_App_Workspace
     */
    private $app = null;


    /**
     * @var workspace_CtrlRecord
     */
    protected $recordController = null;


    /**
     * @param Func_App_Workspace $app
     * @return workspace_WorkspaceTableView
     */
    public function setApp(Func_App_Workspace $app = null)
    {
        $this->app = $app;
        return $this;
    }

    /**
     * @return Func_App_Workspace
     */
    public function App()
    {
        return $this->app;
    }

    /**
     * @param Func_App_Workspace $app
     * @param string $id
     */
    public function __construct(Func_App_Workspace $app = null, $id = null)
    {
        parent::__construct($id);
        $this->setApp($app);
    }



    /**
     * @param workspace_CtrlRecord $recordController
     * @return workspace_WorkspaceTableView
     */
    public function setRecordController(workspace_CtrlRecord $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }


    /**
     * @return workspace_CtrlRecord
     */
    public function getRecordController()
    {
        return $this->recordController;
    }

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $recordSet)
    {
        $App = $this->App();

        $this->addColumn(
            widget_TableModelViewColumn('image', ' ')
                ->setSelectable(true, $App->translate('Color / Image'))
                ->addClass('widget-align-center')
        );
        $this->addColumn(
            widget_TableModelViewColumn($recordSet->name, $App->translate('Name'))
                ->setSearchable(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn($recordSet->description, $App->translate('Description'))
                ->setSearchable(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn('_tags_', $App->translate('Tags'))
                ->setSearchable(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn($recordSet->createdOn, $App->translate('Creation date'))
                ->setSearchable(false)
                ->addClass('widget-10em', 'widget-align-center')
        );
        $this->addColumn(
            widget_TableModelViewColumn($recordSet->lastActivity, $App->translate('Last activity'))
                ->setSearchable(false)
                ->addClass('widget-10em', 'widget-align-center')
        );
        $this->addColumn(
            widget_TableModelViewColumn($recordSet->archivedDate, $App->translate('Archived date'))
            ->setSearchable(false)
            ->addClass('widget-10em', 'widget-align-center')
        );
//         $this->addColumn(
//             widget_TableModelViewColumn($recordSet->isArchived(), $App->translate('Is archived'))
//             ->setSearchable(false)
//             ->addClass('widget-10em', 'widget-align-center')
//         );
        $this->addColumn(
            widget_TableModelViewColumn('_actions_', ' ')
            ->setSelectable(true, $App->translate('[Menu]'))
            ->setExportable(false)
            ->setSortable(false)
            ->addClass('widget-column-thin', 'widget-column-center')
        );
    }



    protected function getSearchItem()
    {
        $W = bab_Widgets();

        return $W->LineEdit()->setName('search')->addClass('widget-100pc');
    }


    protected function initRow(ORM_Record $record, $row)
    {
        $this->addRowClass($row, 'widget-actions-target');

        return parent::initRow($record, $row);
    }

    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();
        $App = workspace_App();

        switch ($fieldPath) {
            case 'image':
                return $W->Label(' ')->setCanvasOptions(
                Widget_Item::options()
                    ->backgroundColor('#' . $record->color)
                    ->width(20, 'px')
                    ->height(20, 'px')

                )->addClass('badge');
            case 'name':
                return $W->Link(
                    $record->name,
                    $App->Controller()->Page()->display('home', $record->id)
                );
            case 'isArchived':
                $label = $record->isArchived() ? workspace_translate('Yes') : workspace_translate('No');
                return $W->Label($label);

            case '_tags_':
                $box = $W->FlowItems()->addClass(Func_Icons::ICON_LEFT_16);
                $tags = $record->getTags();
                foreach ($tags as $tag) {
                    $box->addItem(
                        $W->Label($tag->label)->addClass('badge')
                    );
                }
                $box->addItem(
                    $W->Link(
                        '',
                        $App->Controller()->Tag()->editWorkspaceTags($record->id)
                    )->addClass('icon', Func_Icons::ACTIONS_LIST_ADD)
                    ->setSizePolicy('widget-actions')
                    ->setTitle($App->translate('Edit'))
                    ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                );
                return $box;

            case '_actions_':

                $menu = $W->Menu(null, $W->VBoxItems()->addClass(Func_Icons::ICON_LEFT_16));
                $menu->addItem(
                    $W->Link(
                        $App->translate('Archive...'),
                        $App->Controller()->Admin()->edit($record->id)
                    )->addClass('icon', Func_Icons::ACTIONS_ARCHIVE_CREATE)
                );
                $menu->setButtonClass('icon ' . Func_Icons::ACTIONS_CONTEXT_MENU);
                 $menu->setButtonLabel(' ');
                $actionsBox = $W->FlowItems(
                    $W->Link(
                        '',
                        $App->Controller()->Admin()->edit($record->id)
                    )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                    ->setTitle($App->translate('Edit'))
                    ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD),
                    $menu
//                     ,
//                     $W->Link(
//                         '',
//                         $App->Controller()->Admin()->edit($record->id)
//                     )->addClass('icon', Func_Icons::ACTIONS_CONTEXT_MENU)
//                     ->setTitle($App->translate('Edit'))
                );
                $actionsBox->addClass('widget-actions', 'widget-nowrap', Func_Icons::ICON_LEFT_16);
                return $actionsBox;
        }

        return parent::computeCellContent($record, $fieldPath);
    }



    /**
     * Get an advanced form filter
     * @see Widget_Filter
     *
     * @param   string          $id
     * @param   array           $filter
     *
     * @return Widget_Form
     */
    public function getAdvancedFilterForm($id = null, $filter = null)
    {
        $App = $this->App();

        $W = bab_Widgets();

        $formItem = $this->getSearchItem();

        $activeFiltersFrame = $W->Frame(
            null,
            $W->FlowItems(
//                $formItem
            )
            ->setHorizontalSpacing(2, 'em')
            ->setVerticalAlign('bottom')
        );

        $columns = $this->getVisibleColumns();

        foreach ($columns as $fieldName => $column) {
            $field = $column->getField();
            if (! $column->isSearchable()) {
                continue;
            }

            if (! ($field instanceof ORM_Field)) {
                $field = null;
            }

            $label = $this->handleFilterLabelWidget($fieldName, $field);
            $input = $this->handleFilterInputWidget($fieldName, $field);


            if (isset($input) && isset($label)) {

                $input->setName($fieldName);
                $label->setAssociatedWidget($input);

                $input->addClass('widget-100pc');

                $formItem = $this->handleFilterLabel($label, $input);
                $formItem->setSizePolicy('col-lg-2 col-md-3 col-sm-6 col-xs-12');
                $formItem->addClass('field_' . $fieldName);

                $activeFiltersFrame->addItem($formItem);
            }
        }

        $activeFiltersFrame->addItem(
            $W->LabelledWidget(
                $App->translate('Tags'),
                $App->Ui()->SuggestTag()
                    ->addClass('form-control')
            )->setName('tags')
        );

        $lastActivitySpecify = $W->PeriodPicker()->setName('lastActivityPeriod');

        $activeFiltersFrame->addItem(
            $W->LabelledWidget(
                $App->translate('Last activity'),
                $W->Select()
                    ->addClass('form-control')
                    ->addOption('', '')
                    ->addOption('month', $App->translate('More than one month ago'))
                    ->addOption('year', $App->translate('More than one year ago'))
                    ->addOption('specify', $App->translate('Specify period...'))
                    ->setAssociatedDisplayable($lastActivitySpecify, array('specify'))
            )->setName('lastActivity')
        );
        $activeFiltersFrame->addItem($lastActivitySpecify);

        $createdOnSpecify = $W->PeriodPicker()->setName('createdOnPeriod');

        $activeFiltersFrame->addItem(
            $W->LabelledWidget(
                $App->translate('Creation date'),
                $W->Select()
                ->addClass('form-control')
                    ->addOption('', '')
                    ->addOption('month', $App->translate('More than one month ago'))
                    ->addOption('year', $App->translate('More than one year ago'))
                    ->addOption('specify', $App->translate('Specify period...'))
                    ->setAssociatedDisplayable($createdOnSpecify, array('specify'))
            )->setName('createdOn')
        );
        $activeFiltersFrame->addItem($createdOnSpecify);

        $activeFiltersFrame->addItem(
            $W->LabelledWidget(
                $App->translate('Type'),
                $W->Select()
                    ->addClass('form-control')
                    ->addOption('', '')
                    ->addOption('active', $App->translate('Active'))
                    ->addOption('archived', $App->translate('Archived'))
            )->setName('type')
        );

        if (! $this->submit) {
            $this->submit = $W->SubmitButton();
            $this->submit->setLabel($App->translate('Apply filters'));
        }

        $activeFiltersFrame->addItem($this->submit);

        $form = $W->Form($id);
        $form->setReadOnly(true);
        $form->setName('filter');
        $form->colon();

        $form->addItem(
            $activeFiltersFrame
            ->setSizePolicy('row')
        );

        $form->setHiddenValue('tg', $App->controllerTg);
        $form->setAnchor($this->getAnchor());
        $form->addClass(Func_Icons::ICON_LEFT_16);

        return $form;
    }


    /**
     * Get a advanced filter panel
     * Use setPageLength to define the default number of items per page
     *
     * @param	array	$filter		Filter values
     * @param	string	$name		filter form name
     * @param	string	$anchor		anchor in destination page of filter, the can be a Widget_Tab id
     *
     * @return Widget_Filter
     */
    public function advancedFilterPanel($filter = null, $name = null)
    {
        $W = bab_Widgets();

        $filterPanel = $W->Filter();
        $filterPanel->setLayout($W->VBoxLayout());
        if (isset($name)) {
            $filterPanel->setName($name);
        }

        $pageLength = $this->getPageLength();
        if (null === $pageLength) {
            $pageLength = 15;
        }

        $pageNumber = $this->getCurrentPage();
        if (null === $pageNumber) {
            $pageNumber = 0;
        }

        $this->setPageLength(isset($filter['pageSize']) ? $filter['pageSize'] : $pageLength);
        //        $this->setCurrentPage(isset($filter['pageNumber']) ? $filter['pageNumber'] : $pageNumber);

        if (isset($name)) {
            $this->sortParameterName = $name . '[filter][sort]';
        } else {
            $this->sortParameterName = 'filter[sort]';
        }

        if (isset($filter['sort'])) {
            $this->setSortField($filter['sort']);
        } elseif (!isset($this->sortField)) {

            if (method_exists($this, 'getDefaultSortField'))
            {
                $this->setSortField($this->getDefaultSortField());
            } else {
                $columns = $this->getVisibleColumns();
                list($sortField) = each($columns);
                $this->setSortField($sortField);
            }
        }

        $form = $this->getAdvancedFilterForm(null, $filter);

        if (isset($filter)) {
            if (isset($name)) {
                $path = array($name, 'filter');
            } else {
                $path = array('filter');
            }
            $form->setValues($filter, $path);
        }

        $filterPanel->setFilter($form);
        $filterPanel->setFiltered($this);

        return $filterPanel;
    }

    public function getFilterCriteria($filter = null, workspace_WorkspaceSet $workspaceSet = null)
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

        $W = bab_Widgets();
        $now = BAB_DateTime::now();

        if (!isset($workspaceSet)) {
            $workspaceSet = $this->getRecordSet();
        }
        // Initial conditions are base on read access rights.
        //         $conditions = $Access->canPerformActionOnSet($workspaceSet, 'workspace:read');
        $conditions = array();
        $conditions[] = $workspaceSet->delegation->isNot(0);

        if(isset($filter['tags_ID']) && !empty($filter['tags_ID'])){
            $App = $this->App();
            $linkSet = $App->LinkSet();
            $links = $linkSet->select(
                $linkSet->sourceClass->is($App->WorkspaceClassName())
                ->_AND_($linkSet->targetClass->is($App->TagClassName()))
                ->_AND_($linkSet->targetId->is($filter['tags_ID']))
            );
            $workspaceIds = array();
            foreach ($links as $link){
                if(!in_array($link->sourceId, $workspaceIds)){
                    $workspaceIds[] = $link->sourceId;
                }
            }
            $conditions[] = $workspaceSet->id->in($workspaceIds);
        }
        if (isset($filter['type']) && ! empty($filter['type'])) {
            switch ($filter['type']) {
                case 'archived':
                    $conditions[] = $workspaceSet->isArchived();
                    break;
                case 'active':
                    $conditions[] = $workspaceSet->isArchived()->_not();
                    break;
            }
        }
        if(isset($filter['createdOn']) && !empty($filter['createdOn'])){
            switch($filter['createdOn']){
                case 'month':
                    $month = $now->add(-1, BAB_DATETIME_MONTH);
                    $conditions[] = $workspaceSet->createdOn->lessThanOrEqual($month->getIsoDate());
                    break;
                case 'year':
                    $year = $now->add(-1, BAB_DATETIME_YEAR);
                    $conditions[] = $workspaceSet->createdOn->lessThanOrEqual($year->getIsoDate());
                    break;
                case 'specify':
                    if(isset($filter['createdOnPeriod']['from']) && !empty($filter['createdOnPeriod']['from'])){
                        $createdOnFrom = $W->DatePicker()->getISODate($filter['createdOnPeriod']['from']);
                        if($createdOnFrom){
                            $conditions[] = $workspaceSet->createdOn->greaterThanOrEqual($createdOnFrom);
                        }
                    }
                    if(isset($filter['createdOnPeriod']['to']) && !empty($filter['createdOnPeriod']['to'])){
                        $createdOnTo = $W->DatePicker()->getISODate($filter['createdOnPeriod']['to']);
                        if($createdOnTo){
                            $conditions[] = $workspaceSet->createdOn->lessThanOrEqual($createdOnTo);
                        }
                    }
                    break;
            }
        }
        if(isset($filter['lastActivity']) && !empty($filter['lastActivity'])){
            switch($filter['lastActivity']){
                case 'month':
                    $month = $now->add(-1, BAB_DATETIME_MONTH);
                    $conditions[] = $workspaceSet->lastActivity->lessThanOrEqual($month->getIsoDateTime());
                    break;
                case 'year':
                    $year = $now->add(-1, BAB_DATETIME_YEAR);
                    $conditions[] = $workspaceSet->lastActivity->lessThanOrEqual($year->getIsoDateTime());
                    break;
                case 'specify':
                    if(isset($filter['lastActivityPeriod']['from']) && !empty($filter['lastActivityPeriod']['from'])){
                        $lastActivityFrom = $W->DatePicker()->getISODate($filter['lastActivityPeriod']['from']);
                        if($lastActivityFrom){
                            $lastActivityFrom.=' 00:00:00';
                            $conditions[] = $workspaceSet->lastActivity->greaterThanOrEqual($lastActivityFrom);
                        }
                    }
                    if(isset($filter['lastActivityPeriod']['to']) && !empty($filter['lastActivityPeriod']['to'])){
                        $lastActivityTo = $W->DatePicker()->getISODate($filter['lastActivityPeriod']['to']);
                        if($lastActivityTo){
                            $lastActivityTo.=' 23:59:59';
                            $conditions[] = $workspaceSet->lastActivity->lessThanOrEqual($lastActivityTo);
                        }
                    }
                    break;
            }
        }

        return $workspaceSet->all($conditions);
    }
}


/**
 *
 * @method Func_Crm_Lcrm Crm()
 */
class workspace_WorkspaceCardsView extends workspace_WorkspaceTableView
{
    /**
     * @param Func_App_Workspace $app
     * @param string $id
     */
    public function __construct(Func_App_Workspace $app = null, $id = null)
    {
        parent::__construct($app, $id);

        $W = bab_Widgets();
        $layout = $W->FlowLayout();
        $layout->setVerticalAlign('top');
        $layout->setSpacing(1, 'em');
        $this->setLayout($layout);
    }

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::initHeaderRow()
     */
    protected function initHeaderRow(ORM_RecordSet $set)
    {
        return;
    }

    /**
     * {@inheritDoc}
     * @see Widget_TableView::addSection()
     */
    public function addSection($id = null, $label = null, $class = null, $colspan = null)
    {
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see Widget_TableView::setCurrentSection()
     */
    public function setCurrentSection($id)
    {
        return $this;
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleRow()
     */
    protected function handleRow(ORM_Record $record, $row)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $card = $Ui->WorkspaceCardFrame($record);
        $card->addClass(/*'col-md-3', */'crm-card');
        $card->setSizePolicy('col-xs-12 col-sm-6 col-md-4');
        $this->addItem($card);

        return $row;
    }
}



/**
 *
 * @method Func_Crm_Lcrm Crm()
 */
class workspace_WorkspaceCardFrame extends Widget_Frame
{
    /**
     * @var workspace_WorkspaceSet
     */
    protected $recordSet = null;

    /**
     * @var workspace_Workspace
     */
    protected $record = null;

    /**
     * @var Func_App_Workspace
     */
    private $app = null;


    /**
     * @param Func_Ap_pWorkspace $app
     * @return workspace_WorkspaceTableView
     */
    public function setApp(Func_App_Workspace $app = null)
    {
        $this->app = $app;
        return $this;
    }

    /**
     * @return Func_App_Workspace
     */
    public function App()
    {
        return $this->app;
    }

    /**
     * @param Func_App_Workspace $app
     * @param crm_Record $record
     * @param string $id
     */
    public function __construct(Func_App_Workspace $app, ORM_Record $record, $id = null)
    {
        $W = bab_Widgets();
        $cardLayout = $W->VBoxLayout();//->setHorizontalSpacing(1, 'em');
        parent::__construct($id, $cardLayout);

        $this->setApp($app);
        $app->includeWorkspaceSet();

        $this->record = $record;
        $this->recordSet = $record->getParentSet();


        $cardLayout->addItem(
            $W->FlowItems(
                $W->Label(' ')->setCanvasOptions(
                    Widget_Item::options()
                    ->backgroundColor('#' . $record->color)
                    ->width(15, 'px')
                    ->height(15, 'px')
                )->addClass('badge')
                ->setSizePolicy('pull-left'),
                $W->Link(
                    $record->name,
                    $app->Controller()->Admin()->edit($record->id)
                )->addClass('crm-card-title', 'widget-strong')
                ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            )->setHorizontalSpacing(0.5, 'em')
            ->setVerticalAlign('middle')
        );
        $cardLayout->addItem(
            $W->Label($record->description)
        );

        $tagsBox = $W->FlowItems()->addClass(Func_Icons::ICON_LEFT_16);
        if (!empty($record->tags)) {
            foreach (explode(', ', $record->tags) as $tag) {
                $tagsBox->addItem(
                    $W->Label($tag)->addClass('badge')
                );
            }
        }
        $tagsBox->addItem(
            $W->Link(
                '',
                $app->Controller()->Admin()->edit($record->id)
            )->addClass('icon', Func_Icons::ACTIONS_LIST_ADD)
            ->setSizePolicy('widget-actions')
            ->setTitle($app->translate('Edit'))
            ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );
        $cardLayout->addItem(
            $tagsBox
        );
    }
}








$W->includePhpClass('Widget_Form');


class workspace_Editor extends Widget_Form
{
    protected $ctrl;

    protected $saveAction;
    protected $saveLabel;

    protected $cancelAction;
    protected $cancelLabel;

    protected $buttonsLayout;
    protected $innerLayout;

    protected $failedAction = null;
    protected $successAction = null;

    /**
     * @var Widget_Section[] $sections
     */
    protected $sections = array();


    public $isAjax = false;

    /**
     * @var ORM_Record
     */
    protected $record = null;

    /**
     * @var Func_App_Workspace
     */
    private $app = null;


    /**
     * @param Func_App_Workspace $app
     * @return seld
     */
    public function setApp(Func_App_Workspace $app = null)
    {
        $this->app = $app;
        return $this;
    }

    /**
     * @return Func_App_Workspace
     */
    public function App()
    {
        return $this->app;
    }

    /**
     * @param	Func_Crm		$crm
     * @param 	Widget_Layout 	$layout		The layout that will manage how widgets are displayed in this form.
     * @param 	string 			$id			The item unique id.
     */
    public function __construct(Func_App_Workspace $app, $id = null, Widget_Layout $layout = null)
    {
        $W = bab_Widgets();

        // We simulate inheritance from Widget_VBoxLayout.

        $this->setApp($app);

        $this->innerLayout = $layout;
        $this->buttonsLayout = $this->buttonsLayout();
        $this->buttonsLayout->setSizePolicy('widget-list-element');

        if (!isset($this->innerLayout)) {
            $this->innerLayout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        }

        $this->innerLayout->addClass('crm-loading-box'); // javascript can add the crm-loading-on class to this container to show ajax loading progress in the form

        $layout = $W->VBoxItems(
            $this->innerLayout,
            $this->buttonsLayout
        );
        $layout->setVerticalSpacing(2, 'em');

        parent::__construct($id, $layout);
        $this->setHiddenValue('tg', $app->controllerTg);
        $this->setFormStyles();
        $this->prependFields();

        $this->addClass(Func_Icons::ICON_LEFT_16);
    }


    /**
     * @return Widget_Layout
     */
    protected function buttonsLayout()
    {
        $W = bab_Widgets();
        return $W->HBoxLayout()->setHorizontalSpacing(1, 'em');
    }



    protected function setFormStyles()
    {
//        $this->addClass('crm-editor');
    }


    /**
     * Set record and values to editor
     * @see crm_Editor::getRecord()
     *
     * @param crm_Record $record
     *
     * @return self
     */
    public function setRecord(ORM_Record $record)
    {
        $this->record = $record;
        if ($name = $this->getName()) {
            $values = $record->getFormOutputValues();
            $this->setValues($values, array($name));
        }

        return $this;
    }

    /**
     * @return crm_Record
     */
    protected function getRecord()
    {
        return $this->record;
    }


    /**
     * @return crm_RecordSet
     */
    protected function getRecordSet()
    {
        if ($this->record) {
            return $this->record->getParentSet();
        }
        return null;
    }


//     public function setValues($row, $namePathBase = array())
//     {
//         return true;
//     }

    /**
     *
     * @return self
     */
    public function setController($ctrl)
    {
        $this->ctrl = $ctrl;
        return $this;
    }


    /**
     *
     * @return self
     */
    public function setSaveAction($saveAction, $saveLabel = null)
    {
        $this->saveAction = $saveAction;
        $this->saveLabel = $saveLabel;
        return $this;
    }

    public function setSuccessAction($successAction)
    {
        $this->successAction = $successAction;
        return $this;
    }

    public function setFailedAction($failedAction)
    {
        $this->failedAction = $failedAction;
        return $this;
    }

    /**
     *
     * @return crm_Editor
     */
    public function setCancelAction($cancelAction, $cancelLabel = null)
    {
        $this->cancelAction = $cancelAction;
        $this->cancelLabel = $cancelLabel;
        return $this;
    }




    /**
     * Adds a button to button box of the form.
     *
     * @return crm_Editor
     */
    public function addButton(Widget_Item $button, Widget_Action $action = null)
    {
        $this->buttonsLayout->addItem($button);
        if (isset($action)) {
            $button->setAction($action);
        }

        return $this;
    }


    /**
     * Adds an item to the top part of the form.
     *
     * @return crm_Editor
     */
    public function addItem(Widget_Displayable_Interface $item = null, $position = null)
    {
        $this->innerLayout->addItem($item, $position);

        return $this;
    }

    /**
     *
     *
     */
    protected function appendButtons()
    {
        $W = bab_Widgets();
        $App = $this->App();

        if (isset($this->saveAction)) {
            $saveLabel = isset($this->saveLabel) ? $this->saveLabel : $App->translate('Save');
            $submitButton = $W->SubmitButton();
            $submitButton->addClass('btn btn-primary');
            $submitButton->validate(true)
                ->setAction($this->saveAction)
                ->setFailedAction($this->failedAction)
                ->setSuccessAction($this->successAction)
                ->setLabel($saveLabel);
            if ($this->isAjax) {
                $submitButton->setAjaxAction();
            }
            $this->addButton($submitButton);
        }

        if (isset($this->cancelAction)) {
            $cancelLabel = isset($this->cancelLabel) ? $this->cancelLabel : $App->translate('Cancel');
            $this->addButton(
                $W->SubmitButton(/*'cancel'*/)
                    ->addClass('widget-close-dialog')
                    ->setAction($this->cancelAction)
                    ->setLabel($cancelLabel)
            );
        }

    }

    /**
     * Fields that will appear at the beginning of the form.
     *
     */
    protected function prependFields()
    {

    }


    /**
     * Fields that will appear at the end of the form.
     *
     */
    protected function appendFields()
    {

    }


    /**
     * Adds an item with a label and a description to the form.
     *
     * @param string                       $labelText
     * @param Widget_Displayable_Interface $item
     * @param string                       $fieldName
     * @param string                       $description
     * @param string						$suffix		 suffix for input field, example : unit
     *
     * @return Widget_LabelledWidget
     */
    public function labelledField($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null)
    {
        $W = bab_Widgets();
        return $W->LabelledWidget($labelText, $item, $fieldName, $description, $suffix);
    }



    /**
     *
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->appendFields();
        $this->appendButtons();

        return parent::display($canvas);
    }
}



class workspace_WorkspaceEditor extends workspace_Editor
{
    /**
     *
     * {@inheritDoc}
     * @see workspace_Editor::prependFields()
     */
    protected function appendFields()
    {
        $W = bab_Widgets();
        $App = $this->App();

        $this->addItem($W->Hidden()->setName('id'));

        $this->addItem(
            $W->Items(
                $W->VBoxItems(
                    $this->name(),
                    $this->description(),
                    $this->category(),
                    $this->color(),
                    $this->skin(),
                    $this->theme(),
                    $this->logo(),
                    $this->group()
                )->setSizePolicy('col-md-6')
                ->setVerticalSpacing(1, 'em'),
                $W->VBoxItems(
                    $W->Section(
                        $App->translate('Fonctionalities to provide'),
                        $this->elements()
                    ),
                    $W->Section(
                        $App->translate('Managers'),
                        $this->managers()
                    ),
                    $W->Section(
                        $App->translate('Advanced parameters'),
                        $this->ephemeral()
                    )
                )->setSizePolicy('col-md-6')
                ->setVerticalSpacing(1, 'em')
            )->addClass('row')
        );

        return $this;
    }

    protected function theme()
    {
        $W = bab_Widgets();
        if($this->record->isThemeEditable()){
            return $W->Link(
                $W->Label('Theme editor'),
                $this->App()->Controller()->Admin()->editTheme($this->record->id)
            );
        }
        return null;
    }

    protected function name()
    {
        $W = bab_Widgets();
        $App = $this->App();
        return $W->LabelledWidget(
            $App->translate('Name'),
            $W->LineEdit()
                ->setSize(80)
                ->addClass('form-control'),
            'name'
        );
    }


    protected function description()
    {
        $W = bab_Widgets();
        $App = $this->App();
        return $W->LabelledWidget(
            $App->translate('Description'),
            $W->LineEdit()
                ->setSize(80)
                ->addClass('form-control'),
            'description'
        );
    }

    protected function color()
    {
        $W = bab_Widgets();
        $App = $this->App();
        return $W->LabelledWidget(
            $App->translate('Color'),
            $W->ColorPicker()
                ->addClass('form-control', 'widget-10em'),
            'color'
        );
    }

    protected function skin()
    {
        $W = bab_Widgets();
        $skinBox = $W->FlowItems()->setVerticalAlign('top');

        /* List of skins */
        include_once $GLOBALS['babInstallPath'].'utilit/skinincl.php';

        $styles = array();

        $skinsTmp = bab_skin::getList(false);
        foreach ($skinsTmp as $skin) {
            /* List of styles of the skin */
            $stylestmp = $skin->getStyles();
            $tabcles = array_keys($stylestmp);

            for ($j=0;$j<=count($tabcles)-1;$j++) {
                $styles[$skin->getName()][] = $tabcles[$j];
            }
        }

        // SKINS AND STYLES SELECTOR

        $skinSelect = $W->Select();
        $skinSelect->setName('skin')->setId('workspace_configadmin_skinselector')->setValue('workspace');

        foreach ($styles as $skinName => $skinStyles){
            $skinSelect->addOption($skinName, $skinName);
        }

        $skinSelectBox = $W->LabelledWidget(
            workspace_translate('Graphic theme:'),
            $skinSelect
        );

        $skinBox->addItem($skinSelectBox);

        foreach ($styles as $skinName => $skinStyles){
            $styleSelect = $W->Select()->setName(array($skinName, 'skinStyles'));

            foreach ($skinStyles as $style){
                $styleSelect->addOption($style, $style);
            }

            $styleSelect->setValue($this->record->skinStyles);

            $styleSelectBox = $W->LabelledWidget(
                workspace_translate('CSS styles associated:'),
                $styleSelect
            );

            $skinSelect->setAssociatedDisplayable($styleSelectBox, array($skinName));
            $skinBox->addItem($styleSelectBox);
        }

        return $skinBox;
    }


    protected function logo()
    {
        $W = bab_Widgets();
        $App = $this->App();
        $widget = $App->Controller()->Admin(false)->logo($this->record->id);
        return $W->LabelledWidget(
            $App->translate('Logo'),
            $widget
        );
    }


    protected function group()
    {
        $W = bab_Widgets();
        $App = $this->App();
        return $W->LabelledWidget(
            $App->translate('Group'),
            $W->GroupPicker(),
            'group'
        );
    }




    protected function elements()
    {
        $W = bab_Widgets();
        $App = $this->App();
        $adminCtrl = $App->Controller()->Admin();

        $elementsBox = $W->VBoxItems();

        $workspaceAddons = $App->getWorkspaceAddons();

        if(count($workspaceAddons) > 0){
            foreach ($workspaceAddons as $workspaceAddon){
                $addon = bab_functionality::get('WorkspaceAddon/'.$workspaceAddon);
                $addonValues = $addon->getConfiguration($this->record->id);
                $isWorkspaceEnabled = isset($addonValues['isWorkspaceEnabled']) ? $addonValues['isWorkspaceEnabled'] : 0;

                if($addon instanceof workspace_Configurable){
                    $configureLink = $W->Link('', $adminCtrl->editAddonConfiguration($workspaceAddon, $this->record->id))->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_PROPERTIES)
                        ->setSizePolicy('col-md-2 widget-align-right')
                        ->setAjaxAction();
                }
                else{
                    $configureLink = $W->Label('');
                }

                //For some reason, setAssociatedDisplayable does not work if the displayable is not a LabelledWidget
                $configurationLinkBox = $W->LabelledWidget('', $configureLink);

                $elementsBox->addItem(
                    $W->FlowItems(
                        $W->LabelledWidget(
                            $App->translate($addon->getName()),
                            $W->Checkbox()->setAssociatedDisplayable($configurationLinkBox, array('1'))->setValue($isWorkspaceEnabled),
                            array('addons', $workspaceAddon)
                        )->setSizePolicy('col-md-10'),
                        $configurationLinkBox
                    )->setSizePolicy('widget-list-element')
                    ->setVerticalAlign('middle')
                    ->addClass('row')
                );
            }
        }
        else{
            $elementsBox->addItem(
                $W->FlowItems(
                    $W->Label(workspace_translate('No elements were found'))
                )->addClass('alert alert-warning')
            );
        }

        return $elementsBox;
    }

    protected function managers()
    {
        $App = $this->App();
        return $App->Controller()->Admin(false)->managersEditor($this->record->id);
    }

    protected function category()
    {
        $W = bab_Widgets();
        $App = $this->App();
        $categorySet = $App->CategorySet();
        $categories = $categorySet->select();
        $select = $W->Select();
        foreach ($categories as $category) {
            $select->addOption($category->id, $category->name);
        }

        $select->addOption('', '');
        $select->addOption(1, 'First category');
        $select->addOption(2, 'Second category');

        return $W->LabelledWidget(
            $App->translate('Category'),
            $select,
            'category'
        );
    }


    protected function ephemeral()
    {
        $W = bab_Widgets();
        $App = $this->App();
        $archiveDateLabelledWidget = $W->LabelledWidget($App->translate('Archived date'), $W->DatePicker(), 'archivedDate');
        $ephemeralLabelledWidget = $W->LabelledWidget($App->translate('Ephemeral workspace'), $W->Checkbox()->setAssociatedDisplayable($archiveDateLabelledWidget, array(1)), 'isEphemeral');
        return $W->FlowItems(
            $ephemeralLabelledWidget,
            $archiveDateLabelledWidget
        )->setHorizontalSpacing(4, 'em');
    }
}

class workspace_WorkspaceManagerEditor extends workspace_Editor
{
    public function __construct(Func_App_Workspace $app = null, $itemId = null)
    {
        parent::__construct($app, $itemId);
    }

    protected function appendFields()
    {
        $W = bab_Widgets();
        $this->addItem(
            $W->Items(
                $this->adminsList()
            )
        );
    }

    protected function adminsList()
    {
        $W = bab_Widgets();
        $box = $W->VBoxItems();
        $App = $this->App();

        $box->addItem(
            $W->Link(
                $W->Label(workspace_translate('Add an administrator'))->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_DOCUMENT_NEW),
                $App->Controller()->Admin(true)->selectAdministrator($this->record->delegation)
            )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );

        $headerBox = $W->FlowItems(
            $W->Label(workspace_translate('Name'))
            ->setSizePolicy('widget-20em'),
            $W->Label(workspace_translate('Permanent'))
            ->setSizePolicy('widget-10em widget-align-center')
        );

        $box->addItem($headerBox->setSizePolicy('widget-list-element widget-strong'));

        $administratorIds = $this->record->getAdministratorIds();
        foreach ($administratorIds as $administratorId) {
            $adminBox = $W->FlowItems(
                $W->Items(
                    $W->Link(
                        $W->Label('')->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE),
                        $App->Controller()->Admin(true)->removeAdministrator($this->record->delegation, array(array('id' => $administratorId)))
                    )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD),
                    $W->Label(bab_getUserName($administratorId, true))
                )->setSizePolicy('widget-20em')
            );
            $box->addItem($adminBox->setSizePolicy('widget-list-element'));
        }
        return $box;
    }
}

class workspace_WorkspaceFileManagerConfigurationEditor extends workspace_Editor
{
    public function __construct(Func_App_Workspace $app = null, $itemId = null)
    {
        parent::__construct($app, $itemId);
    }

    protected function appendFields()
    {
        $W = bab_Widgets();
        $this->addItem($W->Hidden()->setName('id'));
        $this->addItem(
            $W->Items(
                $this->version(),
                $this->notification()
//                 $this->size()
            )
        );
    }

    protected function version()
    {
        $W = bab_Widgets();
        $App = $this->App();
        return $W->LabelledWidget(
            $App->translate('Enable file versioning'),
            $W->Checkbox(),
            'version',
            bab_translate('Activate the management of the versions allows to keep a history of all the modifications brought to the same file')
        );
    }

    protected function notification()
    {
        $W = bab_Widgets();
        $App = $this->App();
        return $W->LabelledWidget(
            $App->translate('Enable notifications'),
            $W->Checkbox(),
            'notification',
            bab_translate('Notify members of the workspace when a file is uploaded')
        );
    }

    protected function size()
    {
        $W = bab_Widgets();
        $App = $this->App();
        return $W->LabelledWidget(
            $App->translate('Enable file versioning'),
            $W->NumberEdit(),
            'maxSize',
            bab_translate('The maximum size in bytes of the file manager. Set to 0 to have no maximum size')
        );
    }
}

