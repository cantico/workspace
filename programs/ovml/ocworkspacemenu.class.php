<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/../functions.php';

/**
 * <OCWorkspaceMenu [workspaceid=""] [iconsize="16|32"]>
 *      <OVWorkspaceMenuUrl>
 *      <OVWorkspaceMenuText>
 *      <OVWorkspaceMenuClassnames>
 * </OCWorkspaceTags>
 *
 */
class Func_Ovml_Container_WorkspaceMenu extends Func_Ovml_Container
{
    // @codingStandardsIgnoreEnd
    
    /**
     * @var workspace_WorkspaceSet
     */
    private $recordSet;
        
    /**
     * @var workspace_Workspace
     */
    private $record;    
    
    private $entries;
    
    /**
     * @return Func_App_Workspace
     */
    protected function App()
    {
        return workspace_App();
    }
    
    /**
     * {@inheritDoc}
     * @see Func_Ovml_Container::setOvmlContext()
     */
    public function setOvmlContext(babOvTemplate $ctx)
    {
        parent::setOvmlContext($ctx);
        
        $App = $this->App();
        
        $this->recordSet = $App->WorkspaceSet();        
        
        $workspaceId = $ctx->getAttribute('workspaceid');
        if(!$workspaceId){
            $workspaceId = -1; //Workspace does not exist
        }
     
        $baseClass = 'icon ';
        
        $this->record = $this->recordSet->get($this->recordSet->id->is($workspaceId));
        
        $this->entries = array();
        $this->entries[] = array(
            'text' => workspace_translate('Home'),
            'url' => $this->App()->Controller()->Page()->display('home', $this->record->id)->url(),
            'classnames' => $baseClass.Func_Icons::ACTIONS_GO_HOME
        );
        
        $workspaceAddons = $App->getWorkspaceAddons();
        foreach ($workspaceAddons as $addonName){
            if($this->record->hasFunctionality($addonName)){
                $addon = bab_functionality::get('WorkspaceAddon/' . $addonName);
                if ($addon) {
                    $this->entries[] = array(
                        'text' => $addon->getName(),
                        'url' => $this->App()->Controller()->Page()->display($addonName, $this->record->id)->url(),
                        'classnames' => $baseClass.$addon->getIconClassName()
                    );
                }
            }
        }
        if ($this->record->userIsAdministrator()) {
            $this->entries[] = array(
                'text' => workspace_translate('Configuration'),
                'url' => $this->App()->Controller()->Admin()->edit($this->record->id)->url(),
                'classnames' => $baseClass.Func_Icons::CATEGORIES_PREFERENCES_OTHER
            );
        }
        
        $this->idx = 0;
        $this->count = count($this->entries);
        $this->ctx->curctx->push('CCount', $this->count);        
    }
    
    /**
     * (non-PHPdoc)
     * @see utilit/Func_Ovml_Container#getnext()
     */
    public function getnext()
    {
        if ($this->idx > $this->count - 1){
            return false;
        }

        $this->ctx->curctx->push('CIndex', $this->idx);
        
        $datas = $this->entries[$this->idx];
        
        $this->ctx->curctx->push('WorkspaceMenuUrl', $datas['url']);
        $this->ctx->curctx->push('WorkspaceMenuText', $datas['text']);
        $this->ctx->curctx->push('WorkspaceMenuClassnames', $datas['classnames']);
        
        $this->idx++;
        $this->index = $this->idx;
        return true;
    }
    
}