<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/../functions.php';

/**
 * <OCWorkspaceTags [workspaceid=""]>
 *      <OVTagId>
 *      <OVTagLabel>
 *      <OVTagIsPersonal>
 * </OCWorkspaceTags>
 *
 */
class Func_Ovml_Container_WorkspaceTags extends Func_Ovml_Container
{
    // @codingStandardsIgnoreEnd
    
    /**
     * @var workspace_LinkSet
     */
    private $recordSet;
        
    /**
     * @var workspace_Link[]
     */
    private $records;    
    
    /**
     * @return Func_App_Workspace
     */
    protected function App()
    {
        return workspace_App();
    }
    
    /**
     * {@inheritDoc}
     * @see Func_Ovml_Container::setOvmlContext()
     */
    public function setOvmlContext(babOvTemplate $ctx)
    {
        parent::setOvmlContext($ctx);
        
        $App = $this->App();
        
        $this->recordSet = $App->LinkSet();        
        
        $conditions = array();
        
        $this->limitOffset = 0;
        
        $limit = $ctx->getAttribute('limit');
        if (is_string($limit)) {
            $limits = explode(',', $limit);
            if (count($limits) === 1) {
                $this->limitRows = $limit;
            } else {
                $this->limitOffset = $limits[0];
                $this->limitRows = $limits[1];
            }
        }
        $this->idx += $this->limitOffset;
        
        $workspaceId = $ctx->getAttribute('workspaceid');
        if(!$workspaceId){
            $workspaceId = -1; //Workspace does not exist
        }
        
        $conditions[] = $this->recordSet->sourceClass->is($App->WorkspaceClassName())
        ->_AND_($this->recordSet->sourceId->is($workspaceId))
        ->_AND_($this->recordSet->targetClass->is($App->TagClassName()))
        ->_AND_($this->recordSet->type->is('hasTag')
            ->_OR_($this->recordSet->type->is('hasPersonalTag')->_AND_($this->recordSet->createdBy->is(bab_getUserId())))
        );
        
        $this->records = $this->recordSet->select(
            $this->recordSet->all($conditions)
        );        
        
        $this->records->orderAsc($this->recordSet->id);
        
        $this->records->rewind();
        $this->records->seek($this->idx);
        
        $this->count = $this->records->count();
        $this->ctx->curctx->push('CCount', $this->count);        
    }
    
    /**
     * (non-PHPdoc)
     * @see utilit/Func_Ovml_Container#getnext()
     */
    public function getnext()
    {
        if ($this->idx >= $this->count || (isset($this->limitRows) && ($this->idx >= $this->limitRows + $this->limitOffset))) {
            $this->idx = $this->limitOffset;
            return false;
        }

        $this->ctx->curctx->push('CIndex', $this->idx);
        
        $tagLink = $this->records->current();
        /* @var $tagLink workspace_Link  */
        
        $tagSet = $this->App()->TagSet();
        $tag = $tagSet->request($tagSet->id->is($tagLink->targetId));
        $isPersonal = $tagLink->type == 'hasPersonalTag' ? 1 : 0;
        
        $this->ctx->curctx->push('TagId', $tag->id);
        $this->ctx->curctx->push('TagLabel', $tag->label);
        $this->ctx->curctx->push('TagIsPersonal', $isPersonal);
        
        $this->idx++;
        $this->index = $this->idx;
        $this->records->next();
        return true;
    }
    
}