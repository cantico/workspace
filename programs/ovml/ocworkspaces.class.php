<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/../functions.php';

/**
 * <OCWorkspaces [workspaceid=""] [search="keyword"] [limit="[offset,]maxNumber"] [orderby="Random|WorkspaceName"]>
 *      <OVWorkspaceId>
 *      <OVWorkspaceDelegationId>
 *      <OVWorkspaceName>
 *      <OVWorkspaceDescription>
 *      <OVWorkspaceArchivedDate>
 *      <OVWorkspaceColor>
 *      <OVWorkspaceLogo>           Can be used in <OFImg path="{OVWorkspaceLogo}">
 *      <OVWorkspaceCreatedOn>
 *      <OVWorkspaceLastActivity>
 *      <OVWorkspaceIsArchived>
 *      <OVWorkspaceIsEphemeral>
 *      <OVWorkspaceCategoryId>
 * </OCWorkspaces>
 *
 */
class Func_Ovml_Container_Workspaces extends Func_Ovml_Container
{
    // @codingStandardsIgnoreEnd

    /**
     * @var workspace_WorkspaceSet
     */
    private $recordSet;

    /**
     * @var workspace_Workspace[]
     */
    private $records;

    /**
     * @return Func_App_Workspace
     */
    protected function App()
    {
        return workspace_App();
    }

    /**
     * {@inheritDoc}
     * @see Func_Ovml_Container::setOvmlContext()
     */
    public function setOvmlContext(babOvTemplate $ctx)
    {
        parent::setOvmlContext($ctx);

        $App = $this->App();

        $this->recordSet = $App->WorkspaceSet();

        $conditions = array();

        $this->limitOffset = 0;

        $limit = $ctx->getAttribute('limit');
        if (is_string($limit)) {
            $limits = explode(',', $limit);
            if (count($limits) === 1) {
                $this->limitRows = $limit;
            } else {
                $this->limitOffset = $limits[0];
                $this->limitRows = $limits[1];
            }
        }
        $this->idx += $this->limitOffset;

        $search = $ctx->getAttribute('search');
        if (!empty($search)) {
            //Search by workspace name
            $search = trim($search);
            $keywords = explode(',', $search);
            $workspaceIds = array();
            foreach ($keywords as $keyword){
                $conditions[] = $this->recordSet->name->contains($keyword);

                //Search by workspace tag label
                $linkSet = $App->LinkSet();
                $tagSet = $App->TagSet();
                $tags = $tagSet->select($tagSet->label->contains($keyword));
                $links = array();
                foreach ($tags as $tag){
                    $tagLinks = $linkSet->select(
                        $linkSet->sourceClass->is($App->WorkspaceClassName())
                        ->_AND_($linkSet->targetClass->is($App->TagClassName())
                            ->_AND_($linkSet->targetId->is($tag->id)))
                        );
                    foreach($tagLinks as $link){
                        if(!isset($links[$link->id])){
                            $links[$link->id] = $link;
                            if($link->type == 'hasPersonalTag'){
                                if($link->createdBy == bab_getUserId()){
                                    $workspaceIds[] = $link->sourceId;
                                }
                                continue;
                            }
                            $workspaceIds[] = $link->sourceId;
                        }
                    }
                }
            }

            if(count($workspaceIds) > 0){
                $conditions[] = $this->recordSet->id->in($workspaceIds);
            }
        }

        $genericConditions =
            $this->recordSet->delegation->is(0)->_NOT()
            ->_AND_($this->recordSet->isReadable());

        $workspaceIds = $ctx->getAttribute('workspaceid');
        if (!empty($workspaceIds)) {
            $workspaceIds = explode(',', $workspaceIds);
            $genericConditions = $genericConditions->_AND_($this->recordSet->id->in($workspaceIds));
        }

        $this->records = $this->recordSet->select(
            $this->recordSet->any($conditions)
            ->_AND_($genericConditions)
        );

        $orderBy = $ctx->getAttribute('orderby');
        if (is_string($orderBy)) {
            $orderBys = explode(',', $orderBy);
            foreach ($orderBys as $orderBy) {
                $orderBy = trim($orderBy);
                if ($orderBy == 'WorskpaceName') {
                    $this->records->orderAsc($this->recordSet->name);
                } elseif ($orderBy == 'Random') {
                    $seed = crc32(session_id());
                    $this->records->orderAsc($this->recordSet->id->rand($seed));
                }
            }
        }


        $this->records->orderAsc($this->recordSet->id);

        $this->records->rewind();
        $this->records->seek($this->idx);

        $this->count = $this->records->count();
        $this->ctx->curctx->push('CCount', $this->count);
    }

    /**
     * (non-PHPdoc)
     * @see utilit/Func_Ovml_Container#getnext()
     */
    public function getnext()
    {
        if ($this->idx >= $this->count || (isset($this->limitRows) && ($this->idx >= $this->limitRows + $this->limitOffset))) {
            $this->idx = $this->limitOffset;
            return false;
        }

        $this->ctx->curctx->push('CIndex', $this->idx);

        $workspace = $this->records->current();
        /* @var $workspace workspace_Workspace  */

        $color = $workspace->color;
        if(!empty($color)){
            if(!strpos($color, '#')){
                $color = '#'.$color;
            }
        }

        $logoFullPath = '';
        $logoPath = $workspace->getLogoPath();

        if($logoPath){
            require_once $GLOBALS['babInstallPath'] . '/utilit/pathUtil.class.php';
            $logoFullPath = BAB_PathUtil::sanitize($logoPath);
        }

        $this->ctx->curctx->push('WorkspaceId', $workspace->id);
        $this->ctx->curctx->push('WorkspaceDelegationId', $workspace->delegation);
        $this->ctx->curctx->push('WorkspaceName', $workspace->name);
        $this->ctx->curctx->push('WorkspaceDescription', $workspace->description);
        $this->ctx->curctx->push('WorkspaceArchivedDate', $workspace->archivedDate);
        $this->ctx->curctx->push('WorkspaceColor', $color);
        $this->ctx->curctx->push('WorkspaceLogoFullPath', $logoFullPath);
        $this->ctx->curctx->push('WorkspaceCreatedOn', $workspace->createdOn);
        $this->ctx->curctx->push('WorkspaceLastActivity', $workspace->lastActivity);
        $this->ctx->curctx->push('WorkspaceCategoryId', $workspace->category);

        $this->idx++;
        $this->index = $this->idx;
        $this->records->next();
        return true;
    }

}