<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/controller.class.php';
require_once dirname(__FILE__) . '/workspace.ctrl.php';
require_once dirname(__FILE__) . '/page.class.php';
require_once dirname(__FILE__) . '/workspaces.php';
require_once dirname(__FILE__) . '/search.ui.php';



/**
 */
class workspace_CtrlSearch extends workspace_Controller
{

	/**
	 * Displays the keywords search results.
	 *
	 * @return Widget_Action
	 */
	public function search($keywords)
	{
		workspace_BreadCrumbs::setCurrentPosition($this->proxy()->search($keywords), workspace_translate('Search'));

		$W = bab_Widgets();

		$page = workspace_Page()
					->setTitle(workspace_translate('Search results'))
					->addItemMenu('search', workspace_translate('Search results'), $this->proxy()->search($keywords)->url())
					->addItemMenu('search_calendar', workspace_translate('Search events'), $this->proxy()->searchEvent($keywords)->url())
					->setCurrentItemMenu('search');

		$resultBox = workspace_searchResultList($keywords);

		$resultFrame = $W->Frame()->setLayout($resultBox->setVerticalSpacing(1, 'em'));
		$resultFrame->addClass('workspace-dialog');

		$searchForm = $W->Form()->setLayout($W->FlowLayout()->setSpacing(1, 'em'))
			->setReadOnly()
			->addItem($W->LineEdit()->setName('keywords')->setValue($keywords))
			->addItem($W->SubmitButton()->setLabel(workspace_translate('New search')))
			->setSelfPageHiddenFields();

		$resultBox->addItem($searchForm, 0);


		$page->addItem($resultFrame);

		$page->displayHtml();
	}



	/**
	 * Displays the calendar events search results by period.
	 *
	 * @return Widget_Action
	 */
	public function searchEvent($keywords = null, $from = null, $to = null)
	{
		workspace_BreadCrumbs::setCurrentPosition($this->proxy()->searchEvent(), workspace_translate('Search events'));

		$W = bab_Widgets();

		$page = workspace_Page()
					->setTitle(workspace_translate('Search results'))
					->addItemMenu('search', workspace_translate('Search results'), $this->proxy()->search($keywords)->url())
					->addItemMenu('search_calendar', workspace_translate('Search events'), $this->proxy()->searchEvent($keywords, $from, $to)->url())
					->setCurrentItemMenu('search_calendar');

		$resultBox = workspace_calendarsSearchResultList($keywords, $from, $to);


		$resultFrame = $W->Frame()->setLayout($W->VBoxLayout()->setVerticalSpacing(1, 'em'));
		$resultFrame->addClass('workspace-dialog');

		$searchForm = $W->Form()->setLayout($W->FlowLayout()->setSpacing(2, 'em'))
			->setReadOnly()
			->addItem(
				$W->FlowItems(
					$keywordsLbl = $W->Label(workspace_translate('Keywords')),
					$W->LineEdit()->setName('keywords')->setValue($keywords)->setAssociatedLabel($keywordsLbl)
				)->setHorizontalSpacing(0.5, 'em')
			)
			->addItem(
				$W->FlowItems(
					$FromLbl = $W->Label(workspace_translate('From')),
					$W->DatePicker()->setName('from')->setValue(bab_rp('from'))->setAssociatedLabel($FromLbl),
					$ToLbl = $W->Label(workspace_translate('to')),
					$W->DatePicker()->setName('to')->setValue(bab_rp('to'))->setAssociatedLabel($ToLbl)
				)->setHorizontalSpacing(0.5, 'em')
			)
			->addItem($W->SubmitButton()->setLabel(workspace_translate('New search')))
			->setSelfPageHiddenFields();

		$resultFrame->addItem($searchForm);
		$resultFrame->addItem($resultBox);


		$page->addItem($resultFrame);

		$page->displayHtml();
	}


	/**
	 * Does nothing and return to the previous page.
	 *
	 * @param array	$message
	 * @return Widget_Action
	 */
	public function cancel()
	{
		workspace_redirect(workspace_BreadCrumbs::last());
		die();
	}


	/**
	 * Displays help on this page.
	 *
	 * @return Widget_Action
	 */
	public function help()
	{

	}
}
