<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';




/** FILES **/


/**
 *
 * @param int $workspaceId
 * @param array $usersToNotify			The list of users that should be notified by ovidentia
 * @return array
 */
function workspace_usersToNotify($workspaceId, array $usersToNotify)
{
	require_once dirname(__FILE__) . '/directories.php';

	$directoryId = workspace_getWorkspaceDirectoryId($workspaceId);
	$workspaceGroupId = workspace_getWorkspaceGroupId($workspaceId);

	if (null === $workspaceGroupId)
	{
		return array();
	}

	$groups = bab_getGroups($workspaceGroupId, true);
	$groupIds = array($workspaceGroupId => $workspaceGroupId);
	if(is_array($groups) && isset($groups['id'])){
		foreach ($groups['id'] as $group) {
			$groupIds[$group] = $group;
		}
	}

	$members = bab_getGroupsMembers($groupIds);

	$workspaceUsersToNotify = array();
	foreach ($members as $member) {

		$userId = $member['id'];
		$entryId = workspace_getDirEntryFromUserId($directoryId, $userId);
		$entry = bab_getDirEntry($entryId, BAB_DIR_ENTRY_ID);

		if ((!isset($usersToNotify)) || isset($usersToNotify[$userId])) {
			$workspaceUsersToNotify[$userId] = array(
				'id' => $userId,
				'name' => bab_composeUserName($entry['givenname']['value'], $entry['sn']['value']),
				'email' => $entry['email']['value']
			);
		}
	}

	return $workspaceUsersToNotify;
}


/**
 *
 * @param BAB_FolderFile $folderFile
 * @param array $usersToNotify		The list of users that should be notified by ovidentia.
 * @return array					The list of actually notified users.
 */
function workspace_notifyFileChange(BAB_FolderFile $folderFile, array $usersToNotify, $title = '', $message = '')
{
	// First we check that the file is in one of our workspaces.
	$workspaceId = $folderFile->getDelegationOwnerId();
	$workspaceInfo = workspace_getWorkspaceInfo($workspaceId);
	if (!isset($workspaceInfo)) {
		return array();
	}


	$workspaceUsersToNotify = workspace_usersToNotify($workspaceId, $usersToNotify);

	if (!workspace_fileNotifyMembers($folderFile, $workspaceUsersToNotify, $workspaceInfo, $title, $message)) {
		return array();
	}

	return $workspaceUsersToNotify;
}



/**
 * This function is registered to be called by ovidentia whenever a bab_eventFmFile event is triggered.
 *
 * @param bab_eventFmFile $event
 */
function workspace_onFmFile(bab_eventFmFile $event)
{
	require_once dirname(__FILE__) . '/workspaces.php';

	$references = $event->getReferences();
	if (empty($references)) {
		return true;
	}
	
	$App = workspace_App();
	$set = $App->WorkspaceSet();

	$usersToNotify = $event->getUsersToNotify();

	if ($event instanceOf bab_eventFmAfterAddVersion) {
		$title = workspace_translate('A new file version has been uploaded');
	} else if ($event instanceOf bab_eventFmAfterFileUpdate) {
		$title = workspace_translate('A file has been updated');
	} else if ($event instanceOf bab_eventFmAfterFileUpload) {
		$title = workspace_translate('A new file has been uploaded');
	}

	$informedUsers = array();

	foreach ($references as $reference) {
		/* @var $reference bab_Reference */

		$oFolderFileSet = bab_getInstance('BAB_FolderFileSet');
		$oId			= $oFolderFileSet->aField['iId'];
		$folderFile		= $oFolderFileSet->get($oId->in($reference->getObjectId()));

		if ($folderFile) {
			$message = '';
			$informedUsers += workspace_notifyFileChange($folderFile, $usersToNotify, $title, $message);
			
			//Update workspace last activity date
			$delegationId = $folderFile->getDelegationOwnerId();
			if($delegationId){
			    $workspace = $set->get($set->delegation->is($delegationId)->_AND_($set->isArchived->is(false)));
			    if($workspace){
			        $workspace->updateLastActivity();
			    }
			}	
		}
	}

	foreach ($informedUsers as $informedUserId => $informedUser) {
		$event->addInformedUser($informedUserId);
	}

	return true;
}






/** FORUM **/


/**
 *
 * @param string	$author				Post/thread author full name.
 * @param int		$threadId			The thread id.
 * @param int		$postId				The post id.
 * @param array 	$usersToNotify		The list of users that should be notified by ovidentia.
 * @param string	$title
 * @param string	$message
 * @param string    $changeType			Type of change. One of 'newPost' or 'newThread'.
 *
 * @return array					The list of actually notified users.
 */
function workspace_notifyForumChange(bab_eventForumPost $event, array $usersToNotify, $title = '', $message = '', $changeType = null)
{
	global $babDB;
	require_once dirname(__FILE__) . '/forum.php';

	$req = "
		SELECT id_dgowner from ".BAB_FORUMS_TBL." forum
		LEFT JOIN ".BAB_THREADS_TBL." thread ON thread.forum = forum.id
		WHERE
			thread.active='Y'
		AND thread.id = ".$babDB->quote($event->getThreadId()).'
	';
	$res = $babDB->db_query($req);
	$arr = $babDB->db_fetch_array($res);


	$delegationId = $arr['id_dgowner'];
	if(!wokspace_isExist($delegationId) || wokspace_isDisabled($delegationId)){
		return array();
	}

	$workspaceInfo = workspace_getWorkspaceInfo($delegationId);

	$workspaceUsersToNotify = workspace_usersToNotify($delegationId, $usersToNotify);

	if (!workspace_forumNotifyMembers($event, $workspaceUsersToNotify, $workspaceInfo, $title, $message, $changeType)) {
		return array();
	}

	return $workspaceUsersToNotify;
}


/**
 * This function is registered to be called by ovidentia whenever a bab_eventForumPost event is triggered.
 *
 * @param bab_eventForumPost $event
 */
function workspace_onForumPost(bab_eventForumPost $event)
{

	require_once dirname(__FILE__) . '/workspaces.php';

	$usersToNotify = $event->getUsersToNotify();

	if ($event instanceOf bab_eventForumAfterThreadAdd) {
		$title = workspace_translate('A new thread has been created');
		$changeType = 'newThread';
	} else if ($event instanceOf bab_eventForumAfterPostAdd) {
		$title = workspace_translate('A message has been posted');
		$changeType = 'newPost';
	}

	$message = '';

	$informedUsers = workspace_notifyForumChange($event, $usersToNotify, $title, $message, $changeType);

	foreach ($informedUsers as $informedUserId => $informedUser) {
		$event->addInformedUser($informedUserId);
	}
	
	//Update workspace last activity date
	$delegationId = bab_getForumDelegationId($event->getForumId());
	if($delegationId){
	    $App = workspace_App();
	    $set = $App->WorkspaceSet();
	    $workspace = $set->get($set->delegation->is($delegationId)->_AND_($set->isArchived->is(false)));
	    if($workspace){
	        $workspace->updateLastActivity();
	    }
	}	

	return true;
}






/** ARTICLES **/


/**
 *
 * @param string	$author				Article author full name.
 * @param int		$articleId			The article id.
 * @param array 	$usersToNotify		The list of users that should be notified by ovidentia.
 * @param string	$title
 * @param string	$message
 * @param string    $changeType			Type of change. One of 'newArticle' or 'updatedArticle'.
 *
 * @return array					The list of actually notified users.
 */
function workspace_notifyArticleChange($author, $articleId, array $usersToNotify, $title = '', $message = '', $changeType = null)
{
	require_once dirname(__FILE__) . '/articles.php';
	// First we check that the file is in one of our workspaces.


	$delegationId = bab_getArticleDelegationId($articleId);
	if(!wokspace_isExist($delegationId) || wokspace_isDisabled($delegationId)){}
	$workspaceId = workspace_getCurrentWorkspace();
	$workspaceInfo = workspace_getWorkspaceInfo($workspaceId);

	if (empty($workspaceId)) {
		return array();
	}

	$articleIds = workspace_getArticlesIds(array($articleId), null, $workspaceId);

	if (empty($articleIds)) {
		return array();
	}



	$workspaceUsersToNotify = workspace_usersToNotify($delegationId, $usersToNotify);


	if (!workspace_articleNotifyMembers($author, $articleId, $workspaceUsersToNotify, $workspaceInfo, $title, $message, $changeType)) {
		return array();
	}


	return $workspaceUsersToNotify;
}


/**
 * This function is registered to be called by ovidentia whenever a bab_eventArticle event is triggered.
 *
 * @param bab_eventArticle $event
 */
function workspace_onArticleAdded(bab_eventArticle $event)
{
	require_once dirname(__FILE__) . '/workspaces.php';

	$usersToNotify = $event->getUsersToNotify();

	$title = workspace_translate('An article has been published');

	$changeType = 'newArticle';
	$message = '';
	$informedUsers = workspace_notifyArticleChange($event->getArticleAuthor(), $event->getArticleId(), $usersToNotify, $title, $message, $changeType);

	foreach ($informedUsers as $informedUserId => $informedUser) {
		$event->addInformedUser($informedUserId);
	}
	
	//Update workspace last activity date
	$delegationId = bab_getArticleDelegationId($event->getArticleId());
	if($delegationId){
	    $App = workspace_App();
	    $set = $App->WorkspaceSet();
	    $workspace = $set->get($set->delegation->is($delegationId)->_AND_($set->isArchived->is(false)));
	    if($workspace){
	        $workspace->updateLastActivity();
	    }
	}	

	return true;
}








/** DIRECTORIES **/


/**
 * @param int		$entryId			The directory entry id.
 * @param array 	$usersToNotify		The list of users that should be notified by ovidentia.
 * @param string	$title
 * @param string	$message
 *
 * @return array					The list of actually notified users.
 */
function workspace_notifyDirectoryChange(bab_eventDirectory $event, $title = '', $message = '')
{
	return array();
	//DO NOTHING
	// First we check that the file is in one of our workspaces.
	$workspaceId = workspace_getCurrentWorkspace();
	$workspaceInfo = workspace_getWorkspaceInfo($workspaceId);

	$directoryId = workspace_getWorkspaceDirectoryId();

	$entry = $event->getDirEntry();

	if (!$entry) {
		return array();
	}

	$workspaceUsersToNotify = workspace_usersToNotify($workspaceId, array());

	if (!workspace_directoryNotifyMembers($entry, $workspaceUsersToNotify, $workspaceInfo, $title, $message)) {
		return array();
	}

	return $workspaceUsersToNotify;
}


/**
 * This function is registered to be called by ovidentia whenever a bab_eventDirectory event is triggered.
 *
 * @param bab_eventDirectory $event
 */
function workspace_onDirectoryChange(bab_eventDirectory $event)
{
	require_once dirname(__FILE__) . '/workspaces.php';

//	$usersToNotify = $event->getUsersToNotify();

	switch (true) {
		case $event instanceof bab_eventDirectoryEntryCreated:
			$informedUsers = workspace_notifyDirectoryChange($event, workspace_translate('A directory entry has been added'));
			break;

		case $event instanceof bab_eventDirectoryEntryModified:
			$informedUsers = workspace_notifyDirectoryChange($event, workspace_translate('A directory entry has been modifed'));
			break;

		case $event instanceof bab_eventDirectoryEntryDeleted:
			$informedUsers = workspace_notifyDirectoryChange($event, workspace_translate('A directory entry has been removed'));
			break;
	}

//	foreach ($informedUsers as $informedUserId => $informedUser) {
//		$event->addInformedUser($informedUserId);
//	}

    //Update workspace last activity date
	$App = workspace_App();
	$set = $App->WorkspaceSet();
    $delegationId = workspace_getCurrentWorkspace();
    $workspace = $set->get($set->delegation->is($delegationId)->_AND_($set->isArchived->is(false)));
    if($workspace){
        $workspace->updateLastActivity();
	}

	return true;
}








/** CALENDARS **/


/**
 * Notify by email from parameters received in event
 * @return array
 */
function workspace_notifyEventChange(bab_eventCalendarEvent $event, $title = '', $message = '')
{
	// First we check that the calendar is in one of our workspaces.
	$workspaceUsersToNotify = array();

	$calendars = $event->getCalendars();
	$period = $event->getPeriod();
	$usersToNotify = $event->getUsersToNotify();

	foreach( $calendars as $calendar ){
		$workspaceId = $calendar->getDgOwner();
		$workspaceInfo = workspace_getWorkspaceInfo($workspaceId);
		if( $workspaceId != 0 && $workspaceInfo != null ){
			$arrayUser = workspace_usersToNotify($workspaceId, $usersToNotify);
			if (!workspace_calendarNotifyMembers($period, $arrayUser, $workspaceInfo, $title, $message)) {
				$arrayUser = array();
			}
			foreach ($arrayUser as $arrayUserKey => $arrayUserValue) {
			    $workspaceUsersToNotify[$arrayUserKey] = $arrayUserValue;
			}
		}
	}

	return $workspaceUsersToNotify;
}


/**
 *
 * @param bab_eventCalendarEvent $event
 * @return unknown_type
 */
function workspace_onCalendarEvent(bab_eventCalendarEvent $event)
{
	switch (true) {
		case $event instanceof bab_eventAfterEventAdd:
			$informedUsers = workspace_notifyEventChange($event, workspace_translate('A new event has been scheduled'));
			break;

		case $event instanceof bab_eventAfterEventUpdate:
			$informedUsers = workspace_notifyEventChange($event, workspace_translate('An event has been modifed'));
			break;

		case $event instanceof bab_eventAfterEventDelete:
			$informedUsers = workspace_notifyEventChange($event, workspace_translate('An event has been removed'));
			break;
	}

	foreach ($informedUsers as $informedUserId => $informedUser) {
		$event->addInformedUser($informedUserId);
	}
	
	//Update workspace last activity date
	$App = workspace_App();
	$set = $App->WorkspaceSet();
	$calendars = $event->getCalendars();
	foreach($calendars as $calendar){
	    $delegationId = $calendar->getDgOwner();
	    $workspace = $set->get($set->delegation->is($delegationId)->_AND_($set->isArchived->is(false)));
	    if($workspace){
	        $workspace->updateLastActivity();
	    }
	}
}
