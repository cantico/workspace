<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';

require_once dirname(__FILE__) . '/workspacewidgets.php';


/**
 * @return Widget_Frame
 */
function workspace_FolderEditor()
{
	$W = bab_Widgets();

	$form = $W->Form();
	$frame = new workspace_BaseForm('folder_editor');
	$frame->setName('folder')
			->addClass('workspace-dialog');

	$titleFormItem = workspace_FormField(workspace_translate('Folder name:'),
										 $W->LineEdit()->setSize(80)->setName('name')
										 	->setMandatory(true, workspace_translate("The folder name must not be empty")));

	$frame->addItem($titleFormItem);


	$frame->addButton($W->SubmitButton('save')
						->validate(true)
						->setLabel(workspace_translate("Save"))
						->setAction(workspace_Controller()->Files()->createFolder())
						)
		  ->addButton($W->SubmitButton('cancel')
		  				->setLabel(workspace_translate("Cancel"))
		  				->setAction(workspace_Controller()->Files()->cancel())
		  				)
		;

	$form->setLayout($W->VBoxLayout())->addItem($frame);

	workspace_setSelfPageHiddenFields($form);

	return $form;
}


/**
 * @return Widget_Frame
 */
function workspace_filePropertiesEditor()
{
	$W = bab_Widgets();

	$form = $W->Form();
	$frame = new workspace_BaseForm('file_properties_editor');
	$frame->setName('properties')
			->addClass('workspace-dialog');

	$titleFormItem = workspace_FormField(workspace_translate('File name:'),
										 $W->LineEdit()->setSize(80)->setName('name')
										 	->setMandatory(true, workspace_translate("The file name must not be empty")));

	$frame->addItem($titleFormItem);


	$frame->addButton($W->SubmitButton('save')
						->validate(true)
						->setLabel(workspace_translate("Save"))
						->setAction(workspace_Controller()->Files()->saveFileProperties())
						)
		  ->addButton($W->SubmitButton('cancel')
		  				->setLabel(workspace_translate("Cancel"))
		  				->setAction(workspace_Controller()->Files()->cancel())
		  				)
		;

	$form->setLayout($W->VBoxLayout())->addItem($frame);

	workspace_setSelfPageHiddenFields($form);

	return $form;
}


/**
 * @return Widget_Frame
 */
function workspace_folderPropertiesEditor()
{
	$W = bab_Widgets();

	$form = $W->Form();
	$frame = new workspace_BaseForm('file_properties_editor');
	$frame->setName('properties')
			->addClass('workspace-dialog');

	$titleFormItem = workspace_FormField(workspace_translate('Folder name:'),
										 $W->LineEdit()->setSize(80)->setName('name')
										 	->setMandatory(true, workspace_translate("The file name must not be empty")));

	$frame->addItem($titleFormItem);


	$frame->addButton($W->SubmitButton('save')
						->validate(true)
						->setLabel(workspace_translate("Save"))
						->setAction(workspace_Controller()->Files()->saveFolderProperties())
						)
		  ->addButton($W->SubmitButton('cancel')
		  				->setLabel(workspace_translate("Cancel"))
		  				->setAction(workspace_Controller()->Files()->cancel())
		  				)
		;

	$form->setLayout($W->VBoxLayout())->addItem($frame);

	workspace_setSelfPageHiddenFields($form);

	return $form;
}


/**
 * @return Widget_Frame
 */
function workspace_FileUploadEditor()
{
	$W = bab_Widgets();

	$form = $W->Form();
	$frame = new workspace_BaseForm('file_upload_editor');
	$frame->setName('file')
			->addClass('workspace-dialog');

	$F = @bab_functionality::get('FileListUploader');
	$F = false;
	if (false !== $F) {
		$F->setListUid('workspace_multiple_upload');
		$uploadWidget = $F->getWidget();
	} else {
		$uploadWidget = $W->Uploader();
		$uploadWidget->setMandatory(true, workspace_translate('You must select a file.'));
	}

	$workspaceId = workspace_getCurrentWorkspace();
	$delegationPrefix = 'DG' . $workspaceId;
	$workspaceInfo = workspace_getWorkspaceInfo($workspaceId);
	$workspaceName = $workspaceInfo['name'];
	$path = $GLOBALS['babUploadPath'] . "/fileManager/collectives/" . $delegationPrefix . '/' . $workspaceName;

	$file_size = floor($GLOBALS['babMaxTotalSize']/1048576);
	if( $file_size > floor($GLOBALS['babMaxGroupSize']/1048576) ){
		$file_size = floor($GLOBALS['babMaxGroupSize']/1048576);
	}

	$wsFileSize = $workspaceInfo['file_size'];
	if( $wsFileSize == 0 ){
		$wsFileSize = $file_size;
	}

	$sizeLeft = round($wsFileSize - getDirSize($path)/1048576,2);

	$fileFormItem = workspace_FormField(workspace_translate('File') . ' (' . $sizeLeft . ' ' . workspace_translate('Mo left') . ') :', $uploadWidget->setName('file'));

	$frame->addItem($fileFormItem);


	$frame->addButton($W->SubmitButton('save')
						->setLabel(workspace_translate("Save"))
						->setAction(workspace_Controller()->Files()->uploadFile())
						)
		  ->addButton($W->SubmitButton('cancel')
		  				->setLabel(workspace_translate("Cancel"))
		  				->setAction(workspace_Controller()->Files()->cancel())
		  				)
		;

	$form->setLayout($W->VBoxLayout())->addItem($frame);

	workspace_setSelfPageHiddenFields($form);

	return $form;
}



/**
 *
 * @param Widget_Displayable_Interface  $icon
 * @param string                        $label
 * @param string                        $subLabel
 * @return Widget_Displayable_Interface
 */
function workspace_icon($icon, $label, $subLabel)
{
    $W = bab_Widgets();
    return $W->VBoxItems(
        $icon->addClass('icon')
            ->setSizePolicy('icon-image'),
        $W->VBoxItems(
            $W->Label($label),
            $W->Label($subLabel)->addClass('widget-icon-sub-label', 'widget-small')
        )->setSizePolicy('icon-labels')
    );
}


/**
 * @param string  $icon
 * @param string  $label
 * @param string  $subLabel
 * @return Widget_Displayable_Interface
 */
function workspace_classIcon($iconClassname, $label, $subLabel)
{
    $W = bab_Widgets();
    $icon = $W->Html()
        ->addClass($iconClassname);
    return workspace_icon($icon, $label, $subLabel);
}

/**
 * @param string  $imageUrl
 * @param string  $label
 * @param string  $subLabel
 * @return Widget_Displayable_Interface
 */
function workspace_imageIcon($imageUrl, $label, $subLabel)
{
    $W = bab_Widgets();
    $icon = $W->Image($imageUrl);
    return workspace_icon($icon, $label, $subLabel);
}



/**
 *
 * @param bab_FileInfo $file
 * @return Widget_Displayable_Interface
 */
function workspace_fileIcon(bab_FileInfo $file)
{
    $oBabDir = null;
    $T = null;
    $F = null;
    if (!isset($oBabDir)) {
        $oBabDir = new bab_Directory();
    }
    if (!isset($T)) {
        $T = bab_functionality::get('Thumbnailer');
    }
    if (!isset($F)) {
        $F = bab_functionality::get('FileInfos');
    }

    $filename = $file->getFilename();

    if ($file->isDir()) {

        $sPathName = $file->getFmPathname();
        $oIterator = $oBabDir->getEntries($sPathName, 0);
        $itemCount = 0;
        if ($oIterator instanceof bab_CollectiveDirIterator) {
            /* @var $oItem bab_FileInfo */
            foreach ($oIterator as $oItem) {
                if ($oItem->isDir()) {
                    $itemCount++;
                    continue;
                }
                $fmfile = $oItem->getFmFile();
                if ($fmfile && $fmfile->getState() !== 'D') {
                    $itemCount++;
                }
            }
        }

        if (($itemCount) === 0) {
            $subText = workspace_translate('Empty');
        } else {
            $subText = sprintf(workspace_translate('%d item', '%d items', $itemCount), $itemCount);
        }

        $fileIcon = workspace_classIcon('places-folder', $filename, $subText);

        $fileIcon->addClass('workspace-droppable');

        $fileIcon->setMetadata('filetype', 'folder');

    } else {

//         if ($pathIsTrashPath) {
//             $filename = workspace_revertTrashFilename($filename);
//         }

        $fileSize = $file->getSize();
        if ($fileSize > 1024*1024) {
            $fileSizeText = round($file->getSize() / (1024*1024), 1) . ' ' . workspace_translate('MB');
        } elseif ($fileSize > 1024) {
            $fileSizeText = round($file->getSize() / 1024) . ' ' . workspace_translate('KB');
        } else {
            $fileSizeText = $file->getSize() . ' ' . workspace_translate('Bytes');
        }

        $imageUrl = null;
        if ($F && $T) {
            // We have some problems with pdf, for now only make thumbnails for images.
            $mimetype = $F->getGenericClassName($file->getPathname());
            if ($mimetype == 'mimetypes-image-x-generic') {
                $T->setSourceFile($file->getPathname());
                $T->setBorder(1, '#aaaaaa');
                try {
                    $imageUrl = $T->getThumbnail(128, 42);
                } catch (Exception $e) {
                    $imageUrl = null;
                }
            }
        }
        if ($imageUrl) {
            $fileIcon = workspace_imageIcon($imageUrl, $filename, $fileSizeText);
        } else {
            if ($F) {
                $mimetype = $F->getGenericClassName($file->getPathname());
            } else {
                $mimetype = 'mimetypes-unknown';
            }
            $fileIcon = workspace_classIcon($mimetype, $filename, $fileSizeText);
        }

        $fileIcon->setMetadata('filetype', 'file');
    }

    return $fileIcon;
}


/**
 * Returns a listview containing icons representing the content of the specified folder.
 *
 * @param string $path		The path of the folder that should be displayed in the listview.
 * @return Widget_Frame
 */
function workspace_folderListView($path)
{
    require_once dirname(__FILE__) . '/files.ctrl.php';
    require_once dirname(__FILE__) . '/files.php';

    $babFileNameTranslationBackup = $GLOBALS['babFileNameTranslation'];
    $GLOBALS['babFileNameTranslation'] = array();

    $workspaceId = workspace_getCurrentWorkspace();

    $versioning = workspace_getWorkspaceFileVersioning($workspaceId);
    $trashPath = workspace_getWorkspaceTrashPath($workspaceId);

    //$pathIsTrashPath = ($trashPath === $path);


    $canDeleteFolder = workspace_canDeleteFolderInFolder($path);
    $canDeleteFile = workspace_canDeleteFileInFolder($path);
    $canDefinitiveDeleteFile = ($path === $trashPath && workspace_userIsWorkspaceAdministrator());
    $canUndeleteFile = ($path === $trashPath);
    $canDownloadFile = workspace_canDownloadFileFromFolder($path);
    $canRenameFile = workspace_canRenameFileInFolder($path);

    $W = bab_Widgets();
    $listView = $W->Frame('workspace_file_listview')
        ->setLayout($W->FlowLayout()
            ->setSpacing(4, 'px')
            ->setVerticalAlign('bottom')
        )->addClass('workspace-filemanager-view', Func_Icons::ICON_TOP_48);

    $listView->setMetadata('movefileurl', workspace_Controller()->Files()->moveFile('__1__', '__2__')->url());
    $listView->setMetadata('movefolderurl', workspace_Controller()->Files()->moveFolder('__1__', '__2__')->url());

    if ($path === $trashPath) {
        $files = workspace_listTrash(); /* search files with bab_CollectiveDirIterator() */
    } else {
        $files = workspace_listFolder($path); /* search files with bab_CollectiveDirIterator() */
    }

    foreach ($files as $file) {

        $fileIcon = workspace_fileIcon($file);

        $fileIcon->setMetadata('pathname', urlencode($file->getFmPathname()));

        $fileIcon->addClass('workspace-draggable', 'widget-align-center', 'workspace-fileicon', 'widget-10em');

        $contextMenu = $W->Menu();
        $contextMenu->setLayout($W->FlowLayout())
        	->addClass(Func_Icons::ICON_LEFT_16);
        $theFileIsDeleted = false;

        if ($file->isDir()) {

            // Contextual actions.

            $link = $W->Link($fileIcon, workspace_Controller()->Files()->browse($file->getFmPathname()));
            $renameLink = null;
            $deleteLink = null;

            if ($canRenameFile) {
                $renameLink = $W->Link(
                    workspace_translate('Rename folder...'),
                    workspace_Controller()->Files()->editFolderProperties($file->getFmPathname())
                )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT);
            }

            if ($canDeleteFolder) {
                $deleteLink = $W->Link(
                    workspace_translate('Delete folder'),
                    workspace_Controller()->Files()->deleteFolder($file->getFmPathname())
                )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE);
                $deleteLink->setConfirmationMessage(workspace_translate('Are you sure you want to delete this folder?'));
            }

        } else {

            // Additional information of the file in file manager

            $fmfile = $file->getFmFile(); /* $fmfile : object of BAB_FmFolderFile, object of BAB_FolderFile */
            if (!$fmfile) {
                bab_debug($file->getPathname());
                continue;
            }
            $fmDownloadUrl = $fmfile->getDownloadUrl();
            $fmDisplayUrl = $fmDownloadUrl . '&inl=1';
            $fmDescription = $fmfile->getDescription();
            $fmAuthorId = $fmfile->getAuthorId();
            $fmAuthor = bab_getUserInfos($fmAuthorId);
            $fmModificationAuthorId = $fmfile->getModifierId();
            $fmModificationAuthor = bab_getUserInfos($fmModificationAuthorId);
            $fmCreationDate = $fmfile->getCreationDate(); /* Format : 2010-09-21 16:32:13 */
            $fmModificationDate = $fmfile->getModifiedDate(); /* Format : 2010-09-21 16:32:13 */
            $fmNBDownloads = $fmfile->getDownloads();
            $fmState = $fmfile->getState(); /* '', D(deleted) or X(cutted) */
            if ($fmState === 'D') {
            	$theFileIsDeleted = true;
            }
            $fmMajorVersion = $fmfile->getMajorVer();
            $fmMinorVersion = $fmfile->getMinorVer();

            $pathElements = explode('/', $file->getFmPathname());
            array_pop($pathElements);
            array_shift($pathElements);
            $originalFolder = implode('/', $pathElements);


            // Tooltip creation : with thumbnail if the file is an image.

            $htmlTooltip = $W->Frame();
            $html = '<br />';
            if ($fmAuthor !== false) {
                $html .= '<b>'.workspace_translate('Author').' :</b> '.$fmAuthor['sn'].' '.$fmAuthor['givenname'].'<br />';
            }
            if ($fmModificationAuthor !== false && $fmModificationAuthorId != $fmAuthorId) {
                $html .= '<b>'.workspace_translate('Last modifier').' :</b> '.$fmModificationAuthor['sn'].' '.$fmModificationAuthor['givenname'].'<br />';
            }
            if ($fmCreationDate != '') {
                $html .= '<b>'.workspace_translate('Creation date').' :</b> '.bab_shortDate(bab_mktime($fmCreationDate), true).'<br />';
            }
            if ($fmModificationDate != '' && $fmModificationDate != $fmCreationDate) {
                $html .= '<b>'.workspace_translate('Modification date').' :</b> '.bab_shortDate(bab_mktime($fmModificationDate), true).'<br />';
            }
            if ($fmDescription != '') {
                $html .= '<b>'.workspace_translate('Description').' :</b> '.$fmDescription.'<br />';
            }
            if ($versioning) {
                $html .= '<b>'.workspace_translate('Version').' :</b> '.$fmMajorVersion.'.'.$fmMinorVersion.'<br />';
            }
            $html .= '<b>'.workspace_translate('Downloads').' :</b> '.$fmNBDownloads.'<br />';

            if ($path === $trashPath) {
                $html .= '<b>'.workspace_translate('Original folder').' :</b> '. $originalFolder .'<br />';
            }
            $urlMiniature = workspace_fileIconUrl($fmfile->getFullPathname(), 235, 235);
            if ($urlMiniature !== null) {
                $htmlTooltip = $W->HBoxItems(
                    $W->Image($urlMiniature),
                    $W->Html($html),
                    $W->Label('')
                )->setHorizontalSpacing(1, 'em')
                ->setVerticalAlign('top');
            } else {
                $htmlTooltip = $W->Html($html);
            }

            // Contextual actions.

            $link = $W->Link($fileIcon, $fmDisplayUrl)->setOpenMode(Widget_Link::OPEN_POPUP);

            $downloadLink = null;
            $renameLink = null;
            $deleteLink = null;
            $definitiveDeleteLink = null;
            $undeleteLink = null;

            if ($canDownloadFile) {
                if ($fmDownloadUrl != null) {
                	$urlDownload = $fmDownloadUrl;
                } else {
                	$urlDownload = workspace_Controller()->Files()->displayFile($file->getFmPathname());
                }
                $downloadLink = $W->Link(
                    workspace_translate('Download file'),
                    $urlDownload
                )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_SAVE);
                $contextMenu->addItem($downloadLink);
            }

            if ($canRenameFile) {
                $renameLink = $W->Link(
                    workspace_translate('Rename file...'),
                    workspace_Controller()->Files()->editFileProperties($file->getFmPathname())
                )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT);
            }

            if ($canDeleteFile) {
                $deleteLink = $W->Link(
                    workspace_translate('Delete file'),
                    workspace_Controller()->Files()->deleteFile($file->getFmPathname())
                )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE);
                $deleteLink->setConfirmationMessage(workspace_translate('Are you sure you want to delete this file?'));
            }

            if ($canDefinitiveDeleteFile) {
                $definitiveDeleteLink = $W->Link(
                    workspace_translate('Delete file permanently'),
                    workspace_Controller()->Files()->deleteFile($file->getFmPathname(), 1)
                )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE);
                $definitiveDeleteLink->setConfirmationMessage(workspace_translate('Are you sure you want to delete this file permanently?'));
            }

            if ($canUndeleteFile) {
                $undeleteLink = $W->Link(
                    workspace_translate('Undelete file'),
                    workspace_Controller()->Files()->undeleteFile($file->getFmPathname())
                )->addClass('icon', Func_Icons::ACTIONS_EDIT_PASTE);
            }
        }

        if (isset($renameLink)) {
            $contextMenu->addItem($renameLink);
        }
        if (isset($deleteLink)) {
            $contextMenu->addItem($deleteLink);
        }
        if (isset($undeleteLink)) {
            $contextMenu->addItem($undeleteLink);
        }
        if (isset($definitiveDeleteLink)) {
            $contextMenu->addItem($definitiveDeleteLink);
        }
        if (isset($htmlTooltip)) {
            $contextMenu->addItem($htmlTooltip);
        }

        $menuEmpty = (count($contextMenu->getLayout()->getItems()) == 0);
        if (!$menuEmpty) {
            $contextMenu->attachTo($fileIcon);
        }


        // Deleted files don't appear except when displaying the trash.

        if (!$theFileIsDeleted || $path === $trashPath) {
            if (!$menuEmpty) {
                $listView->addItem($contextMenu);
            }
            $listView->addItem($link);
        }
    }

    $GLOBALS['babFileNameTranslation'] = $babFileNameTranslationBackup;

    return $listView;
}






/**
 * @param string $path
 * @return Widget_SimpleTreeview
 */
function workspace_folderTreeView($path)
{
	require_once dirname(__FILE__) . '/files.ctrl.php';
	require_once dirname(__FILE__) . '/files.php';
	require_once $GLOBALS['babInstallPath'] . '/utilit/filemanApi.php';

	$W = bab_Widgets();

	$treeview = $W->SimpleTreeview('workspace_file_treeview');
	$treeview->addClass(Func_Icons::ICON_LEFT_16)
		->hideToolbar()
		->setPersistent(true);


	$workspaceId = workspace_getCurrentWorkspace();
	$trashPath = workspace_getWorkspaceTrashPath($workspaceId);

	$pathElements = explode('/', $path);
	$rootPath = $currentPath = $pathElements[0];

	$files = workspace_listFolderRecursive($currentPath);


	foreach ($files as $file) {
		if ($file->isDir()) {
			$fmPathname = $file->getFmPathname();

			$folder = $treeview->createElement(
				urlencode($fmPathname),
				'directory',
				bab_toHtml($file->getFilename()),
				bab_toHtml($file->getFilename()),
				workspace_Controller()->Files()->browse($fmPathname)->url()
			);

			$icon = $W->Icon($file->getFilename(), Func_Icons::PLACES_FOLDER);

			if ($trashPath !== $path) {
				$icon->addClass('workspace-droppable');
				$icon->setMetadata('filetype', 'folder');
				$icon->setMetadata('pathname', urlencode($file->getFmPathname()));
			}

			$folder->setItem($icon);
			if (dirname($fmPathname) == $rootPath) {

				if ($trashPath !== $path && substr($path, 0, strlen($fmPathname)) != $fmPathname) {
					// Here we have a root folder which is not the main workspace folder, so we ignore it.
					continue;
				}
				$parentId = null;
			} else {
				$parentId = urlencode(dirname($fmPathname));
			}
			$treeview->appendElement($folder, $parentId);
		}
	}

	if (workspace_canViewTrash()) {
		$workspaceId = workspace_getCurrentWorkspace();
		$trashPath = workspace_getWorkspaceTrashPath($workspaceId);


		$trash = $treeview->createElement(
			urlencode($trashPath),
			'directory',
			bab_toHtml(workspace_translate('Trash')),
			bab_toHtml(workspace_translate('Trash')),
			workspace_Controller()->Files()->browseTrash()->url()
		);

		$icon = $W->Icon(workspace_translate('Trash'), Func_Icons::PLACES_USER_TRASH);

		if ($trashPath !== $path) {
			$icon->addClass('workspace-droppable');
			$icon->setMetadata('filetype', 'folder');
			$icon->setMetadata('pathname', $trashPath);
		}
		$trash->setItem($icon);

		$treeview->appendElement($trash, null);
	}

	$treeview->highlightElement(urlencode($path));

	return $treeview;
}



/**
 * Creates a location bar corresponding to the specified path.
 *
 * @param string $path	The path is something like "DG12/Workspace name/subfolder1/subfolder2"
 *
 * @return Widget_Frame
 */
function workspace_folderLocationBar($path)
{
	$W = bab_Widgets();

	$locationbar = workspace_Locationbar('location-toolbar');

	$locationbar->addItem($W->Icon(workspace_translate("Path:")));

	$workspaceId = workspace_getCurrentWorkspace();
	$trashPath = workspace_getWorkspaceTrashPath($workspaceId);

	if ($trashPath === $path) {
		$trash = $W->Icon(workspace_translate('Trash'), Func_Icons::PLACES_USER_TRASH);
		$trash->setMetadata('pathname', urlencode($trashPath));
		$actionButton = $W->Link($trash, workspace_Controller()->Files()->browseTrash());
		$locationbar->addItem($actionButton);
		return $locationbar;
	}

	$pathElements = explode('/', $path); // The path is something like "DG12/Workspace name/subfolder1/subfolder2"
	$delegation = array_shift($pathElements); // First part of the path: "DG12"
	$currentPath = array_shift($pathElements); // Second part of the path: "Workspace name"
	$home = $W->Icon($currentPath, 'places-user-home');
	$home->setMetadata('pathname', urlencode($delegation . '/' . $currentPath));
	$home->addClass('workspace-droppable');

	$actionButton = $W->Link($home, workspace_Controller()->Files()->browse($delegation . '/' . $currentPath));
	$locationbar->addItem($actionButton);
	foreach($pathElements as $pathElement) {
		if ($pathElement) {
			$currentPath .= '/' . $pathElement;
			$arrow = $W->Icon($pathElement, Func_Icons::ACTIONS_ARROW_RIGHT);
			$arrow->setMetadata('pathname', urlencode($delegation . '/' . $currentPath));
			$arrow->addClass('workspace-droppable');

			$actionButton = $W->Link($arrow, workspace_Controller()->Files()->browse($delegation . '/' . $currentPath));
			$locationbar->addItem($actionButton);
		}
	}

	return $locationbar;
}



/**
 * @param string $path			The path. Eg. 'DG0/folder1/folder1_1'
 * @return Widget_Frame
 */
function workspace_folderSplitView($path)
{
	require_once dirname(__FILE__) . '/files.ctrl.php';
	require_once dirname(__FILE__) . '/files.php';

	$canCreateFolder = workspace_canCreateFolderInFolder($path);
	$canUploadFile = workspace_canUploadFileInFolder($path);

	$W = bab_Widgets();

	// Toolbar

	$toolbar = workspace_Toolbar('main-toolbar');

	$newFolderIcon = $W->Icon(workspace_translate('Create folder'), Func_Icons::ACTIONS_FOLDER_NEW);
	$newFolderButton = $W->Link($newFolderIcon, workspace_Controller()->Files()->addFolder($path));

	$uploadFileIcon = $W->Icon(workspace_translate('Upload file'), Func_Icons::ACTIONS_DOCUMENT_NEW);
	$uploadFileButton = $W->Link($uploadFileIcon, workspace_Controller()->Files()->addFile($path));


	$slideshowIcon = $W->Icon(workspace_translate("Slideshow"), Func_Icons::MIMETYPES_IMAGE_X_GENERIC);
	if (isset($GLOBALS['bab_UseCoolirisSlideshow']) && $GLOBALS['bab_UseCoolirisSlideshow']) {
		$slideshowButton = $W->Link($slideshowIcon, workspace_Controller()->Files()->browse($path, null, 'slideshow'));
	} else {
		$slideshowButton = $W->Link($slideshowIcon, 'javascript:PicLensLite.start();');
	}

	if ($canCreateFolder) {
		$toolbar->addItem($newFolderButton);
	}
	if ($canUploadFile) {
		$toolbar->addItem($uploadFileButton);
	}
//	$toolbar->addItem($slideshowButton);


	$locationbar = workspace_folderLocationBar($path);

	$treeview = workspace_folderTreeView($path);
//	$treeview->setSizePolicy(Widget_SizePolicy::MINIMUM);
	$treeview->setSizePolicy('workspace-side-panel minimum');
	$treeview->addClass('workspace-resizable-panel');

	$listView = workspace_folderListView($path);

	$explorer = $W->VBoxItems(
		$toolbar,
		$W->HBoxItems(
			$treeview,
			$W->VBoxItems(
				$locationbar,
				$listView->addClass('workspace-vertical-auto')
			)
		)->addClass('expand')
	)->setId('file_explorer');

	return $explorer;
}




/**
 * @return Widget_Form
 */
function workspace_filesConfigurationEditor()
{
	$W = bab_Widgets();

	$form = $W->Form();
	$frame = new workspace_BaseForm('workspace_configuration_editor');
	$frame->setName('configuration');
	$frame->addClass('workspace-dialog', Func_Icons::ICON_LEFT_16);


	$notifyCheckbox = $W->CheckBox()->setName('notify');
	$notifyFormItem = workspace_FormField(workspace_translate('Notify workspace members when a file is uploaded'), $notifyCheckbox);

	$frame->addItem($notifyFormItem);


	$frame->addButton(
	    $W->SubmitButton('save')
			->validate(true)
			->setAction(workspace_Controller()->Files()->saveConfiguration())
			->setLabel(workspace_translate("Save configuration"))
    )
	->addButton(
	    $W->SubmitButton('cancel')
			->setAction(workspace_Controller()->Files()->Cancel())
			->setLabel(workspace_translate("Cancel"))
    );

	$form->setLayout($W->VBoxLayout())->addItem($frame);

	$form->setHiddenValue('tg', 'addon/workspace/main');

	return $form;
}
