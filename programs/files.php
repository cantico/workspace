<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/workspaces.php';


/**
 *
 * @param string 	$path	Path of the file (Example : c:\images\img.jpg)
 * @param int		$width
 * @param int		$height
 * @return Widget_Icon
 */
function workspace_fileIconUrl($path, $width, $height)
{
	$W = bab_Widgets();
	$T = bab_functionality::get('Thumbnailer');
	$F = bab_functionality::get('FileInfos');

//	$fileSize = filesize($path);
//	if ($fileSize > 1024 * 1024) {
//		$fileSizeText = round($fileSize / (1024 * 1024), 1) . ' ' . workspace_translate('MB');
//	} elseif ($fileSize > 1024) {
//		$fileSizeText = round($fileSize / 1024) . ' ' . workspace_translate('KB');
//	} else {
//		$fileSizeText = $fileSize . ' ' . workspace_translate('Bytes');
//	}

	$imageUrl = null;
	if ($T && $F) {

		$mimetype = $F->getGenericClassName($path);

		$T->setSourceFile($path);
        try {
		    $imageUrl = $T->getThumbnail($width, $height);
        } catch (Exception $e) {
            $imageUrl = null;
        }
	}
	if ($imageUrl) {
		$fileIcon = $W->Icon('');
		$fileIcon->setImageUrl($imageUrl);
	} else {
		if ($F) {
			$mimetype = $F->getGenericClassName(basename($path));
		} else {
			$mimetype = 'mimetypes-unknown';
		}
		$fileIcon = $W->Icon('', $mimetype);
	}

	return $imageUrl;
}


/**
 * Checks whether the current user can list the content of the specified folder.
 *
 * @param string $folderPath		The folder path
 * @return bool
 */
function workspace_canBrowseFolder($folderPath)
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/filemanApi.php';
	$directory = new bab_Directory();
	// This is a hack to do some initialization. Do not remove.
	$canManage = $directory->canManage($folderPath);
	$pathName = $directory->getPathName();

	$canBrowse = canBrowse($pathName);
	return $canBrowse;
}

/**
 * Checks whether the current user can upload a file in the specified folder.
 *
 * @param string $folderPath			The folder path
 * @return bool
 */
function workspace_canUploadFileInFolder($folderPath)
{
	$workspaceId = workspace_getCurrentWorkspace();
	if ($folderPath === workspace_getWorkspaceTrashPath($workspaceId)) {
		return false;
	}
	require_once $GLOBALS['babInstallPath'] . '/utilit/filemanApi.php';
	$directory = new bab_Directory();
	// This is a hack to do some initialization. Do not remove.
	$directory->canManage($folderPath);
	$pathName = $directory->getPathName();

	$canUpload = canUpload($pathName);
	return $canUpload;
}



/**
 * Checks whether the current user can download a file from the specified folder.
 *
 * @param string $folderPath			The folder path
 * @return bool
 */
function workspace_canDownloadFileFromFolder($folderPath)
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/filemanApi.php';
	$directory = new bab_Directory();
	// This is a hack to do some initialization. Do not remove.
	$directory->canManage($folderPath);
	$pathName = $directory->getPathName();

	$canUpload = canDownload($pathName);
	return $canUpload;
}



/**
 * Checks whether the current user can delete a file in the specified folder.
 *
 * @param string $folderPath			The folder path
 * @return bool
 */
function workspace_canDeleteFileInFolder($folderPath)
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/filemanApi.php';
	$directory = new bab_Directory();
	// This is a hack to do some initialization. Do not remove.
	$directory->canManage($folderPath);
	$pathName = $directory->getPathName();

	$canDelete = canDelFile($pathName);
	return $canDelete;
}


/**
 * Checks whether the current user can delete a (sub)folder in the specified folder.
 *
 * @param string $folderPath			The folder path
 * @return bool
 */
function workspace_canDeleteFolderInFolder($folderPath)
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/filemanApi.php';
	$directory = new bab_Directory();
	// This is a hack to do some initialization. Do not remove.
	$directory->canManage($folderPath);
	$pathName = $directory->getPathName();

	$canDelete = canDelFile($pathName);
	return $canDelete;
}


/**
 * Checks whether the current user can create a (sub)folder in the specified folder.
 *
 * @param string $folderPath			The folder path
 * @return bool
 */
function workspace_canCreateFolderInFolder($folderPath)
{
	$workspaceId = workspace_getCurrentWorkspace();
	if ($folderPath === workspace_getWorkspaceTrashPath($workspaceId)) {
		return false;
	}
	require_once $GLOBALS['babInstallPath'] . '/utilit/filemanApi.php';
	$directory = new bab_Directory();
	// This is a hack to do some initialization. Do not remove.
	$directory->canManage($folderPath);
	$pathName = $directory->getPathName();

	$canCreateFolder = canCreateFolder($pathName);
	return $canCreateFolder;
}


/**
 * Checks whether the current user can rename a file in the specified folder.
 *
 * @param string $folderPath			The folder path
 * @return bool
 */
function workspace_canRenameFileInFolder($folderPath)
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/filemanApi.php';
	$directory = new bab_Directory();
	// This is a hack to do some initialization. Do not remove.
	$directory->canManage($folderPath);
	$pathName = $directory->getPathName();

	$canRename = canUpdate($pathName);
	return $canRename;
}



/**
 * Checks whether the current user can replace a file in the specified folder.
 *
 * @param string $folderPath			The folder path
 * @return bool
 */
function workspace_canUpdateFileInFolder($folderPath)
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/filemanApi.php';
	$directory = new bab_Directory();
	// This is a hack to do some initialization. Do not remove.
	$directory->canManage($folderPath);
	$pathName = $directory->getPathName();

	$canUpdate = canUpdate($pathName);
	return $canUpdate;
}



/**
 * Checks whether the current user can move a file between the specified folders.
 *
 * @param string $sourceFolderPath			The source folder path
 * @param string $destFolderPath			The dest folder path
 * @return bool
 */
function workspace_canMoveFile($sourceFolderPath, $destFolderPath)
{
	$canMove = workspace_canDeleteFileInFolder($sourceFolderPath)
				&& workspace_canUploadFileInFolder($destFolderPath);
	return $canMove;
}



/**
 * Checks whether the current user can move a file between the specified folders.
 *
 * @param string $sourceFolderPath			The source folder path
 * @param string $destFolderPath			The dest folder path
 * @return bool
 */
function workspace_canMoveFolder($sourceFolderPath, $destFolderPath)
{
	$canMove = workspace_canDeleteFolderInFolder($sourceFolderPath)
				&& workspace_canCreateFolderInFolder($destFolderPath);
	return $canMove;
}





/**
 * Checks whether the current user can view the trash folder.
 *
 * @return bool
 */
function workspace_canViewTrash()
{
	return workspace_userIsWorkspaceAdministrator();
}


/**
 * Lists the content of a folder in the addon's upload folder.
 *
 * @param string	$relativePath	The relative path of the folder from the addon's upload path.
 * @return array
 */
function workspace_listFolder2($relativePath)
{
	$dirIterator = new DirectoryIterator($GLOBALS['babAddonUpload'] . $relativePath);
	$_files = array();

	foreach ($dirIterator as $file) {
		$filename = $file->getFilename();
		if ($filename === '.' || $filename === '..') {
			continue;
		}

		$_files[$file->getPathname()] = $file->getFileInfo();
	}

	return $_files;
}



function workspace_compareFilesByNameDirFirst(bab_FileInfo $file1, bab_FileInfo $file2)
{
	if ($file1->isDir() && !$file2->isDir()) {
		return -1;
	}
	if (!$file1->isDir() && $file2->isDir()) {
		return +1;
	}
	return strcasecmp($file1->getFilename(), $file2->getFilename());
}





/**
 * Lists the content of a folder in the addon's upload folder.
 *
 * @param string	$relativePath	The relative path of the folder from the addon's upload path.
 * @param int    $iExcludeFilter bab_DirectoryFilter::DIR, bab_DirectoryFilter::HIDDEN, bab_DirectoryFilter::DOT, bab_DirectoryFilter::FILE
 *
 * @return bab_FileInfo[]
 */
function workspace_listFolder($relativePath, $iExcludeFilter = 0)
{
	require_once $GLOBALS['babInstallPath'] . '/utilit/filemanApi.php';

	$oBabDir = new bab_Directory();

	$_files = array();

	$sPathName = $relativePath;
	$oIterator = $oBabDir->getEntries($sPathName, $iExcludeFilter);
	if ($oIterator instanceof bab_CollectiveDirIterator) {
		/* @var $oItem bab_FileInfo */
		foreach ($oIterator as $oItem) {

			$_files[$oItem->getFmPathname()] = $oItem;
		}
	}

	uasort($_files, 'workspace_compareFilesByNameDirFirst');

	return $_files;
}



/**
 *
 * @return multitype:BAB_FolderFile
 */
function workspace_listTrash()
{
	global $babDB, $babBody;

	$delegationId = workspace_getCurrentWorkspace();

	$folderFileSet = new BAB_FolderFileSet();
	$state =& $folderFileSet->aField['sState'];
	$idDgOwner =& $folderFileSet->aField['iIdDgOwner'];

	$criteria = $state->in('D');
	$criteria = $criteria->_and($idDgOwner->in($delegationId));

	$folderFileSet->select($criteria, array('sName' => 'ASC'));

	$_files = array();

	if (!is_null($folderFileSet) && $folderFileSet->count() > 0) {

		/* @var $folderFile BAB_FolderFile */
		while ($folderFile = $folderFileSet->next()) {
			$fileInfo = new bab_FileInfo($folderFile->getFullPathname());
//			$fileInfo = $folderFile->getFullPathname();
			$_files[$fileInfo->getFmPathname()] = $fileInfo;
		}

		uasort($_files, 'workspace_compareFilesByNameDirFirst');
	}

	return $_files;
}




// Prior to PHP5.1 RecursiveIteratorIterator::SELF_FIRST did not exist
if (!defined('RIT_SELF_FIRST')) {
	define('RIT_SELF_FIRST', RecursiveIteratorIterator::SELF_FIRST);
}



/**
 * Recursively lists the sub-folders of a folder in the addon's upload folder.
 *
 * @param string	$relativePath	The relative path of the folder from the addon's upload path.
 * @return array
 */
function workspace_listFolderRecursive($path)
{
	$files = workspace_listFolder($path, bab_DirectoryFilter::FILE);

	foreach ($files as $file) {
		$filename = $file->getFilename();
		if ($filename === '.' || $filename === '..' || !$file->isDir()) {
			continue;
		}
		$files += workspace_listFolderRecursive($file->getFmPathname());
	}

	return $files;
}



/**
 * Renames a file before moving it to the trash.
 *
 * @param string $pathname The pathname of the file to move to trash eg. 'DGxx/RootFolderName/SubFolder/Filename.ext'.
 * @return string The new pathname or false
 */
function workspace_renameFilenameToTrash($pathname)
{
	$directory = new bab_Directory();

	$dirname = dirname($pathname);
	$basename = basename($pathname);

	$newPathname = $dirname . '/.' . $basename;
	if (!$directory->renameFile($pathname, $newPathname)) {
		return false;
	}

	return $newPathname;
}



/**
 * Renames a file before removing it from the trash.
 *
 * @param string $pathname The pathname of the file to remove from trash eg. 'DGxx/RootFolderName/SubFolder/.Filename.ext'.
 * @return string The new pathname or false
 */
function workspace_renameFilenameFromTrash($pathname)
{
	$directory = new bab_Directory();

	$dirname = dirname($pathname);
	$basename = basename($pathname);

	$newPathname = $dirname . '/' . substr($basename, 1);
	if (!$directory->renameFile($pathname, $newPathname)) {
		return false;
	}

	return $newPathname;
}



/**
 * Moves the specified file to the trash.
 *
 * @param string $path The path of the file to move to trash eg. 'DGxx/RootFolderName/SubFolder/Filename.ext'.
 * @return boolean
 */
function workspace_moveFileToTrash($path)
{
	$delegationId = workspace_getCurrentWorkspace();

	// We rename the file before moving it to the trash.
	$path = workspace_renameFilenameToTrash($path);
	if ($path === false) {
		return false;
	}

	$oFolderFileSet = new BAB_FolderFileSet();

	$oState =& $oFolderFileSet->aField['sState'];
	$oPathName =& $oFolderFileSet->aField['sPathName'];
	$oName =& $oFolderFileSet->aField['sName'];
	$idDgOwner =& $oFolderFileSet->aField['iIdDgOwner'];

	$pathElements = explode('/', $path);

	$dg = array_shift($pathElements);
	$filename = array_pop($pathElements);
	$basepath = implode('/', $pathElements) . '/';


	$oCriteria = $oState->in('');
	$oCriteria = $oCriteria->_and($oPathName->in($basepath));
	$oCriteria = $oCriteria->_and($oName->in($filename));
	$oCriteria = $oCriteria->_and($idDgOwner->in($delegationId));

	$oFolderFile = $oFolderFileSet->get($oCriteria);
	if (is_null($oFolderFile)) {
		return false;
	}
	$oFolderFile->setState('D');
	$oFolderFile->save();
	return true;
}




/**
 * Removes the specified file from the trash ans put it back to its origianl folder.
 *
 * @param string $path The path of the file to move to trash eg. 'DGxx/RootFolderName/SubFolder/Filename.ext'.
 * @return boolean
 */
function workspace_removeFileFromTrash($path)
{
	// We rename the file before removing it from the trash.
	$path = workspace_renameFilenameFromTrash($path);
	if ($path === false) {
		return false;
	}

	$delegationId = workspace_getCurrentWorkspace();

	$oFolderFileSet = new BAB_FolderFileSet();

	$oState =& $oFolderFileSet->aField['sState'];
	$oPathName =& $oFolderFileSet->aField['sPathName'];
	$oName =& $oFolderFileSet->aField['sName'];
	$idDgOwner =& $oFolderFileSet->aField['iIdDgOwner'];

	$pathElements = explode('/', $path);

	$dg = array_shift($pathElements);
	$filename = array_pop($pathElements);
	$basepath = implode('/', $pathElements) . '/';


	$oCriteria = $oState->in('D');
	$oCriteria = $oCriteria->_and($oPathName->in($basepath));
	$oCriteria = $oCriteria->_and($oName->in($filename));
	$oCriteria = $oCriteria->_and($idDgOwner->in($delegationId));

	$oFolderFile = $oFolderFileSet->get($oCriteria);
	if (is_null($oFolderFile)) {
		return false;
	}
	$oFolderFile->setState('');
	$oFolderFile->save();
	return true;
}

define('WORKSPACE_TRASH_FILENAME_PREFIX', '.');

function workspace_convertTrashFilename($filename)
{
	return WORKSPACE_TRASH_FILENAME_PREFIX . $filename;
}


function workspace_revertTrashFilename($filename)
{
	if (substr($filename, 0, strlen(WORKSPACE_TRASH_FILENAME_PREFIX)) === WORKSPACE_TRASH_FILENAME_PREFIX) {
		$filename = substr($filename, strlen(WORKSPACE_TRASH_FILENAME_PREFIX));
	}
	return $filename;
}