<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/workspacewidgets.php';
require_once dirname(__FILE__) . '/workspaces.php';
require_once $GLOBALS['babInstallPath'].'utilit/searchapi.php';




/**
 * @return Widget_Displayable_Interface
 */
function workspace_directoriesSearchResultList($keywords)
{
	require_once dirname(__FILE__) . '/directories.ui.php';

	$W = bab_Widgets();
	$directoryId = workspace_getWorkspaceDirectoryId();
	$workspaceId = workspace_getCurrentWorkspace();

	$realm = bab_Search::getRealm('bab_SearchRealmDirectories');

	$directoryField = $realm->getDirField('id_directory');
	$snField = $realm->getDirField('sn');
	$givenNameField = $realm->getDirField('givenname');
	$criteria = $realm->getDefaultCriteria();
	$criteria = $criteria->_AND_($snField->contain($keywords));
	$criteria = $criteria->_OR_($givenNameField->contain($keywords));
	$criteria = $criteria->_AND_($directoryField->is($directoryId));
//	$criteria = $criteria->_AND_($realm->id_dgowner->is($workspaceId));

	$dirEntries = array();
	$results = array();

	foreach($realm->search($criteria) as $record) {
		$results[$record->id] = array();
		foreach($realm->getFields() as $field) {
//			if ($field->searchable()) {
//				$fieldname = $field->getName();
//				$results[$record->id][$fieldname] = array(
//					'name' => $field->getDescription(),
//					'value' => $record->$fieldname
//				);
				$results[$record->id] = bab_composeUserName($record->givenname, $record->sn);
//			}
		}
	}

	bab_Sort::natcasesort($results, bab_Sort::CASE_INSENSITIVE);

	$T = bab_functionality::get('Thumbnailer', false);

	$resultBox = $W->FlowLayout();

	foreach ($results as $resultId => $result) {

		$dirEntry = bab_getDirEntry($resultId, BAB_DIR_ENTRY_ID);


		$resultRow = workspace_detailedContactEntry($dirEntry, $resultId)->addClass('workspace-directory-entry compact');

		$resultBox->addItem($resultRow);
	}

	$section = $W->Section(
		workspace_translate('Directories'),
		$resultBox,
		2
	)
//	->addClass('icon-left-32 icon-left icon-32x32')
	->setFoldable(true);

	$menu = $section->addContextMenu();
	$menu->addItem($W->Label(sprintf(workspace_translate('%d result', '%d results', count($results)), count($results)))->addClass('section-header-tag'));

	return $section;
}





/**
 * @return Widget_Displayable_Interface
 */
function workspace_articlesSearchResultList($keywords)
{
	$W = bab_Widgets();
	$workspaceId = workspace_getCurrentWorkspace();

	$realm = bab_Search::getRealm('bab_SearchRealmArticles');

	$realm->setSortMethod('relevance');

	$criteria = $realm->getDefaultCriteria();
	$criteria = $criteria->_AND_($realm->head->contain($keywords));
	$criteria = $criteria->_OR_($realm->body->contain($keywords));
	$criteria = $criteria->_OR_($realm->title->contain($keywords));
	$criteria = $criteria->_AND_($realm->id_dgowner->is($workspaceId));

	$results = array();

	foreach($realm->search($criteria) as $record) {
		$results[$record->id] = array();
		foreach($realm->getFields() as $field) {
			$fieldname = $field->getName();
			$results[$record->id][$fieldname] = array(
				'name' => $field->getDescription(),
				'value' => $record->$fieldname
			);
		}
	}

	$resultBox = $W->VBoxLayout()->setVerticalSpacing(1, 'em');

	foreach ($results as $resultId => $result) {

		$id_author = $result['id_author']['value'];
		$user = bab_getUserById($id_author);
//		var_dump($user);
		$authorName = bab_composeUserName($user['firstname'], $user['lastname']);


		$resultRow = $W->HBoxItems(
//			$W->Link($W->Icon($article['title']['value'], Func_Icons::MIMETYPES_TEXT_X_GENERIC), workspace_Controller()->Articles()->show($articleId))
			$W->Icon('', Func_Icons::MIMETYPES_OFFICE_DOCUMENT)->setSizePolicy(Widget_SizePolicy::MINIMUM),
			$W->VBoxItems(
				$W->FlowItems(
					$W->Link($W->Title($result['title']['value'], 5), workspace_Controller()->Articles()->show($resultId)),
					$W->Label(workspace_translate('by') . ' ' . $authorName),
					$W->Label('-'),
					$W->Label(bab_longDate(bab_mktime($result['date_publication']['value'])))
				)->setHorizontalSpacing(1, 'em'),
				$W->Html(bab_abbr(strip_tags($result['head']['value']), BAB_ABBR_FULL_WORDS, 200))->addClass('light')
			)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
			);

		$resultBox->addItem($resultRow);
	}


	$section = $W->Section(
		workspace_translate('Articles'),
		$resultBox,
		2
	)
	->addClass('icon-top-24 icon-top icon-24x24')
	->setFoldable(true);

	$menu = $section->addContextMenu();
	$menu->addItem($W->Label(sprintf(workspace_translate('%d result', '%d results', count($results)), count($results)))->addClass('section-header-tag'));

	return $section;
}





/**
 * @return Widget_Displayable_Interface
 */
function workspace_forumSearchResultList($keywords)
{
	$W = bab_Widgets();
	$workspaceId = workspace_getCurrentWorkspace();

	$realm = bab_Search::getRealm('bab_SearchRealmForumPosts');

	$criteria = $realm->getDefaultCriteria();
	$criteria = $criteria->_AND_($realm->subject->contain($keywords));
	$criteria = $criteria->_OR_($realm->message->contain($keywords));
	$criteria = $criteria->_AND_($realm->id_dgowner->is($workspaceId));

	$results = array();

	foreach($realm->search($criteria) as $record) {
		$results[$record->id] = array();
		foreach($realm->getFields() as $field) {
			$fieldname = $field->getName();
			$results[$record->id][$fieldname] = array(
				'name' => $field->getDescription(),
				'value' => $record->$fieldname
			);
		}
	}

	$resultBox = $W->VBoxLayout()->setVerticalSpacing(1, 'em');

	/* @var $replaceFactory workspace_replace */
	$replaceFactory = bab_getInstance('workspace_replace');


	foreach ($results as $resultId => $result) {

	    $replaceFactory->ref($result['message']['value']);

		$resultRow = $W->HBoxItems(
			$W->Icon('', Func_Icons::APPS_FORUMS)->setSizePolicy(Widget_SizePolicy::MINIMUM),
			$W->VBoxItems(
				$W->FlowItems(
					$W->Link($W->Title($result['subject']['value'], 5), workspace_Controller()->Forum()->listPosts($result['id_thread']['value'], $resultId)),
					$W->Label(workspace_translate('by') . ' ' . $result['author']['value']),
					$W->Label('-'),
					$W->Label(bab_longDate(bab_mktime($result['date']['value'])))
				)->setHorizontalSpacing(1, 'em'),
				$W->Html(bab_abbr(strip_tags($result['message']['value']), BAB_ABBR_FULL_WORDS, 200))->addClass('light')
			)->setSizePolicy(Widget_SizePolicy::MAXIMUM)

		);

		$resultBox->addItem($resultRow);
	}

	$section = $W->Section(
		workspace_translate('Forum'),
		$resultBox,
		2
	)
	->addClass('icon-top-24 icon-top icon-24x24')
	->setFoldable(true);

	$menu = $section->addContextMenu();
	$menu->addItem($W->Label(sprintf(workspace_translate('%d result', '%d results', count($results)), count($results)))->addClass('section-header-tag'));

	return $section;
}





/**
 * @return Widget_Displayable_Interface
 */
function workspace_filesSearchResultList($keywords)
{
	$W = bab_Widgets();
	$workspaceId = workspace_getCurrentWorkspace();

//	$postsRealm->setSortMethod('relevance');

	$realm = bab_Search::getRealm('bab_SearchRealmFiles');

	$primaryCriteria = $realm->search->contain($keywords);
	$realm->setFieldlessCriteria($primaryCriteria);

	$criteria = $realm->getDefaultCriteria();
	$criteria = $criteria->_AND_($realm->name->contain($keywords));
	$criteria = $criteria->_AND_($realm->id_dgowner->is($workspaceId));


	$results = array();

	foreach($realm->search($criteria) as $record) {
		if( $record->id_dgowner == $workspaceId ){
			$posts[$record->id] = array();
			foreach($realm->getFields() as $field) {
				$fieldname = $field->getName();
				$results[$record->id][$fieldname] = array(
					'name' => $field->getDescription(),
					'value' => $record->$fieldname
				);
			}
		}
	}
//	var_dump($results);
//	die;

	$resultBox = $W->FlowLayout()->setSpacing(4, 'px');

	foreach ($results as $resultId => $result) {

		$filePathname = 'DG' . workspace_getCurrentWorkspace() . '/' . $result['path']['value'] . '/' . $result['name']['value'];
		$resultRow = $W->VBoxItems(
			$W->Link($W->Icon($result['name']['value'], Func_Icons::MIMETYPES_TEXT_X_GENERIC), workspace_Controller()->Files()->displayFile($filePathname))
		);

		$resultBox->addItem($resultRow);
	}

	$section = $W->Section(
		workspace_translate('Files'),
		$resultBox,
		2
	)
	->addClass('workspace-filemanager-view')
	->addClass('icon-top-48 icon-top icon-48x48')
	->setFoldable(true);

	$menu = $section->addContextMenu();
	$menu->addItem($W->Label(sprintf(workspace_translate('%d result', '%d results', count($results)), count($results)))->addClass('section-header-tag'));

	return $section;
}





/**
 * @param array $calendarItem
 * @return Widget_Displayable_Interface
 */
function workspace_calendarSearchResultItem(array $calendarItem)
{
	$W = bab_Widgets();

	return $W->HBoxItems(
			$W->Icon('', Func_Icons::APPS_CALENDAR)->setSizePolicy(Widget_SizePolicy::MINIMUM),
			$W->VBoxItems(
				$W->FlowItems(
					$W->Link($W->Title($calendarItem['summary']['value'], 5), workspace_Controller()->Calendars()->display(workspace_CtrlCalendars::TYPE_WORKWEEK)),
					$W->Label(bab_longDate(bab_mktime($calendarItem['start_date']['value']))),
					$W->Label('-'),
					$W->Label(bab_longDate(bab_mktime($calendarItem['end_date']['value'])))
				)->setHorizontalSpacing(1, 'em'),
				$W->Label($calendarItem['location']['value']),
				$W->Html(bab_abbr(strip_tags($calendarItem['description']['value']), BAB_ABBR_FULL_WORDS, 200))->addClass('light')
			)->setSizePolicy(Widget_SizePolicy::MAXIMUM)

		);
}





/**
 * @return Widget_Displayable_Interface
 */
function workspace_calendarsSearchResultList($keywords = null, $fromDate = null, $toDate = null)
{
	require_once dirname(__FILE__) . '/calendars.php';

	$W = bab_Widgets();
	$workspaceId = workspace_getCurrentWorkspace();

	$realm = bab_Search::getRealm('bab_SearchRealmCalendars');

	$criteria = $realm->getDefaultCriteria();
	if (isset($keywords) && !empty($keywords)) {
		$criteria = $criteria->_AND_(
			$realm->summary->contain($keywords)->_OR_($realm->location->contain($keywords))
		);
	}

	if (isset($fromDate) && !empty($fromDate)) {
		$criteria = $criteria->_AND_($realm->end_date->greaterThanOrEqual($fromDate));
	}
	if (isset($toDate) && !empty($toDate)) {
		$criteria = $criteria->_AND_($realm->start_date->lessThanOrEqual($toDate));
	}

	$criteria1 = $criteria->_AND_(
		$realm->calendar->is(workspace_getSharedCalendarId())
	);

	$criteria2 = $criteria->_AND_(
		$realm->calendar->is(workspace_getPersonalCalendarId())
	);

	$results1 = array();

	foreach($realm->search($criteria1) as $record) {
		$events[$record->uid] = array();
		foreach($realm->getFields() as $field) {
			$fieldname = $field->getName();
			$results1[$record->uid][$fieldname] = array(
				'name' => $field->getDescription(),
				'value' => $record->$fieldname
			);
		}
	}

	$results2 = array();

	foreach($realm->search($criteria2) as $record) {
		$events[$record->uid] = array();
		foreach($realm->getFields() as $field) {
			$fieldname = $field->getName();
			$results2[$record->uid][$fieldname] = array(
				'name' => $field->getDescription(),
				'value' => $record->$fieldname
			);
		}
	}

	$resultBox1 = $W->VBoxLayout()->setVerticalSpacing(1, 'em');

	foreach ($results1 as $resultId => $result) {

		$resultRow = workspace_calendarSearchResultItem($result);

		$resultBox1->addItem($resultRow);
	}

	$resultBox2 = $W->VBoxLayout()->setVerticalSpacing(1, 'em');

	foreach ($results2 as $resultId => $result) {

		$resultRow = workspace_calendarSearchResultItem($result);

		$resultBox2->addItem($resultRow);
	}


	$section1 = $W->Section(
		workspace_translate('Shared calendar'),
		$resultBox1,
		2
	)
	->addClass('icon-top-24 icon-top icon-24x24')
	->setFoldable(true);

	$section2 = $W->Section(
		workspace_translate('Personal calendar'),
		$resultBox2,
		2
	)
	->addClass('icon-top-24 icon-top icon-24x24')
	->setFoldable(true);

	$menu1 = $section1->addContextMenu();
	$menu1->addItem($W->Label(sprintf(workspace_translate('%d result', '%d results', count($results1)), count($results1)))->addClass('section-header-tag'));
	$menu2 = $section2->addContextMenu();
	$menu2->addItem($W->Label(sprintf(workspace_translate('%d result', '%d results', count($results2)), count($results2)))->addClass('section-header-tag'));

	$section = $W->VBoxItems(
		$section1,
		$section2
	)->setVerticalSpacing(1,'em');

	return $section;
}





/**
 * @return Widget_Displayable_Interface
 */
function workspace_searchResultList($keywords)
{
	$W = bab_Widgets();

	$resultBox = $W->VBoxLayout();


	// Directories

	$resultBox->addItem(workspace_directoriesSearchResultList($keywords));

	// Articles

	$resultBox->addItem(workspace_articlesSearchResultList($keywords));


	// Forum

	$resultBox->addItem(workspace_forumSearchResultList($keywords));


	// Calendars

	$resultBox->addItem(workspace_calendarsSearchResultList($keywords));


	// Files

	$resultBox->addItem(workspace_filesSearchResultList($keywords));



	return $resultBox;
}