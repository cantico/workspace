<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


/* @var $App Func_App_Workspace */
$App = bab_functionality::get('App/Workspace');

if (!$App) {
    return;
}

$App->includeRecordController();

class workspace_Controller extends workspace_CtrlRecord
{

	protected function getControllerTg()
	{
		return 'addon/workspace/main';
	}

	/**
	 * Get object name to use in URL from the controller classname
	 * @param string $classname
	 * @return string
	 */
	protected function getObjectName($classname)
	{
		$prefix = strlen('workspace_Ctrl');
		return strtolower(substr($classname, $prefix));
	}


	/**
	 * @return workspace_CtrlCalendars
	 */
	public function Calendars($proxy = true)
	{
		require_once WORKSPACE_CTRL_PATH.'calendars.ctrl.php';
		return $this->App()->ControllerProxy('workspace_CtrlCalendars', $proxy);
	}

	/**
	 * @return workspace_CtrlFiles
	 */
	public function Files($proxy = true)
	{
		require_once WORKSPACE_CTRL_PATH.'files.ctrl.php';
		return $this->App()->ControllerProxy('workspace_CtrlFiles', $proxy);
	}


	/**
	 * @return workspace_CtrlDirectories
	 */
	public function Directories($proxy = true)
	{
		require_once WORKSPACE_CTRL_PATH.'directories.ctrl.php';
		return $this->App()->ControllerProxy('workspace_CtrlDirectories', $proxy);
	}

	/**
	 * @return workspace_CtrlPage
	 */
	public function Page($proxy = true)
	{
	    require_once WORKSPACE_CTRL_PATH.'page.ctrl.php';
	    return $this->App()->ControllerProxy('workspace_CtrlPage', $proxy);
	}


	/**
	 * @return workspace_CtrlArticles
	 */
	function Articles($proxy = true)
	{
		require_once WORKSPACE_CTRL_PATH.'articles.ctrl.php';
		return $this->App()->ControllerProxy('workspace_CtrlArticles', $proxy);
	}


	/**
	 * @return workspace_CtrlForum
	 */
	public function Forum($proxy = true)
	{
		require_once WORKSPACE_CTRL_PATH.'forum.ctrl.php';
		return $this->App()->ControllerProxy('workspace_CtrlForum', $proxy);
	}


	/**
	 * @return workspace_CtrlAdmin
	 */
	public function Admin($proxy = true)
	{
// 		if( !bab_isUserAdministrator() ){
// 			global $babBody;
// 			$babBody->addError(workspace_translate('Access denied'));
// 			workspace_redirect(workspace_Controller()->User()->displayHome());
// 			die;
// 		}
	    require_once WORKSPACE_CTRL_PATH.'admin.ctrl.php';
	    return $this->App()->ControllerProxy('workspace_CtrlAdmin', $proxy);
	}


	/**
	 * @return workspace_CtrlUser
	 */
	public function User($proxy = true)
	{
	    require_once WORKSPACE_CTRL_PATH.'user.ctrl.php';
	    return $this->App()->ControllerProxy('workspace_CtrlUser', $proxy);
	}




	/**
	 * @return workspace_CtrlPad
	 */
	public function Pad($proxy = true)
	{
	    require_once WORKSPACE_CTRL_PATH.'pad.ctrl.php';
	    return $this->App()->ControllerProxy('workspace_CtrlPad', $proxy);
	}



	/**
	 * @return workspace_CtrlSearch
	 */
	public function Search($proxy = true)
	{
		require_once WORKSPACE_CTRL_PATH.'search.ctrl.php';
		return $this->App()->ControllerProxy('workspace_CtrlSearch', $proxy);
	}

	/**
	 * @return workspace_CtrlTag
	 */
	public function Tag($proxy = true)
	{
	    require_once WORKSPACE_CTRL_PATH.'tag.ctrl.php';
	    return $this->App()->ControllerProxy('workspace_CtrlTag', $proxy);
	}
}