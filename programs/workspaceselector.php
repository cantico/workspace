<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

include_once 'base.php';
require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/workspaces.php';

function workspace_workspaceSelectorUL()
{
	/* @var $W Func_Widgets */
	$W = bab_Widgets();

	$workspaces = workspace_getWorkspaceList();
	$currentWorkspace = workspace_getCurrentWorkspace();

	$htmlUL = '<span id="workspaceselectorblock"><span id="workspaceselectorlbl"><b>' . workspace_translate('Workspaces') . '</b></span></span>';

	$htmlUL .= '<div id="workspaceselector" style="display:none;"><ul>';

	foreach ($workspaces as $workspace) {
		$curSelectWork = ($currentWorkspace == $workspace['id']) ? 'class="selectedWorkspace"' : '';
		$htmlUL .= '<li ' . $curSelectWork . ' ><a href="' . workspace_Controller()->User()->selectWorkspace($workspace['id'])->url() . '"><div>' . $workspace['name'] . '</div></a></li>';
	}

	$htmlUL .= '</ul></div>';

	return $W->html($htmlUL);
}
