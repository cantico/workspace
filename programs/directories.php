<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/workspaces.php';



function workspace_getDirectoryEntry($id, $directoryId = null)
{
    if (!isset($directoryId)) {
        $directoryId = workspace_getWorkspaceDirectoryId();
    }
    $dirEntry = bab_admGetDirEntry($id, BAB_DIR_ENTRY_ID, $directoryId);
    if (!is_array($dirEntry) || empty($dirEntry)) {
        return null;
    }

    $entry = array(
        'id' => $id,
        'givenname' => $dirEntry['givenname']['value'],
        'sn' => $dirEntry['sn']['value'],
        'email' => $dirEntry['email']['value'],
        'organisationname' => $dirEntry['organisationname']['value'],
        'departmentnumber' => $dirEntry['departmentnumber']['value'],
        'bstreetaddress' => $dirEntry['bstreetaddress']['value'],
        'bpostalcode' => $dirEntry['bpostalcode']['value'],
        'bcity' => $dirEntry['bcity']['value'],
        'bcountry' => $dirEntry['bcountry']['value'],
        'mobile' => $dirEntry['mobile']['value'],
        'btel' => $dirEntry['btel']['value'],
        'bfax' => $dirEntry['bfax']['value'],
        'photo' => isset($dirEntry['jpegphoto']['photo'])?$dirEntry['jpegphoto']['photo']:''
    );

    return $entry;
}




function workspace_getDirectoryEntryByUserId($userId)
{
    $dirEntry = bab_admGetDirEntry($userId, BAB_DIR_ENTRY_ID_USER);
    $entry = array(
        'givenname' => $dirEntry['givenname']['value'],
        'sn' => $dirEntry['sn']['value'],
        'email' => $dirEntry['email']['value'],
        'organisationname' => $dirEntry['organisationname']['value'],
        'departmentnumber' => $dirEntry['departmentnumber']['value'],
        'bstreetaddress' => $dirEntry['bstreetaddress']['value'],
        'bpostalcode' => $dirEntry['bpostalcode']['value'],
        'bcity' => $dirEntry['bcity']['value'],
        'bcountry' => $dirEntry['bcountry']['value'],
        'mobile' => $dirEntry['mobile']['value'],
        'btel' => $dirEntry['btel']['value'],
        'bfax' => $dirEntry['bfax']['value'],
        'photo' => isset($dirEntry['jpegphoto']['photo'])?$dirEntry['jpegphoto']['photo']:'',
        'userid' => $userId
    );

    return $entry;
}


function workspace_updateUserDirectoryEntryFromPortal($userId, $directoryId)
{
    global $babDB;

    $entries = $babDB->db_query(
                    'SELECT id FROM '.BAB_DBDIR_ENTRIES_TBL.'
                        WHERE id_directory=' . $babDB->quote($directoryId) . ' AND id_user='.$babDB->quote($userId)
    );
    if ($entry = $babDB->db_fetch_assoc($entries)) {
        $entryId = $entry['id'];
    } else {
        return false;
    }


    $entries = $babDB->db_query(
                'SELECT * FROM '.BAB_DBDIR_ENTRIES_TBL.'
                    WHERE id_directory=0 AND id_user='.$babDB->quote($userId)
    );

    if ($entry = $babDB->db_fetch_assoc($entries)) {
        $entry['id'] = $entryId;
        try {
            workspace_updateDbContact($entry, $entry['photo_data'], false);
        } catch (Exception $e) {
        }
    }
}


/**
 *
 * @param int		$id			The directoryId
 * @param array		$fields
 * @param string	$file		Original filename
 * @param string	$tmp_file	Temporary filename
 * @return int
 */
function workspace_addDbContact($id, $fields, $photoData = null, $userId = 0)
{
    global $babDB;

	if ($id == '') {
		$id = 0;
	}

//     if ( !empty($fields['email']) && !bab_isEmailValid($fields['email'])) {
//         throw new Exception("Your email is not valid !!");
//     }

    $res = $babDB->db_query('SELECT * FROM '.BAB_DBDIR_FIELDS_TBL);
    $req = '';
    while ($arr = $babDB->db_fetch_array($res)) {
        $rr = $babDB->db_fetch_array(
            $babDB->db_query('
                SELECT required
                FROM '.BAB_DBDIR_FIELDSEXTRA_TBL."
                WHERE id_directory='".($id !=0 ? 0: $babDB->db_escape_string($id))."'
                AND id_field='".$babDB->db_escape_string($arr['id'])."'"
            )
        );
        if ($arr['name'] != 'jpegphoto' && $rr['required'] === 'Y' && empty($fields[$arr['name']])) {
            $fields[$arr['name']] = '-';
//			throw new Exception('You must complete required fields: ' . $arr['name']);
        }

        if ($arr['name'] === 'jpegphoto' && $rr['required'] === 'Y' && empty($photoData)) {
            $tmp = $babDB->db_fetch_assoc(
                $babDB->db_query("
                    SELECT photo_data
                    FROM ".BAB_DBDIR_ENTRIES_TBL."
                    WHERE id_directory='".($id !=0 ? 0: $babDB->db_escape_string($id))."'
                    AND id='".$babDB->db_escape_string($userId)."'"
                )
            );

            if (empty($tmp['photo_data'])) {
                throw new Exception('You must complete required fields: ' . $arr['name']);
            }
        }

        if (isset($fields[$arr['name']]) && $arr['name'] !== 'jpegphoto') {
            if ($id > 0) {
                $req .= $arr['name'] . ',';
            } else {
                $req .= $arr['name'] . ',';
            }
        }
    }

    if (!empty($photoData)) {
        if ($id > 0) {
            $req .= 'photo_data,';
        } else {
            $req .= 'photo_data,';
        }
    }

    if (!empty($req)) {
        $req = 'INSERT INTO '.BAB_DBDIR_ENTRIES_TBL.' ('.$req.'id_directory, id_user) VALUES (';
        $babDB->db_data_seek($res, 0);
        while( $arr = $babDB->db_fetch_array($res)) {
            if( isset($fields[$arr['name']])) {
                $req .= $babDB->quote($fields[$arr['name']]) . ',';
            }
        }
        if (!empty($photoData)) {
            $req .= $babDB->quote($photoData).',';
        }
        $req .= $babDB->quote($id).', ' . $babDB->quote($userId) .  ')';
        $babDB->db_query($req);
        $iddbu = $babDB->db_insert_id();

        $babDB->db_query('
            UPDATE '.BAB_DBDIR_ENTRIES_TBL.'
            SET
                date_modification=NOW(),
                id_modifiedby='.$babDB->quote($GLOBALS['BAB_SESS_USERID']).'
            WHERE id='.$babDB->quote($iddbu)
        );

        include_once $GLOBALS['babInstallPath'].'utilit/eventdirectory.php';
        $event = new bab_eventDirectoryEntryCreated($iddbu);
        bab_fireEvent($event);

        return 1;
    }

    return 0;
}



/**
 * @param array		$entry
 * @param string	$photoData				Binary content of photo
 * @return int
 */
function workspace_updateDbContact($entry, $photoData)
{
    global $babDB;

    $babDB->db_query(
        'UPDATE '.BAB_DBDIR_ENTRIES_TBL.'
            SET
                sn=' . $babDB->quote($entry['sn']) . ',
                givenname=' . $babDB->quote($entry['givenname']) . ',
                email=' . $babDB->quote($entry['email']) . ',
                organisationname=' . $babDB->quote($entry['organisationname']) . ',
                departmentnumber=' . $babDB->quote($entry['departmentnumber']) . ',
                bstreetaddress=' . $babDB->quote($entry['bstreetaddress']) . ',
                bpostalcode=' . $babDB->quote($entry['bpostalcode']) . ',
                bcity=' . $babDB->quote($entry['bcity']) . ',
                bcountry=' . $babDB->quote($entry['bcountry']) . ',
                mobile=' . $babDB->quote($entry['mobile']) . ',
                bfax=' . $babDB->quote($entry['bfax']) . ',
                btel=' . $babDB->quote($entry['btel']) . ',
                date_modification=NOW(),
                id_modifiedby='.$babDB->quote($GLOBALS['BAB_SESS_USERID']).'
            WHERE id='.$babDB->quote($entry['id'])
    );

    $babDB->db_query(
        'UPDATE '.BAB_DBDIR_ENTRIES_TBL.'
            SET
                photo_data='.$babDB->quote($photoData) . '
            WHERE id='.$babDB->quote($entry['id'])
    );

    include_once $GLOBALS['babInstallPath'].'utilit/eventdirectory.php';
    $event = new bab_eventDirectoryEntryModified($entry['id']);
    bab_fireEvent($event);

    return 1;
}


function workspace_getDirEntryFromUserId($directoryId, $userId)
{
    global $babDB;

    $sql = 'SELECT id FROM ' . BAB_DBDIR_ENTRIES_TBL . ' WHERE id_directory=' . $babDB->quote($directoryId) . ' AND id_user=' . $babDB->quote($userId);
    $res = $babDB->db_query($sql);
    $entry = $babDB->db_fetch_assoc($res);
    return $entry['id'];
}



/**
 * Checks if the directory entry stored in the workspace directory
 * for the specified user ($userId) is more recent than the one in the directory
 * for all users group.
 *
 * @param int $userId
 * @param int $directoryId Directory to check (optional),
 * 	                       workspace directory if not specified.
 * @return boolean	False if the workspace directory entry is not up-to-date.
 */
function workspace_dirEntryIsUpToDate($userId, $directoryId = null)
{
    global $babDB;

    if (!isset($directoryId)) {
        $directoryId = workspace_getWorkspaceDirectoryId();
    }

    $sql = 'SELECT date_modification FROM ' . BAB_DBDIR_ENTRIES_TBL . ' WHERE id_directory=' . $babDB->quote($directoryId) . ' AND id_user=' . $babDB->quote($userId);
    $res = $babDB->db_query($sql);
    $entry = $babDB->db_fetch_assoc($res);
    $workspaceModification = $entry['date_modification'];

    $sql = 'SELECT date_modification FROM ' . BAB_DBDIR_ENTRIES_TBL . ' WHERE id_directory=0 AND id_user=' . $babDB->quote($userId);
    $res = $babDB->db_query($sql);
    $entry = $babDB->db_fetch_assoc($res);
    $portalModification = $entry['date_modification'];

    return $workspaceModification >= $portalModification;
}



/**
 * Deletes the specified directory entry.
 *
 * @param int 	$entryId
 * @return void
 */
function workspace_deleteDbContact($entryId)
{
    global $babDB;

//	$directoryId = workspace_getDirectoryId();
    $directoryId = workspace_getWorkspaceDirectoryId();

//	$sql = 'SELECT id_group FROM '.BAB_DB_DIRECTORIES_TBL.'
//				WHERE id='.$babDB->quote($directoryId);
//	list($idgroup) = $babDB->db_fetch_assoc($babDB->db_query($sql));
//	if ($idgroup != 0) {
//		include_once $GLOBALS['babInstallPath'] . 'utilit/delincl.php';
//		$sql = 'SELECT id_user FROM '.BAB_DBDIR_ENTRIES_TBL.'
//				 WHERE id='.$babDB->quote($entryId);
//		list($iddu) = $babDB->db_fetch_array($babDB->db_query($sql));
//		bab_deleteUser($iddu);
//		return;
//	}

    $sql = 'DELETE FROM '.BAB_DBDIR_ENTRIES_EXTRA_TBL.'
                WHERE id_entry=' . $babDB->quote($entryId);
    $babDB->db_query($sql);

    $sql = 'DELETE FROM '.BAB_DBDIR_ENTRIES_TBL.'
                WHERE id_directory=' . $babDB->quote($directoryId).'
                AND id=' . $babDB->quote($entryId);
    $babDB->db_query($sql);

    include_once $GLOBALS['babInstallPath'].'utilit/eventdirectory.php';
    $event = new bab_eventDirectoryEntryDeleted($entryId);
    bab_fireEvent($event);
}



require_once  $GLOBALS['babInstallPath'].'utilit/mailincl.php';

class workspace_MailDispatcher
{
    const MAIL_DISPATCH_OK = 0;
    const MAIL_DISPATCH_ERROR = 1;

    var $mail;
    var $nbRecipientsByMail;

    var $lb;
    var $stack;
    var $debug;
    var $log;
    var $status;

    function __construct()
    {
        $this->mail = bab_mail();

        if (!$this->mail) {
            $GLOBALS['babBody']->addError(bab_translate(""));
            return false;
        }

        if ($GLOBALS['BAB_SESS_LOGGED']) {
            $this->setSender($GLOBALS['BAB_SESS_EMAIL'], $GLOBALS['BAB_SESS_USER']);
        }

        $this->nbRecipientsByMail = 1;
        $this->lb = "\n";
        $this->stack = array();
        $this->debug = array();
        $this->status = array();
        $this->log = '';
    }



    /**
     * Sets sender information
     *
     * @param unknown_type $emailAddress
     * @param unknown_type $name
     */
    function setSender($emailAddress, $name)
    {
        if (!$this->mail) {
            return false;
        }

        $this->mail->mailFrom($GLOBALS['BAB_SESS_EMAIL'], $GLOBALS['BAB_SESS_USER']);
    }



    /**
     * Defines the data of the email to send.
     *
     * @param string	$subject
     * @param string	$message
     * @param string	$link
     * @param string	$linklabel
     * @param int		$from				User id of sender
     *
     * @return bool
     */
    function setData($subject, $message, $link = false, $linklabel = '', $from = false)
    {
        if (!$this->mail) {
            return false;
        }

        if (false !== $from) {
            $this->mail->mailFrom(bab_getUserEmail($from), bab_getUserName($from));
        }

        $this->mail->mailSubject($subject);

        $this->message_txt = bab_toHtml(strip_tags($message), BAB_HTML_ALL);

        $this->message_html = $message;
        if (false !== $link) {
            $this->link = true;
            $this->link_txt = $link;
            $this->link_html = bab_toHtml($link);
            $this->linklabel_txt = $linklabel;
            $this->linklabel_html = bab_toHtml($linklabel);
        } else {
            $this->link = false;
        }

        $html = $this->mail->mailTemplate($this->message_html);
//		$html = bab_printTemplate($this, $GLOBALS['babAddonHtmlPath'].'email.html', 'html');
//		$text = bab_printTemplate($this, $GLOBALS['babAddonHtmlPath'].'email.html', 'text');
        $this->mail->mailBody($html, 'text/html');
        $this->mail->mailAltBody($this->message_txt);

        return true;
    }



    function attachFile($fname, $realname, $type)
    {
        $this->mail->mailFileAttach($fname, $realname, $type);
    }


    /**
     * Checks the validity of an email address.
     *
     * @static
     * @param string $email
     * @return bool
     */
    function emailAddressIsValid($email)
    {
        return (strpos($email, '@') !== false);
    }



    /**
     * Defines the list of recipients of the email.
     *
     * @param array		$recipients
     * @param string	$type			'mailTo' or 'mailBcc'
     * @return bool
     */
    function mailDestArray($recipients, $type)
    {
        if (!$this->mail
            || !in_array($type, array('mailTo','mailBcc'))
            || !is_array($recipients)
            || count($recipients) == 0) {
            return false;
        }

        $keys = array_keys($recipients);
        foreach ($keys as $key) {
            if (!self::emailAddressIsValid($recipients[$key])) {
                unset($recipients[$key]);
            } else {
                $recipients[$key] = trim($recipients[$key]);
            }
        }

        if (isset($this->stack[$type])) {
            $this->stack[$type] = array_merge($this->stack[$type], $recipients);
        } else {
            $this->stack[$type] = $recipients;
        }

        $this->stack[$type] = array_unique($this->stack[$type]);

        return true;
    }


    function mail_pop($type)
    {
        for ($i = 0; $i < $this->nbRecipientsByMail; $i++) {
            $mail = array_pop($this->stack[$type]);
            if (!$mail && $i == 0) {
                return false;
            }
            if (!empty($mail)) {
                $this->mail->$type($mail);
            }
        }
        return true;
    }


    function get_gust_recipients()
    {
        $this->mail->clearAllRecipients();
        $out = false;
        $types = array_keys($this->stack);
        foreach ($types as $type) {
            if ($this->mail_pop($type)) {
                $out = true;
            }
        }
        return $out;
    }



    /**
     * Sends the prepared emails.
     *
     * @return bool
     */
    function send()
    {
        if (!$this->mail) {
            return false;
        }
        $result = true;
        while ($this->get_gust_recipients()) {
            $retry = 0;
            while (true !== $this->mail->send() && $retry < 5) {
                sleep(1);
                $retry++;
            }

            $dest = $this->mail->mailTo[0];
            if ($retry < 5) {
                $errorStatus = self::MAIL_DISPATCH_OK;
            } else {
                $errorStatus = self::MAIL_DISPATCH_ERROR;
                $result = false;
            }
            $this->status[] = array(
                'status' => $errorStatus,
                'attempts' => $retry,
                'dest' => $dest[0],
                'error' => $this->mail->ErrorInfo()
            );
        }

        $this->stack = array();
        return $result;
    }
}




/**
 * Creates a new user and associates it to the current workspace group.
 *
 * @param array $member
 * @param array $entry
 * @return int 		The new user id or false on error.
 */
function workspace_createUser(array $member, array $entry, &$error)
{
    require_once $GLOBALS['babInstallPath'] . 'utilit/userincl.php';

    $userId = bab_registerUser($entry['givenname'], $entry['sn'], '', $entry['email'], $member['identifier'], $member['password'], $member['password2'], 1, $error, false);
    if (!$userId) {
        return false;
    }

    if (!empty($member['notifyuser']))
    {
        // notify the user about creation of the account
        bab_registerUserNotify($entry['sn'].' '.$entry['givenname'], $entry['email'], $member['identifier'], (empty($member['sendpwd']) ? null : $member['password']));
    }

    $groupId = workspace_getWorkspaceGroupId();
    bab_addUserToGroup($userId, $groupId);

    return $userId;
}



/**
 * Checks whether the user is allowed to browse the specified directory.
 *
 * @param int   $directoryId If not specified or null, check is done for the current workspace's directory.
 * @param int   $userId      If not specified or null, check is done for the currently logged user.
 * @return bool
 */
function workspace_canBrowseDirectory($directoryId = null, $userId = null)
{
    if (!isset($userId)) {
        $userId = '';
    }
    if (!isset($directoryId)) {
        $directoryId = workspace_getWorkspaceDirectoryId();
    }
    if ($directoryId) {
        return bab_isAccessValid(BAB_DBDIRVIEW_GROUPS_TBL, $directoryId, $userId);
    }
    return false;
}


/**
 * Checks whether the user is allowed to delete entries from the specified directory.
 *
 * @param int   $directoryId If not specified or null, check is done for the current workspace's directory.
 * @param int   $userId      If not specified or null, check is done for the currently logged user.
 * @return bool
 */
function workspace_canDeleteEntriesFromDirectory($directoryId = null, $userId = null)
{
    if (!isset($userId)) {
        $userId = '';
    }
    if (!isset($directoryId)) {
        $directoryId = workspace_getWorkspaceDirectoryId();
    }
    if ($directoryId) {
        return bab_isAccessValid(BAB_DBDIRDEL_GROUPS_TBL, $directoryId, $userId);
    }
    return false;
}


/**
 * Checks whether the user is allowed to update entries from the specified directory.
 *
 * @param int   $directoryId If not specified or null, check is done for the current workspace's directory.
 * @param int   $userId      If not specified or null, check is done for the currently logged user.
 * @return bool
 */
function workspace_canUpdateEntriesFromDirectory($directoryId = null, $userId = null)
{
    if (!isset($userId)) {
        $userId = '';
    }
    if (!isset($directoryId)) {
        $directoryId = workspace_getWorkspaceDirectoryId();
    }
    if ($directoryId) {
        return bab_isAccessValid(BAB_DBDIRUPDATE_GROUPS_TBL, $directoryId, $userId);
    }
    return false;
}


/**
 * Checks whether the user is allowed to update entries from the specified directory.
 *
 * @param int	$updatedUserId    The user to update.
 * @param int	$userId           If not specified or null, check is done for the currently logged user.
 * @param int	$workspaceId      The workspace id. If not specified or null, check is done for the current workspace.
 * @return bool
 */
function workspace_canUpdateEntriesFromMember($updatedUserId, $userId = null, $workspaceId = null)
{
    if (/*$updatedUserId == $GLOBALS['BAB_SESS_USERID'] || */ workspace_userIsWorkspaceAdministrator(null, $workspaceId)) {
        return true;
    }
    return false;
}


/**
 * Checks whether the user is allowed to add entries to the specified directory.
 *
 * @param int   $directoryId If not specified or null, check is done for the current workspace's directory.
 * @param int   $userId      If not specified or null, check is done for the currently logged user.
 * @return bool
 */
function workspace_canAddEntriesToDirectory($directoryId = null, $userId = null)
{
    if (!isset($userId)) {
        $userId = '';
    }
    if (!isset($directoryId)) {
        $directoryId = workspace_getWorkspaceDirectoryId();
    }
    if ($directoryId) {
        return bab_isAccessValid(BAB_DBDIRADD_GROUPS_TBL, $directoryId, $userId);
    }
    return false;
}


/**
 * Checks whether the specified user is allowed to update user profiles.
 *
 * @param int	$userId           If not specified or null, check is done for the currently logged user.
 * @param int	$workspaceId      The workspace id. If not specified or null, check is done for the current workspace.
 * @return bool
 */
function workspace_canUpdateUsersProfile($userId = null, $workspaceId = null)
{
    return workspace_userIsWorkspaceAdministrator($userId, $workspaceId);
}


/**
 * Checks whether the current user can create a user account.
 *
 * @return bool
 */
function workspace_canCreateUsers()
{
    return bab_isUserAdministrator() || bab_isDelegated('users');
}




/**
 * Returns an array containing email addresses of the workspace members corresponding to the filter.
 *
 * @param string $filter 'all', 'members', 'reader', 'writer' or administrator'.
 * @return array
 */
function  workspace_getMembersEmailAddresses($filter)
{
    $workspaceId = workspace_getCurrentWorkspace();
    $groupId = workspace_getWorkspaceGroupId($workspaceId);
    $directoryId = workspace_getWorkspaceDirectoryId($workspaceId);

    $users = bab_getGroupsMembers($groupId);

    $memberEmailAddresses = array();

    if ($users) {
        foreach ($users as $user) {


            $userProfile = workspace_getUserProfile($user['id']);
            if ($filter !== 'all' && $filter !== 'members' && $userProfile !== $filter) {
                continue;
            }

            $dirEntryId = workspace_getDirEntryFromUserId($directoryId, $user['id']);
            if ($dirEntryId == null) {
                $dirEntry = $user;
                //continue;
            } else {
                $dirEntry = bab_getDirEntry($dirEntryId, BAB_DIR_ENTRY_ID);
            }


            if ($dirEntryId == null) {
                if (!empty($dirEntry['email'])) {
                    $memberEmailAddresses[$dirEntry['email']] = $dirEntry['email'];
                }
            } else {
                if (!empty($dirEntry['email']['value'])) {
                    $memberEmailAddresses[$dirEntry['email']['value']] = $dirEntry['email']['value'];
                }
            }
        }
    }

    return $memberEmailAddresses;
}
