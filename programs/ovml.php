<?php
/**
 * <OCAddon name="workspace" action="getVisible" [order="asc|rand"]>
 *
 * OVWorkspaceId : The workspace id
 * OVWorkspaceName : The workspace name
 * OVWorkspaceHomeUrl : The workspace home url
 *
 *
 *
 * <OCAddon name="workspace" action="getActivities" workspace="" >
 *
 * OVWorkspaceActivityName :
 * OVWorkspaceActivityLabel :
 * OVWorkspaceActivityUrl :
 *
 *
 *
 * <OCAddon name="workspace" action="getArticles" rows="3" workspace="" >
 *
 * OVArticleId
 * OVArticleTitle
 * OVArticleHead
 * OVArticleBody
 * OVArticleDate
 *
 */

include_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/workspaces.php';
require_once dirname(__FILE__) . '/controller.class.php';
require_once dirname(__FILE__) . '/workspace.ctrl.php';


function workspace_ovml($args)
{
    global $babDB;

    $ovContainer = array();

    if (!isset($args['action']))
    {
        trigger_error('Parameter \'action\' must be specified.');
        return $ovContainer;
    }

    switch ($args['action'])
    {
        case 'dateFormat':
            $ovContainer[] = array('dateFormat' => workspace_getWorkspacesDateFormat() );
            break;
        case 'timeFormat':
            $ovContainer[] = array('timeFormat' => workspace_getWorkspacesTimeFormat() );
            break;

        case 'articleNumber':
            $ovContainer[] = array('articleNumber' => workspace_getWorkspaceArticleNum(workspace_getCurrentWorkspace()) );
            break;
        case 'posteNumber':
            $ovContainer[] = array('posteNumber' => workspace_getWorkspaceForumNum(workspace_getCurrentWorkspace()) );
            break;
        case 'fileNumber':
            $ovContainer[] = array('fileNumber' => workspace_getWorkspaceFileNum(workspace_getCurrentWorkspace()) );
            break;
        case 'eventNumber':
            $ovContainer[] = array('eventNumber' => workspace_getWorkspaceAgendaNum(workspace_getCurrentWorkspace()) );
            break;
        case 'otherThem':
            $ovContainer[] = array('otherThem' => workspace_getWorkspaceOtherThem(workspace_getCurrentWorkspace()),
                                    'articleNumber' => workspace_getWorkspaceArticleNum(workspace_getCurrentWorkspace()) );
            break;
        case 'getVisible':

            $workspaces = workspace_getWorkspaceList();
            $currentWorkspace = workspace_getCurrentWorkspace();

            foreach ($workspaces as $workspace) {
                $ovElement = array(
                    'WorkspaceId' => $workspace['id'],
                    'WorkspaceName' => $workspace['name'],
                    'WorkspaceCurrent' => ($currentWorkspace == $workspace['id']) ? 'selected="selected"' : '',
                    'WorkspaceCurrentName' => ($currentWorkspace == $workspace['id']) ? $workspace['name'] : '',
                    'WorkspaceHomeUrl' => workspace_Controller()->User()->selectWorkspace($workspace['id'])->url()
                );
                $ovContainer[] = $ovElement;
            }
            if (isset($args['order'])) {
                if ($args['order'] == 'asc') {
                    $ovContainer = bab_Sort::asort($ovContainer, 'name', bab_Sort::CASE_INSENSITIVE);
                } else if ($args['order'] == 'rand') {
                    shuffle($ovContainer);
                }
            }
            break;


        case 'getCurrent':

            $workspaces = workspace_getWorkspaceList();
            $currentWorkspaceId = workspace_getCurrentWorkspace();
            if ($currentWorkspaceId) {
                $currentWorkspace = $workspaces[$currentWorkspaceId];

                $ovElement = array(
                    'WorkspaceId' => $currentWorkspaceId,
                    'WorkspaceName' => $currentWorkspace['name'],
                    'WorkspaceDescription' => $currentWorkspace['description'],
                    'WorkspaceHomeUrl' => workspace_Controller()->User()->selectWorkspace($currentWorkspaceId)->url()
                );
                $ovContainer[] = $ovElement;
            }
            break;


        case 'getActivities':

            $currentWorkspace = workspace_getCurrentWorkspace();
            //Aucun Workspace selectionne
            if( $currentWorkspace != '0' && workspace_workspaceExist($currentWorkspace) ){
                $topicId = workspace_getWorkspaceTopicId($currentWorkspace);
                $forumId = workspace_getWorkspaceForumId($currentWorkspace);
    //			$calendarId = workspace_getWorkspaceCalendarId($currentWorkspace, true);
                $directoryId = workspace_getWorkspaceDirectoryId($currentWorkspace);
                $rootFolderId = workspace_getWorkspaceRootFolderId($currentWorkspace);


                /* Workspace Home */
                $ovContainer[] = array(
                    'WorkspaceActivityName' => 'home',
                    'WorkspaceActivityLabel' => workspace_translate('Homepage'),
                    'WorkspaceActivityUrl' => '?tg=addon/workspace/main&idx=user.displayHome'
                );

    //			if ($calendarId) {
                    $ovContainer[] = array(
                        'WorkspaceActivityName' => 'calendar',
                        'WorkspaceActivityLabel' => workspace_translate('Calendars'),
                        'WorkspaceActivityUrl' => '?tg=addon/workspace/main&idx=calendars.display&type=week'
                    );
    //			}
                if ($rootFolderId) {
                    $ovContainer[] = array(
                        'WorkspaceActivityName' => 'filemanager',
                        'WorkspaceActivityLabel' => workspace_translate('File manager'),
                        'WorkspaceActivityUrl' => '?tg=addon/workspace/main&idx=files.browse'
                    );
                }
                if ($forumId) {
                    $ovContainer[] = array(
                        'WorkspaceActivityName' => 'forum',
                        'WorkspaceActivityLabel' => workspace_translate('Forum'),
                        'WorkspaceActivityUrl' => '?tg=addon/workspace/main&idx=forum.listThreads'
                    );
                }
                if ($directoryId) {
                    $ovContainer[] = array(
                        'WorkspaceActivityName' => 'directory',
                        'WorkspaceActivityLabel' => workspace_translate('Directories'),
                        'WorkspaceActivityUrl' => '?tg=addon/workspace/main&idx=directories.displayEntries'
                    );
                }
                if ($topicId) {
                    $ovContainer[] = array(
                        'WorkspaceActivityName' => 'articles',
                        'WorkspaceActivityLabel' => workspace_translate('Articles'),
                        'WorkspaceActivityUrl' => '?tg=addon/workspace/main&idx=articles.displayList'
                    );
                }
                if (isset($args['order'])) {
                    if ($args['order'] == 'asc') {
                        $ovContainer = bab_Sort::asort($ovContainer, 'name', bab_Sort::CASE_INSENSITIVE);
                    } else if ($args['order'] == 'rand') {
                        shuffle($ovContainer);
                    }
                }
                if (@bab_functionality::get('etherpad') !== false) {
                    $ovContainer[] = array(
                        'WorkspaceActivityName' => 'pads',
                        'WorkspaceActivityLabel' => workspace_translate('Pads'),
                        'WorkspaceActivityUrl' => '?tg=addon/workspace/main&idx=pad.displayList'
                    );
                }
            }



            if (bab_isUserAdministrator()){
                $ovContainer[] = array(
                    'WorkspaceActivityName' => 'admin',
                    'WorkspaceActivityLabel' => workspace_translate('Administration'),
                    'WorkspaceActivityUrl' => '?tg=addon/workspace/main&idx=admin.listWorkspaces'
                );
            }

            /* Back to Intranet */
            $showSignoffButton = (isset($_SESSION['workspace_redirectToWorkspacesPerformed'])
                && isset($_SESSION['workspace_userIsOnlyInGroupsOfWorkspaces'])
                && $_SESSION['workspace_userIsOnlyInGroupsOfWorkspaces']) || workspace_getWorkspacesShowSignoffButton();

            $logoutUrl = '?';
            $signoffUrl = '?tg=login&cmd=signoff';


            if ($showSignoffButton) {
                $ovContainer[] = array(
                    'WorkspaceActivityName' => 'logout',
                    'WorkspaceActivityLabel' => workspace_translate('Sign off'),
                    'WorkspaceActivityUrl' => $signoffUrl
                );
            } else {
                $ovContainer[] = array(
                    'WorkspaceActivityName' => 'logout',
                    'WorkspaceActivityLabel' => workspace_translate('Leave the workspace'),
                    'WorkspaceActivityUrl' => $logoutUrl
                );
            }

            break;


        case 'getArticles':
            require_once $GLOBALS['babInstallPath'].'utilit/artapi.php';
//			$articleId = isset($args['article_id']) ? $args['article_id'] : null;
            $articles = workspace_getArticles(null, null, workspace_getCurrentWorkspace());

            $r = new workspace_replace();

            if (isset($args['rows'])) {
                $nbArticles = $args['rows'];
            } else {
                $nbArticles = 100;
            }
            while ($articles && ($nbArticles-- > 0) && ($article = $babDB->db_fetch_assoc($articles))) {
                $r->ref($article['body']);
                $r->ref($article['head']);
                $nbComment = bab_getArticleNbComment($article['id']);
                if( $nbComment != 0 ){
                    $strComment = workspace_translate("Comments");
                    $strComment.= " (". $nbComment . ")";
                }else{
                    $strComment = workspace_translate("Add a comment");
                }
                if(bab_isAccessValid("bab_topicsmod_groups", $article['id_topic'])){
                    $article['canUpdate'] = 1;
                }else{
                    $article['canUpdate'] = 0;
                }
                $ovContainer[] = array(
                    'ArticleId' => $article['id'],
                    'ArticleTitle' => $article['title'],
                    'ArticleHead' => $article['head'],
                    'ArticleBody' => $article['body'],
                    'ArticleDate' => bab_mktime($article['date']),
                    'ArticleTopic' => $article['id_topic'],
                    'ArticleCanUpdate' => $article['canUpdate'],
                    'ArticleCommentStr' => $strComment
                );
            }

//			if (isset($args['order'])) {
//				if ($args['order'] == 'date') {
//					$ovContainer = bab_Sort::asort($ovContainer, 'ArticleDate', bab_Sort::CASE_INSENSITIVE);
//					$ovContainer = array_reverse($ovContainer);
//				} else if ($args['order'] == 'rand') {
//					shuffle($ovContainer);
//				}
//			}

            break;


        default:
            break;
    }

    return $ovContainer;
}



function workspace_getHtml($node) {

    $return = '';
    $classnames = array();

    $id = $node->getId();
    $siteMapItem = $node->getData(); 	// bab_siteMapItem

    if (!empty($siteMapItem->iconClassnames)) {
        $icon = 'icon '.$siteMapItem->iconClassnames;
    } else {
        $icon = 'icon';
    }

    if ($siteMapItem->url) {

        if ($siteMapItem->onclick) {
            $onclick = ' onclick="'.bab_toHtml($siteMapItem->onclick).'"';
        } else {
            $onclick = '';
        }

        $htmlData = '<a class="'.bab_toHtml($icon).'" href="'.bab_toHtml($siteMapItem->url).'" '.$onclick.'>'.bab_toHtml($siteMapItem->name).'</a>';
    } else {
        $htmlData = '<span class="'.bab_toHtml($icon).'">'.bab_toHtml($siteMapItem->name).'</span>';
    }



    $classnames[] = 'sitemap-'.$siteMapItem->id_function;

    if ($siteMapItem->folder) {
        $classnames[] = 'sitemap-folder';
    }


    if ('babUserSectionAddons' === $node->parentNode()->getData()->id_function) {
        $classnames[] = 'sitemap-menumain';
        $return .= '<li class="'.implode(' ', $classnames).'"><div class="sitemap-menumain-button">' . workspace_translate('>') . '</div>';
    } else {
        $return .= '<li class="'.implode(' ', $classnames).'">'.$htmlData;
    }

    //  icon-16x16 icon-left icon-left-16

    if ($node->hasChildNodes()) {
        $return .= "<ul>\n";

        $node = $node->firstChild();
        do {
            $return .= workspace_getHtml($node);
        } while ($node = $node->nextSibling());

        $return .= "</ul>\n";
    }

    $return .= "</li>\n";

    return $return;
}



function workspace_getMenu() {

    $sitemap = bab_siteMap::get();

    $node = $sitemap->getNodeById('babworkspaces');
    if ($node === null) {
        // For compatibility with ovidentia 7.1.x.
        $node = $sitemap->getNodeById('DGAll-babworkspaces');
    }

    $return = '';

    if ($node) {
        $return .= workspace_getHtml($node);
    }
    return $return;
}
