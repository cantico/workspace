<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/functions.php';



interface workspace_Configurable
{
    /**
     * @param   int     $workspaceId
     * @return  Widget_Displayable_Interface
     */
    public function getConfigurationEditor($workspaceId);


    /**
     * @param   int     $workspaceId
     * @param   array   $configuration
     * @return  void
     */
    public function setConfiguration($workspaceId, $configuration);

    /**
     * @param   int     $workspaceId
     * @return  void
     */
    public function applyConfiguration($workspaceId);

    /**
     * @param   int     $workspaceId
     * @return  array
     */
    public function getConfiguration($workspaceId);
    
    /**
     * @param   int     $workspaceId
     * @return  void
     */
    public function setDefaultConfiguration($workspaceId);
}

/**
 *
 */
class Func_WorkspaceAddon extends bab_functionality
{
    /**
     * {@inheritDoc}
     * @see bab_functionality::getDescription()
     */
    public function getDescription()
    {
        return workspace_translate('Provides connectors to include addons into a workspace');
    }


    /**
     * Returns a translated human-readable name that will be displayed in workspace UI.
     *
     * @return string
     */
    public function getName()
    {
        return '';
    }
    
    public function getIconClassName()
    {
        return Func_Icons::ACTIONS_GO_HOME;
    }


    /**
     * @param int $workspaceId
     * @return Widget_Form
     */
    public function getConfigurationEditor($workspaceId)
    {
        $W = bab_Widgets();
        $App = workspace_App();
        $set = $App->WorkspaceSet();
        
        /* @var $workspace workspace_Workspace */
        $workspace = $set->request($set->id->is($workspaceId));
        
        $editor = $W->Form();

        $editor->setHiddenValue('workspaceId', $workspaceId);
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setHiddenValue('addonName', $this->getFunctionalityPath());

        $editor->setName('configuration');
        $editor->setValues($this->getConfiguration($workspaceId), array('configuration'));

        $editor->addItem(
            $W->SubmitButton()
                ->setAction($App->Controller()->Admin()->saveAddonConfiguration())
                ->setAjaxAction()
        );

        return $editor;
    }


    /**
     *
     * @param int $workspaceId
     * @param array $configuration
     */
    public function setConfiguration($workspaceId, $configuration)
    {
        $App = workspace_App();
        $set = $App->WorkspaceSet();
        
        /* @var $workspace workspace_Workspace */
        $workspace = $set->request($set->id->is($workspaceId));
        
        $functionalityPath = $this->getFunctionalityPath();
        foreach ($configuration as $name => $value){
            bab_Registry::set('/workspace/workspaces/' . $workspace->delegation . '/addons/' . $functionalityPath . '/' . $name, $value);
        }
    }


    /**
     *
     * @param int $workspaceId
     * @param mixed $defaultValue
     * @return array
     */
    public function getConfiguration($workspaceId, $defaultValue = null)
    {
        $App = workspace_App();
        $set = $App->WorkspaceSet();
        
        /* @var $workspace workspace_Workspace */
        $workspace = $set->request($set->id->is($workspaceId));
        
        $functionalityPath = $this->getFunctionalityPath();
        return bab_Registry::toArray('/workspace/workspaces/' . $workspace->delegation . '/addons/' . $functionalityPath . '/', $defaultValue);
    }


    /**
     * @return string
     */
    public function getFunctionalityPath()
    {
        $functionalityPath = $this->getPath();
        $functionalityPathElements = explode('/', $functionalityPath);
        array_shift($functionalityPathElements);
        $functionalityPath = implode('/', $functionalityPathElements);
        return $functionalityPath;
    }
    
    /**
     * Returns the value defined in the getId() of the portlet definition.
     * If the addon does not have a portlet, returns false.
     * @return bool|string Portlet definition id, or false if there is no portlet
     */
    public function getPortletDefinitionId()
    {
        return false;
    }
    
    /**
     * Returns the value defined in the getId() of the portlet definition for the home page.
     * If the addon does not have a portlet, returns false.
     * @return bool|string Portlet definition id, or false if there is no portlet
     */
    public function getHomePortletDefinitionId()
    {
        return $this->getPortletDefinitionId();
    }
    
    /**
     * If the addon has a portlet, returns the configuration of this portlet based on the workspace given.
     * If the addon does not have a portlet, returns an empty array.
     * @param int $workspaceId
     * @return array
     */
    public function getPortletConfiguration($workspaceId)
    {
        return array();
    }
    
    /**
     * If the addon has a portlet for the home page, returns the configuration of this portlet based on the workspace given.
     * If the addon does not have a portlet, returns an empty array.
     * @param int $workspaceId
     * @return array
     */
    public function getHomePortletConfiguration($workspaceId)
    {
        return $this->getPortletConfiguration($workspaceId);
    }
    
    /**
     * Set the default configuration into the registry for the specified workspace
     * @param int $workspaceId
     * @return void
     */
    public function setDefaultConfiguration($workspaceId)
    {
        return;
    }
}
